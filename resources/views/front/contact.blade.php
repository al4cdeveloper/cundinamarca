@extends('layout.header')

    @section('title','Contacto')
    @section('additionalPreMain')
    


	<section class="parallax-window" data-parallax="scroll" data-image-src="{{asset('img/contact.jpg')}}" data-natural-width="1400" >
		<div class="parallax-content-1">
			<div class="animated fadeInDown">
				<h1>Contáctanos</h1>
			</div>
		</div>
	</section>
	<!-- End Section -->
    @endsection
    
    @section('main')
		<div id="position">
			<div class="container">
				<ul>
					<li><a href="{{url('/')}}">Home</a>
					</li>
					<li>Contacto</li>
				</ul>
			</div>
		</div>
		<!-- End Position -->

		<div class="container margin_60">
			<div class="row">
				<div class="col-md-8 col-sm-8">
					@if(Session::has('message'))
					<div class="alert alert-success">
					  <strong>Correcto!</strong> {{ Session::get('message') }}
					</div>
					@endif
					<div class="form_title">
						<h3><strong><i class="icon-pencil"></i></strong>Ingresa los siguientes datos</h3>
					</div>
					<div class="step">

						<div id="message-contact"></div>
						<form method="post" action="{{url('storeemail')}}" id="contactform">
                            {{csrf_field()}}
							<div class="row">
								<div class="col-md-12 col-sm-12">
									<div class="form-group">
										<label>Nombre</label>
										<input type="text" class="form-control" id="name_contact" name="name" placeholder="Ingrese su nombre">
									</div>
								</div>
							</div>
							<!-- End row -->
							<div class="row">
								<div class="col-md-6 col-sm-6">
									<div class="form-group">
										<label>Email</label>
										<input type="email" id="email_contact" name="email" class="form-control" placeholder="Ingrese su email">
									</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="form-group">
										<label>Teléfono</label>
										<input type="text" id="phone_contact" name="phone" class="form-control" placeholder="Ingrese su teléfono">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label>Mensaje</label>
										<textarea rows="5" id="message" name="message" class="form-control" placeholder="Escribe tu mensaje" style="height:200px;"></textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<input type="submit" value="Enviar" class="btn_1" id="submit-contact">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- End row -->
		</div>
		<!-- End container -->
    @endsection
	<!-- End main -->
