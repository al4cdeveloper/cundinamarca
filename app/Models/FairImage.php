<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FairImage extends Model
{
	protected $primaryKey = 'id_fair_image';

	protected $fillable = ['link_image','fk_fair'];
}
