@extends('admin.layout.auth')

@section('title', 'Listado de noticias')


@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Noticias</h1>
                    </div>
                </div>
            </div>
        </div>

                <div class="block full">
                    <div class="block-title">
                        <h2>Noticias</h2>
                        <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/notices/create')}}" class="btn btn-primary btn-sm"><i class="far fa-plus-square fa-lg"></i></a></div>
                    </div>
                    <div class="table-responsive">
                        <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 50px;">Noticia</th>
                                    <th>Descripcion</th>
                                    <th style="width: 120px;">Estado</th>
                                    <th class="text-center" style="width: 75px;"><i class="fas fa-sync-alt"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($notices as $notice)
                                <tr>
                                    <td>{{$notice->notice_name}}</td>
                                    <td>{{$notice->description}}</td>
                                    <td>@if($notice->state=="activo")
                                        <a class="label label-info">{{$notice->state}}</a>
                                        @else
                                        <span class="label label-danger">{{$notice->state}}</span>
                                        @endif
                                    </td>
                                    <td class="text-center">

                                        <a href="{{url('admin/notice/edit/'.$notice->slug)}}" data-toggle="tooltip" title="Editar noticia" class="btn btn-effect-ripple btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                        {{-- <a href="{{url('admin/municipalities/images/'.$notice->slug)}}" data-toggle="tooltip" title="Administrar multimedia" class="btn btn-effect-ripple btn-xs btn-info"><i class="fa fa-images"></i></a>
                                        @if($notice->state=="activo")
                                        <a href="{{url('admin/municipalities/desactivate/'.$notice->id_notice)}}" data-toggle="tooltip" title="Desactivar municipio" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                        @else
                                        <a href="{{url('admin/municipalities/activate/'.$notice->id_notice)}}" data-toggle="tooltip" title="Activar municipio" class="btn btn-effect-ripple btn-xs btn-success"><i class="fas fa-check"></i></a>
                                        @endif --}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END Datatables Block -->
            {{-- </div> --}}
        {{-- </div> --}}
    </div>
@endsection

@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
@endsection