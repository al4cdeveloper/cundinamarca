<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Region extends Model
{
	use Sluggable;


	protected $primaryKey="id_region";
	protected $fillable=['id_region','region_name','description','multimedia_type','slug','type_last_user','keywords','link_image','fk_last_edition','state'];

	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'region_name'
            ]
        ];
    }

    public function images()
    {
        return $this->hasMany('App\Models\RegionImage', 'fk_region');
    }
}
