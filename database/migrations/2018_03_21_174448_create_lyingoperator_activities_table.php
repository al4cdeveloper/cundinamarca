<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLyingoperatorActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lyingoperator_activities', function (Blueprint $table) {
            $table->increments('id_lyingoperator_activity');
            $table->integer('fk_activity')->unsigned();
            $table->foreign('fk_activity')->references('id_site')->on('interest_sites');
            $table->integer('fk_operator')->unsigned();
            $table->foreign('fk_operator')->references('id_site')->on('interest_sites');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lyingoperator_activities');
    }
}
