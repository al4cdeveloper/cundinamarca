<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\Service;
use App\Models\ServiceOperator;


Route::get('/', 'User\UserController@index');
Route::get('/welcome','User\UserController@index2');

Route::get('services/{slug}','User\ServiceController@index');
Route::get('contact',function()
{
  return view('front.contact');
});

Route::get('routes/{slug}','User\UserController@route');
Route::get('municipalities','User\UserController@allMunicipality');
Route::get('municipality/{slug}','User\UserController@detailMunicipality');
Route::get('operators','User\UserController@allOperator');
Route::get('operator/{slug}','User\UserController@detailOperator');
Route::get('interestsites','User\UserController@allSites');
Route::get('site/{slug}','User\UserController@detailSite');
Route::post('storeemail','User\UserController@saveEmail');
Route::get('fairs','User\UserController@allFairs');
Route::get('expocundinamarca','User\UserController@Expo');
Route::get('gallery','User\UserController@gallery');
Route::get('activity/{slug}','User\UserController@activity');

// Route::get('reservation','User\ReservationController@index');
// Route::post('make-reservation','User\ReservationController@prereservation');
// Route::get('serviceslist','User\ReservationController@returnServices');

// Route::put('add_cart/{id}','User\CartController@add_cart');
// Route::get('allcart','User\CartController@allcart');
// Route::get('subtotal','User\CartController@subtotal');
// Route::get('remove_cart/{rowId}','User\CartController@removeCart');
// Route::get('delete_cart','User\CartController@deleteCart');
// Route::post('logincart','User\userController@loginCart')->name('logincart');
// Route::post('registercart','User\userController@registerCart')->name('registercart');

// Route::get('freehours/{id}/{date}','User\ReservationController@freeHours');



Route::group(['prefix' => 'admin'], function () {
  Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'AdminAuth\LoginController@login');
  Route::post('/logout', 'AdminAuth\LoginController@logout')->name('logout');

  Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'AdminAuth\RegisterController@register');

  Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'reporter'], function () {
  Route::get('/login', 'ReporterAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'ReporterAuth\LoginController@login');
  Route::post('/logout', 'ReporterAuth\LoginController@logout')->name('logout');

  Route::get('/register', 'ReporterAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'ReporterAuth\RegisterController@register');

  Route::post('/password/email', 'ReporterAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'ReporterAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'ReporterAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'ReporterAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'user'], function () {
  Route::get('/login', 'UserAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'UserAuth\LoginController@login');
  Route::post('/logout', 'UserAuth\LoginController@logout')->name('logout');

  Route::get('/register', 'UserAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'UserAuth\RegisterController@register');

  Route::post('/password/email', 'UserAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'UserAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'UserAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'UserAuth\ResetPasswordController@showResetForm');
});


Route::get('user/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');



// Route::post('savecomment','User\ReservationController@saveComment');
// Route::get('/home', 'HomeController@index')->name('home');
// Route::post('encuesta','User\userController@sendQuestions');
// Route::post('mailchimp_register', 'User\userController@mailchimpRegister');
// Route::get('search','User\ServiceController@search');

// Route::get('userprofile','User\userController@profile');
// Route::post('updateprofile','User\userController@updateprofile');
// Route::get('logeruser/reservation','User\ReservationController@reservationLogerUser');

// Route::get('auth/{provider}', 'Auth\SocialAuthController@redirectToProvider')->name('social.auth');
// Route::get('auth/{provider}/callback', 'Auth\SocialAuthController@handleProviderCallback');


Route::group(['prefix' => 'operator'], function () {
  Route::get('/login', 'OperatorAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'OperatorAuth\LoginController@login');
  Route::post('/logout', 'OperatorAuth\LoginController@logout')->name('logout');

  Route::get('/register', 'OperatorAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'OperatorAuth\RegisterController@register');

  Route::post('/password/email', 'OperatorAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'OperatorAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'OperatorAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'OperatorAuth\ResetPasswordController@showResetForm');
});
