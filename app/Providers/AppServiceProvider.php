<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Route;
use App\Models\Municipality;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $turisticas = Route::where('id_route',6)->get();
        // $routes = [];
        // $routes[] =Route::where('id_route',6)->first();
        $routes = Route::where('state','activo')->where('id_route','!=',6)->get();  
        // foreach($routesF as $route)
        // {
        //     $routes[] = $route;
        // }
        $nimaima = Municipality::where('slug','nimaima')->first();
        $zipaquira = Municipality::where('slug','zipaquira')->first();
        $guatavita = Municipality::where('slug','guatavita')->first();
        view()->share(['routesM'=>$routes,'nimaima'=>$nimaima,'zipaquira'=>$zipaquira,'guatavita'=>$guatavita]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
