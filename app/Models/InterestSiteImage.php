<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InterestSiteImage extends Model
{
	protected $primaryKey = 'id_site_image';

	protected $fillable = ['link_image','fk_site'];

	public function Site()
	{
		return $this->hasOne('App\Models\InterestSite','id_site','fk_site');
	}
}
