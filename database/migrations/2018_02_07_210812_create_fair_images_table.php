<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFairImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fair_images', function (Blueprint $table) {
            $table->increments('id_fair_image');
            $table->mediumText('link_image');
            $table->integer('fk_fair')->unsigned();
            $table->foreign('fk_fair')->references('id_fair')->on('fairs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fair_images');
    }
}
