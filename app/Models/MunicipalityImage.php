<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MunicipalityImage extends Model
{
	protected $primaryKey = 'id_municipality_image';

	protected $fillable = ['link_image','fk_municipality'];

	public function Municipality()
	{
		return $this->hasOne('App\Models\Municipality','id_municipality','fk_municipality');
	}
}
