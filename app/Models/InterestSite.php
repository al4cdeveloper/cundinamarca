<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class InterestSite extends Model
{
	use Sluggable;

	protected $primaryKey = "id_site";
	protected $fillable = ['site_name','fk_category','fk_municipality','address','phone','web','multimedia_type','latitude','longitude','slug','link_image','link_icon','keywords','description','state'];

	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'site_name'
            ]
        ];
    }

    public function Category()
    {
    	return $this->hasOne('App\Models\InterestSiteCategory','id_category','fk_category');
    }

    public function Municipality()
    {
    	return $this->hasOne('App\Models\Municipality','id_municipality','fk_municipality');
    }

    public function images()
    {
        return $this->hasMany('App\Models\InterestSiteImage', 'fk_site');
    }

    public function Services()
    {
        return $this->belongsToMany('App\Models\Service','lyingoperator_services','fk_operator','fk_service');
    }
    public function Video()
    {
        return $this->hasMany('App\Models\Video','fk_relation','id_site')->where('type_relation','site');
    }

    public function Operators()
    {
        return $this->belongsToMany('App\Models\InterestSite','lyingoperator_activities','fk_activity','fk_operator');
    }
}
