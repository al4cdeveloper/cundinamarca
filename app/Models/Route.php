<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Route extends Model
{
	use Sluggable;
	
	protected $primaryKey = "id_route";
	protected $fillable = ['name','type_relation','description','link_map','keywords','fk_relation','slug'];

	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function Relations()
    {
        return $this->hasMany('App\Models\RelationRoute','fk_route','id_route');
    }

    public function Departments()
    {
        return $this->hasMany('App\Models\RelationRoute','fk_route','id_route')->where('type_relation','department');
    }

    public function Municipalities()
    {
        return $this->hasMany('App\Models\RelationRoute','fk_route','id_route')->where('type_relation','municipality');
    }

    public function MunicipalitiesF()
    {
        return $this->belongsToMany('App\Models\Municipality','relation_routes','fk_route','fk_relation')->where('type_relation','municipality')->where('municipalities.state','activo')->inRandomOrder()->take(6);
    }
    public function MunicipalitiesA()
    {
        return $this->belongsToMany('App\Models\Municipality','relation_routes','fk_route','fk_relation')->where('type_relation','municipality')->where('municipalities.state','activo');
    }

    public function Sites()
    {
        return $this->hasMany('App\Models\RelationRoute','fk_route','id_route')->where('type_relation','site');
    }

    public function SitesF()
    {
        return $this->belongsToMany('App\Models\InterestSite','relation_routes','fk_route','fk_relation')->where('type_relation','site')->where('interest_sites.state','activo');
    }


    public function PrincipalDepartment()
    {
        return $this->hasOne('App\Models\Department','id_department','fk_relation');
    }

    public function PrincipalMunicipality()
    {
        return $this->hasOne('App\Models\Municipality','id_municipality','fk_relation');
    }
}
