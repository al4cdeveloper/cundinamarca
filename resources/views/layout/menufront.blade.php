
        <div class="container">
            <div class="row" style="height: 100px!important;">
                <div class="col-md-3 col-sm-3 col-xs-3">
                    {{-- <div style="margin-top: -12%!important;">
                        <h1><a href="{{url('/')}}" title="Tour por cundinamarca"><img src="{{asset('img/logo.png')}}" class="img-responsive"></a></h1>
                    </div> --}}
                   <div id="logo" >
                        <a href="{{url('/')}}"><img src="{{asset('img/logo.png')}}" width="200" height="auto" alt="Cundinamarca La Leyenda Vive" data-retina="true"  id="logo_head">
                        </a>
                        
                    </div>
                </div>
                {{-- <div class="col-md-3 col-sm-3 col-xs-3">
                    <div id="logo_home">
                        <h1><a href="index.html" title="City tours travel template">City Tours travel template</a></h1>
                    </div>
                </div> --}}
                
                <div class="col-md-9 hidden-sm hidden-xs">
                        <ul id="top_links">
                            <li><a href="http://idecut.gov.co"><img src="{{asset('img/header/idecut.png')}}" alt="Idecut"></a>
                            </li>
                            <li style="border-left:0!important;"><a href="http://idecut.gov.co"><img src="{{asset('img/header/gobcundinamarca.png')}}" alt="Gobernación de Cundinamarca"></a>
                            </li>
                        </ul>
                    </div>
                <nav class="col-md-12 col-sm-12 col-xs-9 full-horizontal" id="responsiveMenu">
                    <a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>
                    <div class="main-menu ">
                        <div id="header_menu">
                            <img src="{{asset('img/logo_sticky.png')}}" width="300" height="90" alt="Cundinamarca La Leyenda Vive" data-retina="true">
                        </div>
                        <a href="#" class="open_close" id="close_in"><i class="icon_set_1_icon-77"></i></a>
                        <ul>
                            <li class="submenu">
                                <a href="javascript:void(0);" class="show-submenu">Rutas <i class="icon-down-open-mini"></i></a>
                                <ul>
                                    @foreach($routesM as $route)
                                    <li><a href="{{url('routes/'.$route->slug)}}">{{$route->name}}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <li class="megamenu submenu">
                                <a href="{{url('municipalities')}}" class="show-submenu-mega">Municipios<i class="icon-down-open-mini"></i></a>
                                <div class="menu-wrapper">
                                    <div class="col-md-4">
                                        <h3>{{$nimaima->municipality_name}}</h3>
                                        <ul>
                                            <li><a href="{{url('municipality/'.$nimaima->slug)}}"><img src="{{asset($nimaima->link_image)}}" class="img-responsive"></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4">
                                        <h3>{{$zipaquira->municipality_name}}</h3>
                                        <ul>
                                            <li><a href="{{url('municipality/'.$zipaquira->slug)}}"><img src="{{asset($zipaquira->link_image)}}" class="img-responsive"></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4">
                                        <h3>{{$guatavita->municipality_name}}</h3>
                                        <ul>
                                            <li><a href="{{url('municipality/'.$guatavita->slug)}}"><img src="{{asset($guatavita->link_image)}}" class="img-responsive"></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div><!-- End menu-wrapper -->
                            </li>
                             <li class="submenu">
                                <a href="{{url('interestsites')}}" >Destinos Turísticos </a>
                            </li>
                            <li class="submenu">
                                <a href="{{url('fairs')}}" >Ferias y Fiestas </a>
                            </li>
                            <li class="submenu">
                                <a href="{{url('expocundinamarca')}}" >ExpoCundinamarca</a>
                            </li>
                            <li class="submenu">
                                <a href="{{url('operators')}}" >Operadores Turísticos </a>
                            </li>
                        </ul>
                    </div>
                    <ul id="top_tools">
                        <li>
                            <div class="dropdown dropdown-search">
                                <a href="#" class="search-overlay-menu-btn" data-toggle="dropdown"><i class="icon-search"></i></a>
                            </div>
                        </li>
                        <li>
                            <div class="dropdown dropdown-cart">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-globe-1"></i> </a>
                                <ul class="dropdown-menu submenu" id="">
                                   <div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
 new google.translate.TranslateElement({pageLanguage: 'es', includedLanguages: 'de,en,fr', autoDisplay: false}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                                </ul>
                            </div><!-- End dropdown-cart-->
                        </li>
                    </ul>
                    <!-- End main-menu -->
                    {{-- <ul id="top_tools">
                        <li>
                            <div class="dropdown dropdown-access">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="access_link">Iniciar sesión</a>
                                    <div class="dropdown-menu">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="#" class="bt_facebook">
                                                    <i class="icon-facebook"></i>Facebook </a>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <a href="#" class="bt_paypal">
                                                    <i class="icon-paypal"></i>Paypal </a>
                                            </div>
                                        </div>
                                        <div class="login-or">
                                            <hr class="hr-or">
                                            <span class="span-or">or</span>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="inputUsernameEmail" placeholder="Email">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" id="inputPassword" placeholder="Password">
                                        </div>
                                        <a id="forgot_pw" href="#">Forgot password?</a>
                                        <input type="submit" name="Sign_in" value="Sign in" id="Sign_in" class="button_drop">
                                        <input type="submit" name="Sign_up" value="Sign up" id="Sign_up" class="button_drop outline">
                                    </div>
                            </div><!-- End Dropdown access -->
                        </li>
                    </ul> --}}
                </nav>
            </div>
    </div><!-- container -->