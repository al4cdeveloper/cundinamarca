<?php
function cop_format($number){
    $fmt = new NumberFormatter( 'es_CO', NumberFormatter::CURRENCY );
    $number = ceil($number);

    return 'COP '.$fmt->formatCurrency($number, "COP");
}

function getTheDate($date){
    $dias = array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
    $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

    $date = $dias[date('w', strtotime($date))]." ".date('d', strtotime($date))." de ".$meses[date('n', strtotime($date))-1]. " de ".date('Y', strtotime($date)). date(' h:i:s A', strtotime($date));

    return $date;
}


function youtube_match($texto) {
    $url = urldecode(rawurldecode($texto));
    preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);

    if (is_array($matches) && isset($matches[1])) {
        return $matches[1];
    } else {
        return '';
    }
}

?>