<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relation_routes', function (Blueprint $table) {
            $table->increments('id_relation_route');
            $table->string('type_relation');
            $table->integer('fk_relation');
            $table->integer('fk_route')->unsigned();
            $table->foreign('fk_route')->references('id_route')->on('routes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relation_routes');
    }
}
