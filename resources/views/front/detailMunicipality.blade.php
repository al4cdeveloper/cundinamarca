@extends('layout.header')

    @section('title',$municipality->municipality_name)
    @section('keywords',$municipality->keywords)
    @section('additionalStyle')

	<!-- CSS -->
	<link href="{{asset('front/css/slider-pro.min.css')}}" rel="stylesheet">
	
        <!-- CSS -->
    <link href="{{asset('front/css/slider-pro.min.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/date_time_picker.css')}}" rel="stylesheet">
    
	<!-- SPECIFIC CSS -->
	<link rel="stylesheet" href="{{asset('front/css/weather.css')}}">		
    @endsection

    @section('additionalPreMain')
	<!-- Slider -->
	<section class="parallax-window" data-parallax="scroll" data-image-src="{{asset($municipality->link_image)}}" style="width: 100%;height: auto; z-index: 1">
		<div class="parallax-content-2">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-sm-8">
						<h1>{{$municipality->municipality_name}}</h1>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Slider -->
    @endsection

    @section('main')
		<!-- Navigation -->
		<div id="position">
			<div class="container">
				<ul>
					<li><a href="{{url('/')}}">Home</a>
					</li>
					<li><a href="{{url('municipalities')}}">Municipios</a>
					</li>
					<li>{{$municipality->municipality_name}}</li>
				</ul>
			</div>
		</div>
		<!-- End Navigation -->

		<div class="container margin_60">
			<div class="row">
				<div class="col-md-8" id="single_tour_desc">
                    
                    @if($municipality->iframe != null)
                    <div id="col-md-12" class="slo">
                            
                        <iframe src="{{$municipality->iframe}}" style="position: relative!important; height: 400px!important;width: 100%!important;border: 0px" allowfullscreen></iframe>
                           
                    </div>
                    
					<hr>
                    @endif
					<!-- Descripcion -->
						<div class="row">
							<div class="main_title"  style="padding: 20px;">
								<h2><span>Información</span></h2><br />
								<span>{!!$municipality->description!!}</span>
							</div>
						</div>
					<!-- End Descripcion -->
				
					<!-- Weather -->
                    <div id="weather"> 
						
                    </div>
					<!-- End Weather -->
					{{-- @if(count($municipality->Video)>0) --}}
{{-- Carrusel de imagenes --}}
                    @if(count($municipality->images)>0 )
                    <hr>
                        <div id="Img_carousel" class="slider-pro">
                            <div class="sp-slides">
                                
                                @foreach($municipality->images as $image)
                                <div class="sp-slide">
                                    <img alt="Image" class="sp-image" src="css/images/blank.gif" data-src="{{asset($image->link_image)}}" data-small="{{asset($image->link_image)}}" data-medium="{{asset($image->link_image)}}" data-large="{{asset($image->link_image)}}" data-retina="{{asset($image->link_image)}}">
                                </div>
                                @endforeach
                            </div>
                            <div class="sp-thumbnails">
                                @foreach($municipality->images as $image)
                                <img alt="Image" class="sp-thumbnail" src="{{asset($image->link_image)}}">
                                @endforeach
                            </div>
                        </div>

                    @endif

				</div>
				<!--End  single_tour_desc-->

				<aside class="col-md-4">
                    


					<p class="hidden-sm hidden-xs">
						<a class="btn_map" data-toggle="collapse" href="#collapseMap" aria-expanded="false" aria-controls="collapseMap" data-text-swap="Ver mapa" data-text-original="Esconder mapa">Esconder mapa</a>
						<div class="collapse in" id="collapseMap">
							<div id="map" class="map"></div>
					</div>
					</p>
                    <hr>
                    @if($municipality->downloadimage!=null)
                    <p >
                        <a class="btn_map" href="{{url(asset($municipality->downloadimage))}}" target="_blank">Descargar contenido multimedia</a>
                    </p>
                    <hr>
                    @endif
                    @if($municipality->officialpage!=null)
                    <p>
                        <h3><a href="{{$municipality->officialpage}}">Visitar sitio oficial</a></h3>
                        </p>
                    <hr>
                    @endif
                    <!-- Video -->
                    @if(count($municipality->Video)>0)
                        <div class="row">
                            <div class="main_title">
                                <h2><span>Vídeo</span></h2><br />
                                <div id="player">
                                	

                                </div>
                                @foreach($municipality->Video as $video)
                                <div class="col-sm-12">
                                    <iframe width="100%" height="200" src="https://www.youtube.com/embed/{{$video->link_video}}?rel=0&controls=1&modestbranding=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    <!-- End Video -->
				    
                    @if(count($municipality->LyingOperators)>0)
                    <div class="box_style_1 expose">
                        <h3 class="inner">- Operadores -</h3>
                        <br>
                        
                        <div class="row">

                                    @foreach($municipality->LyingOperators as $operator)
                                    <div class="col-md-6 col-sm-6 wow zoomIn " data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: zoomIn;">
                                        <div class="tour_container">
                                            <div class="img_container">
                                                <a href="{{url('operator/'.$operator->slug)}}">
                                                    <img src="@if($operator->images->first()['link_image']!=null){{asset($operator->images->first()['link_image'])}}@else {{asset('img/defaultimg.jpg')}}@endif" width="800" height="533" class="img-responsive" alt="Image">
                                                </a>
                                            </div>
                                            <div class="tour_title">
                                                <h4><strong>{{$operator->site_name}}</strong></h4>
                                            </div>
                                        </div>
                                        <!-- End box tour -->
                                    </div>
                                    @endforeach

                                </div>

                    </div>
                    @endif
                    @if(count($municipality->Activities)>0)
					<div class="box_style_1 expose">
						<h3 class="inner">- Actividades -</h3>
						<br>
						
						<div class="row">

                                    @foreach($municipality->Activities as $activity)
									<div class="col-md-6 col-sm-6 wow zoomIn" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: zoomIn;">
										<div class="tour_container">
											<div class="img_container">
												<a href="{{url('activity/'.$activity->slug)}}">
													<img src="@if($activity->link_image!=null){{asset($activity->link_image)}}@else {{asset('img/defaultimg.jpg')}}@endif" width="800" height="533" class="img-responsive" alt="Image">
												</a>
											</div>
											<div class="tour_title">
												<h4><strong>{{$activity->Services->first()['service']}}</strong></h4>
											</div>
										</div>
										<!-- End box tour -->
									</div>
                                    @endforeach

								</div>

					</div>
                    @endif
				</aside>
                <!-- Sitios relacionados -->
                <div class="col-md-12">
                    @if(count($municipality->InterestSiteW)>0)
                    <hr>
                        <div class="row" style="padding: 20px;">
                            <div class="main_title">
                                <h2><span>Sitios Relacionados</span></h2><br />
                    
                                <div class="row">
                                    @foreach($municipality->InterestSiteW as $site)
                                    <div class="col-md-4 col-sm-4 wow zoomIn" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: zoomIn;">
                                        <div class="tour_container">
                                            <div class="ribbon_3 popular"><span>{{$site->Category->category_name}}</span>
                                            </div>
                                            <div class="img_container">
                                                <a href="{{url('site/'.$site->slug)}}">
                                                    <img src="@if($site->link_icon!=null){{asset($site->link_icon)}}@else {{asset('img/defaultimg.jpg')}}@endif" width="800" height="533" class="img-responsive" alt="Image">
                                                </a>
                                            </div>
                                            <div class="tour_title">
                                                <h3><strong>{{$site->site_name}}</strong></h3>

                                                    <p style="font-size: 14px!important;">{!!substr($site->description,0,120) !!}... <a href="{{url('site/'.$site->slug)}}">Ver más</a> {!!'</p>'!!}</p>

                                                <!-- end rating -->
                                            </div>
                                        </div>
                                        <!-- End box tour -->
                                    </div>

                                    <!-- End col-md-6 -->
                                    @endforeach
                                    <!-- End col-md-6 -->
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                    
                    <!-- End Sitios relacionados -->
			</div>
			<!--End row -->
		</div>
		<!--End container -->
        
        <div id="overlay"></div>
		<!-- Mask on input focus -->
        
    @endsection
	<!-- End Navigation -->
    
    @section('additionalScript')

	<!-- Specific scripts -->
	<script src="{{asset('front/js/icheck.js')}}"></script>
	<script>
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-grey',
			radioClass: 'iradio_square-grey'
		});
	</script>
	
	<!-- Specific scripts -->
	<script src="{{asset('front/js/jquery.simpleWeather.min.js')}}"></script>
	<script>
		$(document).ready(function () {
			$.simpleWeather({
				location: '{{$municipality->municipality_name}}, Colombia',
				woeid: '',
				unit: 'c',
				success: function (weather) {
                    html ='<hr class="add_bottom_45"><div class="main_title"><h2><span>Estado </span>del tiempo</h2></div>';
                    html +='<div class="clearfix">'
                    // <h4><i class="weathericon-26"></i> 5°C</h4>
                        //     <ul>
                        //         <li></li>
                        //         <li class="currently"></li>
                        //         <li></li></ul>
                        //     <span class="details_forecast">Mon: 6</span>
                        //     <span class="details_forecast">Tue: 6</span>
                        //     <span class="details_forecast">Wed: 5</span>
                        //     <span class="details_forecast">Thu: 4</span>
                        //     <span class="details_forecast">Fri: 3</span>
                        //     <span class="details_forecast">Sat: 4</span>
                        //     <span class="details_forecast">Sun: 3</span>
                        // </div>
					html += '<h4><i class="weathericon-' + weather.code + '"></i> ' + weather.temp + '&deg;' + weather.units.temp + '</h4>';
					html += '<ul><li>' + weather.city + ', ' + weather.region + '</li>';
					html += '<li class="currently">' + weather.currently + '</li>';
					html += '<li>' + weather.wind.direction + ' ' + weather.wind.speed + ' ' + weather.units.speed + '</li></ul>';
                    html += '</div>';
					//Weather forecast
					//for (var i = 0; i < weather.forecast.length; i++) {html += '<span class="details_forecast">' + weather.forecast[i].day + ': ' + weather.forecast[i].high + '</span>';}

					$("#weather").html(html);
				},
				error: function (error) {
					// $("#weather").html('<p>' + error + '</p>');
				}
			});
		});
	</script>
        <!-- Date and time pickers -->
    <script src="{{asset('front/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('front/js/bootstrap-timepicker.js')}}"></script>
    <script>
        $('input.date-pick').datepicker('setDate', 'today');
        $('input.time-pick').timepicker({
            minuteStep: 15,
            showInpunts: false
        })
    </script>
	<!-- Date and time pickers -->
	<script src="{{asset('front/js/jquery.sliderPro.min.js')}}"></script>
	<script type="text/javascript">
		$(document).ready(function ($) {

			$('#Img_carousel').sliderPro({
				width: 960,
				height: 500,
				fade: true,
				arrows: true,
				buttons: false,
				fullScreen: false,
				smallSize: 500,
				startSlide: 0,
				mediumSize: 1000,
				largeSize: 3000,
				thumbnailArrows: true,
				autoplay: false
			});
		});
	</script>


	<!--Review modal validation -->
	<script src="{{asset('front/assets/validate.js')}}"></script>

	<!-- Map -->
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBXtArR0GzW4Y_EezVpjGdvjrlwsQBcayw"></script>

	<script type="text/javascript">
        $(document).ready(function(e){
    (function(A) {

    if (!Array.prototype.forEach)
        A.forEach = A.forEach || function(action, that) {
            for (var i = 0, l = this.length; i < l; i++)
                if (i in this)
                    action.call(that, this[i], i, this);
            };

        })(Array.prototype);

        var
        mapObject,
        markers = [],
        markersData = {
            'Hotels': [
            {
                name: '{{$municipality->municipality_name}}',
                location_latitude: {{$municipality->latitude}}, 
                location_longitude: {{$municipality->longitude}},
                map_image_url: '{{asset($municipality->link_image)}}',
                name_point: '{{$municipality->municipality_name}}',
                description_point: '{!!substr($municipality->description,0,120) !!}...{!!'</p>'!!}',
                get_directions_start_address: '',
                // phone: '{{$municipality->phone}}',
                // url_point: '{{$municipality->municipality_name}}'
            },
            ]
        };

            var mapOptions = {
                zoom: 8,
                center: new google.maps.LatLng(4.7542131, -74.0264114),
                mapTypeId: google.maps.MapTypeId.ROADMAP,

                mapTypeControl: false,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                panControl: false,
                panControlOptions: {
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.TOP_LEFT
                },
                scrollwheel: false,
                scaleControl: false,
                scaleControlOptions: {
                    position: google.maps.ControlPosition.TOP_LEFT
                },
                streetViewControl: true,
                streetViewControlOptions: {
                    position: google.maps.ControlPosition.LEFT_TOP
                },
                styles: [
                                             {
                    "featureType": "landscape",
                    "stylers": [
                        {
                            "hue": "#FFBF00"
                        },
                        {
                            "saturation": 43.400000000000006
                        },
                        {
                            "lightness": 37.599999999999994
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "stylers": [
                        {
                            "hue": "#B18904"
                        },
                        {
                            "saturation": -61.8
                        },
                        {
                            "lightness": 45.599999999999994
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "stylers": [
                        {
                            "hue": "#DBA901"
                        },
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 51.19999999999999
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "stylers": [
                        {
                            "hue": "#FFBF00"
                        },
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 52
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "stylers": [
                        {
                            "hue": "#0078FF"
                        },
                        {
                            "saturation": -13.200000000000003
                        },
                        {
                            "lightness": 2.4000000000000057
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                // {
                //     "featureType": "poi",
                //     "stylers": [
                //         {
                //             "hue": "#FF0000"
                //         },
                //         {
                //             "saturation": -1.0989010989011234
                //         },
                //         {
                //             "lightness": 11.200000000000017
                //         },
                //         {
                //             "gamma": 1
                //         }
                //     ]
                // }
                ]
            };
            var
            marker;
            mapObject = new google.maps.Map(document.getElementById('map'), mapOptions);
            for (var key in markersData)
                markersData[key].forEach(function (item) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(item.location_latitude, item.location_longitude),
                        map: mapObject,
                        icon: "{{asset('img/pins/Hotels.png')}}",
                    });

                    if ('undefined' === typeof markers[key])
                        markers[key] = [];
                    markers[key].push(marker);
                    google.maps.event.addListener(marker, 'click', (function () {
      closeInfoBox();
      getInfoBox(item).open(mapObject, this);
      mapObject.setCenter(new google.maps.LatLng(item.location_latitude, item.location_longitude));
     }));

                    
                });
    

        function hideAllMarkers () {
            for (var key in markers)
                markers[key].forEach(function (marker) {
                    marker.setMap(null);
                });
        };

        function closeInfoBox() {
            $('div.infoBox').remove();
        };

        function getInfoBox(item) {
            return new InfoBox({
                content:
                '<div class="marker_info" id="marker_info">' +
                '<img src="' + item.map_image_url + '" alt="Image" width="280" height="auto"/>' +
                '<h3>'+ item.name_point +'</h3>' +
                '<span>'+ item.description_point +'</span>' +
                '<div class="marker_tools">' +
                '<form action="http://maps.google.com/maps" method="get" target="_blank" style="display:inline-block""><input name="saddr" value="'+ item.get_directions_start_address +'" type="hidden"><input type="hidden" name="daddr" value="'+ item.location_latitude +',' +item.location_longitude +'"><button type="submit" value="Get directions" class="btn_infobox_get_directions">¿Como llegar?</button></form>' +
                    // '<a href="tel://'+ item.phone +'" class="btn_infobox_phone">'+ item.phone +'</a>' +
                    '</div>' +
                    // '<a href="'+ item.url_point + '" class="btn_infobox">Details</a>' +
                '</div>',
                disableAutoPan: false,
                maxWidth: 0,
                pixelOffset: new google.maps.Size(10, 125),
                closeBoxMargin: '5px -20px 2px 2px',
                closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                isHidden: false,
                alignBottom: true,
                pane: 'floatPane',
                enableEventPropagation: true
            });


        };

    });
    </script>
	<script src="{{asset('front/js/infobox.js')}}"></script>
@endsection
