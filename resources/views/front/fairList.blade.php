@extends('layout.header')

    @section('title','Ferias y fiestas')
     @section('additionalStyle')

	<!-- CSS -->
	<link href="{{asset('front/css/slider-pro.min.css')}}" rel="stylesheet">
	
    
    @endsection
	@section('additionalPreMain')
	<section class="parallax-window" data-parallax="scroll" data-image-src="{{asset('img/feriasyfiestas.jpg')}}" style="width: 100%;height: auto; z-index: 1">
		<div class="parallax-content-1">
			<div class="animated fadeInDown">
				<h1 style="webkit-text-fill-color: yellowgreen;-webkit-text-stroke: 1px black;">Ferias y fiestas</h1>
			</div>
		</div>
	</section>
	@endsection
	<!-- End Section -->

	@section('main')
		<div id="position">
			<div class="container">
				<ul>
					<li><a href="{{url('/')}}">Home</a>
					</li>
					<li>Ferias y fiestas</li>
				</ul>
			</div>
		</div>
		<!-- End Position -->
		<div class="container " >
				<div class="row margin_60" >
					<div class="col-md-12 ">
						<a href="{{url('expocundinamarca')}}" ><img src="{{asset('img/expo.png')}}" width="100%"></a>
					</div>
				</div>
			<hr>
				<div class="row">
					<div class="col-md-4 other_tours">
						<ul>
							<li><a href="{{url('fairs?filter=Jan')}}"><i class="icon_set_1_icon-3"></i>Enero<span class="other_tours_price">{{$counts['Jan']}}</span></a>
							</li>
							<li><a href="{{url('fairs?filter=Feb')}}"><i class="icon_set_1_icon-30"></i>Febrero<span class="other_tours_price">{{$counts['Feb']}}</span></a>
							</li>
							<li><a href="{{url('fairs?filter=Mar')}}"><i class="icon_set_1_icon-44"></i>Marzo<span class="other_tours_price">{{$counts['Mar']}}</span></a>
							</li>
							<li><a href="{{url('fairs?filter=Apr')}}"><i class="icon_set_1_icon-3"></i>Abril<span class="other_tours_price">{{$counts['Apr']}}</span></a>
							</li>
						</ul>
					</div>
					<div class="col-md-4 other_tours">
						<ul>
							<li><a href="{{url('fairs?filter=May')}}"><i class="icon_set_1_icon-1"></i>Marzo<span class="other_tours_price">{{$counts['May']}}</span></a>
							</li>
							<li><a href="{{url('fairs?filter=Jun')}}"><i class="icon_set_1_icon-4"></i>Junio<span class="other_tours_price">{{$counts['Jun']}}</span></a>
							</li>
							<li><a href="{{url('fairs?filter=Jul')}}"><i class="icon_set_1_icon-30"></i>Julio<span class="other_tours_price">{{$counts['Jul']}}</span></a>
							</li>
							<li><a href="{{url('fairs?filter=Aug')}}"><i class="icon_set_1_icon-3"></i>Agosto<span class="other_tours_price">{{$counts['Aug']}}</span></a>
							</li>
						</ul>
					</div>
					<div class="col-md-4 other_tours">
						<ul>
							<li><a href="{{url('fairs?filter=Sep')}}"><i class="icon_set_1_icon-37"></i>Septiembre<span class="other_tours_price">{{$counts['Sep']}}</span></a>
							</li>
							<li><a href="{{url('fairs?filter=Oct0')}}"><i class="icon_set_1_icon-1"></i>Octubre<span class="other_tours_price">{{$counts['Oct']}}</span></a>
							</li>
							<li><a href="{{url('fairs?filter=Nov1')}}"><i class="icon_set_1_icon-50"></i>Noviembre<span class="other_tours_price">{{$counts['Nov']}}</span></a>
							</li>
							<li><a href="{{url('fairs?filter=Dec2')}}"><i class="icon_set_1_icon-44"></i>Diciembre<span class="other_tours_price">{{$counts['Dec']}}</span></a>
							</li>
						</ul>
					</div>
				</div>
		</div>
		<div class="container ">
			{{-- <div class="row">
				<div class="col-md-4 col-sm-6">
					@if(count($expo->images)>0 )
                        <div id="Img_carousel" class="slider-pro">
                            <div class="sp-slides">
                                
                                @foreach($expo->images as $image)
                                <div class="sp-slide">
                                    <img alt="Image" class="sp-image" src="css/images/blank.gif" data-src="{{asset($image->link_image)}}" data-small="{{asset($image->link_image)}}" data-medium="{{asset($image->link_image)}}" data-large="{{asset($image->link_image)}}" data-retina="{{asset($image->link_image)}}">
                                </div>
                                @endforeach
                            </div>
                            <div class="sp-thumbnails">
                                @foreach($expo->images as $image)
                                <img alt="Image" class="sp-thumbnail" src="{{asset($image->link_image)}}">
                                @endforeach
                            </div>
                        </div>

                    @endif
                </div>
				<div class="col-md-7 col-md-offset-1 col-sm-6">
					<h3>Expo <span>Cundinamarca</span></h3>
					<h4>Fecha</h4> {{$expo->date}}
					{!!$expo->description!!}

					<div class="general_icons">
						<ul>
							@if(count($expo->Video)>0)
								@foreach($expo->Video as $video)
								<li><a href="https://www.youtube.com/watch?v={{$video->link_video}}" class="video"><i class="icon-play-circled2-1"></i></a> Video</li>
								@endforeach
							@endif
							@if(count($expo->PDF)>0)
           						@foreach($fair->PDF as $pdf)
								<li><a href="{{asset($pdf->link_pdf)}}"><i class="icon_set_1_icon-92"></i>Documento</a></li>
								@endforeach
							@endif
							{{-- <li><i class="icon_set_1_icon-37"></i>Municipio</li>
						</ul>
					</div>
				</div>
			</div> --}}
			<!-- End row -->
			@foreach($fairs as $fair)
			<hr>
			<div class="row">
				<div class="col-md-4">
					@if(count($fair->images)>0 )
                        <div id="Img_carousel{{$fair->id_fair}}" class="slider-pro">
                            <div class="sp-slides">
                                
                                @foreach($fair->images as $image)
                                <div class="sp-slide">
                                    <img alt="Image" class="sp-image" src="css/images/blank.gif" data-src="{{asset($image->link_image)}}" data-small="{{asset($image->link_image)}}" data-medium="{{asset($image->link_image)}}" data-large="{{asset($image->link_image)}}" data-retina="{{asset($image->link_image)}}">
                                </div>
                                @endforeach
                            </div>
                            <div class="sp-thumbnails">
                                @foreach($fair->images as $image)
                                <img alt="Image" class="sp-thumbnail" src="{{asset($image->link_image)}}">
                                @endforeach
                            </div>
                        </div>
                    @endif
				</div>
				<div class="col-md-7 col-md-offset-1">
					<h3><span>{{$fair->fair_name}}</span></h3>
					<h4>Fecha <small>{{$fair->date}}</small></h4>
					{!!$fair->description!!}
					<div class="general_icons">
						<ul>
							@if(count($fair->Video)>0)
								@foreach($fair->Video as $video)
								<li><a href="https://www.youtube.com/watch?v={{$video->link_video}}" class="video"><i class="icon-play-circled2-1"></i></a> Video</li>
								@endforeach
							@endif
							@if(count($fair->PDF)>0)
           						@foreach($fair->PDF as $pdf)
								<li><a href="{{asset($pdf->link_pdf)}}"><i class="icon_set_1_icon-92"></i>Documento</a></li>
								@endforeach
							@endif
							<li><a href="{{url('municipality/'.$fair->PrincipalMunicipality->slug)}}"><i class="icon_set_1_icon-37"></i>Municipio</a></li>
						</ul>
					</div>
				</div>
			</div>
			@endforeach
			<hr>
			<!-- End row -->
			
		</div>

		<!-- End container-fluid -->
	@endsection
	<!-- End main -->

	@section('additionalScript')

	<!-- Specific scripts -->
	<script src="{{asset('front/js/icheck.js')}}"></script>
	<script>
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-grey',
			radioClass: 'iradio_square-grey'
		});
	</script>
	<!-- Date and time pickers -->
	<script src="{{asset('front/js/jquery.sliderPro.min.js')}}"></script>
	<script type="text/javascript">

		$(document).ready(function ($) {
			@foreach($fairs as $fair)
				$('#Img_carousel{{$fair->id_fair}}').sliderPro({
					width: 960,
					height: 500,
					fade: true,
					arrows: true,
					buttons: false,
					fullScreen: false,
					smallSize: 500,
					startSlide: 0,
					mediumSize: 1000,
					largeSize: 3000,
					thumbnailArrows: true,
					autoplay: false
				});
			@endforeach
		});
	</script>


	<!--Review modal validation -->
	<script src="{{asset('front/assets/validate.js')}}"></script>
	@endsection