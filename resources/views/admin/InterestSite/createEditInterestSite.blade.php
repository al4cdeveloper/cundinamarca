@extends('admin.layout.auth')

@if(isset($site))
    @section('title', 'Actualizar sitio de interés')
@else
    @section('title', 'Crear sitio de interés')
@endif

@section('aditionalStyle')
    <link href="{{asset('plugins/kartik-v-bootstrap-fileinput/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <style>
.kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar {
    display: inline-block;
}
.kv-avatar .file-input {
    display: table-cell;
    width: 213px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@endsection

@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Sitios de interés</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
            @if(isset($site))
			{!!Form::model($site,['url'=>['admin/interestsites/update',$site->id_site],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@else
			{!!Form::open(['url'=>'admin/interestsites/store', 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@endif
            <div class="form-group col-sm-12">
                <div class="col-sm-5 col-xs-12">
                    <div class="form-group">
                        <label for="costo">Categoria de sitio de interés</label>
                        {!!Form::select('fk_category', $categories, null, ['class'=>'select-chosen','id'=>'category'])!!}
                    </div>
                </div>
                <div class="col-sm-5 col-sm-offset-1 col-xs-12">
                    <div class="form-group">
                        <label for="costo">Municipio al que pertenece</label>
                        {!!Form::select('fk_municipality', $municipalities, null, ['class'=>'select-chosen','id'=>'municipalities'])!!}
                    </div>
                </div>
            </div>
            <div class="form-group col-sm-12">
    			<div class="col-sm-5 col-xs-12">
                    <div class="form-group">
                        <label for="costo">Nombre</label>
                        {!!Form::text('site_name', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre del sitio de interés', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('site_name') }}</span>
                    </div>
                </div>
                <div class="col-sm-5 col-sm-offset-1  col-xs-12 toHidden">
                    <div class="form-group">
                        <label for="costo">Dirección</label>
                        {!!Form::text('address', null, ['class'=>'form-control', 'placeholder' => 'Inserte dirección', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('address') }}</span>
                    </div>
                </div>
                <div class="col-sm-5 col-sm-offset-1  col-xs-12 toShow hidden">
                    <div class="form-group">
                        <label for="costo">Actividades</label>
                        <select name="services[]" id="services" class="select-chosen" multiple="">
                            @if(isset($site))
                                @foreach($services as $servicesO)
                                    <option value="{{$servicesO->id_service}}" 
                                    @if(count($site->Services)>0) 
                                        @foreach($site->Services as $relationService)
                                         @if($relationService->id_service == $servicesO->id_service) selected @endif
                                        @endforeach
                                    @endif>{{$servicesO->service}}</option>
                                @endforeach
                            @else
                                @foreach($services as $servicesO)
                                    <option value="{{$servicesO->id_service}}" >{{$servicesO->service}}</option>
                                @endforeach
                            @endif
                        </select>
                        <span class="label label-danger">{{$errors->first('services') }}</span>
                    </div>
                </div>

            </div>
        <div class="toHidden">
            <div class="form-group col-sm-12">
                <div class="col-sm-5  col-xs-12">
                    <div class="form-group">
                        <label for="costo">Teléfono</label>
                        {!!Form::text('phone', null, ['class'=>'form-control', 'placeholder' => 'Inserte telefono', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('phone') }}</span>
                    </div>
                </div>
                <div class="col-sm-5 col-sm-offset-1  col-xs-12">
                    <div class="form-group">
                        <label for="costo">Web</label>
                        {!!Form::text('web', null, ['class'=>'form-control', 'placeholder' => 'Inserte sitio web (en caso de tenerlo)', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('web') }}</span>
                    </div>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div class="col-sm-5  col-xs-12">
                    <div class="form-group">
                        <label for="costo">Facebook</label>
                        {!!Form::text('facebook', null, ['class'=>'form-control', 'placeholder' => 'Inserte link de facebook (en caso de tenerlo)', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('facebook') }}</span>
                    </div>
                </div>
                <div class="col-sm-5 col-sm-offset-1  col-xs-12">
                    <div class="form-group">
                        <label for="costo">Twitter</label>
                        {!!Form::text('twitter', null, ['class'=>'form-control', 'placeholder' => 'Inserte link de twitter (en caso de tenerlo)', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('twitter') }}</span>
                    </div>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div class="col-sm-5  col-xs-12">
                    <div class="form-group">
                        <label for="costo">Instagram</label>
                        {!!Form::text('instagram', null, ['class'=>'form-control', 'placeholder' => 'Inserte link de instagram (en caso de tenerlo)', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('instagram') }}</span>
                    </div>
                </div>
                <div class="col-sm-5 col-sm-offset-1  col-xs-12">
                    <div class="form-group">
                        <label for="costo">Canal de youtube</label>
                        {!!Form::text('youtube', null, ['class'=>'form-control', 'placeholder' => 'Inserte link de youtube (en caso de tenerlo)', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('youtube') }}</span>
                    </div>
                </div>
            </div>
        </div>
             <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Iframe (Opcional)</label>
                    {!!Form::text('iframe', null, ['class'=>'form-control', 'placeholder' => 'Inserte url de iframe en caso de tenerlo', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('iframe') }}</span>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div class="col-sm-5 col-xs-12">
                    <div class="form-group">
                        <label for="costo">Latitud</label>
                        {!!Form::text('latitude', null, ['class'=>'form-control', 'placeholder' => 'Ingrese latitud', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('latitude') }}</span>
                    </div>
                </div>
                <div class="col-sm-5 col-sm-offset-1 col-xs-12">
                    <div class="form-group">
                        <label for="costo">Longitud</label>
                        {!!Form::text('longitude', null, ['class'=>'form-control', 'placeholder' => 'Ingrese longitud', 'required'])!!}
    					<span class="label label-danger">{{$errors->first('longitude') }}</span>
                    </div>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div class="col-sm-6 col-md-offset-3">
                    <div class="form-group">
                        <label for="costo">Imagen para descargar (opcional)</label>
                        @if(isset($site))
                            @if($site->downloadimage)
                                <h5>Ya se ha cargado anteriormente la imagen, si se quiere actualizar por favor seleccionar acontinuacion</h5>
                            @endif
                        @endif
                        <div class="file-loading">
                            <input id="input-20" type="file" placeholder="Seleccione" name="downloadimage">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-sm-12 toShow hidden">
                    <div class="form-group">
                        <label for="costo">Operadores relacionados</label>
                        <div id="divoperators">
                        <select name="operators[]" id="operators" class="select-chosen hidden" multiple="">
                            @if(isset($site))
                                @foreach($operators as $operator)
                                    <option value="{{$operator->id_site}}" 
                                    @if(count($site->Operators)>0) 
                                        @foreach($site->Operators as $relationOperator)
                                         @if($relationOperator->id_site == $operator->id_site) selected @endif
                                        @endforeach
                                    @endif>{{$operator->site_name}}</option>
                                @endforeach
                            @endif
                        </select>
                        </div>
                        <span class="label label-danger">{{$errors->first('services') }}</span>
                    </div>
                </div>
            <div class="col-sm-12">
                <div class="form-group">
                <label >Palabras clave <small>Separadas por comas (,)</small></label>
                    {!!Form::text('keywords',null,['class'=>'form-control input-tags', 'placeholder'=>'Palabras clave'])!!}
                    <span class="label label-danger">{{$errors->first('keywords') }}</span>
                </div>
            </div>
            @if(isset($site)&& $site->description!=null)
            <div class="col-sm-12 form-group" id="description">
                <label for="duration">Descripción</label>
                <div class="form-group">
                    <textarea id="textarea-ckeditor" name="description" class="ckeditor">{!!$site->description!!}</textarea>
					<span class="label label-danger">{{$errors->first('description') }}</span>
                </div>
	        </div>
            @else
            <div class="col-sm-12 form-group" id="description">
                <label for="duration">Descripción</label>
                <div class="form-group">
                    <textarea id="textarea-ckeditor" name="description" class="ckeditor"></textarea>
                    <span class="label label-danger">{{$errors->first('description') }}</span>
                </div>
            </div>
            @endif
            <div class="col-sm-12">

                 <div class="col-sm-4 text-center col-sm-offset-1">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen</label>
                            <div class="file-loading">

                                {!!Form::file('link_image',['id'=>'avatar-1'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('link_image') }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 text-center col-sm-offset-1">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Icono</label>
                            <div class="file-loading">

                                {!!Form::file('link_icon',['id'=>'avatar-2'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('link_icon')}}</span>
                        </div>
                    </div>
                </div>
            </div>
            {!!Form::hidden('type',0,['id'=>'type'])!!}
            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                <a href="{{url('admin/interestsites')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection

@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>

    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/plugins/sortable.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/fileinput.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/fr.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/es.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/explorer-fa/theme.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/fa/theme.js')}}" type="text/javascript"></script>

    <script>

    $('#category').change(function()
    {
        if($('#category option:selected').text()=="Actividades")
        {
            $(".toHidden").addClass('hidden');
            $(".toShow").removeClass('hidden');
            $("#type").val('1');
        }
        else
        {
            $(".toHidden").removeClass('hidden');
            $(".toShow").addClass('hidden');
            $("#type").val('0');
        }
    });
        $(document).on('ready', function() {
            if($('#category option:selected').text()=="Actividades")
        {
            $(".toHidden").addClass('hidden');
            $(".toShow").removeClass('hidden');
            $("#type").val('1');
        }
        else
        {
            $(".toHidden").removeClass('hidden');
            $(".toShow").addClass('hidden');
            $("#type").val('0');
        }
            $("#input-20").fileinput({
                browseClass: "btn btn-primary btn-block",
                browseLabel: "Seleccionar imagen",
                showRemove: false,
                showUpload: false

            });
        });
    $("#municipalities").change(function()
    {  
        route= '/admin/interestsites/getoperators';
        token = '{{csrf_token()}}';
        municipality = $(this).val();
        $.ajax({
            url: route,
            type: 'GET',
            headers:{'X-CSRF-TOKEN': token},
            dataType: 'JSON',
            data: {municipality},
            complete:function(transport)
            {
                response = transport.responseJSON;
                if(response.length>0)
                {
                    $("#operators").empty();
                    $("#operators").removeClass('hidden');
                    $("#textoOp").addClass('hidden');

                    for(x in response)
                    {
                        services =" (";
                        for(y in response[x].services)
                        {
                            services += response[x].services[y].service+",";
                        }
                        services +=").";
                        $("#operators").append('<option value="'+response[x].id_site+'">'+response[x].site_name+'<small>'+services+'</small></option>').trigger("chosen:updated");
                    }
                }
                else
                {
                    $("#operators").addClass('hidden');
                    $("#operators").empty();
                    $("#operators").trigger("chosen:updated");
                    $("#divoperators").append('<p id="textoOp">No se han encontrado operadores para este municipio</p>');
                }
            }

        });
        
    });

    $("#avatar-1").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="fab fa-searchengin"></i>',
        removeIcon: '<i class="far fa-times-circle"></i>',
        viewIcon:'<i class="far fa-times-circle"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        @if(isset($site) && $site->link_image!=null)
        defaultPreviewContent: '<img src="{{asset($site->link_image)}}" alt="Avatar por defecto" width="50%">',
        @else
        defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
        @endif
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });
    $("#avatar-2").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="fab fa-searchengin"></i>',
        removeIcon: '<i class="far fa-times-circle"></i>',
        viewIcon:'<i class="far fa-times-circle"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        @if(isset($site)&& $site->link_icon!=null)
        defaultPreviewContent: '<img src="{{asset($site->link_icon)}}" alt="Icono por defecto" width="50%">',
        @else
        defaultPreviewContent: '<img src="{{asset('img/default-icon.png')}}" alt="Icono por defecto" width="50%">',
        @endif
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg","ico", "png",]
    });
</script>
@endsection