@extends('layout.header')

    @section('title','Cundinamarca ¡La leyenda vive!')


    @section('additionalPreMain')
        <section id="hero" style="background: #4d536d url({{asset('img/bannerwelcome.jpg')}}) no-repeat center center; width: 100%; max-height: 500px!important;    background-size: cover;">
        </section>
    @endsection
    @section('main')


        <div class="container margin_60">

            <div class="main_title">
                <h2><span>Cundinamarca</span> ¡La leyenda vive!</h2>
                <br>
                {{-- <p>
                    En este departamento convergen la naturaleza, la historia y la cultura, con planes para todos los públicos, a los que se puede acceder desde diversas rutas. Aquí podrás disfrutar de una variada y deliciosa oferta gastronómica, impregnada de exquisitos sabores indígenas. <br><br>
No te pierdas el imponente paisaje del <strong>Parque Nacional Natural Chingaza</strong>, la experiencia de escalar rocas en Suesca, volar parapente en Sopó y La Calera, practicar canotaje en Tobia, hacer caminatas ecológicas con la familia por los senderos y reservas de La Vega o recorrer las empedradas calles que antaño recorrió la <strong>Ruta Libertadora </strong>en municipios como Villapinzón, Chocontá, Suesca, Nemocón, Zipaquirá, Cajicá y Chía.
                </p> --}}
            </div>

            <div class="row">

                <div class="col-md-4 wow zoomIn" data-wow-delay="0.2s">
                    <div class="feature_home" >
                        <h3><span>Bandera</span> </h3>
                        <img src="{{asset('img/simbolos/flag_cundinamarca.png')}}" alt="Bandera Cundinamarca"  style="height: 150px!important;width: auto;align-self: center;">
                        <br>
                        <br>

                        <p>Don Antonio Nariño, siendo Presidente del Estado de Cundinamarca, fue comisionado por el Serenísimo Colegio Electoral, en sesión del 17 de julio de 1813, para que formase los diseños de las divisas, tanto en la bandera y escudo de la Nueva República, así como la cucarda del ejército y la banda presidencial. </p>
                    </div>
                </div>

                <div class="col-md-4 wow zoomIn" data-wow-delay="0.4s">
                    <div class="feature_home " >
                        <h3><span>Escudo</span> </h3>
                        <img src="{{asset('img/simbolos/escudo.png')}}" alt="Escudo Cundinamarca" style="height: 150px;width: auto;align-self: center;" >
<br>
                        <br>
                        <p>El escudo propuesto por Nariño, estaba formado por "un águila con las alas abiertas, en actitud de emprender el vuelo, una espada en la garra derecha y una granada en la izquierda. En la cabeza tiene el gorro frigio; en la parte superior llevaba la inscripción morir o vencer y en la parte inferior una leyenda con el nombre: Cundinamarca".</p>
                    </div>
                </div>

                <div class="col-md-4 wow zoomIn" data-wow-delay="0.6s">
                    <div class="feature_home">
                        <h3><span>Himno</span> </h3>
                        <div class="row">
                        <audio controls="" style="margin-top: 10%;margin-bottom: 10%; ">><source src="http://www.cundinamarca.gov.co/wcm/connect/9a2e632b-e91a-4906-96fa-61c56c198dcd/himno-cundinamarca.mp3?MOD=AJPERES&CONVERT_TO=url&CACHEID=ROOTWORKSPACE-9a2e632b-e91a-4906-96fa-61c56c198dcd-k7rYan4" type=""></audio>
                            </div>
                        <p >El Himno de Cundinamarca se adopta mediante Decreto No. 1819 de Julio 24 de 1972, compuesto por el músico Hernando Rivera Páez y la letra del compositor Alberto Perico Cárdenas, elegido en concurso convocado para el efecto.</p>
                    </div>
                </div>

            </div>
            <div class="row">
                <div id="tour_guide">
                    <p>
                        <img src="{{asset('img/simbolos/jorge_rey.jpg')}}" alt="Image" class="img-circle styled" height="250" width="250" style="    border: 2px solid #9c9c9c!important;">
                    </p>
                    <h2>Jorge Emilio Rey Ángel</h2>
                    <p class="lead add_bottom_30">
                        Desde muy joven, <strong>Jorge Emilio Rey</strong>, actual gobernador de Cundinamarca, se ha interesado por el desarrollo social, siendo parte de procesos comunales en Funza, llegando incluso a liderar procesos con la Administración Municipal
                    </p>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <h3>¿Quien es Jorge Rey?</h3>
                        <p>
                            Jorge Rey es un amigo, un líder natural. Es un hombre con experiencia, inquieto, elocuente, idealista y con profundo carácter a la hora de gobernar. 
                        </p>
                        <p>
                        Representa para muchos, gracias a su eminente capacidad de gestión y de trabajo, el nuevo liderazgo en Cundinamarca, amparado en la más rasa y sincera fórmula de hacer política: ser y sentirse cercano a la gente, comprometido con la comunidad, demostrando con hechos palpables, ágiles y prósperos su palpitar por Cundinamarca. 
                        </p>
                        <p>
                            Por legado de su madre, doña Mercedes Ángel de Rey, una mujer recia y comunal, es un convencido de que solo sirviendo a la comunidad se pueden generar acciones que redunden en el crecimiento personal y económico de la sociedad. De su mano adoptó el espíritu de servicio social y a través de su ejemplo despertó el interés por los procesos de participación y por la política misma.
                        </p>
                    </div>
                    <div class="col-md-4">
                        <h3><i class=""></i>Trayectoria</h3>
                        <ul class="list_ok">
                            <li>Líder comunal desde los 20 años de edad.</li>
                            <li>2003-2006: Secretario de Gobierno del municipio de Funza.</li>
                            <li>2008-2012: Alcalde del municipio de Funza. Uno de los mejore del país.</li>
                            <li>2012: Director del Instituto Departamental de Acción Comunal y Participación Ciudadana (Idaco)</li>
                            <li>2014: Representante a la Cámara por Cundinamarca, elegido por 61.707 conciudadanos (la más alta votación en la historia del departamento)</li>
                            <li>2016 - 2019: Gobernador de Cundinamarca</li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <hr>
                    <div class="row " >
                    <div class="col-md-12 ">
                        <a href="{{url('expocundinamarca')}}" ><img src="{{asset('img/expo.png')}}" width="100%"></a>
                    </div>
                </div>
                <hr>
            <!--End row -->

            <div class="main_title">
                <h2>Razones <span>para</span> visitarnos</h2>
                <br>
                <p>
                    En este departamento convergen la naturaleza, la historia y la cultura, con planes para todos los públicos, a los que se puede acceder desde diversas rutas. Aquí podrás disfrutar de una variada y deliciosa oferta gastronómica, impregnada de exquisitos sabores indígenas. <br><br>
No te pierdas el imponente paisaje del <strong>Parque Nacional Natural Chingaza</strong>, la experiencia de escalar rocas en Suesca, volar parapente en Sopó y La Calera, practicar canotaje en Tobia, hacer caminatas ecológicas con la familia por los senderos y reservas de La Vega o recorrer las empedradas calles que antaño recorrió la <strong>Ruta Libertadora </strong>en municipios como Villapinzón, Chocontá, Suesca, Nemocón, Zipaquirá, Cajicá y Chía. <br><br>
El nombre de <strong>Cundinamarca</strong> viene de los vocablos quechuas ‘kuntur’ y ‘marca’, que unidos significan ’tierra del cóndor’ y de los 116 municipios, más de la mitad tienen origen indígena. <a href="https://goo.gl/fxomsJ" target="_blank">Aquí</a> puedes consultar más información sobre el significado de los sonoros nombres de los municipios del departamento.
                </p>
            </div>

            <div class="row">

                <div class="col-md-4 wow zoomIn" data-wow-delay="0.2s">
                    <div class="feature_home">
                        <i class="icon_set_1_icon-48"></i>
                        <h3><span>Destinos</span>  Maravillosos</h3>
                        {{-- <p>
                            Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset.
                        </p> --}}
                        <a href="{{url('/municipalities')}}" class="btn_1 outline">Leer más</a>
                    </div>
                </div>

                <div class="col-md-4 wow zoomIn" data-wow-delay="0.4s">
                    <div class="feature_home">
                        <i class="icon_set_1_icon-30"></i>
                        <h3><span>Actividades</span> que te encantarán</h3>
                        {{-- <p>
                            Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset.
                        </p> --}}
                        <a href="{{url('/operators')}}" class="btn_1 outline">Leer más</a>
                    </div>
                </div>

                <div class="col-md-4 wow zoomIn" data-wow-delay="0.6s">
                    <div class="feature_home">
                        <i class="icon_set_1_icon-41"></i>
                        <h3><span>Eventos </span> Imperdibles</h3>
                        {{-- <p>
                            Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset.
                        </p> --}}
                        <a href="{{url('/fairs')}}" class="btn_1 outline">Leer más</a>
                    </div>
                </div>

            </div>
            <!--End row -->


        </div>
        <hr>
        <section class="promo_full">
            <div class="promo_full_wp magnific">
                <div>
                    <div class="row">
                        <div class="main_title">
                            <h2><span>Rutas Únicas</span> </h2>
                        </div>
                        <div class="col-md-2 col-sm-6 text-center col-md-offset-1">
                            <p>
                                <a href="{{url('routes/ruta-del-agua')}}"><img src="{{asset('img/rutaagua.png')}}" alt="Ruta Del Agua" class="logo-route"></a>
                            </p>
                            
                        </div>
                        <div class="col-md-2 col-sm-6 text-center">
                            <p>
                                <a href="{{url('routes/ruta-dulce-y-de-aventura')}}"><img src="{{asset('img/rutadulce.png')}}" alt="Ruta Dulce y Aventura" class="logo-route"></a>
                            </p>
                            
                            
                        </div>
                        <div class="col-md-2 col-sm-6 text-center">
                            <p>
                                <a href="{{url('routes/ruta-del-dorado')}}"><img src="{{asset('img/rutadorado.png')}}" alt="Ruta del Dorado" class="logo-route"></a>
                            </p>
                            
                            
                        </div>
                        <div class="col-md-2 col-sm-6 text-center">
                            <p>
                                <a href="{{url('routes/ruta-del-rio-y-el-encanto-natural')}}"><img src="{{asset('img/rutario.png')}}" alt="Ruta del Río y Encanto Natural" class="logo-route"></a>
                            </p>
                            
                            
                        </div>
                        <div class="col-md-2 col-sm-6 text-center">
                            <p>
                                <a href="{{url('routes/ruta-del-del-vuelo-del-condor')}}"><img src="{{asset('img/rutacondor.png')}}" alt="Ruta del Vuelo Del Condor" class="logo-route"></a>
                            </p>
                           
                            
                        </div>                   
                    </div>

                </div>

            </div>
        </section>
        <!-- End container -->
        <!-- END REVOLUTION SLIDER -->
        <section class="container margin_60">

            <div class="main_title">
                <h2><span>Destinos</span> Maravillosos</h2>
            </div>

            <div class="row">
                @foreach($municipalities as $municipality)
                    <div class="col-md-4 col-sm-6 col-lg-4 wow zoomIn targetList" data-wow-delay="0.1s" height="400" >
                        <div class="tour_container">
                            <div class="img_container">
                                <a href="{{url('municipality/'.$municipality->slug)}}">
                                    <img src="{{asset($municipality->link_image)}}" class="img-responsive" alt="image">
                                    <div class="short_info">
                                        <h2 style="color:white!important">{{$municipality->municipality_name}}</h2>
                                    </div>
                                </a>
                            </div>
                            <div class="tour_title" style="padding-top: 0px!important">
                                    <p>{!!substr($municipality->description,0,120) !!}... <a href="{{url('municipality/'.$municipality->slug)}}">Ver más</a> {!!'</p>'!!}</p>
                            </div>
                        </div>
                        <!-- End box tour -->
                    </div >
                    <!-- End col-md-4 -->
                @endforeach

                {{-- @if(count($municipalities)>6)  --}}
                    <div class="col-xs-12">
                        <p class="text-center add_bottom_30">
                            <a href="{{url('municipalities')}}" class="btn_1 medium"><i class="icon-eye-7"></i>Ver todos los municipios ({{$numMunicipalities}}) </a>
                        </p>
                    </div>
                {{-- @endif --}}
        </section>
    <hr>
        <section class="container margin_60">

            <div class="main_title">
                <h2><span>Sitios de interés</span></h2>
            </div>

            <div class="row">
                @foreach($sites as $site)
                <div class="col-md-4 col-sm-4 col-lg-4 wow zoomIn targetListOperatorMd" data-wow-delay="0.1s" height="400">
                    <div class="tour_container">
                        <div class="ribbon_3 popular"><span>{{$site->Municipality->municipality_name}}</span>
                        </div>
                        <div class="img_container">
                            <a href="{{url('site/'.$site->slug)}}">
                                <img src="@if($site->link_image!=null){{asset($site->link_image)}}@else {{asset('img/defaultimg.jpg')}}@endif" width="800" height="530" class="img-responsive" alt="Image">
                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong> {{$site->site_name}}</strong></h3>
                        </div>
                    </div>
                    <!-- End box tour -->
                </div>
                @endforeach

                {{-- @if(count($municipalities)>6)  --}}
                    <div class="col-xs-12">
                        <p class="text-center add_bottom_30">
                            <a href="{{url('interestsites')}}" class="btn_1 medium"><i class="icon-eye-7"></i>Ver todos los sitios de interés ({{$numSites}}) </a>
                        </p>
                    </div>
                {{-- @endif --}}
        </section>

         <hr>
        <section class="container margin_60">

            <div class="main_title">
                <h2><span>Operadores</span></h2>
            </div>

            <div class="row">
                @foreach($operators as $operator)
                        <div class="col-md-4 col-sm-4 col-lg-4 wow zoomIn targetListOperatorMd" data-wow-delay="0.1s" height="400">
                            <div class="tour_container">
                                <div class="ribbon_3 popular"><span>{{$operator->Municipality->municipality_name}}</span>
                                </div>
                                <div class="img_container">
                                    <a href="{{url('operator/'.$operator->slug)}}">
                                        <img src="@if($operator->link_icon!=null){{asset($operator->link_icon)}}@else {{asset('img/defaultimg.jpg')}}@endif" width="800" height="533" class="img-responsive" alt="Image">
                                    </a>
                                </div>
                                <div class="tour_title">
                                    <h3><strong> {{$operator->site_name}}</strong></h3>
                                    <div class="rating">
                                        @foreach($operator->Services as $service)
                                            - {{$service->service}}
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <!-- End box tour -->
                        </div>
                        @endforeach

                {{-- @if(count($municipalities)>6)  --}}
                    <div class="col-xs-12">
                        <p class="text-center add_bottom_30">
                            <a href="{{url('operators')}}" class="btn_1 medium"><i class="icon-eye-7"></i>Ver todos los operadores ({{$numOperators}}) </a>
                        </p>
                    </div>
                {{-- @endif --}}
        </section>
        <!-- End section -->

    @endsection
    <!-- End main -->
