<?php

namespace App\Http\Controllers\Operator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ServiceCategory;
use App\Models\Service;
use App\Operator;
use App\Models\ServiceOperator;
use App\Models\Contact;
use App\User;
use Illuminate\Validation\Rule;
use Auth;
use Session;

class operatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $categories = ServiceCategory::all();
        // $services = Service::all();

        return view('infoOperator',compact('categories','services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = ServiceCategory::all();
        $services = Service::all();
        return view('registerOperator',compact('categories','services'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $operator = new Operator;
        $operator->name  =$request->name;
        $operator->national_register  =$request->national_register;
        $operator->contact_personal  =$request->contact_personal;
        $operator->contact_phone  =$request->contact_phone;
        $operator->email  =$request->email;
        $operator->web  =$request->web;
        $operator->service_category  =$request->service_category;
        $operator->save();
        foreach($request->services  as $serviceO)
        {
            $service = new ServiceOperator;
            $service->fk_service = $serviceO;
            $service->fk_operator  =$operator->id;
            $service->save();
        } 
        $message  = "Se ha realizado tu registro correctamente!";
        return view('notifation',compact('message'));

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = Auth::user();
        return view('operator.profile',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $operator     = Operator::find(Auth::user()->id_operator);
        if($request->password)
        {
            $validator = \Validator::make($request->all(), [
             'name' => 'required|max:255',
             'national_register'  => 'required|max:255',
             'email'      => ['required','email',Rule::unique('operators')->ignore($operator->id_operator, 'id_operator'),Rule::unique('users')],
             'password'   => 'required|min:6|confirmed',
            ]); 
        }
        else
        {
            $validator = \Validator::make($request->all(), [
             'name' => 'required|max:255',
             'national_register'  => 'required|max:255',
             'email'      => ['required','email',Rule::unique('operators')->ignore($operator->id_operator, 'id_operator'),Rule::unique('users')],
            ]); 
        }

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {

            $files = $request->file('file');

            $operator->name  =$request->name;
            $operator->national_register  =$request->national_register;
            $operator->contact_personal  =$request->contact_personal;
            $operator->contact_phone  =$request->contact_phone;
            $operator->email  =$request->email;
            $operator->web  =$request->web;

            if($request->password)              $operator->password = bcrypt($request->password);
            if($files)
            {
                if($operator->avatar!="")
                {
                    chmod(public_path().'/images/profile',0777);
                    fopen(public_path().$operator->avatar, 'wb');
                    unlink(public_path().$operator->avatar);
                }
                $path = public_path().'/images/profile';

                $nombre ="";


                $filename = 'Profile_'.date('YmdHis', time()).rand().'.'.$files->getClientOriginalName();
                $nombre = '/images/profile/'.$filename;
                $files->move($path, $filename);

                $operator->avatar=$nombre;
            }
            $operator->save();

            Session::flash('message', 'Se ha realizado la actualización de información.');
            return redirect('operator/profile');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function preferences()
    {
        $user = Auth::User();
        return view('operator.preferences',compact('user'));
    }
    public function updatepreferences(Request $request)
    {
        $user = Auth::User();
        $user->days_for_reservation = $request->days_for_reservation;
        $user->save();
        Session::flash('message','Se ha realizado la actualización correctamente.');
        return redirect('operator/preferences');
    }
}
