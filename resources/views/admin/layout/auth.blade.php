<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>
    
    
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{asset('icon/favicon.png')}}">
        <link rel="apple-touch-icon" href="{{asset('icon/icon57.png')}}" sizes="57x57">
        <link rel="apple-touch-icno" href="{{asset('icon/icon72.png')}}" sizes="72x72">
        <link rel="apple-touch-icon" href="{{asset('icon/icon76.png')}}" sizes="76x76">
        <link rel="apple-touch-icon" href="{{asset('icon/icon114.png')}}" sizes="114x114">
        <link rel="apple-touch-icon" href="{{asset('icon/icon120.png')}}" sizes="120x120">
        <link rel="apple-touch-icon" href="{{asset('icon/icon144.png')}}" sizes="144x144">
        <link rel="apple-touch-icon" href="{{asset('icon/icon152.png')}}" sizes="152x152">
        <link rel="apple-touch-icon" href="{{asset('icon/icon180.png')}}" sizes="180x180">
    <!-- END Icons -->

    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('auth-panel/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('auth-panel/css/plugins.css')}}">
    <link rel="stylesheet" href="{{asset('auth-panel/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('auth-panel/css/themes.css')}}">

    <link rel="stylesheet" href="{{asset('plugins/fontawesome-free-5.0.4/svg-with-js/css/fa-svg-with-js.css')}}">

    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <!-- Scripts -->
    <script src="{{asset('auth-panel/js/vendor/modernizr-3.3.1.min.js')}}"></script>
    

    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    @include('layout.alert');

    @yield('aditionalStyle')

</head>
<body>


    <div id="page-wrapper" class="page-loading"> 
            <div class="preloader">
                <div class="inner">
                    <div class="preloader-spinner themed-background hidden-lt-ie10"></div>

                    <!-- Text for IE9 -->
                    <h3 class="text-primary visible-lt-ie10"><strong>Cargando...</strong></h3>
                </div>
            </div>
                <div id="page-container" class="header-fixed-top sidebar-visible-lg-full sidebar-light">
                <!-- Main Sidebar -->
                <div id="sidebar">
                    <!-- Sidebar Brand -->
                    <div id="sidebar-brand" class="themed-background">
                        <a href="{{url('/admin/home')}}" class="sidebar-title">
                            <img src="{{asset('img/idecut.png')}}"  height="100%"> <span class="sidebar-nav-mini-hide"></span>
                        </a>
                    </div>
                    <!-- END Sidebar Brand -->

                    <!-- Wrapper for scrolling functionality -->
                    @include('admin.layout.menuAdmin')
                    <!-- END Wrapper for scrolling functionality -->

                    <!-- Sidebar Extra Info -->
                    <div id="sidebar-extra-info" class="sidebar-content sidebar-nav-mini-hide">
                        <div class="text-center">
                            <h5>Rutas Colombia </h5>
                            <small><span>2018</span> &copy; </small>
                        </div>
                    </div>
                    <!-- END Sidebar Extra Info -->
                </div>
                <!-- END Main Sidebar -->

                <!-- Main Container -->
                <div id="main-container">
                    <header class="navbar navbar-inverse navbar-fixed-top">
                        <!-- Left Header Navigation -->
                        <ul class="nav navbar-nav-custom">
                            <!-- Main Sidebar Toggle Button -->
                            <li>
                                <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
                                    <i class="fa fa-ellipsis-v fa-fw animation-fadeInRight" id="sidebar-toggle-mini"></i>
                                    <i class="fa fa-bars fa-fw animation-fadeInRight" id="sidebar-toggle-full"></i>
                                </a>
                            </li>
                            <!-- END Main Sidebar Toggle Button -->

                            <!-- Header Link -->
                            <li class="hidden-xs animation-fadeInQuick">
                                <a href=""><strong>@yield('title')</strong></a>
                            </li>
                            <!-- END Header Link -->
                        </ul>
                        <!-- END Left Header Navigation -->

                        <!-- Right Header Navigation -->
                        <ul class="nav navbar-nav-custom pull-right">
                            <!-- Search Form -->

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    @if(Auth::user()->email=="al4cdeveloper@gmail.com")
                                    <img src="{{asset('img/darkar.jpg')}}" alt="avatar">
                                    @else
                                    <img src="{{asset('img/default-avatar.png')}}" alt="avatar">
                                    @endif
                                    {{Auth::user()->name}}<span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    {{-- <li>
                                        <a href="{{url('admin/profile')}}">
                                            <i class="fa fa-picture-o fa-fw pull-right"></i>
                                            Mi perfil
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('admin/preferences')}}">
                                            <i class="gi gi-settings fa-fw pull-right"></i>
                                            Preferencias
                                        </a>
                                    </li> --}}
                                    <li>
                                        <a href="{{ url('/admin/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="fa fa-power-off fa-fw pull-right"></i>
                                            Cerrar sesión
                                        </a>

                                        <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <!-- END Right Header Navigation -->
                    </header>
                    <!-- END Header -->

                    <!-- Page content -->
                   
                    @yield('content')
                    <!-- END Page Content -->
                </div>
                <!-- END Main Container -->
            </div>
            <!-- END Page Container -->
        </div>
        <!-- END Page Wrapper -->



    <!-- jQuery, Bootstrap, jQuery plugins and Custom JS code -->
    <script src="{{asset('auth-panel/js/vendor/jquery-2.2.4.min.js')}}"></script>
    <script src="{{asset('auth-panel/js/vendor/bootstrap.min.js')}}"></script>
    <script src="{{asset('auth-panel/js/plugins.js')}}"></script>
    <script src="{{asset('auth-panel/js/app.js')}}"></script>
    <script src="{{asset('plugins/fontawesome-free-5.0.4/svg-with-js/js/fontawesome-all.js')}}"></script>
    @yield('aditionalScript')

</body>
</html>

