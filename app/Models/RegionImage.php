<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RegionImage extends Model
{
	protected $primaryKey = 'id_region_image';

	protected $fillable = ['link_image','fk_region'];
}
