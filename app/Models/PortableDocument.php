<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PortableDocument extends Model
{
	protected $primaryKey = 'id_pdf';
	protected $fillable = ['link_pdf','type_relation','fk_relation'];
}
