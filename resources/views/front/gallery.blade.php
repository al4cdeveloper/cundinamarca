@extends('layout.header')

    @section('title','Galería')

@section('additionalStyle')
	<!-- SPECIFIC CSS - GRID GALLERY -->
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="{{asset('front/css/finaltilesgallery.css')}}" rel="stylesheet">
	<link href="{{asset('front/css/lightbox2.css')}}" rel="stylesheet">
@endsection


    @section('main')

		<div id="position">
			<div class="container">
				<ul>
					<li><a href="{{url('/')}}">Home</a>
					</li>
					<li>Galería</li>
				</ul>
			</div>
		</div>
		<!-- End Position -->

		<div class="container margin_60">

<div class="row " >
                    <div class="col-md-12 ">
                        <a href="{{url('expocundinamarca')}}" ><img src="{{asset('img/expo.png')}}" width="100%"></a>
                    </div>
                </div>
	<br>
	<br>
			<div class="main_title">
				<h2>Conoce <span>Cundinamarca</span></h2>
				<p>
					El escenario ideal para tus próximas vacaciones.
				</p>
			</div>
			<hr>

			<div id="gallery" class="final-tiles-gallery  effect-dezoom effect-fade-out caption-top social-icons-right">
				<div class="ftg-items">
					@foreach($allImages as $image)
					<div class="tile ftg-set-rome">
						<a class="tile-inner" data-title="@if($image->type=="municipality"){{$image->Municipality->municipality_name}}@else{{$image->Site->site_name}}@endif" data-lightbox="gallery" href="{{$image->link_image}}">
							<img class="item" width="{{$image->size['width']}}" height="{{$image->size['height']}}" src="{{$image->link_image}}" data-src="{{$image->link_image}}">
							<span class='title'>@if($image->type=="municipality"){{$image->Municipality->municipality_name}}@else{{$image->Site->site_name}}@endif</span>
							<div class="ftg-social">
								<a href="@if($image->type=="municipality"){{url('/municipality/'.$image->Municipality->slug)}}@else{{url('/site/'.$image->Site->slug)}}@endif" ><i class="fa fa-external-link"></i></a>

								<a href="#" data-social="twitter"><i class="fa fa-twitter"></i></a>
								<a href="#" data-social="facebook"><i class="fa fa-facebook"></i></a>
								<a href="#" data-social="google-plus"><i class="fa fa-google"></i></a>
								<a href="#" data-social="pinterest"><i class="fa fa-pinterest"></i></a>

							</div>
						</a>
					</div>
					@endforeach
					<!-- End image -->

				</div>

			</div>
		</div>
		<!-- End container -->
	@endsection

	@section('additionalScript')
	<!-- Specific scripts - Grid gallery -->
	<script src="{{asset('front/js/jquery.finaltilesgallery.js')}}"></script>
	<script src="{{asset('front/js/lightbox2.js')}}"></script>
	<script>
		$(function () {
			'use strict';
			//we call Final Tiles Grid Gallery without parameters,
			//see reference for customisations: http://final-tiles-gallery.com/index.html#get-started
			$(".final-tiles-gallery").finalTilesGallery();
		});
	</script>
	@endsection
