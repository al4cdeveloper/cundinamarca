<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
class Notice extends Model
{
	use Sluggable;


	protected $primaryKey="id_notice";
	protected $fillable=['id_notice','notice_name','image','description','slug','keywords','state','fk_fair'];

	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'notice_name'
            ]
        ];
    }

    public function images()
    {
        return $this->hasMany('App\Models\RegionImage', 'fk_region');
    }
}
