@extends('admin.layout.auth')

@section('title', 'Listado de operadores')


@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Operadores</h1>
                    </div>
                </div>
            </div>
        </div>
                <div class="block full">
                    <div class="block-title">
                        <h2>Operadores</h2>
                        <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/operators/create')}}" class="btn btn-primary btn-sm"><i class="far fa-plus-square fa-lg"></i></a></div>
                    </div>
                    <div class="table-responsive">
                        <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 50px;">Nombre</th>
                                    <th>Municipio</th>
                                    <th>Dirección</th>
                                    <th>Servicios</th>
                                    <th style="width: 120px;">Estado</th>
                                    <th class="text-center" style="width: 75px;"><i class="fas fa-sync-alt"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($operators as $operator)
                                <tr>
                                    <td>{{$operator->site_name}}</td>
                                    <td>{{$operator->Municipality->municipality_name}}</td>
                                    <td>{{$operator->address}}</td>
                                    <td><ul>
                                            @foreach($operator->Services as $service)
                                            <li>{{$service->service}}</li>
                                            @endforeach
                                    </ul></td>
                                    <td>@if($operator->state=="activo")
                                        <a class="label label-info">{{$operator->state}}</a>
                                        @else
                                        <span class="label label-danger">{{$operator->state}}</span>
                                        @endif
                                    </td>
                                    <td class="text-center">

                                        <a href="{{url('admin/operators/edit/'.$operator->slug)}}" data-toggle="tooltip" title="Editar operador" class="btn btn-effect-ripple btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                        <a href="{{url('admin/operators/images/'.$operator->slug)}}" data-toggle="tooltip" title="Administrar multimedia" class="btn btn-effect-ripple btn-xs btn-info"><i class="fa fa-images"></i></a>
                                        @if($operator->state=="activo")
                                            <a href="{{url('admin/operators/desactivate/'.$operator->id_site)}}" data-toggle="tooltip" title="Desactivar operador" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                        @else
                                            <a href="{{url('admin/operators/activate/'.$operator->id_site)}}" data-toggle="tooltip" title="Activar operador" class="btn btn-effect-ripple btn-xs btn-success"><i class="fas fa-check"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END Datatables Block -->
            {{-- </div> --}}
        {{-- </div> --}}
    </div>
@endsection

@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
@endsection