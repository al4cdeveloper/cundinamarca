<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Route;
use App\Models\Department;
use App\Models\Municipality;
use App\Models\RelationRoute;
use App\Models\InterestSite;
use Session;
use File;


class RouteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $routes = Route::all();
        return view('admin.Route.listRoute',compact('routes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $municipalities = Municipality::where('state','activo')->get();

        return view('admin.Route.newEditRoute',compact('municipalities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
             'principal'=>'required',
             'name'=>'required',
             'keywords'  => 'required',
             'description'  => 'required',
             'map'  => 'required'
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $route = new Route;
            if($request->typePrincipal == "department")
            {
                $route->type_relation = "department";
            }
            else
            {
                $route->type_relation = "municipality";
            }
            $route->name = $request->name;
            $route->fk_relation = $request->principal;
            $route->description = $request->description;
            $route->keywords = $request->keywords;

            $files = $request->file('map');
            $map = $this->cargar_imagen($files);

            $route->link_map = "images/routes/".$map;
            $route->save();

            if(isset($request->relation_departaments))
            {
                foreach ($request->relation_departaments as $department) 
                {
                    $relation = new RelationRoute;
                    $relation->type_relation = "department";
                    $relation->fk_relation = $department;
                    $relation->fk_route = $route->id_route;
                    $relation->save();
                }
            }
            if(isset($request->relation_municipalities))
            {
                foreach ($request->relation_municipalities as $municipality) 
                {
                    $relation = new RelationRoute;
                    $relation->type_relation = "municipality";
                    $relation->fk_relation = $municipality;
                    $relation->fk_route = $route->id_route;
                    $relation->save();
                }
            }
            if(isset($request->relation_sites))
            {
                foreach ($request->relation_sites as $site) 
                {
                    $relation = new RelationRoute;
                    $relation->type_relation = "site";
                    $relation->fk_relation = $site;
                    $relation->fk_route = $route->id_route;
                    $relation->save();
                }
            }

            Session::flash('message', 'Se ha realizado correctamente el registro de información.');
            return redirect('admin/routes');
        }
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $route = Route::where('slug',$slug)->first();
        if($route->type_relation=='municipality')
        {
            $municipalitiesToRelation = Municipality::where('state','activo')->where('id_municipality','!=',$route->fk_relation)->get();
        }
        else
        {
            $municipalitiesToRelation = Municipality::where('state','activo')->get();
        }


        $municipalities = Municipality::where('state','activo')->get();

        $sites = InterestSite::where('state','activo')->get();
        return view('admin.Route.newEditRoute',compact('route','municipalities','sites','municipalitiesToRelation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
             'principal'=>'required',
             'name'=>'required',
             'keywords'  => 'required',
             'description'  => 'required',
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $route = Route::find($id);
            if($request->typePrincipal == "department")
            {
                $route->type_relation = "department";
            }
            else
            {
                $route->type_relation = "municipality";
            }
            $route->name = $request->name;
            $route->fk_relation = $request->principal;
            $route->description = $request->description;
            $route->keywords = $request->keywords;

            if($request->map)
            {
                if($route->link_map)
                {
                    $exists = File::exists(public_path($route->link_map));
                    if ($exists) 
                    {
                        File::delete(public_path($route->link_map));
                    }
                }
                $files = $request->file('map');
                $map = $this->cargar_imagen($files);
                $route->link_map = "images/routes/".$map;
            }

            $route->save();

            foreach($route->Relations as $relation)
            {
                RelationRoute::destroy($relation->id_relation_route);
            }

            if(isset($request->relation_departaments))
            {
                foreach ($request->relation_departaments as $department) 
                {
                    $relation = new RelationRoute;
                    $relation->type_relation = "department";
                    $relation->fk_relation = $department;
                    $relation->fk_route = $route->id_route;
                    $relation->save();
                }
            }
            if(isset($request->relation_municipalities))
            {
                foreach ($request->relation_municipalities as $municipality) 
                {
                    $relation = new RelationRoute;
                    $relation->type_relation = "municipality";
                    $relation->fk_relation = $municipality;
                    $relation->fk_route = $route->id_route;
                    $relation->save();
                }
            }
            if(isset($request->relation_sites))
            {
                foreach ($request->relation_sites as $site) 
                {
                    $relation = new RelationRoute;
                    $relation->type_relation = "site";
                    $relation->fk_relation = $site;
                    $relation->fk_route = $route->id_route;
                    $relation->save();
                }
            }

            Session::flash('message', 'Se ha realizado correctamente el registro de información.');
            return redirect('admin/routes');
        }
    }

    private function cargar_imagen($file, $imageName = false)
    {
        if ($imageName) 
        {
            $exists = File::exists(public_path("images/routes/".$imageName));
            if ($exists) 
            {
                File::delete(public_path("images/routes/".$imageName));
            }

            $image = explode('.', $imageName);
            $imageName = $image[0].'.'.$file->getClientOriginalExtension();
        } 
        else 
        {
            $imageName = 'route'.date('YmdHis', time()).rand().'.'.$file->getClientOriginalExtension();
        }

        $file->move(public_path('images/routes'), $imageName);

        $exists = File::exists(public_path("images/routes/".$imageName));

        if ($exists) 
        {
            return $imageName;
        } 
        else 
        {
            return false;
        }
    }

    public function desactivate($id)
    {
        $route = Route::find($id);
        if($route)
        {
            $route->state = "inactivo";
            $route->save();
            Session::flash('message','Se ha desactivado la ruta correctamente');
            return redirect('admin/routes');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado la ruta.");
            return redirect('admin/routes');
        }
    }

    public function activate($id)
    {
        $route = Route::find($id);
        if($route)
        {
            $route->state = "activo";
            $route->save();
            Session::flash('message','Se ha activado la ruta correctamente');
            return redirect('admin/routes');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado la ruta.");
            return redirect('admin/routes');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
