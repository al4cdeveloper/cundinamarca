<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
	protected $primaryKey = "id_service";
	protected $fillable = ['id_service','service','type_service','fk_service_category','fk_ecosystem_category'];

	public function users()
    {
        return $this->belongsToMany('App\Operator', 'service_operators', 'fk_service','fk_operator');
    }

    public function ecosystem()
    {
    	return $this->hasOne('App\Models\EcosystemCategory','id_ecosystem_category','fk_ecosystem_category');
    }

    public function Operators()
    {
        return $this->belongsToMany('App\Models\InterestSite','lyingoperator_services','fk_service','fk_operator')->where('fk_category',2);
    }
}
