<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Fair extends Model
{
	use Sluggable;

	protected $primaryKey = 'id_fair';
	protected $fillable = ['fair_name','date','enddate','keywords','description','multimedia_type','slug','state','type_relation','fk_relation'];

	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'fair_name'
            ]
        ];
    }

    public function PrincipalDepartment()
    {
        return $this->hasOne('App\Models\Department','id_department','fk_relation');
    }

    public function PrincipalMunicipality()
    {
        return $this->hasOne('App\Models\Municipality','id_municipality','fk_relation');
    }

    public function images()
    {
        return $this->hasMany('App\Models\FairImage', 'fk_fair');
    }
    public function PDF()
    {
        return $this->hasMany('App\Models\PortableDocument','fk_relation','id_fair')->where('type_relation','fair');
    }

    public function Video()
    {
        return $this->hasMany('App\Models\Video','fk_relation','id_fair')->where('type_relation','fair');
    }
    public function Notices()
    {
        return $this->hasMany('App\Models\Notice','fk_fair','id_fair');
    }
}
