<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Wallpaper;
use File;
use Session;
class WallpaperController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wallpapers = Wallpaper::all();
        return view('admin.Wallpaper.listWallpaper',compact('wallpapers'));
    }

    public function create()
    {
        return view('admin.Wallpaper.newEditWallpaper');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
             // 'primary_color'=>'required',
             'url'=>'required',
             'link_image'  => 'required',
             'primary_text'=>'required',
             // 'secundary_text'=>'required',
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $wall = new Wallpaper;
            $wall->url = $request->url;
            $wall->primary_text = $request->primary_text;
            $wall->secundary_text = $request->secundary_text;

            $files = $request->file('link_image');
            $image = $this->cargar_imagen($files);

            $wall->link_image = "wallpapers/".$image;
            $wall->save();
            Session::flash('message', 'Se ha realizado correctamente el registro de información.');
            return redirect('admin/wallpapers');
        }
    }

    private function cargar_imagen($file, $imageName = false)
    {
        if ($imageName) 
        {
            $exists = File::exists(public_path("wallpapers/".$imageName));
            if ($exists) 
            {
                File::delete(public_path("wallpapers/".$imageName));
            }

            $image = explode('.', $imageName);
            $imageName = $image[0].'.'.$file->getClientOriginalExtension();
        } 
        else 
        {
            $imageName = 'wall_'.date('YmdHis', time()).rand().'.'.$file->getClientOriginalExtension();
        }

        $file->move(public_path('wallpapers'), $imageName);

        $exists = File::exists(public_path("wallpapers/".$imageName));

        if ($exists) 
        {
            return $imageName;
        } 
        else 
        {
            return false;
        }
    }

    public function edit($id)
    {
        $wall = Wallpaper::find($id);
        if($wall)
        {
            return view('admin.Wallpaper.newEditWallpaper',compact('wall'));
        }
        else
            return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'url'=>'required',
             // 'link_image'  => 'required',
             'primary_text'=>'required',
             // 'secundary_text'=>'required',
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $wall = Wallpaper::find($id);
            // $wall->primary_color = $request->primary_color;
            // $wall->secundary_color = $request->secundary_color;
            $wall->url = $request->url;
            $wall->primary_text = $request->primary_text;
            $wall->secundary_text = $request->secundary_text;

            if($request->link_image)
            {
                if($wall->link_image)
                {
                    $exists = File::exists(public_path($wall->link_image));
                    if ($exists) 
                    {
                        File::delete(public_path($wall->link_image));
                    }
                }
                $files = $request->file('link_image');
                $image = $this->cargar_imagen($files);
                $wall->link_image = "wallpapers/".$image;
            }

            $wall->save();
            Session::flash('message', 'Se ha realizado correctamente la actualización de información.');
            return redirect('admin/wallpapers');
        }
    }
    public function desactivate($id)
    {
        $wall = Wallpaper::find($id);
        if($wall)
        {
            $wall->state = "inactivo";
            $wall->save();
            Session::flash('message','Se ha desactivado la ruta correctamente');
            return redirect('admin/wallpapers');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado la ruta.");
            return redirect('admin/wallpapers');
        }
    }

    public function activate($id)
    {
        $wall = Wallpaper::find($id);
        if($wall)
        {
            $wall->state = "activo";
            $wall->save();
            Session::flash('message','Se ha activado la ruta correctamente');
            return redirect('admin/wallpapers');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado la ruta.");
            return redirect('admin/wallpapers');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
