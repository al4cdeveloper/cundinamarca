<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
	protected $primaryKey = 'id_video';
	protected $fillable = ['link_video','type_relation','fk_relation'];
}
