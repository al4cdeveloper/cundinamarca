<?php

namespace App;

use App\Notifications\OperatorResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Operator extends Authenticatable
{
    use Notifiable;
    protected $primaryKey='id_operator';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'national_register','contact_personal','contact_phone','email','service_category','web','role' ,'password','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new OperatorResetPassword($token));
    }

    public function services()
    {
        return $this->belongsToMany('App\Models\Service', 'service_operators','fk_operator', 'fk_service')->withPivot('id_service_operator','address','cost','capacity','duration','requisites','description','location','state');
    }

    public function directService()
    {
        return $this->hasMany('App\Models\ServiceOperator','fk_operator');
    }
}
