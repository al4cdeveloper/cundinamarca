@extends('operator.layout.auth')

@section('title', 'Reservaciones')

{{-- @section('aditionalStyle')
    <link rel="stylesheet" type="text/css" href="{{asset('vue/css/app.css')}}">
@endsection --}}
@section('aditionalStyle')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
@endsection

@section('content')
    <div id="page-content">

     <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <form>
                    <div class="col-sm-12">
                        <div class="header-section">
                            <h1>Reservaciones</h1>
                        </div>
                        
                    </div>
                </form>
            </div>
        </div>
        <!-- END Page Header -->

        <!-- Example Block -->
        <div class="block" id="service">
            <table id="datatable" class="table" role="grid" aria-describedby="datatable_info">
                <thead>
                    <th>ID</th>
                    <th>Servicio</th>
                    <th>Lugar</th>
                    <th>Fecha</th>
                    <th>Hora</th>
                    <th>Estado</th>
                    
                </thead>
                <tbody>
                    @foreach($reservations[0] as $reservation)
                    <tr >
                        <td>{{$reservation->id_reservation}}</td>
                        <td>{{$reservation->Service->PrincipalService->service}}</td>
                        <td>{{$reservation->Service->location}}</td>
                        <td>{{$reservation->date}}</td>
                        <td>{{$reservation->time}}</td>
                        <td>{{$reservation->state}}</td>
                        @if($reservation->state=="Por realizar")
                        <td width="10px">
                            <a href="{{url('operator/servicesuccess/'.$reservation->id_reservation)}}" class="btn btn-success btn-sm" >Realizado!</a> 
                        </td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>



@endsection