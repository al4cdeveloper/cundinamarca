<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Municipality;
use App\Models\InterestSite;
use App\Models\Fair;
use App\Models\Route;

class ApiController extends Controller
{
	public function search(Request $request)
	{
		$result = array();

		$municipalities = Municipality::where('municipality_name','like','%'.$request->busqueda."%")->get();
		if(count($municipalities)>0)
		{
			foreach($municipalities as $municipality)
			{
				array_push($result,['redirect'=>'/municipality/'.$municipality->slug,
								'name'=>$municipality->municipality_name]);
			}
		}
		$sites = InterestSite::where('fk_category','!=',2)->where('fk_category','!=',7)->where('site_name','like','%'.$request->busqueda."%")->get();
		if(count($sites)>0)
		{
			foreach($sites as $site)
			{
				array_push($result,['redirect'=>'/site/'.$site->slug,
								'name'=>$site->site_name]);
			}
		}
		$fairs = Fair::where('fair_name','like','%'.$request->busqueda."%")->get();
		if(count($fairs)>0)
		{
			foreach($fairs as $fair)
			{
				$monthText = date("M",strtotime($fair->date));
				array_push($result,['redirect'=>'/fairs?filter='.$monthText,
								'name'=>$fair->fair_name]);
			}
		}
		$operators = InterestSite::where('fk_category',2)->where('site_name','like','%'.$request->busqueda."%")->get();
		if(count($operators)>0)
		{
			foreach($operators as $site)
			{
				array_push($result,['redirect'=>'/operator/'.$site->slug,
								'name'=>$site->site_name]);
			}
		}
		$activities = InterestSite::where('fk_category',7)->where('site_name','like','%'.$request->busqueda."%")->get();
		if(count($activities)>0)
		{
			foreach($activities as $site)
			{
				array_push($result,['redirect'=>'/activity/'.$site->slug,
								'name'=>$site->site_name]);
			}
		}
		$routes = Route::where('name','like','%'.$request->busqueda."%")->get();
		if(count($routes)>0)
		{
			foreach($routes as $route)
			{
				array_push($result,['redirect'=>'/routes/'.$route->slug,
								'name'=>$route->name]);
			}
		}
		return response()->json($result);
	}
}
