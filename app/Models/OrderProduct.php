<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
	protected $primaryKey = "id_order_product";
	protected $fillable = ['fk_order','fk_service','units','unit_price'];
}
