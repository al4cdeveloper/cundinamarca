<form method="POST" v-on:submit.prevent="updateService(fillService.id_service)">
	<div class="modal fade" id="edit">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span>&times;</span>
					</button>
					<h4>Editar </h4>
				</div>
				<div class="modal-body form-horizontal">
					<div class="form-group ">
                        <label for="service" class="col-md-4 control-label">Servicio</label>
                        <div class="col-md-6">
                            <input id="service" type="service" class="form-control" name="service" v-model="fillService.service" readonly>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="address" class="col-md-4 control-label">Direccion</label>
                        <div class="col-md-6">
                            <input id="address" type="text" class="form-control" name="address" v-model="fillService.address" >
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="since" class="col-md-4 control-label">Desde</label>
                        <div class="col-md-6">
                            <input id="since" type="text" class="form-control" name="since" v-model="fillService.since" >
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="to" class="col-md-4 control-label">Hasta</label>
                        <div class="col-md-6">
                            <input id="to" type="text" class="form-control" name="to" v-model="fillService.to" >
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="days" class="col-md-4 control-label">Días de funcionamiento</label>
                        <div class="col-md-6">
                            <input id="days" type="text" class="form-control" name="days" v-model="fillService.days" >
                          {{-- https://sagalbot.github.io/vue-select/ --}}
                          {{-- https://medium.com/@hemnys25/creaci%C3%B3n-de-un-select-dependiente-con-laravel-5-4-vue-js-select2-957ac043bbdd --}}
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="capacity" class="col-md-4 control-label">Capacidad de personas</label>
                        <div class="col-md-6">
                            <input id="capacity" type="text" class="form-control" name="capacity" v-model="fillService.capacity" >
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="location" class="col-md-4 control-label">Lugar/Ciudad</label>
                        <div class="col-md-6">
                            <input id="location" type="text" class="form-control" name="location" v-model="fillService.location" >
                        </div>
                    </div>
                  
                    <div class="form-group ">
                        <label for="requisites" class="col-md-4 control-label">Requisitos</label>
                        <div class="col-md-6">
                            <textarea class="form-control" name="requisites" id="requisites" v-model="fillService.requisites"></textarea>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="description" class="col-md-4 control-label">Descripcion</label>
                        <div class="col-md-6">
                            <textarea class="form-control" name="description" id="description" v-model="fillService.description"></textarea>
                        </div>
                    </div>
                    <div class="form-group " v-if="fillService.state != 'Inhabilitado'">
                        <label for="state" class="col-md-4 control-label">Estado</label>
                        <div class="col-md-6">
                            <textarea class="form-control" name="state" id="state" v-model="fillService.state"></textarea>
                        </div>
                    </div>

					<span v-for="error in errors" class="text-danger">@{{ error}}</span>
				</div>
				<div class="modal-footer">
					<input type="submit" value="Actualizar" class="btn btn-primary">
				</div>
			</div>
		</div>
	</div>
</form>