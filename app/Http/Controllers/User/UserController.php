<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Wallpaper;
use App\Models\Municipality;
use App\Models\Service;
use App\Models\Route;
use App\Models\InterestSite;
use App\Models\Contact;
use App\Models\MunicipalityImage;
use App\Models\Fair;
use Session;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wallpapers = Wallpaper::all()->where('state','activo');
        
      //   $servicesO = ServiceOperator::where('state','Activo')->get();

        // return view('welcome',compact('services','servicesO'));

      return view('welcome',compact('wallpapers'));

    }

    public function index2()
    {
        $routes = Route::all()->where('state','activo');
        $numMunicipalities = Municipality::where('state','activo')->count();
        $municipalities = Municipality::where('state','activo')->inRandomOrder()->take(3)->get();

        $numOperators = InterestSite::where('state','activo')->where('fk_category',2)->count();
        $operators = InterestSite::where('state','activo')->where('fk_category',2)->inRandomOrder()->take(3)->get();

        $numSites = InterestSite::all()->where('state','activo')->where('fk_category','!=',2)->count();
        $sites = InterestSite::where('state','activo')->where('fk_category','!=',2)->where('fk_category','!=',7)->inRandomOrder()->take(3)->get();

        // $services = Service::all();
      //   $servicesO = ServiceOperator::where('state','Activo')->get();

        // return view('welcome',compact('services','servicesO'));

      return view('front.index2',compact('wallpapers','routes','municipalities','numMunicipalities','operators','numOperators','sites','numSites'));

    }

    public function gallery()
    {
        $images = MunicipalityImage::inRandomOrder()->limit(40)->get();

        $sizes = ['0'=>['width'=>'250',
                        'height'=>'150'],
                    '1'=>['width'=>'500',
                        'height'=>'300'],
                    '2'=>['width'=>'800',
                        'height'=>'400'],
                    ];
        $interestSite = InterestSite::where('state','activo')->where('fk_category','!=',2)->inRandomOrder()->limit(20)->get();
        $allImages = [];
        foreach($images as $image)
        {
            $size = array_random($sizes);
            array_add($image,'type','municipality');
            array_add($image,'size',$size);
            array_push($allImages,$image);
        }
        foreach($interestSite as $site)
        {
            if(count($site->images)>0)
            {
                foreach($site->images as $image)
                {
                    $size = array_random($sizes);
                    array_add($image,'size',$size);
                    array_add($image,'type','site');
                    array_push($allImages,$image);
                }
            }
        }
        shuffle($allImages);
        return view('front.gallery',compact('allImages','municipalities','interestSite'));
    }

    public function route($slug)
    {
        $route = Route::where('slug',$slug)->first();

        return view('front.Route',compact('route'));
    }

    public function allMunicipality(Request $request)
    {
        if($request->filter)
        {
            $municipalities = Municipality::where('state','activo')->where('weather',$request->filter)->get();
            if(count($municipalities)>0)
            {
                $image = Wallpaper::where('state','activo')->inRandomOrder()->take(1)->first();
                $weather = ['Tropical'=>'Tropical','Seco'=>'Seco','Templado'=>'Templado','Frío'=>'Frío'];
                return view('front.municipalityList',compact('municipalities','image','weather'));
            }
            else
            {
                Session::flash('message-error','No se ha encontrado ningún municipio con el clima seleccionado.');
                return redirect('/municipalities');
            }
        }
        else
        {
            $municipalities = Municipality::where('state','activo')->orderBy('id_municipality','desc')->get();
            $image = Wallpaper::where('state','activo')->inRandomOrder()->take(1)->first();
            $weather = ['Tropical'=>'Tropical','Seco'=>'Seco','Templado'=>'Templado','Frío'=>'Frío'];
            return view('front.municipalityList',compact('municipalities','image','weather'));
        }
    }

    public function allOperator(Request $request)
    {
        $operators = InterestSite::all()->where('state','activo')->where('fk_category',2);
        $services = [];
        $numOperators = count($operators);
        foreach($operators as $operator)
        {
            foreach ($operator->Services as $service) 
            {
                if(!in_array($service->service,$services))
                {
                    array_push($services,$service->service);
                }
            }
        }
        if(!$request->filter)
        {
            return view('front.operatorList',compact('operators','services','numOperators'));
        }
        else
        {
            $service = Service::where('service',$request->filter)->first();
            $operators = $service->Operators;
            $filter = $request->filter;
            return view('front.operatorList',compact('operators','services','numOperators','filter'));
        }

    }
    public function allSites(Request $request)
    {
        $sites = InterestSite::all()->where('state','activo')->where('fk_category','!=',2)->where('fk_category','!=',7);
        $municipalities = [];
        $numSites = count($sites);
        foreach($sites as $site)
        {
            if(!in_array($site->Municipality->municipality_name,$municipalities))
            {
                array_push($municipalities,$site->Municipality->municipality_name);
            }
        }
        if(!$request->filter)
        {
            return view('front.siteList',compact('sites','municipalities','numSites'));
        }
        else
        {
            $municipality = Municipality::where('municipality_name',$request->filter)->first();
            $sites = InterestSite::where('state','activo')->where('fk_category','!=',2)->where('fk_municipality',$municipality->id_municipality)->get();
            $filter = $request->filter;
            return view('front.siteList',compact('sites','municipalities','numSites','filter'));
        }
    }

    public function allFairs(Request $request)
    {
        $expo = Fair::where('slug','expo-cundinamarca')->first();
        $fairs = Fair::all()->where('state','activo')->where('id_fair','!=',$expo->id_fair);
        $counts = ['Feb'=>0,
                    'Jan'=>0,
                    'Mar'=>0,
                    'Apr'=>0,
                    'May'=>0,
                    'Jun'=>0,
                    'Jul'=>0,
                    'Aug'=>0,
                    'Sep'=>0,
                    'Oct'=>0,
                    'Nov'=>0,
                    'Dec'=>0,
                ];
        foreach ($fairs as $fair) 
        {
            $monthText = date("M",strtotime($fair->date));
            $counts["$monthText"]=$counts["$monthText"]+1 ;
        }
        if(!$request->filter)
        {
            return view('front.fairList',compact('fairs','expo','counts'));
        }


        else
        {
            $filterFairs=array();
            foreach ($fairs as $fair) 
            {
                if(date("M",strtotime($fair->date))==$request->filter)
                {
                    $filterFairs[]=$fair;
                }
            }
            $fairs = $filterFairs;
            if(count($fairs)<=0)
            {
                return redirect('/fairs');
            }

            return view('front.fairList',compact('fairs','expo','counts'));
        }
    }

    public function Expo()
    {
        $expo = Fair::where('slug','expo-cundinamarca')->first();

        return view('front.expo',compact('expo'));
    }

    public function detailMunicipality($slug)
    {
        $municipality = Municipality::where('slug',$slug)->first();
        if($municipality)
        {
            return view('front.detailMunicipality',compact('municipality'));
        }
        else
        {
            return redirect()->back();
        }
    }

    public function detailOperator($slug)
    {
        $operator = InterestSite::where('slug',$slug)->first();
        if($operator)
        {
            return view('front.detailOperator',compact('operator'));
        }
        else
        {
            return redirect()->back();
        }
    }

    public function detailSite($slug)
    {
        $site = InterestSite::where('slug',$slug)->first();

        if($site)
        {
            $municipality = $site->Municipality;
            $sites = InterestSite::where('fk_municipality',$municipality->id_municipality)->where('fk_category','!=',2)->where('id_site','!=',$site->id_site)->get(); 
            return view('front.detailSite',compact('site','sites'));
        }
        else
        {
            return redirect()->back();
        }
    }

    public function saveEmail(Request $request)
    {
        $contact = new Contact;
        $contact->name = $request->name;
        $contact->phone = $request->phone;
        $contact->email = $request->email;
        $contact->message = $request->message;
        $contact->state = "new";
        $contact->save();

        Session::flash('message','Hemos registrado tu información correctamente, gracias por ponerte en contacto con nosotros');
        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function activity($slug)
    {
        $activity = InterestSite::where('slug',$slug)->first();

        if($activity)
        {
            return view('front.activity',compact('activity'));
        }
        else
        {
            return redirect()->back();
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
