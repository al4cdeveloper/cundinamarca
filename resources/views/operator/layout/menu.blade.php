<div id="sidebar-scroll" style="margin-top: 15%">
    <!-- Sidebar Content -->
    <div class="sidebar-content">
        <!-- Sidebar Navigation -->
        <ul class="sidebar-nav">
            <li>
                <a href="{{url('operator/home')}}"><i class="gi gi-compass sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard</span></a>
            </li>
            {{-- <li class="sidebar-separator">
                <i class="fa fa-ellipsis-h"></i>
            </li> --}}
            <li>
                <a href="{{url('operator/services')}}"><i class="fa fa-subway sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Mis servicios</span></a>
            </li>
            <li>
                <a href="#" class="sidebar-nav-menu">
                    {{-- Flecha que indica select --}}
                    <i class="fa fa-chevron-left sidebar-nav-indicator sidebar-nav-mini-hide"></i> 
                    {{-- Icono de la izquierda --}}
                    <i class="fa fa-ravelry" aria-hidden="true"></i>
                    <span class="sidebar-nav-mini-hide">Reservas</span></a>
                <ul>
                    <li>
                        <a href="{{url('operator/reservas')}}">Lista de reservas</a>
                    </li>
                    <li>
                        <a href="{{url('operator/calendar')}}">Calendario</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- END Sidebar Navigation -->
    </div>
    <!-- END Sidebar Content -->
</div>