@extends('admin.layout.auth')

@if(isset($fair))
    @section('title', 'Editar evento')
@else
    @section('title', 'Crear evento ')
@endif

@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Evento</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
			
            @if(isset($fair))
            {!!Form::model($fair,['url'=>['admin/fairs/update',$fair->id_fair],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
            @else
            {!!Form::open(['url'=>'admin/fairs/store', 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
            @endif
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Nombre</label>
                    {!!Form::text('fair_name', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre del evento', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('fair_name') }}</span>
                </div>
            </div>
            <input type="hidden" name="typePrincipal" id="typePrincipal" @if(isset($fair)) value="{{$fair->type_relation}}" @endif>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Evento del municipio: </label>
                    <select class="select-chosen" id="firstSelect" name="principal">
                        <option></option>
                        <optgroup label="Municipio" id="municipality">
                            @foreach($municipalities as $municipality)
                                <option value="{{$municipality->id_municipality}}" @if(isset($fair)) @if($fair->type_relation=="municipality") @if($fair->fk_relation==$municipality->id_municipality) selected @endif @endif @endif>{{$municipality->municipality_name}}</option>
                            @endforeach
                        </optgroup>
                    </select>
                    <span class="label label-danger">{{$errors->first('name') }}</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-5 col-xs-12">
                    <div class="form-group">
                    <label >Fecha </label>
                        {!!Form::text('date',null,['class'=>'form-control input-datepicker','data-date-format' =>"yyyy-mm-dd",'placeholder' =>"yyyy-mm-dd",'readonly'])!!}
                        <span class="label label-danger">{{$errors->first('date') }}</span>
                    </div>
                </div>
                <div class="col-sm-5 col-md-offset-1 col-xs-12">
                    <div class="form-group">
                    <label >Fecha final </label>
                        {!!Form::text('enddate',null,['class'=>'form-control input-datepicker','data-date-format' =>"yyyy-mm-dd",'placeholder' =>"yyyy-mm-dd",'readonly'])!!}
                        <span class="label label-danger">{{$errors->first('enddate') }}</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                <label >Palabras clave <small>Separadas por comas (,)</small></label>
                    {!!Form::text('keywords',null,['class'=>'form-control input-tags', 'placeholder'=>'Palabras clave'])!!}
                    <span class="label label-danger">{{$errors->first('keywords') }}</span>
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <label for="duration">Descripción</label>
                <div class="form-group">
                    <textarea id="textarea-ckeditor" name="description" class="ckeditor">@if(isset($fair)){!!$fair->description!!}@endif</textarea>
					<span class="label label-danger">{{$errors->first('description') }}</span>
                </div>
	        </div>
            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                <a href="{{url('admin/fairs')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection

@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>

     <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/plugins/sortable.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/fileinput.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/fr.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/es.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/explorer-fa/theme.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/fa/theme.js')}}" type="text/javascript"></script>

    <script>

        $("#firstSelect").change(function()
        {
            var selected = $("option:selected", this);
            if(selected.parent()[0].id == "department")
            {
                $("#typePrincipal").val('department');

            } 
            else if(selected.parent()[0].id == "municipality")
            {
                $("#typePrincipal").val('municipality');
            }
        });

    </script>
@endsection