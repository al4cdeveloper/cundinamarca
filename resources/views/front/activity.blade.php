@extends('layout.header')

    @section('title',$activity->site_name)
    @section('keywords',$activity->keywords)
    @section('additionalStyle')

	        <!-- CSS -->
    <link href="{{asset('front/css/slider-pro.min.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/date_time_picker.css')}}" rel="stylesheet">


 @endsection
@section('additionalPreMain')
    <section class="parallax-window" data-parallax="scroll" data-image-src="{{asset($activity->link_image)}}" style="width: 100%;height: auto; z-index: 1">
        <div class="parallax-content-2">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-8">
                        <h1>{{$activity->site_name}}</h1>
                        <span >Ubicado en el municipio <a href="{{url('municipality/'.$activity->Municipality->slug)}}">{{$activity->Municipality->municipality_name}}</a></span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End section -->
 @endsection

    @section('main')

	<main>
		<div id="position">
			<div class="container">
				<ul>
					<li><a href="{{url('/')}}">Home</a>
					</li>
					<li><a href="{{url('/municipalities')}}">Municipios</a>
					</li>
                    <li>{{$activity->site_name}}</li>
				</ul>
			</div>
		</div>
		<!-- End Position -->

		<!-- End Map -->

		<div class="container margin_60">
			<div class="row">
				<div class="col-md-8" id="single_tour_desc">
					<!-- Map button for tablets/mobiles -->
                    @if(count($activity->images)>0 )
					<div id="Img_carousel" class="slider-pro">
						<div class="sp-slides">

							@foreach($activity->images as $image)
                            <div class="sp-slide">
                                <img alt="Image" class="sp-image" src="css/images/blank.gif" data-src="{{asset($image->link_image)}}" data-small="{{asset($image->link_image)}}" data-medium="{{asset($image->link_image)}}" data-large="{{asset($image->link_image)}}" data-retina="{{asset($image->link_image)}}">
                            </div>
                            @endforeach
						</div>
						<div class="sp-thumbnails">
                            @foreach($activity->images as $image)
                            <img alt="Image" class="sp-thumbnail" src="{{asset($image->link_image)}}">
                            @endforeach
                        </div>
					</div>
                        
					<hr>
                    @endif
					<div class="row">
						<div class="col-md-3">
							<h3>Descripción</h3>
						</div>
						<div class="col-md-9">
							{!!$activity->description!!}
						</div>
					</div>
				</div>

                <aside class="col-md-4">
                    <p class="hidden-sm hidden-xs">
                        <a class="btn_map" data-toggle="collapse" href="#collapseMap" aria-expanded="false" aria-controls="collapseMap" data-text-swap="Ver mapa" data-text-original="Esconder mapa">Esconder mapa</a>
                        <div class="collapse in" id="collapseMap">
                            <div id="map" class="map"></div>
                       </div>
                    </p>
                    @if(count($activity->Video)>0)
                    <hr>
                        <div class="row">
                            <div class="main_title">
                                <h2><span>Video</span></h2><br />
                                @foreach($activity->Video as $video)
                                <div class="col-sm-12">
                                    <iframe width="100%" height="200" src="https://www.youtube.com/embed/{{$video->link_video}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    @endif

                </aside>
				<!--End  single_tour_desc-->
			</div>
            @if(count($activity->Operators)>0)
            <hr>
            <div class="row" style="padding: 20px;">
                <div class="main_title">
                    <h2><span>Operadores</span> relacionados</h2><br />
        
                    <div class="row">
                        @foreach($activity->Operators as $operator)
                        <div class="col-md-4 col-sm-4 wow zoomIn targetListOperatorLg" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: zoomIn;">
                            <div class="tour_container">
                                <div class="img_container">
                                    <a href="{{url('operator/'.$operator->slug)}}">
                                        <img src="@if($operator->link_icon){{asset($operator->link_icon)}}@else{{asset('img/defaultimg.jpg')}}@endif" width="800" height="533" class="img-responsive" alt="Image">
                                    </a>
                                </div>
                                <div class="tour_title">
                                    <h3><strong> {{$operator->site_name}}</strong></h3>
                                    <div class="rating">
                                        @foreach($operator->Services as $service)
                                            - {{$service->service}}
                                        @endforeach
                                    </div>
                                    <!-- end rating -->
                                </div>
                            </div>
                            <!-- End box tour -->
                        </div>

                        <!-- End col-md-6 -->
                        @endforeach
                        <!-- End col-md-6 -->
                    </div>
                </div>
            </div>
            @endif
            @if(count($activity->Municipality->InterestSiteW)>0)
            <hr>
            <div class="row" style="padding: 20px;">
                <div class="main_title">
                    <h2><span>Vea también</span></h2><br />
        
                    <div class="row">
                        @foreach($activity->Municipality->InterestSiteW as $site)
                        <div class="col-md-4 col-sm-4 wow zoomIn" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: zoomIn;">
                            <div class="tour_container">
                                <div class="ribbon_3 popular"><span>{{$site->Category->category_name}}</span>
                                </div>
                                <div class="img_container">
                                    <a href="{{url('site/'.$site->slug)}}">
                                        <img src="@if($site->link_icon){{asset($site->link_icon)}}@else{{asset('img/defaultimg.jpg')}}@endif" width="800" height="533" class="img-responsive" alt="Image">
                                    </a>
                                </div>
                                <div class="tour_title">
                                    <h3><strong>{{$site->site_name}}</strong></h3>

                                        <p style="font-size: 14px!important;">{!!substr($site->description,0,120) !!}... <a href="{{url('site/'.$site->slug)}}">Ver más</a> {!!'</p>'!!}</p>

                                    <!-- end rating -->
                                </div>
                            </div>
                            <!-- End box tour -->
                        </div>

                        <!-- End col-md-6 -->
                        @endforeach
                        <!-- End col-md-6 -->
                    </div>
                </div>
            </div>
            @endif
			<!--End row -->
		</div>
		<!--End container -->
        
        <div id="overlay"></div>
		<!-- Mask on input focus -->
    
    @endsection
	<!-- End main -->

    @section('additionalScript')
    <!-- Specific scripts -->
    <script src="{{asset('front/js/icheck.js')}}"></script>
    <script>
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-grey',
            radioClass: 'iradio_square-grey'
        });
    </script>
	<!-- Date and time pickers -->
    <script src="{{asset('front/js/jquery.sliderPro.min.js')}}"></script>
	<script type="text/javascript">
		$(document).ready(function ($) {
			$('#Img_carousel').sliderPro({
				width: 960,
				height: 500,
				fade: true,
				arrows: true,
				buttons: false,
				fullScreen: false,
				smallSize: 500,
				startSlide: 0,
				mediumSize: 1000,
				largeSize: 3000,
				thumbnailArrows: true,
				autoplay: false
			});
		});
	
    </script>
	<!-- Map -->
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBXtArR0GzW4Y_EezVpjGdvjrlwsQBcayw"></script>

    <script type="text/javascript">
        $(document).ready(function(e){
    (function(A) {

    if (!Array.prototype.forEach)
        A.forEach = A.forEach || function(action, that) {
            for (var i = 0, l = this.length; i < l; i++)
                if (i in this)
                    action.call(that, this[i], i, this);
            };

        })(Array.prototype);

        var
        mapObject,
        markers = [],
        markersData = {
            'Hotels': [
            {
                name: '{{$activity->site_name}}',
                @if($activity->latitude!=null)
                location_latitude: {{$activity->latitude}}, 
                @else
                location_latitude: {{$activity->Municipality->latitude}}, 
                @endif
                @if($activity->longitude!=null)
                location_longitude: {{$activity->longitude}}, 
                @else
                location_longitude: {{$activity->Municipality->longitude}},
                @endif
                map_image_url: '{{asset($activity->link_image)}}',
                name_point: '{{$activity->site_name}}',
                description_point: '{!!substr($activity->description,0,120) !!}...{!!'</p>'!!}',
                get_directions_start_address: '',
                phone: '{{$activity->phone}}',
                // url_point: '{{$activity->municipality_name}}'
            },
            ]
        };

            var mapOptions = {
                zoom: 8,
                center: new google.maps.LatLng(4.7925319, -74.3999466),
                mapTypeId: google.maps.MapTypeId.ROADMAP,

                mapTypeControl: false,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                panControl: false,
                panControlOptions: {
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.TOP_LEFT
                },
                scrollwheel: false,
                scaleControl: false,
                scaleControlOptions: {
                    position: google.maps.ControlPosition.TOP_LEFT
                },
                streetViewControl: true,
                streetViewControlOptions: {
                    position: google.maps.ControlPosition.LEFT_TOP
                },
                styles: [
                                             {
                    "featureType": "landscape",
                    "stylers": [
                        {
                            "hue": "#FFBF00"
                        },
                        {
                            "saturation": 43.400000000000006
                        },
                        {
                            "lightness": 37.599999999999994
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "stylers": [
                        {
                            "hue": "#B18904"
                        },
                        {
                            "saturation": -61.8
                        },
                        {
                            "lightness": 45.599999999999994
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "stylers": [
                        {
                            "hue": "#DBA901"
                        },
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 51.19999999999999
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "stylers": [
                        {
                            "hue": "#FFBF00"
                        },
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 52
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "stylers": [
                        {
                            "hue": "#0078FF"
                        },
                        {
                            "saturation": -13.200000000000003
                        },
                        {
                            "lightness": 2.4000000000000057
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                // {
                //     "featureType": "poi",
                //     "stylers": [
                //         {
                //             "hue": "#FF0000"
                //         },
                //         {
                //             "saturation": -1.0989010989011234
                //         },
                //         {
                //             "lightness": 11.200000000000017
                //         },
                //         {
                //             "gamma": 1
                //         }
                //     ]
                // }
                ]
            };
            var
            marker;
            mapObject = new google.maps.Map(document.getElementById('map'), mapOptions);
            for (var key in markersData)
                markersData[key].forEach(function (item) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(item.location_latitude, item.location_longitude),
                        map: mapObject,
                        icon: "{{asset('img/pins/Skyline.png')}}",
                    });

                    if ('undefined' === typeof markers[key])
                        markers[key] = [];
                    markers[key].push(marker);
                    google.maps.event.addListener(marker, 'click', (function () {
      closeInfoBox();
      getInfoBox(item).open(mapObject, this);
      mapObject.setCenter(new google.maps.LatLng(item.location_latitude, item.location_longitude));
     }));

                    
                });
    

        function hideAllMarkers () {
            for (var key in markers)
                markers[key].forEach(function (marker) {
                    marker.setMap(null);
                });
        };

        function closeInfoBox() {
            $('div.infoBox').remove();
        };

        function getInfoBox(item) {
            return new InfoBox({
                content:
                '<div class="marker_info" id="marker_info">' +
                '<img src="' + item.map_image_url + '" alt="Image" width="280" height="auto"/>' +
                '<h3>'+ item.name_point +'</h3>' +
                '<span>'+ item.description_point +'</span>' +
                '<div class="marker_tools">' +
                '<form action="http://maps.google.com/maps" method="get" target="_blank" style="display:inline-block""><input name="saddr" value="'+ item.get_directions_start_address +'" type="hidden"><input type="hidden" name="daddr" value="'+ item.location_latitude +',' +item.location_longitude +'"><button type="submit" value="Get directions" class="btn_infobox_get_directions">¿Como llegar?</button></form>' +
                    // '<a href="tel://'+ item.phone +'" class="btn_infobox_phone">'+ item.phone +'</a>' +
                    '</div>' +
                    // '<a href="'+ item.url_point + '" class="btn_infobox">Details</a>' +
                '</div>',
                disableAutoPan: false,
                maxWidth: 0,
                pixelOffset: new google.maps.Size(10, 125),
                closeBoxMargin: '5px -20px 2px 2px',
                closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                isHidden: false,
                alignBottom: true,
                pane: 'floatPane',
                enableEventPropagation: true
            });


        };

    });
    </script>
    <script src="{{asset('front/js/infobox.js')}}"></script>

    @endsection