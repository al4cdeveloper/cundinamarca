<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ImageRequest;
use Illuminate\Support\Facades\URL;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Municipality;
use App\Models\MunicipalityImage;
use App\Models\Department;
use App\Models\Video;
use App\Admin;
use Session;
use Auth;
use File;


class MunicipalityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $municipalities = Municipality::all();
        foreach ($municipalities as $municipality) 
        {
            if($municipality->type_last_user =="admin")
            {
                $admin = Admin::find($municipality->fk_last_edition);
                array_add($municipality,'last_edition',$admin->name." - Administrador");
            }

            //WORK HERE WHERE HAVE DIFFFERENT EDITIONS
        }
        return view('admin.Municipality.listMunicipality',compact('municipalities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $weather = ['Tropical'=>'Tropical','Seco'=>'Seco','Templado'=>'Templado','Frío'=>'Frío'];
        return view('admin.Municipality.createEditMunicipality',compact('weather'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = \Validator::make($request->all(), [
             'municipality_name' => 'required|max:50',
             'latitude' => 'required|max:50',
             'longitude'  => 'required',
             'keywords'  => 'required',
             'description'  => 'required',
             'link_image'  => 'required',
             'weather'=>'required',
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {

            $files = $request->file('link_image');
            $link_image = $this->cargar_imagen($files);

            $municipality = new Municipality;
            $municipality->municipality_name = $request->municipality_name;
            $municipality->description = $request->description;
            $municipality->multimedia_type = "images";
            $municipality->latitude = $request->latitude;
            $municipality->longitude = $request->longitude;
            $municipality->type_last_user = "admin";
            if(isset($request->iframe))            $municipality->iframe = $request->iframe;
            if(isset($request->officialpage))            $municipality->officialpage = $request->officialpage;
            $municipality->weather = $request->weather;
            if(isset($request->downloadimage))
            {
                $files = $request->file('downloadimage');

                $downloadimage = $this->cargar_imagen($files);

                $municipality->downloadimage = "images/municipalities/".$downloadimage;
            }
            $municipality->link_image = "images/municipalities/".$link_image;
            $municipality->fk_last_edition = Auth::user()->id;
            $municipality->keywords = $request->keywords;
            $municipality->save();

            Session::flash('message', 'Se ha realizado correctamente el registro de información.');
            return redirect('admin/municipalities');
        }
    }

    public function upload_video(Request $request,$id)
    {
        // dd(youtube_match('https://www.youtube.com/watch?v=pXRviuL6vMY'));
        // https://www.youtube.com/embed/pXRviuL6vMY
        // <a href="#{{slugify_text($additional->title)}}" aria-controls="videos" role="tab" data-toggle="tab">{{$additional->title}}</a>
        
        $url = youtube_match($request->link_url);
        
        $newvideo = ['link_video' => $url, 'type_relation'=>'municipality','fk_relation' => $id];
        $create = Video::create($newvideo);

        Session::flash('message', 'Vídeo cargado correctamente');

        return redirect()->back();        
    }

    public function delete_video($id)
    {
        Video::destroy($id);
        Session::flash('message', 'El vídeo se ha borrado');
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $municipality = Municipality::where('slug',$slug)->first();
        if($municipality)
        {
            $weather = ['Tropical'=>'Tropical','Seco'=>'Seco','Templado'=>'Templado','Frío'=>'Frío'];
            return view('admin.Municipality.createEditMunicipality',compact('municipality','weather'));
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el municipio.");
            return redirect('admin/municipalities');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validator = \Validator::make($request->all(), [
             'municipality_name' => 'required|max:50',
             'latitude' => 'required|max:50',
             'latitude' => 'required|max:50',
             'longitude'  => 'required',
             'keywords'  => 'required',
             'description'  => 'required',
             'weather'=>'required',
            ]); 
        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {

            $municipality = Municipality::find($id);
            $municipality->municipality_name = $request->municipality_name;
            $municipality->description = $request->description;
            $municipality->latitude = $request->latitude;
            $municipality->longitude = $request->longitude;
            if(isset($request->iframe))            $municipality->iframe = $request->iframe;
            if(isset($request->officialpage))            $municipality->officialpage = $request->officialpage;
            $municipality->weather = $request->weather;
            if($request->link_image)
            {
                $exists = File::exists(public_path($municipality->link_image));
                if ($exists) 
                {
                    File::delete(public_path($municipality->link_image));
                }
                $files = $request->file('link_image');

                $link_image = $this->cargar_imagen($files);

                $municipality->link_image = "images/municipalities/".$link_image;

            }
            if(isset($request->downloadimage))
            {
                if($municipality->downloadimage)
                {
                    $exists = File::exists(public_path($municipality->downloadimage));
                    if ($exists) 
                    {
                        File::delete(public_path($municipality->downloadimage));
                    }
                }
                $files = $request->file('downloadimage');

                $downloadimage = $this->cargar_imagen($files);

                $municipality->downloadimage = "images/municipalities/".$downloadimage;
            }
            $municipality->type_last_user = "admin";
            $municipality->fk_last_edition = Auth::user()->id;
            $municipality->keywords = $request->keywords;
            $municipality->save();

            Session::flash('message', 'Se ha realizado correctamente la actualización de información.');
            return redirect('admin/municipalities');
        }
    }

    public function images($slug)
    {
        $municipality = Municipality::where('slug',$slug)->first();
        return view('admin.Municipality.imagesMunicipality', compact('municipality'));
    }

    public function upload_images($id, ImageRequest $request)
    {
        $files = $request->file('file');

        $new_service = [];

        foreach ($files as $file) {
            $imageName = $this->cargar_imagen($file);

            if ($imageName)  
            {
                $new_image = ['link_image' => 'images/municipalities/'.$imageName, 'fk_municipality' => $id];
                $service = MunicipalityImage::create($new_image);
                $new_service[] = $service->toArray();
                unset($new_image);
            }
        }

        if (count($new_service) > 0) {
            return \Response::json(['response' => 'Subida de imágenes completa'], 200);
        } else {
            return \Response::json(['response' => 'Falla en la subida de imágenes2'], 404);
        }
    }

    public function delete_image($id)
    {
        $image = MunicipalityImage::find($id);

        $exists = File::exists(public_path("images/municipalities/".$image->link_imagen));
        if ($exists) {
            File::delete(public_path("images/municipalities/".$image->link_imagen));
        }

        if ($image->delete()) 
        {
            \Session::flash('message', 'Imagen Borrada Correctamente');
        } 
        else 
        {
            \Session::flash('message-error', 'La Imagen no puede ser borrada');
        }

        return redirect()->back();
    }

    public function desactivate($id)
    {
        $municipality = Municipality::find($id);
        if($municipality)
        {
            $municipality->state = "inactivo";
            $municipality->save();
            Session::flash('message','Se ha desactivado el municipio correctamente');
            return redirect('admin/municipalities');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el municipio.");
            return redirect('admin/municipalities');
        }
    }

    public function activate($id)
    {
        $municipality = Municipality::find($id);
        if($municipality)
        {
            $municipality->state = "activo";
            $municipality->save();
            Session::flash('message','Se ha activado el municipio correctamente');
            return redirect('admin/municipalities');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el municipio.");
            return redirect('admin/municipalities');
        }
    }


    private function cargar_imagen($file, $imageName = false)
    {
        if ($imageName) 
        {
            $exists = File::exists(public_path("images/municipalities/".$imageName));
            if ($exists) 
            {
                File::delete(public_path("images/municipalities/".$imageName));
            }

            $image = explode('.', $imageName);
            $imageName = $image[0].'.'.$file->getClientOriginalExtension();
        } 
        else 
        {
            $imageName = 'Municipality_'.date('YmdHis', time()).rand().'.'.$file->getClientOriginalExtension();
        }

        $file->move(public_path('images/municipalities'), $imageName);

        $exists = File::exists(public_path("images/municipalities/".$imageName));

        if ($exists) 
        {
            return $imageName;
        } 
        else 
        {
            return false;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
