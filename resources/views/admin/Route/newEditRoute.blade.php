@extends('admin.layout.auth')

@if(isset($route))
    @section('title', 'Editar ruta')
@else
    @section('title', 'Crear ruta ')
@endif

@section('aditionalStyle')
    <link href="{{asset('plugins/kartik-v-bootstrap-fileinput/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <style>
.kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar {
    display: inline-block;
}
.kv-avatar .file-input {
    display: table-cell;
    width: 213px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@endsection

@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Crear nueva ruta</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
			
            @if(isset($route))
            {!!Form::model($route,['url'=>['admin/routes/update',$route->id_route],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
            @else
            {!!Form::open(['url'=>'admin/routes/store', 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
            @endif
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Nombre</label>
                    {!!Form::text('name', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre de ruta', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('name') }}</span>
                </div>
            </div>
            <input type="hidden" name="typePrincipal" id="typePrincipal" @if(isset($route)) value="{{$route->type_relation}}" @endif>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Ruta del municipio: </label>
                    <select class="select-chosen" id="firstSelect" name="principal">
                        <option></option>
                        <optgroup label="Municipio" id="municipality">
                            @foreach($municipalities as $municipality)
                                <option value="{{$municipality->id_municipality}}" @if(isset($route)) @if($route->type_relation=="municipality") @if($route->fk_relation==$municipality->id_municipality) selected @endif @endif @endif>{{$municipality->municipality_name}}</option>
                            @endforeach
                        </optgroup>
                    </select>
                    <span class="label label-danger">{{$errors->first('name') }}</span>
                </div>
            </div>
            <div class="col-sm-12" id="municipalitydiv">
                <div class="form-group">
                    <label for="costo">Municipios relacionados: </label>
                    <select name="relation_municipalities[]" id="relation_municipalities" class="select-chosen" multiple="">
                        @if(isset($route))
                            @foreach($municipalitiesToRelation as $municipality)
                                <option value="{{$municipality->id_municipality}}" 
                                @if(count($route->Municipalities)>0) 
                                    @foreach($route->Municipalities as $relationMunicipality)
                                     @if($relationMunicipality->fk_relation == $municipality->id_municipality) selected @endif
                                    @endforeach
                                @endif>{{$municipality->municipality_name}}</option>
                            @endforeach
                        @endif
                    </select>

                </div>
            </div>
            <div class="col-sm-12" id="sitediv" @if(!isset($route)) style="display: none" @endif>
                <div class="form-group">
                    <label for="costo">Lugares relacionados: </label>
                    <select name="relation_sites[]" id="relation_sites" class="select-chosen" multiple="">
                        @if(isset($route))
                            @foreach($sites as $site)
                                <option value="{{$site->id_site}}" 
                                @if(count($route->Sites)>0) 
                                    @foreach($route->Sites as $relationSite)
                                     @if($relationSite->fk_relation == $site->id_site) selected @endif
                                    @endforeach
                                @endif>{{$site->site_name}}</option>
                            @endforeach
                        @endif
                    </select>

                </div>
            </div>
            <div class="form-group col-sm-12">
    			<div class="col-sm-6 col-md-offset-3">
                    <div class="form-group">
                        <label for="costo">Mapa</label>
                        @if(isset($route))
                            <h5>Ya se ha cargado anteriormente la imagen, si se quiere actualizar por favor seleccionar acontinuacion</h5>
                        @endif
                        <div class="file-loading">
                            <input id="input-20" type="file" placeholder="Seleccione" name="map">
                        </div>
                        <span class="label label-danger">{{$errors->first('map') }}</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                <label >Palabras clave <small>Separadas por comas (,)</small></label>
                    {!!Form::text('keywords',null,['class'=>'form-control input-tags', 'placeholder'=>'Palabras clave'])!!}
                    <span class="label label-danger">{{$errors->first('keywords') }}</span>
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <label for="duration">Descripción</label>
                <div class="form-group">
                    <textarea id="textarea-ckeditor" name="description" class="ckeditor">@if(isset($route)){!!$route->description!!}@endif</textarea>
					<span class="label label-danger">{{$errors->first('description') }}</span>
                </div>
	        </div>
            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                <a href="{{url('admin/routes')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection

@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>

     <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/plugins/sortable.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/fileinput.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/fr.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/es.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/explorer-fa/theme.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/fa/theme.js')}}" type="text/javascript"></script>

    <script>
        $(document).on('ready', function() {
            $("#input-20").fileinput({
                browseClass: "btn btn-primary btn-block",
                browseLabel: "Seleccionar imagen",
                showRemove: false,
                showUpload: false

            });
        });

        $("#firstSelect").change(function()
        {
            var selected = $("option:selected", this);
            if(selected.parent()[0].id == "department")
            {
                $("#typePrincipal").val('department');
                var token = "{{csrf_token()}}";
                var route="/admin/circuits/getdepartment";
                var idDepartment = selected.val();
                //Limpiar relational departments
                $('#relation_departaments option').remove();
                //Limpiar relational municipality
                $('#relation_municipalities optgroup').remove();
                
                $.ajax({
                    url: route,
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'POST',
                    dataType: 'json',
                    data:{idDepartment},
                    complete: function(transport)
                    {
                        data = transport.responseJSON;
                        //DEPARTAMENTOS
                        departments = data['departments'];
                        $('#departmentdiv').css('display','inline');
                        for (x in departments) 
                        {
                            if ($('#relation_departaments').find('[value='+departments[x].id_department+']').length == 0) 
                            {
                                $("#relation_departaments").append('<option value="'+departments[x].id_department+'">'+departments[x].department_name+'</option>').trigger("chosen:updated");
                            }
                        }
                        //MUNICIPIOS
                        $("#relation_municipalities").append('<optgroup label="'+$("#firstSelect option:selected").text()+'" id="relation_municipalities_firtsgroup"></optgroup>').trigger("chosen:updated");

                        municipalities = data['municipalities'];
                        $('#municipalitydiv').css('display','inline');
                        //Creacion de optiongrup
                        for (x in municipalities) 
                        {
                            if ($('#relation_municipalities_firtsgroup').find('[value='+municipalities[x].id_municipality+']').length == 0) 
                            {
                                $("#relation_municipalities_firtsgroup").append('<option value="'+municipalities[x].id_municipality+'">'+municipalities[x].municipality_name+'</option>').trigger("chosen:updated");
                            }
                        }
                         //Sitios relacionados
                        $('#relation_sites optgroup').remove();
                        $('#relation_sites option').remove();

                        $('#sitediv').css('display','inline');
                        sites = data['sites'];
                        for(x in sites)
                        {
                            nombre = x.replace(" ", "_");
                            if($('#relation_sites'+nombre).length==0)
                            {
                                $("#relation_sites").append('<optgroup label="'+x+'" id="relation_sites'+nombre+'"></optgroup>').trigger("chosen:updated");
                            }
                            for(y in sites[x])
                            {
                                $("#relation_sites"+nombre).append('<option value="'+y+'">'+sites[x][y]+'</option>').trigger("chosen:updated");
                            }
                        }
                    }
                  });
                

            } 
            else if(selected.parent()[0].id == "municipality")
            {
                $("#typePrincipal").val('municipality');
                
                var token = "{{csrf_token()}}";
                var route="/admin/circuits/getmunicipality";
                var idMunicipality = selected.val();
                //Limpiar relational departments
                $('#relation_departaments option').remove();
                $('#departmentdiv').css('display','none');
                
                
                $.ajax({
                    url: route,
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'POST',
                    dataType: 'json',
                    data:{idMunicipality},
                    complete: function(transport)
                    {
                        data = transport.responseJSON;
                        municipalities = data['municipalities'];
                        $('#municipalitydiv').css('display','inline');
                        //Limpiar relational municipality
                        $('#relation_municipalities optgroup').remove();
                        $('#relation_municipalities option').remove();

                        //Creacion de optiongrup
                        for (x in municipalities) 
                        {
                            if ($('#relation_municipalities').find('[value='+municipalities[x].id_municipality+']').length == 0) 
                            {
                                $("#relation_municipalities").append('<option value="'+municipalities[x].id_municipality+'">'+municipalities[x].municipality_name+'</option>').trigger("chosen:updated");
                            }
                        }
                         //Sitios relacionados
                        $('#relation_sites optgroup').remove();
                        $('#relation_sites option').remove();

                        $('#sitediv').css('display','inline');
                        sites = data['sites'];
                        for(x in sites)
                        {
                            nombre = x.replace(" ", "_");
                            if($('#relation_sites'+nombre).length==0)
                            {
                                $("#relation_sites").append('<optgroup label="'+x+'" id="relation_sites'+nombre+'"></optgroup>').trigger("chosen:updated");
                            }
                            for(y in sites[x])
                            {
                                $("#relation_sites"+nombre).append('<option value="'+y+'">'+sites[x][y]+'</option>').trigger("chosen:updated");
                            }
                        }
                    }
                });
            }
        });

        $('#relation_departaments').change(function()
        {
            principal = $('#firstSelect').val();
            relationalDepartments = $('#relation_departaments').val();
            var token = "{{csrf_token()}}";
            var route="/admin/circuits/getreladepartment";

            // //Limpiar relational departments
            // $('#relation_departaments option').remove();
            // //Limpiar relational municipality
            // $('#relation_municipalities optgroup').remove();
            
            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'POST',
                dataType: 'json',
                data:{principal,relationalDepartments},
                complete: function(transport)
                {
                    data = transport.responseJSON;
                    $('#relation_municipalities optgroup').remove();
                    // //MUNICIPIOS
                    municipalities = data['municipalities'];

                    for(y in municipalities)
                    {
                        nombre = y.replace(" ", "_");
                        if($('#relation_municipalities'+y).length==0)
                        {
                            $("#relation_municipalities").append('<optgroup label="'+y+'" id="relation_municipalities'+nombre+'"></optgroup>').trigger("chosen:updated");
                        }
                        for(x in municipalities[y])
                        {
                            $("#relation_municipalities"+nombre).append('<option value="'+municipalities[y][x].id_municipality+'">'+municipalities[y][x].municipality_name+'</option>').trigger("chosen:updated");
                        }
                    }
                    //Sitios relacionados
                    $('#relation_sites optgroup').remove();

                    sites = data['sites'];

                    for(x in sites)
                    {
                        nombre = x.replace(" ", "_");
                        if($('#relation_sites'+nombre).length==0)
                        {
                            $("#relation_sites").append('<optgroup label="'+x+'" id="relation_sites'+nombre+'"></optgroup>').trigger("chosen:updated");
                        }
                        for(y in sites[x])
                        {
                            $("#relation_sites"+nombre).append('<option value="'+sites[x][y].id_site+'">'+sites[x][y].site_name+'</option>').trigger("chosen:updated");
                        }
                    }
                }
              });

        });
    </script>
@endsection