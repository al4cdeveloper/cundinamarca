@extends('layout.header')
    
    @section('title',$expo->fair_name)
    @section('keywords',$expo->keywords)
    
    @section('additionalStyle')
    <!-- REVOLUTION SLIDER CSS -->
    <link href="{{asset('front/rs-plugin/css/settings.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/extralayers.css')}}" rel="stylesheet">
    @endsection


    @section('main')
    
    <div class="tp-banner-container">
        <div class="tp-banner">
            <ul>
                <!-- SLIDE  -->
                @foreach($expo->images as $image)
                <li data-transition="fade" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Intro Slide">
                    <!-- MAIN IMAGE -->
                    <img src="{{asset($image->link_image)}}" alt="slidebg1" data-lazyload="{{asset($image->link_image)}}" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                </li>
                @endforeach
            </ul>
            <div class="tp-bannertimer tp-bottom"></div>
        </div>
    </div>

		<div id="position">
            <div class="container">
                <ul>
                    <li><a href="{{url('/')}}}">Home</a>
                    </li>
                    <li>ExpoCundinamarca</li>
                </ul>
            </div>
        </div>
        <!-- End Position -->

        <div class="container margin_60">
            
            <div class="main_title">
                <h2>Expo <span>Cundinamarca </span></h2>
                <br><br>
                {!!$expo->description!!}
            </div>


            <div class="row">
                <div class="col-md-6 col-sm-6 hidden-xs">
                    <iframe width="100%" height="300" src="https://www.youtube.com/embed/R0HUCefb3pk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
                <div class="col-md-6 col-sm-6">
                    <br>
                    <br>
                    <p><strong>#ExpoCundinamarca</strong> Con la mayor de las energías abrimos las puertas del que será el evento insignia de nuestro departamento. Una amplia exposición, que reunirá, bajo un mismo techo, lo mejor de la cultura, la gastronomía, el turismo y la economía de este territorio. <br>
                    Con el evento se busca tener una vitrina para lo que se produce en el departamento. Se contará con varios estands en los 30.000 metros cuadrados del hipódromo donde estarán 250 expositores provenientes de los 116 municipios que conforman al departamento. 
                    <br>
                    Para ello se tienen programadas ruedas de negocios, con las que se espera que se beneficien 200 empresas del departamento que contarán con el apoyo de las cámaras de comercio aliadas con la Gobernación. 
                    </p>
                </div>
            </div>
            <!-- End row -->
            <hr>

            <div class="main_title">
                <h2><span>Noticias</span> </h2>
            </div>
            <div class="container">
            <div class="row" {{$count=0}}>
                @foreach($expo->Notices as $notice)
                <div class="col-md-6 wow fadeIn " data-wow-delay="0.1s">
                        <div class="feature" style="padding: 10px!important;">
                            <div class="row">
                            <div class="col-md-12">
                                
                           <img src="{{asset($notice->image)}}" alt="" class="img-responsive" width="100%" height="auto">
                            </div>


                            <div class="col-md-12">
                                <h3>{{$notice->notice_name}} </h3>
                                {!!$notice->description!!}
                            </div>
                            </div>
                        </div>
                </div {{$count++}}>
                @endforeach
            </div>
            </div>
        </div>
        <!-- End container -->
    @endsection
	<!-- End main -->
    @section('additionalScript')


<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <script src="{{asset('front/rs-plugin/js/jquery.themepunch.tools.min.js')}}"></script>
    <script src="{{asset('front/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>
    <script src="{{asset('front/js/revolution_func.js')}}"></script>
    


    @endsection
