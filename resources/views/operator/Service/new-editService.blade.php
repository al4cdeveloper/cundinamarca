@extends('operator.layout.auth')

@section('title', 'Servicios')
@section('content')
    <div id="page-content">

     <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="header-section">
                    	@if(isset($service))
                        <h1>Editar servicio</h1>
                        @else
                        <h1>Nuevo servicio</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
            <!-- END Labels on top Form Title -->

            <!-- Labels on top Form Content -->
            @if(isset($service))
			{!!Form::model($service,['route'=>['operator.services.update',$service->id_service_operator],'method'=>'PUT', 'class'=> 'form-bordered', 'enctype' => 'multipart/form-data','novalidate'])!!}
			@else
			{!!Form::open(['route'=>'operator.services.store', 'method'=>'POST', 'class'=> 'form-bordered', 'enctype' => 'multipart/form-data','novalidate'])!!}
			@endif
                <div class="form-group">
                    <label for="example-nf-email">Servicio</label>
                    <select id="example-chosen" name="service" class="select-chosen" data-placeholder="Seleccione el servicio.." required>
                        <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
						@foreach($services as $listService)
                          <option value="{{$listService->id_service}}" @if(isset($service)) @if($service->PrincipalService->service == $listService->service) selected @endif @endif>{{$listService->service}}</option>
                        @endforeach
                    </select>
					<span class="label label-danger">{{$errors->first('service') }}</span>

                </div>
                <div class="col-sm-6">
	                <div class="form-group">
	                    <label for="costo">Costo</label>
	                    {!!Form::number('cost', null, ['class'=>'form-control', 'placeholder' => 'Inserte costo por persona', 'required'])!!}
						<span class="label label-danger">{{$errors->first('cost') }}</span>

	                </div>
                </div>
                <div class="col-sm-6">
	                <div class="form-group">
	                    <label for="address">Dirección</label>
	                    {!!Form::text('address', null, ['class'=>'form-control', 'placeholder' => 'Inserte dirección', 'required'])!!}
						<span class="label label-danger">{{$errors->first('address') }}</span>

	                </div>
                </div>
                <div class="col-sm-6">
	                <div class="form-group">

						{!!Form::label('days', 'Días de funcionamiento')!!}
	                    <select name="days[]" class="select-chosen" data-placeholder="Selecciona los días de servicio" multiple required>
                            @foreach($days as $day)
                                <option value="{{$day}}" @if(isset($service))

                                    @foreach($service->Schedule as $daySave)

                                            @if($daySave->day == $day) selected @endif 
                                    @endforeach

                                @endif>{{$day}}</option>
                            @endforeach
                        </select>
						<span class="label label-danger">{{$errors->first('days') }}</span>

                    </div>
	            </div>
	            <div class="col-sm-6">
	                <div class="form-group">
	                    <label for="capacity">Capacidad de personas</label>
	                    {!!Form::number('capacity', null, ['class'=>'form-control', 'placeholder' => 'Ingrese capacidad de personas', 'required'])!!}
						<span class="label label-danger">{{$errors->first('capacity') }}</span>
	                </div>

                </div>
                <div class="col-sm-6">
	                <div class="form-group">
	                    <label for="duration">Horas de duración del servicio</label>
	                    {!!Form::number('duration', null, ['class'=>'form-control', 'placeholder' => 'Ingrese horas de duración del servicio', 'required'])!!}
						<span class="label label-danger">{{$errors->first('duration') }}</span>
	                </div>
                </div>
                <div class="col-sm-6">
	                <div class="form-group">
	                    <label for="location">Lugar/Ciudad</label>
	                    {!!Form::text('location', null, ['class'=>'form-control', 'placeholder' => 'Ingrese Lugar/Ciudad donde se realiza la actividad', 'required'])!!}
	                </div>
                </div>
                <div class="form-group">
	                <label for="duration">Descripción</label>
                    <div class="form-group">
                        <textarea id="textarea-ckeditor" name="description" class="ckeditor">@if(isset($service)){!!$service->description!!}@endif</textarea>
						<span class="label label-danger">{{$errors->first('description') }}</span>
                    </div>
	            </div>
	            <div class="form-group">
	                <label for="duration">Recomendaciones</label>
                    <div class="form-group">
                        <textarea id="textarea-ckeditor" name="requisites" class="ckeditor">@if(isset($service)){!!$service->requisites!!}@endif</textarea>
						<span class="label label-danger">{{$errors->first('requisites') }}</span>
                    </div>
	            </div>
                

                <div class="form-group form-actions" align="center">
                    <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                    <a href="{{url('operator/services')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
                </div>
			{!!Form::close()!!}
            <!-- END Labels on top Form Content -->
        </div>
	</div>
@endsection
@section('aditionalScript')
	
    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>

@endsection