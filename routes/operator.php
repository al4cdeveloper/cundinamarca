<?php

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('operator')->user();

    //dd($users);

    return view('operator.home');
})->name('home');

Route::resource('services','Operator\serviceController');
Route::post('updateservice/{id}','Operator\serviceController@update');
Route::get('shedule/{id}','Operator\serviceController@shedule');
Route::post('updateshedule','Operator\serviceController@updateshedule');
Route::get('reservas','Operator\serviceController@listService');
Route::get('calendar','Operator\serviceController@calendarReservation');

Route::get('serviceimages/{id}','Operator\serviceController@images');
Route::put('service/upload_images/{id}','Operator\serviceController@upload_images');
Route::delete('service/delete_image/{id}','Operator\serviceController@delete_image');
Route::get('service/desactivate/{id}','Operator\serviceController@desactivateService');
Route::get('service/activate/{id}','Operator\serviceController@activateService');
Route::get('servicesuccess/{id}','Operator\serviceController@serviceSuccess');

Route::get('items/{id}','Operator\serviceController@indexitem');
Route::get('items/create/{id}','Operator\serviceController@createItem');
Route::post('items/store','Operator\serviceController@storeItem');
Route::get('items/edit/{id}','Operator\serviceController@editItem');
Route::put('items/update/{id}','Operator\serviceController@updateItem');
Route::get('items/desactivate/{id}','Operator\serviceController@desactivate');
Route::get('items/activate/{id}','Operator\serviceController@activate');

Route::get('profile','Operator\operatorController@show');
Route::post('update','Operator\operatorController@update');

Route::get('preferences','Operator\operatorController@preferences');
Route::post('updatepreferences','Operator\operatorController@updatepreferences');