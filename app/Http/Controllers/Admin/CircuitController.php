<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Circuit;
use App\Models\Department;
use App\Models\Municipality;
use App\Models\RelationCircuit;
use App\Models\InterestSite;
use Session;
use File;

class CircuitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $circuits = Circuit::all();
        return view('admin.Circuit.listCircuit',compact('circuits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::where('state','activo')->get();
        $municipalities = Municipality::where('state','activo')->get();

        return view('admin.Circuit.newEditCircuit',compact('departments','municipalities'));
    }

    public function getDepartment(Request $request)
    {
        $departments = Department::where('id_department','!=',$request->idDepartment)->where('state','activo')->get();
        $municipalities = Municipality::where('fk_department',$request->idDepartment)->where('state','activo')->get();
        $sites = [];
        foreach($municipalities as $municipality)
        {
            $sites[$municipality->municipality_name] = $municipality->InterestSite->pluck('site_name','id_site');
        }
        return response()->json([
            'departments' => $departments,
            'municipalities' => $municipalities,
            'sites'=>$sites,
        ]);
    }

    public function getRelationalDepartment(Request $request)
    {
        $principalDepartment = Department::find($request->principal);
        $municipalities[$principalDepartment->department_name]= $principalDepartment->Municipalities->toArray();

        foreach ($request->relationalDepartments as $department) 
        {
            $thisDepartment = Department::find($department);
            $municipalities[$thisDepartment->department_name] = $thisDepartment->Municipalities->toArray();
        }

        $sites = [];
        foreach($municipalities as $department)
        {
            foreach ($department as $municipality) 
            {

                $thisMunicipality = Municipality::find($municipality['id_municipality']);
                $sites[$thisMunicipality->municipality_name] = $thisMunicipality->InterestSite;
            }
        }
        return response()->json([
            'municipalities' => $municipalities,
            'sites'=>$sites,
        ]);
    }

    public function getMunicipality(Request $request)
    {
        $municipality = Municipality::where('id_municipality',$request->idMunicipality)->first();
        $municipalities = Municipality::where('id_municipality','!=',$request->idMunicipality)->where('state','activo')->get();
        $sites = [];
        foreach($municipalities as $municipalityF)
        {
            $sites[$municipalityF->municipality_name] = $municipalityF->InterestSite->pluck('site_name','id_site');
        }
        $sites[$municipality->municipality_name] = $municipality->InterestSite->pluck('site_name','id_site');
        return response()->json([
            'municipalities' => $municipalities,
            'sites'=>$sites,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
             'principal'=>'required',
             'name'=>'required',
             'keywords'  => 'required',
             'description'  => 'required',
             'map'  => 'required'
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $circuit = new Circuit;
            if($request->typePrincipal == "department")
            {
                $circuit->type_relation = "department";
            }
            else
            {
                $circuit->type_relation = "municipality";
            }
            $circuit->name = $request->name;
            $circuit->fk_relation = $request->principal;
            $circuit->description = $request->description;
            $circuit->keywords = $request->keywords;

            $files = $request->file('map');
            $map = $this->cargar_imagen($files);

            $circuit->link_map = "images/circuits/".$map;
            $circuit->save();

            if(isset($request->relation_departaments))
            {
                foreach ($request->relation_departaments as $department) 
                {
                    $relation = new RelationCircuit;
                    $relation->type_relation = "department";
                    $relation->fk_relation = $department;
                    $relation->fk_circuit = $circuit->id_circuit;
                    $relation->save();
                }
            }
            if(isset($request->relation_municipalities))
            {
                foreach ($request->relation_municipalities as $municipality) 
                {
                    $relation = new RelationCircuit;
                    $relation->type_relation = "municipality";
                    $relation->fk_relation = $municipality;
                    $relation->fk_circuit = $circuit->id_circuit;
                    $relation->save();
                }
            }
            if(isset($request->relation_sites))
            {
                foreach ($request->relation_sites as $site) 
                {
                    $relation = new RelationCircuit;
                    $relation->type_relation = "site";
                    $relation->fk_relation = $site;
                    $relation->fk_circuit = $circuit->id_circuit;
                    $relation->save();
                }
            }

            Session::flash('message', 'Se ha realizado correctamente el registro de información.');
            return redirect('admin/circuits');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $circuit = Circuit::where('slug',$slug)->first();
        $departments = Department::where('state','activo')->get();
        $municipalities = Municipality::where('state','activo')->get();
        $sites = InterestSite::where('state','activo')->get();
        return view('admin.Circuit.newEditCircuit',compact('circuit','municipalities','departments','sites'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
             'principal'=>'required',
             'name'=>'required',
             'keywords'  => 'required',
             'description'  => 'required',
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $circuit = Circuit::find($id);
            if($request->typePrincipal == "department")
            {
                $circuit->type_relation = "department";
            }
            else
            {
                $circuit->type_relation = "municipality";
            }
            $circuit->name = $request->name;
            $circuit->fk_relation = $request->principal;
            $circuit->description = $request->description;
            $circuit->keywords = $request->keywords;

            if($request->map)
            {
                if($circuit->link_map)
                {
                    $exists = File::exists(public_path($circuit->link_map));
                    if ($exists) 
                    {
                        File::delete(public_path($circuit->link_map));
                    }
                }
                $files = $request->file('map');
                $map = $this->cargar_imagen($files);
                $circuit->link_map = "images/circuits/".$map;
            }

            $circuit->save();

            foreach($circuit->Relations as $relation)
            {
                RelationCircuit::destroy($relation->id_relation_circuit);
            }

            if(isset($request->relation_departaments))
            {
                foreach ($request->relation_departaments as $department) 
                {
                    $relation = new RelationCircuit;
                    $relation->type_relation = "department";
                    $relation->fk_relation = $department;
                    $relation->fk_circuit = $circuit->id_circuit;
                    $relation->save();
                }
            }
            if(isset($request->relation_municipalities))
            {
                foreach ($request->relation_municipalities as $municipality) 
                {
                    $relation = new RelationCircuit;
                    $relation->type_relation = "municipality";
                    $relation->fk_relation = $municipality;
                    $relation->fk_circuit = $circuit->id_circuit;
                    $relation->save();
                }
            }
            if(isset($request->relation_sites))
            {
                foreach ($request->relation_sites as $site) 
                {
                    $relation = new RelationCircuit;
                    $relation->type_relation = "site";
                    $relation->fk_relation = $site;
                    $relation->fk_circuit = $circuit->id_circuit;
                    $relation->save();
                }
            }

            Session::flash('message', 'Se ha realizado correctamente el registro de información.');
            return redirect('admin/circuits');
        }
    }

    private function cargar_imagen($file, $imageName = false)
    {
        if ($imageName) 
        {
            $exists = File::exists(public_path("images/circuits/".$imageName));
            if ($exists) 
            {
                File::delete(public_path("images/circuits/".$imageName));
            }

            $image = explode('.', $imageName);
            $imageName = $image[0].'.'.$file->getClientOriginalExtension();
        } 
        else 
        {
            $imageName = 'circuit'.date('YmdHis', time()).rand().'.'.$file->getClientOriginalExtension();
        }

        $file->move(public_path('images/circuits'), $imageName);

        $exists = File::exists(public_path("images/circuits/".$imageName));

        if ($exists) 
        {
            return $imageName;
        } 
        else 
        {
            return false;
        }
    }

    public function desactivate($id)
    {
        $circuit = Circuit::find($id);
        if($circuit)
        {
            $circuit->state = "inactivo";
            $circuit->save();
            Session::flash('message','Se ha desactivado el circuito correctamente');
            return redirect('admin/circuits');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el circuito.");
            return redirect('admin/circuits');
        }
    }

    public function activate($id)
    {
        $circuit = Circuit::find($id);
        if($circuit)
        {
            $circuit->state = "activo";
            $circuit->save();
            Session::flash('message','Se ha activado el circuito correctamente');
            return redirect('admin/circuits');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el circuito.");
            return redirect('admin/circuits');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
    // Made by Sergio Hernandez 2018 😎
// support al4cdeveloper@gmail.com 
