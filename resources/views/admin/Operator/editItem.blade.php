@extends('admin.layout.auth')

@section('title', 'Servicios adicionales')
@section('content')
    <div id="page-content">

     <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="header-section">
                        <h1>Editar servicio adicional</h1>
                       
                    </div>
                </div>
            </div>
        </div>

        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
            <!-- END Labels on top Form Title -->
            <!-- Labels on top Form Content -->
			{!!Form::model($item,['url'=>['admin/operators/items/update',$item->id_service_item],'method'=>'PUT', 'class'=> 'form-bordered', 'enctype' => 'multipart/form-data','novalidate'])!!}
			
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="costo">Nombre</label>
                        {!!Form::text('item_name', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre de servicio adicional', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('item_name') }}</span>

                    </div>
                </div>
                <div class="col-sm-6">
	                <div class="form-group">
	                    <label for="costo">Costo</label>
	                    {!!Form::number('cost', null, ['class'=>'form-control', 'placeholder' => 'Inserte costo por persona', 'required'])!!}
						<span class="label label-danger">{{$errors->first('cost') }}</span>

	                </div>
                </div>
                

                <div class="form-group">
	                <label for="duration">Descripción</label>
                    <div class="form-group">
                        <textarea id="textarea-ckeditor" name="description" class="ckeditor">@if(isset($item)){!!$item->description!!}@endif</textarea>
						<span class="label label-danger">{{$errors->first('description') }}</span>
                    </div>
	            </div>
	            {!!Form::hidden('fk_service', $service->id_service_operator)!!}

                <div class="form-group form-actions" align="center">
                    <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                    <a href="{{url('admin/operators/service/upload_images',$service->operador->id_operator)}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
                </div>
			{!!Form::close()!!}
            <!-- END Labels on top Form Content -->
        </div>
	</div>
@endsection

@section('aditionalScript')
	
    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>

@endsection