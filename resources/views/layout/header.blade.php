<!DOCTYPE html>
<!--[if IE 8]><html class="ie ie8"> <![endif]-->
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="description" content="RutasColombia">
    <meta name="author" content="Al4c Developer">
    <title>@yield('title') | Cundinamarca</title>
    <meta name="keywords" content="@yield('keywords')">
    <!-- Favicons-->
     <link rel="shortcut icon" href="{{asset('icon/favicon.png')}}">
        <link rel="apple-touch-icon" href="{{asset('icon/icon57.png')}}" sizes="57x57">
        <link rel="apple-touch-icon" href="{{asset('icon/icon72.png')}}" sizes="72x72">
        <link rel="apple-touch-icon" href="{{asset('icon/icon76.png')}}" sizes="76x76">
        <link rel="apple-touch-icon" href="{{asset('icon/icon114.png')}}" sizes="114x114">
        <link rel="apple-touch-icon" href="{{asset('icon/icon120.png')}}" sizes="120x120">
        <link rel="apple-touch-icon" href="{{asset('icon/icon144.png')}}" sizes="144x144">
        <link rel="apple-touch-icon" href="{{asset('icon/icon152.png')}}" sizes="152x152">
        <link rel="apple-touch-icon" href="{{asset('icon/icon180.png')}}" sizes="180x180">
    <!-- Google web fonts -->
    <link href="https://fonts.googleapis.com/css?family=Gochi+Hand|Lato:300,400|Montserrat:400,400i,700,700i" rel="stylesheet">

    <!-- CSS -->
    <link href="{{asset('front/css/base.css')}}" rel="stylesheet">
        <!-- Stylesheets -->
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,400italic,300italic' rel='stylesheet' type='text/css'>
    {{-- <link rel="stylesheet" href="{{asset('plugins/OwlCarousel2-2.2.1/docs/assets/css/docs.theme.min.css')}}"> --}}

    <!-- Owl Stylesheets -->
    <link rel="stylesheet" href="{{asset('plugins/OwlCarousel2-2.2.1/docs/assets/owlcarousel/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/OwlCarousel2-2.2.1/docs/assets/owlcarousel/assets/owl.theme.default.min.css')}}">
    <link href="{{asset('css/frontstyle.css')}}" rel="stylesheet">
    {{-- @include('layout.alert') --}}
    <!-- javascript -->
    {{-- <script src="{{asset('plugins/OwlCarousel2-2.2.1/docs/assets/vendors/jquery.min.js')}}"></script> --}}

    {{-- ESTILOS POR BANNER DE MOVISTAR --}}
    {{-- <style type="text/css">
        header{
            top:206px!important;
        }
        header.sticky{ 
            top: 0!important;
        }
    </style> --}}
    @yield('additionalStyle')
        <!--[if lt IE 9]>
      <script src="{{asset('front/js/html5shiv.min.js')}}"></script>
      <script src="{{asset('front/js/respond.min.js')}}"></script>
    <![endif]-->

</head>
<body>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-74674600-1', 'auto');
      ga('send', 'pageview');

    </script>

<!--[if lte IE 8]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a>.</p>
<![endif]-->

    <div id="preloader">
        <div class="sk-spinner sk-spinner-wave">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
    </div>
    <!-- End Preload -->

    <div class="layer"></div>
    <!-- Mobile menu overlay mask -->

    <!-- Header================================================== -->
    <header>
        
                @include('layout.menufront')
    </header><!-- End Header -->
        
    @yield('additionalPreMain')

    <main>
        @yield('main')
    </main>

    @include('layout.footerfront')
    @yield('additionalScript')
</body>
</html>



