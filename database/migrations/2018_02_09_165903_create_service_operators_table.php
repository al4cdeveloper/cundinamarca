<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceOperatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_operators', function (Blueprint $table) {
            $table->increments('id_service_operator');
            $table->integer('fk_service')->unsigned();
            $table->foreign('fk_service')->references('id_service')->on('services'); 
            $table->integer('fk_operator')->unsigned();
            $table->foreign('fk_operator')->references('id_operator')->on('operators');
            $table->string('cost')->nullable();
            $table->string('address')->nullable();
            $table->integer('capacity')->nullable();
            $table->integer('duration')->nullable();
            $table->text('requisites')->nullable();
            $table->text('description')->nullable();
            $table->string('location')->nullable();
            $table->string('state')->default('Inhabilitado');
            $table->string('slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_operators');
    }
}
