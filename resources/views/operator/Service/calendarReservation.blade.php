@extends('operator.layout.auth')

@section('title', 'Servicios')

{{-- @section('aditionalStyle')
    <link rel="stylesheet" type="text/css" href="{{asset('vue/css/app.css')}}">
@endsection --}}
@section('aditionalStyle')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
@endsection

@section('content')
    <div id="page-content">

     <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <form>
                    <div class="col-sm-12">
                        <div class="header-section">
                            <h1>Servicios</h1>
                        </div>
                        
                    </div>
                </form>
            </div>
        </div>
        <!-- END Page Header -->

        <!-- Example Block -->
        <div class="block" id="service">
            <div class="row">
                <div id="calendar"></div>
              
            </div>
        </div>
            <!-- END Example Block -->
        
{{--         <div class="col-sm-6 hidden-xs">
                        <div class="header-section">
                            <ul class="breadcrumb breadcrumb-top">
                                <li>Category</li>
                                <li><a href="">Page</a></li>
                            </ul>
                        </div>
                    </div>
        </div> --}}


@endsection
@section('aditionalScript')
    <script src="{{asset('auth-panel/js/plugins/moment/min/moment.min.js')}}"></script>
       <script src="{{asset('auth-panel/js/pages/compCalendar.js')}}"></script>
<script type="text/javascript">
       $(document).ready(function() {

                // page is now ready, initialize the calendar...

                $('#calendar').fullCalendar({
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles',
                 'Jueves', 'Viernes', 'Sábado'],
                 dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
                  buttonText:
                  {
                      prev:     ' ◄ ',
                      next:     ' ► ',
                      prevYear: ' &lt;&lt; ',
                      nextYear: ' &gt;&gt; ',
                      today:    'hoy',
                      month:    'mes',
                      week:     'semana',
                      day:      'día'
                  },
                 header: {
                     left: 'prev,next today',
                     center: 'title',
                     right: 'month,basicWeek,basicDay'
                 },
                  events: [
                    @foreach($reservations[0] as $reservation)
                  {
                    id: "{{$reservation->id_reservation}}",
                  title: "{{$reservation->Service->PrincipalService->service}}",
                  start: '{{$reservation->date}}.{{$reservation->time}}',
                  @if($reservation->state =="Realizada")
                  backgroundColor: "green",
                  editable: false,
                  @endif
                  },@endforeach
                   ]
                 })
             });
            </script>
@endsection