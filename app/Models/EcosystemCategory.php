<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EcosystemCategory extends Model
{
	protected $primaryKey="id_ecosystem_category";
}
