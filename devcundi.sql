-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Servidor: localhost:3306
-- Tiempo de generación: 06-03-2018 a las 09:34:23
-- Versión del servidor: 5.6.36-cll-lve
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `devcundi`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'AL4C', 'al4cdeveloper@gmail.com', '$2y$10$qaJWUgrS7cSGvTh0l5bKjuzYgQTILwFecOMwi/spom.LTxUxpfTOa', NULL, '2018-02-15 20:49:33', '2018-02-15 20:49:33'),
(2, 'Rafael Alsina', 'webmaster@rutascolombia.com', '$2y$10$nIlSthMEJU07MV/j554Av.Q/pV.VooOPzai6RDF2ALYJtRi6nNGjm', 'VIKhBAzl1PSe6HJoYrQ5vnbc8sy6KbYA8MWDPAS6ipjMvilm35Be6w6LyWmH', '2018-02-16 04:27:59', '2018-02-16 04:27:59'),
(3, 'Andrea Castellanos', 'prensa@rutascolombia.com', '$2y$10$GE7nKmocoq7QOVJvLWNRSewPt.KSqNJCLlpK8eUr0U8bIFz6w7Cs6', 'aM4EoAnE1IuXdUBuY5xrIE97WLVbuGhpAB2S4KARbFMnBpsGpXmLjQC82Ltj', '2018-02-16 04:39:37', '2018-02-16 04:39:37'),
(4, 'Brian', 'b.ramirez@pappcorn.com', '$2y$10$mxLfVAJ/a/cJMUK6T6byyu5vEMeYrkd7r0HqwhgF6s8.JTMbOTKQu', NULL, '2018-03-03 02:47:36', '2018-03-03 02:47:36'),
(5, 'Jonathan Ramirez', 'jonathan.ramirez@cundinamarca.gov.co', '$2y$10$XxvZR29VehnsuhTlAc.4B.QchHwSbUkdcuHBhAPGh2UnPhgRHp7w.', '2XNsM4YpO03TKGzlKet0kUjim1vun8U2DWe1zNDAR1sid4dTx9dk19m772a4', '2018-03-05 22:47:46', '2018-03-05 22:47:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_password_resets`
--

CREATE TABLE IF NOT EXISTS `admin_password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `admin_password_resets_email_index` (`email`),
  KEY `admin_password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id_contact` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_contact`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `contacts`
--

INSERT INTO `contacts` (`id_contact`, `name`, `phone`, `email`, `message`, `state`, `created_at`, `updated_at`) VALUES
(1, 'PRUEBA', '3203233082', 'sergio.hernandez.goyeneche@gmail.com', 'azsdasdas dasd as', 'new', '2018-03-03 05:19:56', '2018-03-03 05:19:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ecosystem_categories`
--

CREATE TABLE IF NOT EXISTS `ecosystem_categories` (
  `id_ecosystem_category` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ecosystem_category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_ecosystem_category`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `ecosystem_categories`
--

INSERT INTO `ecosystem_categories` (`id_ecosystem_category`, `ecosystem_category`, `created_at`, `updated_at`) VALUES
(1, 'Agua', '2017-10-24 01:55:27', '2017-10-24 01:55:27'),
(2, 'Tierra', '2017-10-24 01:55:27', '2017-10-24 01:55:27'),
(3, 'Aire', '2017-10-24 01:55:27', '2017-10-24 01:55:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fairs`
--

CREATE TABLE IF NOT EXISTS `fairs` (
  `id_fair` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fair_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `type_relation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_relation` int(11) NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `multimedia_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_fair`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Volcado de datos para la tabla `fairs`
--

INSERT INTO `fairs` (`id_fair`, `fair_name`, `date`, `type_relation`, `fk_relation`, `state`, `multimedia_type`, `keywords`, `description`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Festival de la Cultura Cafetera', '2018-03-17', 'municipality', 37, 'inactivo', 'images', 'municipio,viotá,ferias,fiestas,festival,cultura,cafetera', '<p>.</p>', 'sergio-hernandez', '2018-02-15 21:32:06', '2018-03-03 11:22:17'),
(2, 'Reinado Departamental de la Panela', '2018-01-23', 'municipality', 10, 'activo', 'images', 'municipio,villeta,reinado,panela,cundinamarca', '<p>Del 23 al 28 de enero, Villeta, la ciudad dulce de Colombia, ser&aacute; epicentro de folclor y cultura gracias al cuadrag&eacute;simo primer Festival Tur&iacute;stico y Reinado Nacional e Internacional de la Panela. Este a&ntilde;o, el evento rendir&aacute; homenaje a las tradiciones de la regi&oacute;n, convirtiendo la celebraci&oacute;n en un importante impulso para la econom&iacute;a local.</p>', 'reinado-departamental-de-la-panela', '2018-02-19 04:47:06', '2018-03-03 07:12:05'),
(3, 'Festival de la Alegría Ubatense', '2018-07-22', 'municipality', 40, 'activo', 'images', 'festival,ubaté,alegría,cundinamarca', '<p>Desde el 22 de julio hasta el 07 de agosto Ubat&eacute; gozar&aacute; de diferentes eventos tradicionales donde se espera la visita de miles de turistas, quienes podr&aacute;n disfrutar de: Festival de Promeseros, Exposici&oacute;n de Especies Menores, I Festival de M&uacute;sica de la Regi&oacute;n Andina, Festival de Deportes extremos, v&iacute;speras en honor al Santo Cristo (patrono de los Ubatenses), cabalgatas, Expo Show Canino, desfile de carrozas, XXIV Reinado de la Simpat&iacute;a, concurso de carros esferados, II Cuarto de Milla Equina, I Encuentro de Bandas Marciales, conciertos y mucho m&aacute;s.</p>', 'festival-de-la-alegria-ubatense', '2018-02-19 04:54:06', '2018-03-03 12:05:19'),
(4, 'Festival de la Colombianidad', '2018-09-20', 'municipality', 25, 'activo', 'images', 'municipio,tocancipá,festival,colombianidad,cundinamarca', '<p>Adem&aacute;s de industria, en Tocancip&aacute; tambi&eacute;n se respira cultura. Basta con citar un ejemplo: m&aacute;s del 80 por ciento de los estudiantes de las instituciones del municipio hacen parte de la sinf&oacute;nica, la banda de guerra o el coro, una muestra de lo que hay detr&aacute;s de una celebraci&oacute;n que despu&eacute;s de 17 ediciones se proyecta como la m&aacute;s reconocida en la regi&oacute;n.</p>\r\n\r\n<p>El &lsquo;<strong>Festival de la Colombianidad, la M&uacute;sica, la Cultura y la Actividad L&uacute;dica</strong>&rsquo; se celebra desde el a&ntilde;o 2000 en Tocancip&aacute; y se ha consolidado como uno de los eventos culturales m&aacute;s grandes de Cundinamarca. Tiene lugar cada a&ntilde;o a finales de septiembre, de tal manera que coincida con el cumplea&ntilde;os del municipio.</p>', 'festival-de-la-colombianidad', '2018-02-19 04:56:47', '2018-03-03 12:08:43'),
(5, 'Encuentro Nacional del Torbellino', '2018-11-02', 'municipality', 28, 'activo', 'images', 'encuentro,nacional,torbellino,municipio,tabio,cundinamarca', '<p>Desde hace m&aacute;s de un cuarto de siglo, Tabio se viste de fiesta para celebrar el Encuentro Nacional del Torbellino y Danzas Tradicionales, en el cual participan agrupaciones de diferentes regiones del pa&iacute;s. Este a&ntilde;o los invitados especiales son: Los Aut&eacute;nticos Gaiteros de San Jacinto, Recital Folcl&oacute;rico Mar&iacute;a Barilla, Gloria Perea y Beatriz Arellano.</p>\r\n\r\n<p>En danza participan delegaciones de Barranquilla, Acac&iacute;as, Buenaventura, Aracataca, V&eacute;lez, Cucaita, F&oacute;meque, Bogot&aacute;, Cajic&aacute; y Tabio. Con m&uacute;sica colombiana intervienen: Orquesta de Bandolas de Bogot&aacute;; la Facultad de M&uacute;sica de la Universidad de los Andes de Bogot&aacute;; el Tr&iacute;o Cristal de Tabio; el Dueto Fernando y Jos&eacute; de L&eacute;rida; los Cocomes de V&eacute;lez; Lorena y Jorge, de Tabio; el Coro de Zipaquir&aacute;, y Ensamble de m&uacute;sica llanera de Cajic&aacute;.</p>', 'encuentro-nacional-del-torbellino', '2018-02-19 05:00:11', '2018-03-03 12:11:22'),
(6, 'Semana Artística y Cultural', '2018-03-07', 'municipality', 1, 'activo', 'images', 'municipio,suesca,cultural,cundinamarca', '<p>Del 7 al 15 de marzo se realizar&aacute; en el municipio de Suesca, Cundinamarca, la XII Semana Art&iacute;stica y Cultural. Encuentro de danzas, festival de m&uacute;sica campesina, cabalgata tur&iacute;stica y otras actividades hacen parte de la programaci&oacute;n que se vivir&aacute; en el marco de los 478 a&ntilde;os de existencia del municipio.</p>', 'semana-artistica-y-cultural', '2018-02-19 05:08:46', '2018-03-03 07:07:18'),
(7, 'Ferias y Fiestas en Gachetá', '2018-02-01', 'municipality', 16, 'activo', 'images', 'municipio,festival,cundinamarca', '<p>Gachet&aacute;, en el departamento de Cundinamarca, realizar&aacute; una nueva versi&oacute;n de sus tradicionales Ferias y Fiestas. Reinado, desfile de comparsas y carrozas, corrida de toros y las presentaciones de Walter Silva, Los 8 de Colombia, Alkilados, Fulanito y Alejandro Palacio, hacen parte de las actividades.</p>', 'festival-cultural-de-los-reyes-magos', '2018-02-19 05:20:34', '2018-03-03 11:15:02'),
(8, 'Festival de Música Colombiana', '2018-06-08', 'municipality', 6, 'inactivo', 'images', 'municipio,puerto salgar,festival,música,colombiana', '<p>.</p>', 'festival-de-musica-colombiana', '2018-02-19 05:22:09', '2018-03-03 12:15:14'),
(9, 'Festival Agroturístico y Cultural San Pedrino', '2018-07-30', 'municipality', 7, 'inactivo', 'images', 'municipio,nimaima,festival,cultural,san pedrino,cundinamarca', '<p>.</p>', 'festival-agroturistico-y-cultural-san-pedrino', '2018-02-19 05:27:20', '2018-03-03 12:15:00'),
(10, 'Concurso Nacional de Danza Andina', '2018-09-08', 'municipality', 26, 'inactivo', 'images', 'municipio,nemocón,concurso,danza,andina,cundinamarca', '<p>.</p>', 'concurso-nacional-de-danza-andina', '2018-02-19 05:29:36', '2018-03-03 12:16:04'),
(11, 'Concurso Nacional de Bandas Musicales', '2018-10-13', 'municipality', 11, 'inactivo', 'images', 'municipio,la vega,concurso,bandas,musicales,cundinamarca', '<p>.</p>', 'concurso-nacional-de-bandas-musicales', '2018-02-19 05:31:09', '2018-03-03 12:14:34'),
(12, 'Festival del Dorado', '2018-09-11', 'municipality', 14, 'inactivo', 'images', 'municipio,guatavita,festival,dorado,cundinamarca', '<p>.</p>', 'festival-del-dorado', '2018-02-19 05:37:44', '2018-03-03 12:14:27'),
(13, 'Festividades de Policarpa Salavarrieta', '2018-01-26', 'municipality', 5, 'inactivo', 'images', 'municipio,guaduas,policarpa,salavarrieta,cundinamarca', '<p>.</p>', 'festividades-de-policarpa-salavarrieta', '2018-02-19 05:44:40', '2018-03-03 07:17:59'),
(14, 'Festival Turístico y Cultural del Río y el Pescador', '2018-03-16', 'municipality', 5, 'inactivo', 'images', 'municipio,guaduas,festival,río,pescador,cundinamarca', '<p>.</p>', 'festival-turistico-y-cultural-del-rio-y-el-pescador', '2018-02-19 05:45:59', '2018-03-03 11:22:51'),
(15, 'Festival de la Luna, la Leyenda y el Maíz', '2018-10-10', 'municipality', 27, 'inactivo', 'images', 'municipio,chía,luna,leyenda,maíz,cundinamarca', '<p>.</p>', 'festival-de-la-luna-la-leyenda-y-el-maiz', '2018-02-19 05:48:37', '2018-03-03 12:14:15'),
(16, 'Expo Cundinamarca', '2018-09-14', 'municipality', 1, 'activo', 'images', 'Expo,Cundinamarca,septiembre', '<p>Est&aacute;n dadas las condiciones para sentir, vivir y enamorarse de Cundinamarca, el cual se vestir&aacute; de fiesta y de cultura para que nacionales y extranjeros experimenten al m&aacute;ximo de su riqueza en un solo lugar.</p>', 'expo-cundinamarca', '2018-03-01 03:36:01', '2018-03-03 06:45:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fair_images`
--

CREATE TABLE IF NOT EXISTS `fair_images` (
  `id_fair_image` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_fair` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_fair_image`),
  KEY `fair_images_fk_fair_foreign` (`fk_fair`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `fair_images`
--

INSERT INTO `fair_images` (`id_fair_image`, `link_image`, `fk_fair`, `created_at`, `updated_at`) VALUES
(1, 'images/fairs/Fair_201802151638561515773800.jpg', 1, '2018-02-15 21:38:56', '2018-02-15 21:38:56'),
(2, 'images/fairs/Fair_20180303000038665675416.jpg', 16, '2018-03-03 07:00:39', '2018-03-03 07:00:39'),
(4, 'images/fairs/Fair_201803030000431397667365.jpg', 16, '2018-03-03 07:00:43', '2018-03-03 07:00:43'),
(6, 'images/fairs/Fair_201803030009301522826182.jpg', 6, '2018-03-03 07:09:30', '2018-03-03 07:09:30'),
(7, 'images/fairs/Fair_201803030013311416366582.png', 2, '2018-03-03 07:13:31', '2018-03-03 07:13:31'),
(8, 'images/fairs/Fair_20180303041655722364457.jpg', 7, '2018-03-03 11:16:55', '2018-03-03 11:16:55'),
(9, 'images/fairs/Fair_201803030506071684210562.jpg', 3, '2018-03-03 12:06:07', '2018-03-03 12:06:07'),
(10, 'images/fairs/Fair_20180303050931487663079.jpg', 4, '2018-03-03 12:09:31', '2018-03-03 12:09:31'),
(11, 'images/fairs/Fair_20180303051257785299488.jpg', 5, '2018-03-03 12:12:57', '2018-03-03 12:12:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `interest_sites`
--

CREATE TABLE IF NOT EXISTS `interest_sites` (
  `id_site` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_category` int(10) unsigned NOT NULL,
  `fk_municipality` int(10) unsigned NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `web` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iframe` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `downloadimage` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `multimedia_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link_icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_site`),
  KEY `interest_sites_fk_category_foreign` (`fk_category`),
  KEY `interest_sites_fk_municipality_foreign` (`fk_municipality`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=32 ;

--
-- Volcado de datos para la tabla `interest_sites`
--

INSERT INTO `interest_sites` (`id_site`, `site_name`, `fk_category`, `fk_municipality`, `address`, `phone`, `web`, `facebook`, `twitter`, `instagram`, `youtube`, `iframe`, `downloadimage`, `multimedia_type`, `latitude`, `longitude`, `slug`, `link_image`, `link_icon`, `description`, `keywords`, `state`, `created_at`, `updated_at`) VALUES
(3, 'Piscilago', 3, 4, 'km 105, vía Bogotá-Girardot', '317 510 5647', 'http://www.piscilago.co', 'www.facebook.com/PiscilagoCol', 'https://twitter.com/Piscilago', 'https://www.instagram.com/piscilagocol/', 'https://www.youtube.com/user/PiscilagoParque', 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'images/interestsites/site201803021731061040866337.jpg', 'images', '5.103929', '-73.798632', 'lugar1', 'images/interestsites/site201803021731061047231618.jpg', 'images/interestsites/site201803022041171605274597.jpg', '<p>Uno de los parques recreativos con mayor reconocimiento en la regi&oacute;n y el pa&iacute;s. Situado entre Girardot y Melgar, departamento del Tolima, su atractivo principal es el agua, con la que el visitante interact&uacute;a en distintas instalaciones como toboganes, r&iacute;os artificiales y piscinas.</p>\r\n\r\n<p>Visite tambi&eacute;n el zool&oacute;gico, donde podr&aacute; conocer leones, anacondas y cocodrilos, entre otras especies de animales. <strong>Horario</strong> Ma. a vi.: 8:30 a.m. a 5:30 p.m. y s&aacute;., do. y fest. 8:30 a.m. a 6 p.m.&nbsp;</p>', 'Cundinamarca', 'activo', '2018-02-17 03:25:54', '2018-03-03 03:41:17'),
(4, 'Parapente Paraíso', 2, 24, 'km 4 vía Pionono', '312 490 9593', 'http://parapenteparaiso.com', 'https://www.facebook.com/parapenteparaiso/', NULL, 'https://www.instagram.com/parapenteparaiso/', 'https://www.youtube.com/channel/UCo-DSvFJs3IsVcvvJJYtd8A', 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', NULL, 'images', '1', '2', 'parapente-paraiso', NULL, 'images/interestsites/site20180302225653295169043.jpg', '<p>Ubicado en sop&oacute; cundinamarca a solo 40 minutos de la ciudad de Bogot&aacute; D.C. , ofrece a pilotos y turistas las mejores condiciones de vuelo en la sabana. Un desnivel de 400 metros y vientos de oriente a occidente entre 15 y 20 kms hacen de este lugar el sitio ideal para volar durante todo el a&ntilde;o con una altura promedio de 500 mts sobre el despegue.</p>\r\n\r\n<p>Este lugar de ensue&ntilde;o cuenta con temperaturas aproximadas entre los 12 y 15&deg;c. y el mejor paisaje de la regi&oacute;n por lo que adem&aacute;s de volar tu puedes deleitarte con el canto y observaci&oacute;n de las aves, la tranquilidad de la naturaleza, el aire puro y la vista excepcional del embalse de Tomin&eacute; y el valle de Guasca y Guatavita o si lo prefieres puedes contemplar la puesta del sol al calor de la chimenea de nuestro refugio Caf&eacute; y restaurante donde encontrar&aacute;s bebidas fr&iacute;as, calientes y platos a la carta.</p>\r\n\r\n<p>Si eres piloto, cuentas con tu propio equipo y licencia de vuelo, este es el sitio ideal para poner en practica todos tus conocimientos y mejorar tus destrezas y habilidades. Con condiciones termodin&aacute;micas excelentes, vientos de 15 a 20 k/h, un desnivel de 400 mts, area de despegue de 2000 mts2 y parqueadero a menos de 10 mts del despegue.</p>\r\n\r\n<p>Definitivamente en <strong>PARAPENTE PARAISO</strong> tu y los tuyos podr&aacute;n tener una inolvidable experiencia de vuelo y diversi&oacute;n en la Sabana de Bogot&aacute;.</p>', 'operador,aventura', 'activo', '2018-02-17 04:03:14', '2018-03-03 05:56:53'),
(5, 'Club Náutico El Portillo', 2, 14, 'km 10 vía Sesquilé-Guatavita', '320 294 4297', 'http://www.clubnauticoelportillo.com', NULL, NULL, NULL, NULL, 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'null', 'images', '1', '2', 'club-nautico-el-portillo', '', 'images/interestsites/site201803022159241955443437.jpg', '<p>Muy cerca de Bogot&aacute;, estamos en un para&iacute;so natural en donde vivimos toda la emoci&oacute;n de la navegaci&oacute;n rodeados de un ambiente seguro y familiar. El primer Club N&aacute;utico en el embalse de Tomin&eacute; le da la bienvenida a su sitio web y espera acogerle pronto.</p>', 'operadores', 'activo', '2018-02-17 04:16:45', '2018-03-03 04:59:24'),
(6, 'Colombia Xtrema', 2, 13, 'km 9 vía La Calera', '311 876 3633', 'http://www.colombiaextrema.inf.travel/', NULL, NULL, NULL, NULL, 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'null', 'images', '1', '2', 'colombia-xtrema', '', 'images/interestsites/site20180302221323798702037.jpg', '<p><strong>Colombia Extrema</strong>, donde usted encontrar&aacute; planes de deporte extremo y turismo aventura. Hemos dado origen a un nuevo estilo de vida, un movimiento y una cultura urbana llena de oportunidades y beneficios para las nuevas generaciones que quieren disfrutar las cosas buenas de nuestro pa&iacute;s.</p>\r\n\r\n<p>Contamos con nuestro propio portal en internet, con nuestra propia agencia de publicidad, con nuestros propios talleres, estudios y centros de dise&ntilde;o, con nuestro propio pull de arquitectos, ingenieros y especialistas de los deportes extremos. Tambi&eacute;n contamos con una central de reservas dedicada las 24 horas del d&iacute;a a tender y responder todas las compras y solicitudes de los que a diario intentan compartir nuestra locura, respondiendo as&iacute; con un excelente servicio al cliente y un perfecto control de calidad.</p>', 'operadores', 'activo', '2018-02-17 04:19:23', '2018-03-03 05:13:23'),
(7, 'A pata con Joaco', 2, 10, 'Parque Molienda', '310 239 9812', 'http://www.apataconjoaco.com/', NULL, NULL, NULL, NULL, 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'null', 'images', '1', '2', 'a-pata-con-joaco', '', 'images/interestsites/site201803022138541541289290.jpg', '<p><strong>A pata con Joaco</strong> le invita a gozar de la naturaleza con las mejores opciones en turismo de aventura de Cundinamarca. A solo hora y media de Bogot&aacute;, en el municipio de Villeta, venga a disfrutar de ofertas &uacute;nicas para el descanso y la diversi&oacute;n.</p>', 'operadores', 'activo', '2018-02-17 04:22:52', '2018-03-03 04:38:54'),
(8, 'Útica Xtrema', 2, 9, 'Cr. 13 # 61-47, l. 117, c.c. Chapicentro, Bogotá', '(1) 249 7792', 'http://www.uticaxtrema.com', 'https://www.facebook.com/uticaxtrema/', NULL, NULL, NULL, 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'null', 'images', '1', '2', 'utica-xtrema', '', 'images/interestsites/site201803022310562021770642.jpg', '<p><strong>&Uacute;tica Xtrema</strong> es una empresa legalmente constituida que nace del esfuerzo de j&oacute;venes capacitados en el &aacute;rea de recreaci&oacute;n y turismo, comprometidos con el esparcimiento y la diversi&oacute;n sana de la poblaci&oacute;n de nuestro pa&iacute;s, y con la &uacute;nica finalidad de presentar un servicio con calidad, puntualidad y atenci&oacute;n que el usuario se merece, todo para llenar las espectativas tanto del sector p&uacute;blico como el privado, desarrollando su actividad alrededor de todo lo relacionado al deporte y la recreaci&oacute;n de aventura en un medio totalmente Agro-ecol&oacute;gico y seguro, ya que nuestro grupo de trabajo cuenta con una alta capacitaci&oacute;n y experiencia en este tipo de servicios.</p>', 'operadores', 'activo', '2018-02-17 04:30:03', '2018-03-03 06:10:56'),
(9, 'Explora Suesca', 2, 1, 'Cra, 6 # 8-20  Suesca, Cundinamarca', '311 249 3491', 'http://www.explorasuesca.com', NULL, NULL, NULL, NULL, 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'null', 'images', '1', '2', 'explora-suesca', '', 'images/interestsites/site201803022226511949753405.jpg', '<p>Damos a conocer los atractivos naturales de Suesca y sus alrededores a trav&eacute;s de diferentes actividades de aventura con el fin de fomentar el turismo, la cultura ecol&oacute;gica respetando el medio ambiente y generando sentido de pertenencia y orgullo de los hermosos lugares naturales.</p>', 'operadores', 'activo', '2018-02-17 04:33:48', '2018-03-03 05:26:51'),
(10, 'Pacho Aventura Natural', 2, 21, 'Pacho, Cundinamarca', '3214855649', 'http://pachoaventuranatural.tumblr.com', NULL, 'https://twitter.com/@PachoAventuraN', NULL, NULL, 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'null', 'images', '1', '2', 'pacho-aventura-natural', '', 'images/interestsites/site201803022253181848294492.jpg', '<p>Imag&iacute;nate realizar un plan de aventura, con deportes extremos que no solo te pongan en contacto con la naturaleza, sino que tambi&eacute;n promuevan en ti, el trabajo en equipo, el liderazgo y el desarrollar de capacidades y habilidades para enfrentar los diferentes obst&aacute;culos profesionales y personales.</p>', 'operadores', 'activo', '2018-02-17 04:42:20', '2018-03-03 05:53:18'),
(11, 'Salto de los Micos', 4, 10, 'Parque Molienda ', '310 239 9812', NULL, NULL, NULL, NULL, NULL, 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'images/interestsites/site20180302205928902315871.jpg', 'images', '5.017437', '-74.478964', 'salto-de-los-micos', 'images/interestsites/site201803022059281615108960.jpg', 'images/interestsites/site20180302205928758158865.jpg', '<p style="text-align:justify">Es el atractivo natural m&aacute;s importante del municipio de Villeta. Se trata una reserva natural que alberga siete cascadas de agua manantial, con pozos y balnearios, ideales para un ba&ntilde;o refrescante. Tambi&eacute;n es un espacio recomendado para avistar la fauna y la flora propias de este ecosistema, habitado por numerosas especies de aves e insectos.</p>', 'reserva,natural,villeta', 'activo', '2018-02-17 04:49:24', '2018-03-06 03:55:43'),
(12, 'Laguna del Cacique de Guatavita', 1, 14, 'Vereda Tierra Negra, Sesquilé', '(1) 847 8232', 'http://www.colparques.net/LGUATAVITA', NULL, NULL, NULL, NULL, 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'images/interestsites/site20180302174932483629554.jpg', 'images', '4.977596', '-73.774997', 'laguna-del-cacique-de-guatavita', 'images/interestsites/site20180302174932251433241.jpg', 'images/interestsites/site201803021806581244597581.jpg', '<p style="text-align:justify">En la &eacute;poca prehisp&aacute;nica, all&iacute; era donde el cacique del pueblo muisca tomaba posesi&oacute;n de su investidura. En la ceremonia, su cuerpo desnudo era cubierto con polvo de oro, miel y resina de frailej&oacute;n antes de abordar una balsa desde la cual ofrendaba a la laguna piezas de oro. Este ritual, aliment&oacute; la famosa leyenda de El Dorado. Luego de 45 minutos de caminata desde el punto de partida, la casa ceremonial, llegar&aacute; a las aguas de la laguna, cuyo particular color turquesa y esmeralda se debe a las algas que crecen en ella. Alrededor hay 613 hect&aacute;reas de monta&ntilde;as.</p>', 'reserva natural,san francisco', 'activo', '2018-02-17 05:02:39', '2018-03-03 03:44:23'),
(13, 'Catedral de Sal de Zipaquirá', 1, 23, 'Cl. 13 # 8-36, Zipaquirá', '(1) 852 9890', 'http://www.catedraldesal.gov.co', 'https://www.facebook.com/catedraldesaldezipaquira/', 'http://www.catedraldesal.gov.co/', NULL, NULL, 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'images/interestsites/site201803021637382084094982.jpg', 'images', '5.018850', '-74.009968', 'catedral-de-sal-de-zipaquira', 'images/interestsites/site20180302163738275922985.jpg', 'images/interestsites/site201803021804461517658616.jpg', '<p>Es considerada <strong>la primera maravilla de Colombia</strong>, pues se trata de una compleja construcci&oacute;n, tallada completamente en sal, a 180 m bajo tierra. En el recorrido se visitan las 14 paradas del v&iacute;a crucis, ambientadas con luces e im&aacute;genes que invitan a la introspecci&oacute;n y la espiritualidad, para llegar al centro de la catedral, compuesto por tres naves: Del Nacimineto, De la Vida y De la Muerte.</p>\r\n\r\n<p>Dentro del complejo de la catedral podr&aacute;s ver tu reflejo en el espejo de agua, al final del recorrido, as&iacute; como asistir a las funciones 3D de pel&iacute;culas que narran la historia y la cultura de la catedral, la mina y Zipaquir&aacute;. A la entrada y la salida de la catedral, podr&aacute;s tomarte un momento para descansar y refrescarte con un jugo natural o probar alguna de las preparaciones locales y animarte a subir el muro de escalada, frente a la plazoleta de comidas del complejo.</p>', 'zipaquirá,catedral,sal,lugar', 'activo', '2018-02-17 05:13:33', '2018-03-03 01:04:46'),
(14, 'Parque Jaime Duque', 3, 25, 'km 34 autopista Norte, Tocancipá', '(1) 620 0681', 'http://www.parquejaimeduque.com', 'https://www.facebook.com/ParqueJaimeDuque', 'https://twitter.com/ParqJaimeduque?lang=es', 'https://www.instagram.com/parquejaimeduque/', 'https://www.youtube.com/channels?q=PARQUE+JAIME+DUQUE', 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'images/interestsites/site20180302203507603599792.jpg', 'images', '4.947064', '-73.960640', 'parque-jaime-duque', 'images/interestsites/site20180302203507827001551.jpg', 'images/interestsites/site20180302203507916822378.jpg', '<p style="text-align:justify">Abri&oacute; sus puertas el 27 de febrero de 1983, gracias a la labor filantr&oacute;pica del aviador <strong>Jaime Duque Grisales</strong>. Se trata de un parque tem&aacute;tico con divertidas atracciones y escenarios dedicados a resaltar el valor de la cultura, la educaci&oacute;n y la identidad, en p&uacute;blicos de todas las edades.</p>\r\n\r\n<p style="text-align:justify">Cuenta con cinco espacios principales: <strong>Puerto Caribe</strong>, inspirado en la costa atl&aacute;ntica colombiana; <strong>Zona de Juegos</strong>, con atracciones mec&aacute;nicas para j&oacute;venes y mayores; <strong>Bioparque Wakat&aacute;</strong>, que cuenta con 450 animales de 100 especies diferentes; el <strong>Paseo de la Cultura</strong>, donde se brinda un recorrido por la historia, la geograf&iacute;a y las letras de Colombia y el mundo; as&iacute; como el <strong>Jard&iacute;n de los Monumentos</strong>, espacio dedicado a exaltar la identidad nacional y humana, con estructuras dedicadas a la nacionalidad, la espiritualidad, la historia y el mundo.</p>\r\n\r\n<p style="text-align:justify">En esta &uacute;ltima secci&oacute;n se encuentra el <strong>Taj Mahal</strong>, que en sus tres pisos alberga colecciones de obras de arte cl&aacute;sico, un recorrido que narra la independencia de Colombia y una exposici&oacute;n de insectos gigantes, conocida como &ldquo;El Mundo de Dr. Ariel&rdquo;.</p>', 'parque', 'activo', '2018-02-17 05:16:00', '2018-03-03 03:35:07'),
(15, 'Mina de Sal de Nemocón', 1, 26, 'Cl. 2 # 0-05, Nemocón', '(1) 854 4120', 'http://www.minadesal.com', 'https://www.facebook.com/MinadeSal/', 'https://twitter.com/minadesal', 'https://www.instagram.com/minadesaldenemocon/', 'https://www.youtube.com/channel/UCir3IxQSnTflw1p_0b3KovQ', 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'images/interestsites/site20180302180110559247194.jpg', 'images', '5.063839', '-73.874753', 'mina-de-sal-de-nemocon', 'images/interestsites/site201803021801101161113460.jpg', 'images/interestsites/site20180302180730104183655.jpg', '<p style="text-align:justify">Es el principal atractivo tur&iacute;stico del municipio y el que mejor representa la estrecha relaci&oacute;n cultural entre sus pobladores y la explotaci&oacute;n de este preciado mineral. Se trata de una antigua mina de sal, a 80 metros de profundidad bajo tierra, que fue reacomodada para ofrecer un incre&iacute;ble recorrido, ambientado con luces y atractivos naturales, como las Cascadas de sal y la Ciudad de las Estalactitas, formadas por copos de sal que se cristalizan en el ambiente h&uacute;medo de los socavones; el Tanque Santa B&aacute;rbara, con el espejo natural de salmuera subterr&aacute;neo m&aacute;s grande del mundo, o la r&eacute;plica de la c&aacute;psula F&eacute;nix 2, con la que fueron rescatados, en el a&ntilde;o 2010, los 33 obreros de la mina de San Jos&eacute;, en Chile. <strong>Horario</strong> Lu.-do.: 9 a.m. a 5 p.m.</p>', 'lugar,mina,sal,nemocón', 'activo', '2018-02-17 05:18:38', '2018-03-03 01:07:30'),
(16, 'Parque Arqueológico Piedras del Tunjo', 3, 31, 'Km 40 vía Mosquera-Madrid', '(1) 842 18 08', NULL, NULL, NULL, NULL, NULL, 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'images/interestsites/site2018030220034214859951.jpg', 'images', '4.816109', '-74.346284', 'parque-arqueologico-piedras-del-tunjo', 'images/interestsites/site201803022003421676382617.jpg', 'images/interestsites/site201803022003421979576441.jpg', '<p style="text-align:justify">Es el &uacute;nico del pa&iacute;s ubicado en el casco urbano de un municipio. Aqu&iacute;, en cerca de 60 grandes rocas, se exhiben muestras de pinturas rupestres hechas por civilizaciones precolombinas, que plasmaron en sus ilustraciones objetos de la naturaleza de los cuales, quiz&aacute;, eran devotos. Las m&aacute;s representativas son &ldquo;La rana&rdquo; y &ldquo;El presidente&rdquo;.</p>\r\n\r\n<p style="text-align:justify">Por su cercan&iacute;a con la ciudad capital, el tambi&eacute;n conocido como <strong>Parque Arqueol&oacute;gico de Facatativ&aacute;</strong>, es un destino predilecto para los bogotanos que lo visitan los fines de semana, en pos de sus amplias zonas verdes, ideales para actividades y planes campestres.</p>', 'parque,piedras,tunjo,facatativa', 'activo', '2018-02-17 05:23:00', '2018-03-03 03:03:42'),
(17, 'Parque Natural Chicaque', 4, 32, 'km 21 variante La Mesa- Soacha', '(1) 368 3114', NULL, NULL, NULL, NULL, NULL, 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'images/interestsites/site201803022039471095154860.jpg', 'images', '4.606344', '-74.304754', 'parque-natural-chicaque', 'images/interestsites/site20180302203947484342609.jpg', 'images/interestsites/site201803022039471435251091.jpg', '<p style="text-align:justify">Inaugurado en abril de 1990, ofrece en las afueras de Bogot&aacute; un contacto &iacute;ntimo con el medio ambiente. En su bosque de niebla, extendido entre los 2000 y 2700 msnm, podr&aacute; recorrer siete senderos naturales; visitar las tres quebradas del parque, realizar cabalgatas y tambi&eacute;n actividades de aventura, como tirolesa o arborismo.</p>\r\n\r\n<p style="text-align:justify">Tambi&eacute;n puede avistar <strong>halcones, loros, colibr&iacute;es, pericos, urracas, golondrinas y azulejos</strong>, entre otras cientos de especies que alberga la reserva.</p>\r\n\r\n<p style="text-align:justify">Para quienes deseen alojarse en el parque, se ofrecen servicios de hospedaje en <em>campings</em>, caba&ntilde;as, nidos entre los &aacute;rboles y refugios, con capacidad para 35 personas.</p>', 'parque,natural,chicaque,soacha', 'activo', '2018-02-17 05:25:03', '2018-03-03 03:39:48'),
(18, 'PNN Chingaza', 4, 1, 'Calle 74 No. 11 – 81, piso 1, Bogotá', '(1) 353 2400 ext. 138-139', 'http://www.parquesnacionales.gov.co', NULL, NULL, NULL, NULL, 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'images/interestsites/site2018030220550936681593.jpg', 'images', '4.580350', '-73.756383', 'pnn-chingaza', 'images/interestsites/site201803022055091087156490.jpg', 'images/interestsites/site20180302205509689991781.jpg', '<p style="text-align:justify">Las fuentes h&iacute;dricas de esta reserva natural surten el 80 % del agua que consumen los bogotanos y habitantes de los municipios aleda&ntilde;os. H&aacute;bitat natural del amenazado oso de anteojos, es lugar para estar en contacto con la naturaleza recorriendo sus senderos, que dirigen a puntos como las cuchillas de Siecha y las lagunas de Buitrago, Laguna Seca y Chingaza. Antes de visitarlo, se recomienda verificar el estado de acceso al parque y solicitar los permisos de ingreso con al menos 15 d&iacute;as de antelaci&oacute;n; lleve un impermeable y ropa t&eacute;rmica delgada, pues llueve y las temperaturas pueden variar de forma dr&aacute;stica.</p>', 'reserva,natural,chingaza', 'activo', '2018-02-17 05:30:22', '2018-03-03 03:55:09'),
(19, 'Cueva de los Murciélagos', 1, 43, 'Vereda Peñas Blancas', '314 304 5575', NULL, NULL, NULL, NULL, NULL, 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'images/interestsites/site20180302164717216251277.jpg', 'images', '3.984790', '-74.484181', 'cueva-de-los-murcielagos', 'images/interestsites/site201803021647171269190232.jpg', 'images/interestsites/site2018030218050580518991.jpg', '<p style="text-align:justify">Esta caverna es ideal para realizar una de las actividades favoritas de los visitantes al <strong>municipio de Cabrera</strong>: la espeleolog&iacute;a o exploraci&oacute;n de cuevas a las que solo se atreven a ingresar los m&aacute;s valientes.</p>\r\n\r\n<p style="text-align:justify">Llegar a este destino puede tomar hasta 45 minutos, en una caminata que pasa por el <strong>caser&iacute;o Pe&ntilde;as Blancas</strong>, donde es posible observar una de las panor&aacute;micas m&aacute;s hermosas del <strong>r&iacute;o Sumapaz</strong>.<strong> </strong></p>', 'cueva,murciélagos,cabrera,cundinamarca,colombia', 'activo', '2018-02-18 04:37:10', '2018-03-03 01:05:05'),
(20, 'Aventurando Tours', 2, 43, 'Cra. 3 N. 4 - 48 Venecia, Cundinamarca', '314 3045575', NULL, 'https://www.facebook.com/aventurandotours/', 'https://twitter.com/AventurandoTour', 'https://www.instagram.com/aventurandotours/', NULL, 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'null', 'images', '1', '2', 'aventurando-tours', '', 'images/interestsites/site201803022145141360071204.jpg', '<p><strong>AventurandoTours</strong>, agencia operadora de turismo de naturaleza y aventura, empresa legalmente constituida, con registro nacional de turismo RNT 30381, expedido en el 2012, a lo largo de estos a&ntilde;os nos hemos especializado en la actividad de canyoning, en la cual estamos en proceso de certificaci&oacute;n de la norma NTS AV 015 del viceministerio de turismo de Colombia.</p>\r\n\r\n<p>En la actualidad contamos con equipo de gu&iacute;as, capacitado, con experiencia y conocimiento de la regi&oacute;n y las diversas rutas que tenemos para ustedes, somos socios fundadores de la Asociaci&oacute;n colombiana de exploraci&oacute;n y descenso de ca&ntilde;ones COLOMBIAN CANYONS.</p>', 'operadores', 'activo', '2018-02-18 04:41:49', '2018-03-03 04:45:14'),
(21, 'Zoológico Santacruz', 1, 1, 'San Antonio del Tequendama, Cundinamarca', '310 329 88 80', 'http://www.zoosantacruz.org', 'https://www.facebook.com/zoosantacruz/', 'https://twitter.com/zoosantacruz', NULL, NULL, 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'images/interestsites/site201803022108141596909952.jpg', 'images', '4.583510', '-74.333130', 'zoologico-santacruz', 'images/interestsites/site201803022108141875236737.jpg', 'images/interestsites/site20180302210814760384222.jpg', '<p style="text-align:justify">A solo hora y media de Bogot&aacute;, pasando el maravilloso <strong>salto del Tequendama</strong>,<strong> </strong>encontrar&aacute; este espacio dise&ntilde;ado para aprender sobre la fauna de diversos lugares del mundo. El <strong>zool&oacute;gico Santacruz </strong>alberga diversas especies como el tigre blanco, el c&oacute;ndor de los Andes y el ant&iacute;lope acu&aacute;tico, entre otras especies animales que lo fascinar&aacute;n.</p>\r\n\r\n<p style="text-align:justify">Adem&aacute;s, es reconocido en el &aacute;mbito nacional e internacional por desarrollar programas con la comunidad, buscando su compromiso para cuidar, conocer y entender la biodiversidad.</p>', 'zoológico,zoo,santacruz,san antonio,tequendama,cundinamarca', 'activo', '2018-02-18 04:56:47', '2018-03-03 04:08:14'),
(22, 'Parque Ecológico Pionono', 3, 24, 'Sopó, Cundinamarca', '(1) 857 2802', NULL, NULL, NULL, NULL, NULL, 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'images/interestsites/site201803022017351460421585.jpg', 'images', '4.909211', '-73.918598', 'parque-ecologico-pionono', 'images/interestsites/site201803022017351807063134.jpg', 'images/interestsites/site201803022017351261795685.jpg', '<p style="text-align:justify">Con 60 hect&aacute;reas de parque y 680 de reserva natural, invita a los turistas a aventurarse con actividades como caminatas ecol&oacute;gicas por dos senderos (Los Encenillos y Los Frailejones), llegar a los tres miradores en el cerro de las &Aacute;guilas para admirar la mejor panor&aacute;mica de la sabana y disfrutar de las zonas de <em>camping</em>, con hornillas. La zona de reserva tiene por objetivo la recuperaci&oacute;n de la flora y fauna nativas, los gu&iacute;as o int&eacute;rpretes ambientales le contar&aacute;n historias y datos sobre bot&aacute;nica y cuidado del medio ambiente.</p>', 'parque,ecológico,pionono,municipio,sopó,cundinamarca', 'activo', '2018-02-19 22:30:44', '2018-03-03 03:17:35'),
(23, 'Finkana Parque Temático', 3, 23, 'Km 4, via Briceño - Zipaquirá', '313 418 3647', 'http://www.parquefinkana.com', 'https://www.facebook.com/PARQUE.FINKANA/', NULL, 'https://www.instagram.com/finkana/', NULL, 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'images/interestsites/site201803021744002068153580.jpg', 'images', '4.975602', '-73.960586', 'finkana-parque-tematico', 'images/interestsites/site20180302174400903168079.jpg', 'images/interestsites/site20180302180519599899634.jpg', '<p style="text-align:justify">Este parque tem&aacute;tico ecuestre y pecuario ofrece la oportunidad a las familias de conocer la vida del campo, aproxim&aacute;ndose a los animales de granja, especialmente a los caballos. Ni&ntilde;os y adultos tienen la oportunidad de ver de cerca y acariciar conejos, terneros, ovejas, cuyes, gallinas; adem&aacute;s pueden orde&ntilde;ar vacas.</p>\r\n\r\n<p style="text-align:justify">Tambi&eacute;n hay exhibici&oacute;n de caballos espa&ntilde;oles, huerta, escuela de equitaci&oacute;n y servicio de restaurante.</p>', 'parque,temático,finkana,municipio,zipaquirá', 'activo', '2018-02-19 22:36:31', '2018-03-03 01:05:19'),
(24, 'Vieco Vida y Ecología', 2, 23, 'Calle 3 # 3-07', '305 724 1417', NULL, 'https://www.facebook.com/VIECO-Vida-y-Ecología-1659333791027466/', NULL, 'https://www.flickr.com/photos/137383910@N02/', NULL, 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'null', 'images', '1', '2', 'vieco-vida-y-ecologia', 'images/interestsites/site20180302182308166779093.jpg', 'images/interestsites/site201803022153061937861945.jpg', '<p>&iexcl;Con nosotros tu meta est&aacute; cada d&iacute;a m&aacute;s cerca! Natural Training te ofrece excelente entrenamiento al aire libre y control nutricional. Cambia tu vida.</p>', 'operadores', 'activo', '2018-02-19 23:36:32', '2018-03-03 04:53:06'),
(25, 'Club Trango Aventura', 2, 1, 'Vereda Cacicazgo, Suesca', '320 221 5417', NULL, 'https://www.facebook.com/clubtrangoaventura/', 'https://twitter.com/clubtrangoavent?lang=es', 'https://www.instagram.com/clubtrangoaventura/', NULL, 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'null', 'images', '1', '2', 'club-trango-aventura', '', 'images/interestsites/site2018030222034347661775.jpg', '<p>Escalar en el mundo de los negocios no fue suficiente para satisfacer las ansias de adrenalina de esta empresa, que decidi&oacute; darse un empuje adicional a trav&eacute;s del turismo de aventura, especialmente en la escalada en roca y alta monta&ntilde;a. Y aunque lo extremo es parte importante de sus actividades, el &eacute;nfasis est&aacute; en el desarrollo del conocimiento personal, el trabajo en equipo, el liderazgo y la toma de decisiones. Tambi&eacute;n cuenta con cursos y clases de escalada en roca deportiva y cl&aacute;sica tradicional.</p>', 'operadores', 'activo', '2018-02-19 23:52:26', '2018-03-03 05:03:43'),
(26, 'G&G Confort Tours', 2, 1, 'Cra 21 No. 3-28, Mosquera', '3132636359', 'https://www.confortours.com.co', 'https://www.facebook.com/gg.conforttours/', NULL, NULL, NULL, 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'null', 'images', '1', '2', 'g-g-confort-tours', '', 'images/interestsites/site20180302223056679422419.jpg', '<p>G&amp;G CONFORT TOURS es una agencia de viajes que ofrece servicios de organizaci&oacute;n y realizaci&oacute;n de viajes, planes e itinerarios tur&iacute;sticos y de entretenimiento.</p>', 'operadores', 'activo', '2018-02-20 01:22:44', '2018-03-03 05:30:56'),
(27, 'Massai Mara', 2, 11, 'Cr. 3 # 13-64, La Vega', '3213179656', 'http://www.massai-mara.co', NULL, NULL, NULL, NULL, 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'null', 'images', '1', '2', 'massai-mara', '', 'images/interestsites/site20180302224515428307006.jpg', '<p>El vuelo en parapente biplaza es la forma m&aacute;s segura, c&oacute;moda y divertida de disfrutar la sensaci&oacute;n de volar. Ven a disfrutar el mundo del vuelo de forma r&aacute;pida y sencilla, apto para todas las edades. Si sufres de alguna discapacidad no te preocupes adaptaremos nuestros equipos para tu condici&oacute;n. Los ni&ntilde;os pueden volar bajo la autorizaci&oacute;n de sus padres o su tutor.</p>\r\n\r\n<p>Si sufres de problemas card&iacute;acos, asma o osteoporosis avanzada no puedes realizar el deporte por tu seguridad.</p>\r\n\r\n<p>No pierdas la oportunidad de vivir la forma mas extraordinaria de cumplir tu sue&ntilde;o de volar con la mejor seguridad, en el mejor escenario en la Vega Cundinamarca a 28&deg; Grados de Temperatura excelentes paisajes y la mas hermosa verde vista.</p>', 'operadores', 'activo', '2018-02-20 01:31:05', '2018-03-03 05:45:15'),
(28, 'Viajes y Turismo Yinpao', 2, 31, 'Cle 6 N. 3 - 73, lc. 114, Facatativá', '313 203 9932', NULL, 'https://www.facebook.com/Viajes-Y-Turismo-Yinpao-El-Arte-De-Viajar-Facatativá-308221809242010/', NULL, NULL, NULL, 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'null', 'images', '1', '2', 'viajes-y-turismo-yinpao', '', 'images/interestsites/site20180302230653172198159.jpg', '<p>Agencia de Viajes dise&ntilde;ada a promover el amor propio por el pratimonio cultural, y a establecer a Colombia como destino Turistico a nivel internacional.</p>', 'operadores', 'activo', '2018-02-20 01:38:07', '2018-03-03 06:06:53'),
(29, 'Dorado Tours O.V.', 2, 24, 'Carrera 4 # 5-10, Sopó', 'Carrera 4 # 5-10', 'http://viajesdoradotours.com', 'https://www.facebook.com/viajesdoradotours/', 'https://twitter.com/viajesdoradotou', 'https://www.instagram.com/viajesdoradotours/?hl=es', NULL, 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'null', 'images', '1', '2', 'dorado-tours-o-v', '', 'images/interestsites/site201803022220531218052547.jpg', '<p><strong>Viajes DoradoTours</strong> dise&ntilde;a servicios tur&iacute;sticos innovadores a nivel nacional e internacional para personas y empresas, garantizando la satisfacci&oacute;n de sus clientes; con excelentes aliados estrat&eacute;gicos, respaldados por capital humano &iacute;ntegro y competitivo; a trav&eacute;s de un servicio profesional, c&aacute;lido, personalizado y con acompa&ntilde;amiento permanente.</p>', 'operadores', 'activo', '2018-02-20 01:42:28', '2018-03-03 05:20:53'),
(30, 'Travel World Colombia Agencia de Viajes', 2, 34, 'Transversal 12 # 22 - 42, local 136, Centro comercial Manila, Fusagasugá', '320 489 1930', NULL, 'https://www.facebook.com/Travel-World-Colombia-340932725993639/', NULL, NULL, NULL, 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'null', 'images', '1', '2', 'travel-world-colombia-agencia-de-viajes', '', 'images/interestsites/site201803022301231108573898.jpg', '<p>Es hora de programar tus vacaciones &iexcl;Con TRAVEL WORLD COLOMBIA cada d&iacute;a una Oferta diferente!</p>', 'operadores', 'activo', '2018-02-20 01:43:34', '2018-03-03 06:01:23'),
(31, 'Colombia Mi Tierra', 2, 27, 'Cl. 11 # 11-49 lc. 33', '(1) 8624394', NULL, NULL, NULL, NULL, NULL, 'http://www.airpano.ru/files/Iceland-Best-Landscapes/2-3-2', 'null', 'images', '1', '2', 'colombia-mi-tierra', '', '', '<p>Como Agencia de viajes, turismo y transporte (tur&iacute;stico, escolar y empresarial) con oficina principal en Ch&iacute;a, Cundinamarca, Colombia. Ofrecemos nuestros cl&uacute;sters tur&iacute;sticos para todo el territorio nacional as&iacute; como servicios de transporte terrestre y veh&iacute;culos y apoyados por un guia compa&ntilde;ante Conozca, viaje a Colombia, pa&iacute;s maravilloso que invita a recorrer y conocer esta maravillosa tierra.</p>', 'operadores', 'activo', '2018-02-20 01:45:54', '2018-03-03 05:07:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `interest_site_categories`
--

CREATE TABLE IF NOT EXISTS `interest_site_categories` (
  `id_category` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `default_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `default_icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_category`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `interest_site_categories`
--

INSERT INTO `interest_site_categories` (`id_category`, `category_name`, `default_image`, `default_icon`, `state`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Lugar', 'images/interestsitescategory/category201802152159101636339271.png', 'images/interestsitescategory/category201802152159102023217565.png', 'activo', 'lugar', '2018-02-15 21:01:39', '2018-02-16 04:59:10'),
(2, 'Operadores', 'images/interestsitescategory/category201802151640241908801023.jpg', 'images/interestsitescategory/category201802151640241312486750.png', 'activo', 'operadores', '2018-02-15 21:40:24', '2018-02-15 21:40:24'),
(3, 'Parque', 'images/interestsitescategory/category20180215220857200383412.png', 'images/interestsitescategory/category20180215220857453192021.png', 'activo', 'parque', '2018-02-16 05:08:57', '2018-02-16 05:08:57'),
(4, 'Reserva Natural', 'images/interestsitescategory/category201802152210132103115867.png', 'images/interestsitescategory/category20180215221013105241349.png', 'activo', 'reserva-natural', '2018-02-16 05:10:13', '2018-02-16 05:10:13'),
(5, 'Museo', 'images/interestsitescategory/category201802152211291195318264.png', 'images/interestsitescategory/category20180215221129994403597.png', 'activo', 'museo', '2018-02-16 05:11:29', '2018-02-16 05:11:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `interest_site_images`
--

CREATE TABLE IF NOT EXISTS `interest_site_images` (
  `id_site_image` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_site` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_site_image`),
  KEY `interest_site_images_fk_site_foreign` (`fk_site`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=27 ;

--
-- Volcado de datos para la tabla `interest_site_images`
--

INSERT INTO `interest_site_images` (`id_site_image`, `link_image`, `fk_site`, `created_at`, `updated_at`) VALUES
(1, 'images/interestsites/site20180302180927591973245.jpg', 13, '2018-03-03 01:09:27', '2018-03-03 01:09:27'),
(2, 'images/interestsites/site2018030218175058212597.jpg', 19, '2018-03-03 01:17:50', '2018-03-03 01:17:50'),
(3, 'images/interestsites/site20180302181833324044448.jpg', 23, '2018-03-03 01:18:33', '2018-03-03 01:18:33'),
(4, 'images/interestsites/site20180302181901515548056.jpg', 12, '2018-03-03 01:19:01', '2018-03-03 01:19:01'),
(5, 'images/interestsites/site20180302181930700662150.jpg', 15, '2018-03-03 01:19:30', '2018-03-03 01:19:30'),
(6, 'images/interestsites/site201803022004311085992020.jpg', 16, '2018-03-03 03:04:34', '2018-03-03 03:04:34'),
(7, 'images/interestsites/site201803022018161103070117.jpg', 22, '2018-03-03 03:18:16', '2018-03-03 03:18:16'),
(8, 'images/interestsites/site20180302203526135213502.jpg', 14, '2018-03-03 03:35:26', '2018-03-03 03:35:26'),
(9, 'images/interestsites/site201803022040341487810662.jpg', 17, '2018-03-03 03:40:34', '2018-03-03 03:40:34'),
(10, 'images/interestsites/site20180302204214727213327.jpg', 3, '2018-03-03 03:42:14', '2018-03-03 03:42:14'),
(11, 'images/interestsites/site201803022055351820349629.jpg', 18, '2018-03-03 03:55:35', '2018-03-03 03:55:35'),
(12, 'images/interestsites/site20180302210003307304782.jpg', 11, '2018-03-03 04:00:03', '2018-03-03 04:00:03'),
(13, 'images/interestsites/site201803022147231502878040.jpg', 7, '2018-03-03 04:47:23', '2018-03-03 04:47:23'),
(14, 'images/interestsites/site2018030221474523094980.jpg', 20, '2018-03-03 04:47:45', '2018-03-03 04:47:45'),
(15, 'images/interestsites/site201803022159491718091395.jpg', 5, '2018-03-03 04:59:49', '2018-03-03 04:59:49'),
(16, 'images/interestsites/site20180302220405886690365.jpg', 25, '2018-03-03 05:04:05', '2018-03-03 05:04:05'),
(17, 'images/interestsites/site201803022214381319158974.jpg', 6, '2018-03-03 05:14:38', '2018-03-03 05:14:38'),
(18, 'images/interestsites/site201803022221281571770965.jpg', 29, '2018-03-03 05:21:28', '2018-03-03 05:21:28'),
(19, 'images/interestsites/site201803022227281153456602.jpg', 9, '2018-03-03 05:27:28', '2018-03-03 05:27:28'),
(20, 'images/interestsites/site201803022231261430924920.jpg', 26, '2018-03-03 05:31:26', '2018-03-03 05:31:26'),
(21, 'images/interestsites/site201803022245351353948179.jpg', 27, '2018-03-03 05:45:35', '2018-03-03 05:45:35'),
(22, 'images/interestsites/site20180302225337360757021.jpg', 10, '2018-03-03 05:53:37', '2018-03-03 05:53:37'),
(23, 'images/interestsites/site201803022257221651091252.jpg', 4, '2018-03-03 05:57:22', '2018-03-03 05:57:22'),
(24, 'images/interestsites/site201803022302081020253696.jpg', 30, '2018-03-03 06:02:08', '2018-03-03 06:02:08'),
(25, 'images/interestsites/site201803022304542005741811.jpg', 28, '2018-03-03 06:04:54', '2018-03-03 06:04:54'),
(26, 'images/interestsites/site20180302231117111560464.jpg', 8, '2018-03-03 06:11:17', '2018-03-03 06:11:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lyingoperator_services`
--

CREATE TABLE IF NOT EXISTS `lyingoperator_services` (
  `id_lyingoperator_service` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_service` int(10) unsigned NOT NULL,
  `fk_operator` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_lyingoperator_service`),
  KEY `lyingoperator_services_fk_service_foreign` (`fk_service`),
  KEY `lyingoperator_services_fk_operator_foreign` (`fk_operator`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=42 ;

--
-- Volcado de datos para la tabla `lyingoperator_services`
--

INSERT INTO `lyingoperator_services` (`id_lyingoperator_service`, `fk_service`, `fk_operator`, `created_at`, `updated_at`) VALUES
(10, 1, 2, NULL, NULL),
(11, 5, 2, NULL, NULL),
(12, 7, 2, NULL, NULL),
(13, 13, 4, NULL, NULL),
(14, 8, 5, NULL, NULL),
(15, 2, 7, NULL, NULL),
(16, 6, 7, NULL, NULL),
(17, 18, 7, NULL, NULL),
(18, 2, 8, NULL, NULL),
(19, 6, 8, NULL, NULL),
(20, 18, 8, NULL, NULL),
(21, 30, 8, NULL, NULL),
(22, 25, 9, NULL, NULL),
(23, 27, 9, NULL, NULL),
(24, 23, 24, NULL, NULL),
(25, 22, 25, NULL, NULL),
(26, 25, 25, NULL, NULL),
(27, 27, 25, NULL, NULL),
(28, 31, 26, NULL, NULL),
(29, 13, 27, NULL, NULL),
(30, 31, 28, NULL, NULL),
(31, 31, 29, NULL, NULL),
(32, 31, 30, NULL, NULL),
(33, 31, 31, NULL, NULL),
(34, 1, 20, NULL, NULL),
(35, 6, 20, NULL, NULL),
(36, 2, 6, NULL, NULL),
(37, 22, 6, NULL, NULL),
(38, 23, 6, NULL, NULL),
(39, 1, 10, NULL, NULL),
(40, 6, 10, NULL, NULL),
(41, 22, 10, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=60 ;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(34, '2014_10_12_000000_create_users_table', 1),
(35, '2014_10_12_100000_create_password_resets_table', 1),
(36, '2018_01_22_154619_create_admins_table', 1),
(37, '2018_01_22_154620_create_admin_password_resets_table', 1),
(38, '2018_01_25_212046_create_municipalities_table', 1),
(39, '2018_01_29_170933_create_municipality_images_table', 1),
(40, '2018_01_29_180040_create_interest_site_categories_table', 1),
(41, '2018_01_29_214958_create_interest_sites_table', 1),
(42, '2018_02_01_175959_create_interest_site_images_table', 1),
(43, '2018_02_06_183732_create_routes_table', 1),
(44, '2018_02_06_184127_create_relation_routes_table', 1),
(45, '2018_02_06_211404_create_fairs_table', 1),
(46, '2018_02_07_210812_create_fair_images_table', 1),
(47, '2018_02_08_172030_create_portable_documents_table', 1),
(48, '2018_02_08_174141_create_videos_table', 1),
(49, '2018_02_08_204645_create_user_password_resets_table', 1),
(50, '2018_02_08_211010_create_operators_table', 1),
(51, '2018_02_08_211011_create_operator_password_resets_table', 1),
(52, '2018_02_09_165451_create_service_categories_table', 1),
(53, '2018_02_09_165625_create_ecosystem_categories_table', 1),
(54, '2018_02_09_165823_create_services_table', 1),
(55, '2018_02_09_165903_create_service_operators_table', 1),
(56, '2018_02_09_170002_create_contacts_table', 1),
(57, '2018_02_09_170049_create_schedules_table', 1),
(58, '2018_02_09_170744_create_service_pictures_table', 1),
(59, '2018_02_12_212803_create_wallpapers_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipalities`
--

CREATE TABLE IF NOT EXISTS `municipalities` (
  `id_municipality` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `municipality_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `multimedia_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_last_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `downloadimage` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iframe` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fk_last_edition` int(11) NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_municipality`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=50 ;

--
-- Volcado de datos para la tabla `municipalities`
--

INSERT INTO `municipalities` (`id_municipality`, `municipality_name`, `description`, `multimedia_type`, `latitude`, `longitude`, `slug`, `type_last_user`, `link_image`, `downloadimage`, `iframe`, `fk_last_edition`, `keywords`, `state`, `created_at`, `updated_at`) VALUES
(1, 'Suesca', '<p style="text-align:justify">El turismo de aventura y ecol&oacute;gico figuran como los principales atractivos de este municipio, gracias a parajes naturales como <strong>Las Rocas</strong>, donde es posible practicar escalada de piedra, o el <strong>ca&ntilde;&oacute;n de La Lechuza</strong>, que se recorre a bordo de una embarcaci&oacute;n.</p>\r\n\r\n<p style="text-align:justify">Entre sus tesoros culturales se encuentran el <strong>Templo de Nuestra Se&ntilde;ora del Rosario</strong>, construido en el siglo XVII, y las artesan&iacute;as elaboradas con tejidos naturales, madera tallada, piedra, lana virgen y materiales reciclables. Est&aacute; situado por encima de los 2500 msnm y dista 60 kil&oacute;metros de Bogot&aacute;.&nbsp;</p>', 'images', '5.103929', '-73.798632', 'suesca', 'admin', 'images/municipalities/Municipality_2018022819400229161888.jpg', 'images/municipalities/Municipality_201802152253401676863012.JPG', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,suesca,almeidas,cundinamarca,colombia,provincia', 'activo', '2018-02-15 20:53:11', '2018-03-01 02:40:03'),
(2, 'Agua de Dios', '<p style="text-align:justify">Fundado en 1870, en terrenos del presidente Manuel Murillo Toro, este pueblo es quiz&aacute;s uno de los secretos culturales mejor guardados del departamento. Desde su creaci&oacute;n y hasta 1960 fue tristemente c&eacute;lebre por ser un centro de reclusi&oacute;n para enfermos de Hansen de todo el pa&iacute;s.</p>\r\n\r\n<p style="text-align:justify">Pese a los injustos tratos del Estado, los enfermos estructuraron en el encierro una sociedad compleja, con instituciones y una moneda propia, llamada &lsquo;Coscoja&rsquo;. De entonces, se conservan valiosas edificaciones como el <strong>puente de los Suspiros</strong>, el <strong>teatro vargas tejada</strong>, el <strong>colegio miguel unia, </strong>los albergues del <strong>sanatorio de agua de dios </strong>y la <strong>casa Museo Luis A. Calvo</strong>, todos declarados Patrimonio Nacional en el a&ntilde;o 2011, por el Congreso de la Rep&uacute;blica.</p>', 'images', '4.376688', '-74.669800', 'agua-de-dios', 'admin', 'images/municipalities/Municipality_201803011924031368153252.jpg', 'images/municipalities/Municipality_20180220181343266396307.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,Agua de Dios,provincia,alto magdalena,Cundinamarca,Colombia', 'activo', '2018-02-15 21:20:28', '2018-03-02 02:24:03'),
(3, 'Tocaima', '<p style="text-align:justify">Fundado en 1544, por el soldado Hern&aacute;n Venegas Carrillo de las tropas de Gonzalo Jim&eacute;nez de Quesada, en territorio habitado por los ind&iacute;genas panches, es una poblaci&oacute;n que se dedica a la agricultura y el turismo, pues por sus 28&deg;C de temperatura promedio suele recibir visitantes provenientes principalmente de Bogot&aacute;.</p>\r\n\r\n<p style="text-align:justify">En 1972, Manuel Mendoza, hombre dedicado a la extracci&oacute;n de yeso, encontr&oacute; los restos &oacute;seos de lo que m&aacute;s tarde fue identificado como un mastodonte. Estos huesos y otras piezas arqueol&oacute;gicas se exhiben en el <strong>Museo Paleontol&oacute;gico de Pubenza</strong>.<strong>&nbsp;</strong></p>', 'images', '4.457055', '-74.633923', 'tocaima', 'admin', 'images/municipalities/Municipality_20180301193232285346788.jpg', 'images/municipalities/Municipality_201803011932321088711237.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,Tocaima,provincia,Alto Magdalena,Cundinamarca,Colombia', 'activo', '2018-02-16 04:59:00', '2018-03-02 02:32:32'),
(4, 'Girardot', '<p style="text-align:justify">Entre los municipios del Alto Magdalena, <strong>Girardot</strong> es el m&aacute;s poblado y el que ostenta la mayor infraestructura tur&iacute;stica. Fundado en 1881 por el presidente Rafael N&uacute;&ntilde;ez, en su tercer mandato, hoy su oferta de hospedaje supera las 3500 camas en centros vacacionales y hoteles. Su clima c&aacute;lido es atractivo para los habitantes de zonas con temperaturas m&aacute;s bajas de Boyac&aacute; y Cundinamarca.</p>\r\n\r\n<p style="text-align:justify">Tambi&eacute;n se le conoce como la <strong>Ciudad de las Acacias</strong>, por estar pobladas sus calles con estos frondosos &aacute;rboles. Visite la <strong>plaza de mercado</strong>, construida en 1948 y declarada Patrimonio Nacional en 1990.&nbsp;</p>', 'images', '4.304596', '-74.803141', 'girardot', 'admin', 'images/municipalities/Municipality_201803011918201812053070.jpg', 'images/municipalities/Municipality_201803011916041511729266.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,girardot,provincia,alto magdalena,cundinamarca,colombia', 'activo', '2018-02-16 05:00:46', '2018-03-02 02:18:20'),
(5, 'Guaduas', '<p style="text-align:justify">Llamado Villa de San Miguel de Guaduas en la &eacute;poca de la Colonia, este municipio posee numerosas edificaciones hist&oacute;ricas que muestran las t&eacute;cnicas arquitect&oacute;nicas y los materiales de hace m&aacute;s de 200 a&ntilde;os. Es tambi&eacute;n conocido como la Casa de la Pola, en honor a la l&iacute;der independentista Policarpa Salavarrieta, que naci&oacute; all&iacute; a finales del siglo XVIII.</p>\r\n\r\n<p style="text-align:justify">Entre sus principales atractivos se encuentran la <strong>plaza de la Constituci&oacute;n</strong>, rodeada por calles adoquinadas y edificaciones coloniales; la <strong>catedral de San Miguel Arc&aacute;ngel</strong>, &uacute;ltimo templo construido en el pa&iacute;s durante la Colonia, y la <strong>casa museo de Policarpa Salavarrieta</strong>, que atesora objetos alusivos a la vida de la hero&iacute;na. Visite la <strong>bizcocher&iacute;a</strong> <strong>El N&eacute;ctar</strong>, fundada en 1901, y pruebe un delicioso brazo de reina o un bizcochuelo cubierto.</p>', 'images', '5.067291', '-74.595361', 'guaduas', 'admin', 'images/municipalities/Municipality_201802281735112026532585.jpg', 'images/municipalities/Municipality_201802281735111084405971.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,guaduas,provincia,bajo magdalena,cundinamarca,colombia,pueblo patrimonio', 'activo', '2018-02-16 05:04:03', '2018-03-01 00:35:40'),
(6, 'Puerto Salgar', '<p style="text-align:justify">Fundado por el expresidente Eustorgio Salgar, y bautizado en su honor, este c&aacute;lido municipio es ba&ntilde;ado por el principal r&iacute;o del pa&iacute;s, el Magdalena, de donde proviene uno de sus principales sustentos econ&oacute;micos: la pesca. Cuando las aguas del r&iacute;o bajan por el verano, la ganader&iacute;a es la actividad comercial que m&aacute;s desarrollo genera.</p>\r\n\r\n<p style="text-align:justify">Visite Puerto Salgar y aprecie la vegetaci&oacute;n de este lugar, entre los Andes y el Magdalena. Para darse un descanso del calor, visite la <strong>laguna del Coco</strong>, donde podr&aacute; darse un ba&ntilde;o y refrescarse en medio de un atractivo paraje natural.</p>', 'images', '5.467414', '-74.652693', 'puerto-salgar', 'admin', 'images/municipalities/Municipality_201802281724152059327999.jpg', 'images/municipalities/Municipality_20180228172415172161519.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 1, 'municipio,puerto salgar,provincia,bajo magdalena,cundinamarca,colombia', 'activo', '2018-02-16 05:07:06', '2018-03-06 03:23:24'),
(7, 'Nimaima', '<p style="text-align:justify"><span style="font-size:11.0pt">Los habitantes de este lugar, con 23 &deg;C de temperatura promedio, tienen como principales actividades econ&oacute;micas la agricultura y el turismo. Nimaima ofrece a sus visitantes una de las mejores infraestructuras en turismo de aventura y agroecol&oacute;gico de todo el departamento.</span></p>\r\n\r\n<p style="text-align:justify"><span style="font-size:11.0pt">Conozca parajes naturales como el <strong>alto de San Pablo</strong>, practique pesca deportiva en <strong>la laguna de Liverpool</strong>, b&aacute;&ntilde;ese en las aguas termales del <strong>r&iacute;o Pinzama</strong>; mida sus habilidades para el torrentismo en la <strong>quebrada la Berber&iacute;a</strong> y marav&iacute;llese con la imponencia de la <strong>cascada de Barandilla</strong>, con m&aacute;s de 70 m de altura.</span></p>', 'images', '5.126450', '-74.385550', 'nimaima', 'admin', 'images/municipalities/Municipality_20180228174303422095637.jpg', 'images/municipalities/Municipality_201802281743031637794977.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,nimaima,provincia,cundinamarca,colombia,gualivá', 'activo', '2018-02-16 05:40:19', '2018-03-01 00:43:03'),
(8, 'Sasaima', '<p style="text-align:justify">Sasaima atesora uno de los patrimonios culturales m&aacute;s importantes en la regi&oacute;n central del pa&iacute;s. En el lugar conocido como el <strong>Monolito Panche</strong>, en la vereda El Moj&oacute;n, se encuentran m&aacute;s de 400 petroglifos, en 94 hect&aacute;reas, que ilustran objetos y elementos de la naturaleza que fueron importantes para los ind&iacute;genas que poblaron esta zona.</p>\r\n\r\n<p style="text-align:justify"><strong>La cascada San Luis</strong>, con 30 metros de altura, es otro de los atractivos tur&iacute;sticos de este municipio, junto con la <strong>caverna del Indio</strong>, donde antiguamente se realizaban rituales de sepultura ind&iacute;gena.</p>', 'images', '4.963945', '-74.434042', 'sasaima', 'admin', 'images/municipalities/Municipality_201803012228071396211026.jpg', 'images/municipalities/Municipality_20180301222807831766799.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,provincia,cundinamarca,colombia,sasaima,gualivá', 'activo', '2018-02-16 05:40:31', '2018-03-02 05:33:10'),
(9, 'Utica', '<p style="text-align:justify">Luego de que en el a&ntilde;o 2011 una avalancha arrasara las carreteras de este municipio, dej&aacute;ndolo completamente incomunicado,&nbsp; gracias al tes&oacute;n y la amabilidad de su gente y al turismo de aventura &Uacute;tica ha renacido.</p>\r\n\r\n<p style="text-align:justify">Visite impresionantes parajes naturales como las <strong>cascadas de la Papaya</strong>, los <strong>altos de La Virgen y La Cruz</strong>, los <strong>termales de la quebrada Negra</strong> y los <strong>r&aacute;pidos del r&iacute;o Negro</strong>, excelentes para practicar <em>rafting</em> de forma segura.</p>', 'images', '5.187784', '-74.481457', 'utica', 'admin', 'images/municipalities/Municipality_201803012159171619195919.jpg', 'images/municipalities/Municipality_20180301215917924315775.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,útica,provincia,cundinamarca,colombia,gualivá', 'activo', '2018-02-16 22:32:38', '2018-03-02 05:04:40'),
(10, 'Villeta', '<p>Denominado La Ciudad Dulce de Colombia por su valiosa industria de ca&ntilde;a de az&uacute;car y productos derivados, como la panela, de la que Colombia es el segundo mayor productor en el mundo, luego de la India, Villeta (que significa &ldquo;peque&ntilde;a villa&rdquo;) cuenta con asombrosos paisajes monta&ntilde;osos, clima c&aacute;lido,&nbsp; refrescantes brisas que fluyen entre los valles de la cordillera de los Andes. Vis&iacute;tela&nbsp;y deguste una exquisita oferta de dulces aut&oacute;ctonos.</p>\r\n\r\n<p>Si viene en busca de actividades ecotur&iacute;sticas, conozca el <strong>salto de los Micos</strong> y sus siete cascadas con piscinas naturales o el <strong>balneario de la bocatoma de Bagazal</strong>, donde es costumbre realizar asados y paseos de olla. Por &uacute;ltimo visite el <strong>parque de la Molienda</strong> que alberga diferentes estatuas y monumentos que lo acercar&aacute;n a la cultura panelera de este pueblo trabajador.</p>', 'images', '5.011706', '-74.470362', 'villeta', 'admin', 'images/municipalities/Municipality_201802281658101426749596.jpg', 'images/municipalities/Municipality_201802281658101081781533.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,villeta,provincia,gualivá,cundinamarca,colombia', 'activo', '2018-02-16 22:39:02', '2018-02-28 23:58:10'),
(11, 'La Vega', '<p style="text-align:justify">Por su clima c&aacute;lido y su cercan&iacute;a con la capital del pa&iacute;s, este municipio se ha convertido en uno de los mayores receptores de turismo en el departamento, gracias a viajeros que buscan en el ambiente de <strong>La Vega</strong> un espacio de relajaci&oacute;n y descanso.</p>\r\n\r\n<p style="text-align:justify">Llamado en la &eacute;poca colonial San Juan de La Vega, es un destino para disfrutar en caba&ntilde;as y fincas campestres, acondicionadas con piscinas y numerosas acomodaciones para recibir grandes grupos de viajeros. Entre sus atractivos naturales se halla la <strong>laguna El Tabacal</strong>, que en sus alrededores conserva la historia de los antiguos pobladores de estas tierras, los ind&iacute;genas panches.<strong> </strong></p>', 'images', '5.000528', '-74.339439', 'la-vega', 'admin', 'images/municipalities/Municipality_20180228171411665368441.jpg', 'images/municipalities/Municipality_20180228171411829841597.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,provincia,gualivá,cundinamarca,colombia,la vega', 'activo', '2018-02-16 22:39:43', '2018-03-01 00:14:11'),
(12, 'San Francisco', '<p style="text-align:justify">Fundado el 22 de noviembre de 1957 como San Francisco de Sales, en honor al santo hom&oacute;nimo de la iglesia cat&oacute;lica, patrono de los escritores y periodistas, entre sus principales atractivos se encuentran actividades en caba&ntilde;as y fincas campestres, destinadas a la relajaci&oacute;n, descanso y recreaci&oacute;n de sus visitantes, as&iacute; como el <strong>Jard&iacute;n Encantado</strong>, donde podr&aacute; apreciar cerca de 23 especies de colibr&iacute;es.</p>', 'images', '4.974252', '-74.291386', 'san-francisco', 'admin', 'images/municipalities/Municipality_20180228173054612059543.jpg', 'images/municipalities/Municipality_20180228173054137182876.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,provincia,gualivá,cundinamarca,colombia,san francisco', 'activo', '2018-02-16 22:40:02', '2018-03-01 00:30:54'),
(13, 'La Calera', '<p>Este municipio de la provincia del <strong>Guavio</strong>, se encuentra resguardado entre los cerros orientales de Bogot&aacute;. Es precisamente de esta ciudad que proviene la mayor&iacute;a de sus visitantes, en busca de las maravillosas panor&aacute;micas de la capital que ofrecen los miradores apostados a lo largo de la v&iacute;a.</p>\r\n\r\n<p>All&iacute;, disfrute de la paz y la quietud del clima fr&iacute;o, monte a caballo, realice actividades de aventura y deportes extremos o del&eacute;itese con cualquiera de las exquisitas preparaciones t&iacute;picas, como un sabroso caldo de costilla o una deliciosa taza de chocolate caliente con trozos de queso. Bajo la jurisdicci&oacute;n de este municipio se encuentra una de las entradas principales del <strong>Parque Nacional Chingaza</strong>.</p>', 'images', '4.720521', '-73.968631', 'la-calera', 'admin', 'images/municipalities/Municipality_201802201910181832416172.jpg', NULL, 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,provincia,cundinamarca,colombia,la calera,guavio', 'activo', '2018-02-16 22:40:11', '2018-02-28 23:30:20'),
(14, 'Guatavita', '<p style="text-align:justify">Es tal vez uno de los territorios del departamento que atesora m&aacute;s la cultura e historia de sus antiguos habitantes. De la &eacute;poca colonial conserva su arquitectura en la cabecera municipal, reconstruida en la d&eacute;cada de 1960, cuando la poblaci&oacute;n original se inund&oacute; para construir el <strong>embalse del Tomin&eacute;</strong>, donde es posible practicar deportes n&aacute;uticos y disfrutar de una buena oferta de servicios de recreaci&oacute;n. De la &eacute;poca prehisp&aacute;nca conserva una joya:&nbsp; la <strong>laguna de Guatavita</strong>, donde antiguamente se invest&iacute;a de autoridad a los caciques, ritual en el que se inspira la m&iacute;tica leyenda de El Dorado.</p>', 'images', '4.934487', '-73.833644', 'guatavita', 'admin', 'images/municipalities/Municipality_201803011757141420259943.jpg', 'images/municipalities/Municipality_20180301175715329187357.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,provincia,cundinamarca,colombia,guatavita,guavio', 'activo', '2018-02-16 22:40:16', '2018-03-02 00:57:15'),
(15, 'Gachalá', '<p style="text-align:justify">Esta peque&ntilde;a poblaci&oacute;n ha tenido un notable crecimiento gracias al turismo que desde 1992 promueve la Hidroel&eacute;ctrica del Guavio con el <strong>embalse</strong> <strong>del Guavio</strong>,<strong> </strong>de 243 m de altura, que embalsa 950 millones de metros c&uacute;bicos de agua,&nbsp; provenientes de los r&iacute;os Chivor, Farallones, Batatas y otros afluentes del r&iacute;o Guavio, con el objetivo primordial de generar energ&iacute;a el&eacute;ctrica. En la represa<strong> </strong>se practican deportes n&aacute;uticos. Anualmente se celebra en Gachal&aacute; el Festival N&aacute;utico del Guavio, as&iacute; como el Reinado Departamental del Agua.</p>', 'images', '4.693039', '-73.519970', 'gachala', 'admin', 'images/municipalities/Municipality_201802262242561164441615.jpg', 'images/municipalities/Municipality_20180226224258256177512.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,gachalá,provincia,guavio,cundinamarca,colombia', 'activo', '2018-02-16 22:56:26', '2018-02-28 23:38:43'),
(16, 'Gachetá', '<p style="text-align:justify">A 92 kil&oacute;metros de Bogot&aacute;,&nbsp; entre sus principales atractivos tur&iacute;sticos se halla el <strong>cementerio de Gachet&aacute;</strong>, uno de los m&aacute;s bonitos de Latinoam&eacute;rica, por la gran cantidad de m&aacute;rmol presente en su construcci&oacute;n.</p>\r\n\r\n<p style="text-align:justify">En este municipio, ba&ntilde;ando por las aguas del <strong>embalse del Guavio</strong>, tambi&eacute;n es posible navegar por las aguas de la represa, realizar deportes n&aacute;uticos y actividades acu&aacute;ticas.</p>', 'images', '4.816972', '-73.636306', 'gacheta', 'admin', 'images/municipalities/Municipality_201802281606581266412514.jpg', 'images/municipalities/Municipality_201802262249431248910161.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,gachetá,provincia,guavio,cundinamarca,colombia', 'activo', '2018-02-16 23:00:48', '2018-02-28 23:33:52'),
(17, 'Medina', '<p>Con 1200 km<sup>2</sup> de extensi&oacute;n, <strong>Medina</strong> es el municipio con m&aacute;s territorio en el departamento. Fundado en 1620, por su cercan&iacute;a a las sabanas orientales del Meta, tiene notables influencias de la cultura llanera en su gastronom&iacute;a y sus costumbres.</p>\r\n\r\n<p>No deje de visitar el parque principal, un espacio con &aacute;rboles que ofrecen su sombra para el descanso de viajeros y locales; una cancha m&uacute;ltiple, atracciones para los m&aacute;s peque&ntilde;os y una concha ac&uacute;stica para eventos culturales. Uno de los planes m&aacute;s frecuentados por los medineros es el paseo de olla en el <strong>r&iacute;o Gazamumo</strong> y sus playas de ribera.</p>', 'images', '4.508399', '-73.349853', 'medina', 'admin', 'images/municipalities/Municipality_2018022816041774581718.jpg', 'images/municipalities/Municipality_201802271552111683534833.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,medina,provincia,cundinamarca,colombia', 'activo', '2018-02-16 23:04:35', '2018-03-01 03:11:06'),
(18, 'Paratebueno', '<p style="text-align:justify">Ubicado donde se encuentran la cordillera de los Andes y los Llanos Orientales, es el &uacute;nico municipio llanero del departamento. En Paratebueno visite la <strong>Reserva Natural Aguascalientes</strong>, llamada as&iacute; por las aguas termales que brotan de sus suelos y que se concentran en balnearios donde ba&ntilde;istas, locales y turistas, se benefician de sus propiedades curativas. En la reserva tambi&eacute;n podr&aacute; realizar lodoterapia, as&iacute; como actividades de senderismo y ecoturismo.</p>', 'images', '4.376587', '-73.209608', 'paratebueno', 'admin', 'images/municipalities/Municipality_20180302153147142761596.jpg', 'images/municipalities/Municipality_201803021531471707368284.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,paratebueno,cundinamarca,provincia,medina,colombia', 'activo', '2018-02-16 23:09:20', '2018-03-02 22:31:47'),
(19, 'Choachí', '<p style="text-align:justify">Es el territorio con mayor desarrollo tur&iacute;stico de la provincia oriental cundinamarquesa. Se extiende sobre el valle del r&iacute;o Blanco, por lo que en su relieve se encuentran asombrosos parajes naturales como las pe&ntilde;as Azul y de la Bolsa o la <strong>cascada La Chorrera</strong>, que con 590 metros de altura es una de las cataratas m&aacute;s altas de Colombia.</p>\r\n\r\n<p style="text-align:justify">Conozca el <strong>p&aacute;ramo Cruz Verde</strong> y su antiguo camino colonial que lo conecta con Bogot&aacute;. En su casco urbano y periferias la temperatura se mantiene alrededor de los 18 &deg;C; clima ideal para darse un ba&ntilde;o en las aguas curativas de los <strong>Termales de Santa M&oacute;nica</strong>.</p>', 'images', '4.528659', '-73.922968', 'choachi', 'admin', 'images/municipalities/Municipality_20180301224137532338279.jpg', 'images/municipalities/Municipality_20180301224137222174003.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,choachí,provincia,oriente,cundinamarca,colombia', 'activo', '2018-02-16 23:12:02', '2018-03-02 05:41:37'),
(20, 'Chipaque', '<p style="text-align:justify">Este municipio devoto de la Virgen, atrae turistas religiosos a su reserva ecol&oacute;gica <strong>Marilandia</strong>, espacio dedicado a la espiritualidad, la calma y la naturaleza. Para hacer el recorrido en Marilandia se recomienda llevar un buen abrigo para las bajas temperaturas y bloqueador solar, dado que todo el trayecto es al aire libre.</p>\r\n\r\n<p style="text-align:justify">En <strong>Chipaque</strong>, que en lengua chibcha significa &ldquo;Bosque de nuestro padre&rdquo;, disfrute de maravillosos planes naturales y de la gastronom&iacute;a local, con preparaciones como el pan de sag&uacute; y el tradicional tamal. Lleve atractivas artesan&iacute;as y <em>souvenires</em> para sus allegados.</p>', 'images', '4.443184', '-74.045000', 'chipaque', 'admin', 'images/municipalities/Municipality_201803021506492140771394.jpg', 'images/municipalities/Municipality_201803021506511121838812.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,provincia,oriente,cundinamarca,colombia,chipaque', 'activo', '2018-02-16 23:12:23', '2018-03-02 22:06:51'),
(21, 'Pacho', '<p style="text-align:justify">Fundado el 25 de agosto de 1604, Pacho se caracteriza por la laboriosidad de su gente, hist&oacute;ricamente dedicada a actividades agr&iacute;colas y mineras. En su casco urbano se encuentran valiosas edificaciones culturales como la Biblioteca Municipal Guillermo Ruiz Lara, la Casa de la Cultura y su teatro.</p>\r\n\r\n<p style="text-align:justify">Para quienes realizan turismo religioso, cuenta tambi&eacute;n con numerosas construcciones como capillas, templos, im&aacute;genes y grutas, ideales para dedicar un momento a la espiritualidad. Si pasa por Pacho no deje de probar su deliciosa gastronom&iacute;a y deguste platos como el piquete de gallina y la sopa de ruya con poleo; postres como el bomb&oacute;n de panela y man&iacute;, los caca&iacute;tos y las habas tostadas; bebidas como el espichado de curuba con crema de leche o el masato de ma&iacute;z; todas preparaciones t&iacute;picas de la regi&oacute;n. En el <strong>centro artesanal Tacuar&aacute;</strong> podr&aacute; encontrar suvenires para regalar y recordar su estad&iacute;a en Pacho.</p>', 'images', '5.137918', '-74.152868', 'pacho', 'admin', 'images/municipalities/Municipality_201803021513481933535504.jpg', 'images/municipalities/Municipality_20180302151348141010121.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,pacho,provincia,rionegro,cundinamarca,colombia', 'activo', '2018-02-16 23:15:57', '2018-03-02 22:13:48'),
(22, 'Cajicá', '<p style="text-align:justify">Por su cercan&iacute;a con la capital del pa&iacute;s, este municipio ha experimentado un notable crecimiento, tanto por el aumento de su poblaci&oacute;n, como por el n&uacute;mero de visitantes que recibe todos los fines de semana, que ven en <strong>Cajic&aacute;</strong> un lugar para descansar del acelerado ritmo citadino, en un ambiente campestre como el que ofrec3e este municipio.</p>\r\n\r\n<p style="text-align:justify">Su nombre, que en lengua chibcha significa &ldquo;resguardo entre las rocas&rdquo;, se refiere a la ubicaci&oacute;n del municipio entre monta&ntilde;as que lo rodean. En Cajic&aacute; no deje de visitar el parque principal, con un <strong>obelisco en honor al libertador Sim&oacute;n Bol&iacute;var</strong> y las antiguas casas aleda&ntilde;as, que guardan la historia de este municipio fundado en 1598.</p>', 'images', '4.917531', '-74.024773', 'cajica', 'admin', 'images/municipalities/Municipality_20180301180733629348462.jpg', 'images/municipalities/Municipality_20180301180733505434975.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,cajicá,provincia,sabana centro,cundinamarca,colombia', 'activo', '2018-02-16 23:19:33', '2018-03-02 01:07:33'),
(23, 'Zipaquirá', '<p style="text-align:justify">Fue fundado el 18 de julio de 1600, por el oidor Luis Enr&iacute;quez. Los conquistadores ib&eacute;ricos vieron c&oacute;mo los habitantes de estas tierras, los muiscas, comercializaban con la sal, mineral abundante en estas tierras, la cual intercambiaban por esmeraldas, alimentos, herramientas y otros insumos de la vida cotidiana.</p>\r\n\r\n<p style="text-align:justify">Entre los atractivos de este municipio se encuentra la<strong> capilla de los Dolores,</strong> con los restos de los l&iacute;deres de la revuelta comunera del siglo XVIII; el sal&oacute;n del Consejo Municipal, adornado con valiosas pinturas que retratan a Sim&oacute;n Bol&iacute;var; el museo de la Salmuera, la plaza de la Constituci&oacute;n y la catedral Diocesana, construida en 1916.</p>', 'images', '5.021476', '-73.990955', 'zipaquira', 'admin', 'images/municipalities/Municipality_201802281757321977706378.jpg', 'images/municipalities/Municipality_20180228175559822674907.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,zipaquirá,provincia,sabana centro,cundinamarca,colombia', 'activo', '2018-02-16 23:21:15', '2018-03-01 00:57:33'),
(24, 'Sopó', '<p style="text-align:justify">A 39 kil&oacute;metros de la capital del pa&iacute;s, en el norte de la sabana de Bogot&aacute;, los habitantes de Sop&oacute; reciben todos los fines de semana a viajeros que buscan un espacio campestre para almorzar en familia. El <strong>santuario del Se&ntilde;or de la Piedra</strong> es uno de los puntos que m&aacute;s atrae personas, pues asisten a la misa que all&iacute; se celebra los d&iacute;as domingo.</p>\r\n\r\n<p style="text-align:justify">Es indispensable parar en la <strong>caba&ntilde;a de Alpina</strong>, un para&iacute;so para los amantes de lo dulce, donde encontrar&aacute; deliciosas preparaciones como la cuajada con arequipe y mora, las milhojas, el cheesecake de maracuy&aacute;, la leche asada y otras exquisiteces. Si por el contrario busca experimentar algo de adrenalina, Sop&oacute; le ofrece la oportunidad de realizar vuelos en parapente, sobre el paisaje sopose&ntilde;o, los municipios vecinos y<del>&nbsp; </del><ins>&nbsp;</ins>el embalse del Tomin&eacute;.</p>', 'images', '4.908732', '-73.941071', 'sopo', 'admin', 'images/municipalities/Municipality_20180302152811510841923.jpg', 'images/municipalities/Municipality_20180302152814971023467.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,sopó,provincia,sabana centro,cundinamarca,colombia', 'activo', '2018-02-16 23:22:52', '2018-03-02 22:28:14'),
(25, 'Tocancipá', '<p style="text-align:justify">El gentilicio para los habitantes de este municipio es tocancipe&ntilde;os. Por su cercan&iacute;a con Bogot&aacute; se ha convertido en un polo de desarrollo industrial y comercial, que ayuda a descongestionar la producci&oacute;n en masa de algunas f&aacute;bricas en la capital. Entre los principales atractivos de este municipio est&aacute;n el <strong>parque Jaime Duque</strong> y el <strong>Aut&oacute;dromo de Tocancip&aacute;</strong>, que con 2725 metros es una de las pistas con mejor infraestructura f&iacute;sica y administrativa de todo el continente.</p>\r\n\r\n<p style="text-align:justify">La pista cuenta con 5 circuitos y una recta principal de 540 metros. All&iacute; se realizan carreras de motos, carros y camiones. Visite tambi&eacute;n el <strong>Kart&oacute;dromo Juan Pablo Montoya</strong>, que con 1028 metros de extensi&oacute;n es un espacio ideal para poner a prueba sus aptitudes al frente del volante.</p>', 'images', '4.965002', '-73.910823', 'tocancipa', 'admin', 'images/municipalities/Municipality_20180301181928893658718.jpg', 'images/municipalities/Municipality_201803011819281164661546.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,tocancipá,provincia,sabana centro,cundinamarca,colombia', 'activo', '2018-02-16 23:26:19', '2018-03-02 01:19:29'),
(26, 'Nemocón', '<p style="text-align:justify">Fundado hacia 1600,&nbsp; dista 65 kil&oacute;metros de la capital del pa&iacute;s. Ancestralmente la cultura de sus pobladores ha estado ligada al uso y explotaci&oacute;n de la sal, con la que los muiscas comercializaban y hac&iacute;an trueques. Este mineral, que abunda en el territorio, se ha convertido en uno de los principales ejes de desarrollo, tanto comercial, como tur&iacute;stico, gracias a espacios como la <strong>mina de sal de Nemoc&oacute;n.</strong></p>\r\n\r\n<p style="text-align:justify">En el <strong>Museo de la Sal</strong>, tambi&eacute;n conocido como la <strong>Casa del Encomendero</strong>, podr&aacute; conocer m&aacute;s de cerca la relaci&oacute;n de este territorio con el mineral, as&iacute; como la casa original del fundador de la primera encomienda del territorio, el oidor Juan de Olmos, reconocido por querer proteger a los nativos de este lugar.</p>', 'images', '5.066969', '-73.880696', 'nemocon', 'admin', 'images/municipalities/Municipality_20180228181555832318474.jpg', 'images/municipalities/Municipality_201802281815551768052334.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,nemocón,provincia,sabana centro,cundinamarca,colombia', 'activo', '2018-02-16 23:30:08', '2018-03-01 01:15:55'),
(27, 'Chía', '<p style="text-align:justify">Por estar en la periferia de Bogot&aacute;, este municipio ha crecido en infraestructura, poblaci&oacute;n y comercio. Recibe asiduamente visitantes que buscan escapar de la agitada rutina citadina y encuentran en Ch&iacute;a un lugar para realizar actividades campestres y recreativas. Su nombre en lengua muisca tiene varias acepciones, siendo la m&aacute;s popular la de &ldquo;Luna&rdquo;.</p>\r\n\r\n<p style="text-align:justify">Entre sus atractivos tur&iacute;sticos se encuentran el <strong>castillo Marroqu&iacute;n</strong>, construido entre 1899 y 1900, con planos tra&iacute;dos de Europa para el entonces presidente Jos&eacute; Manuel Marroqu&iacute;n; y el <strong>puente del Com&uacute;n</strong>, monumento nacional desde 1967 por servir para conectar a la ciudad de Bogot&aacute; con las regiones de Santander y Boyac&aacute;, alrededor de 220 a&ntilde;os, pero tambi&eacute;n por haber sido el escenario del encuentro fatal entre los comuneros y las autoridades de Santaf&eacute; de Bogot&aacute;.</p>\r\n\r\n<p style="text-align:justify">Ch&iacute;a cuenta con una excelente oferta de restaurantes campestres de comida a la carta, as&iacute; como con grandes y modernos centros comerciales para ir de compras.</p>', 'images', '4.864758', '-74.050918', 'chia', 'admin', 'images/municipalities/Municipality_20180301182648778963403.jpg', 'images/municipalities/Municipality_20180301182648680766674.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,chía,provincia,sabana centro,cundinamarca,colombia', 'activo', '2018-02-16 23:31:23', '2018-03-02 01:26:48'),
(28, 'Tabio', '<p style="text-align:justify">En este territorio de alma muisca encontrar&aacute; la huella de c&oacute;mo avanza el tiempo al caminar entre las v&iacute;as de su centro municipal, que a&uacute;n conserva la arquitectura colonial y las calles adoquinadas de hace 200 a&ntilde;os. Otro atractivo hist&oacute;rico es la <strong>capilla Santa B&aacute;rbara</strong>, construida en el siglo XVII, pocos a&ntilde;os despu&eacute;s de la fundaci&oacute;n del muncipio.</p>\r\n\r\n<p style="text-align:justify">Visite los <strong>termales el Zipa</strong>, donde podr&aacute; darse un ba&ntilde;o en sus aguas de propiedades curativas, que emanan de la tierra.</p>', 'images', '4.916962', '-74.096358', 'tabio', 'admin', 'images/municipalities/Municipality_201803011834391568675583.jpg', 'images/municipalities/Municipality_201803011834391375064095.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,tabio,provincia,sabana centro,cundinamarca,colombia', 'activo', '2018-02-16 23:32:57', '2018-03-02 01:34:39'),
(29, 'Madrid', '<p style="text-align:justify">Los conquistadores espa&ntilde;oles llegaron a este lugar cuando se llamaba Sagazuc&aacute;, como un ind&iacute;gena de la regi&oacute;n. Seg&uacute;n registros, hacia el a&ntilde;o 1563 el oidor Diego de Villafane le cambiar&iacute;a el nombre al territorio por Serrezuela; catalogaci&oacute;n que recibi&oacute; oficialmente hasta el siglo XIX, cuando cambi&oacute; su nombre por el apellido del destacado escritor y pol&iacute;tico Pedro Fern&aacute;ndez Madrid.</p>\r\n\r\n<p style="text-align:justify"><strong>Visite lugares como los parques Luis Carlos Gal&aacute;n y De las Flores</strong>, frecuentados diariamente por los pobladores de Madrid, cuando se dirigen a sus trabajos y compromisos. En este municipio a 21 km de Bogot&aacute; deguste sus deliciosos dulces, t&iacute;picos y tradicionales.</p>', 'images', '4.733779', '-74.262757', 'madrid', 'admin', 'images/municipalities/Municipality_201803052120572072604780.jpg', 'images/municipalities/Municipality_201803052120571278610125.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,madrid,provincia,sabana occidente,cundinamarca,colombia', 'activo', '2018-02-16 23:34:27', '2018-03-06 04:20:57'),
(30, 'Mosquera', '<p style="text-align:justify">A 10 kil&oacute;metros de la capital, saliendo por la calle 80, este municipio fundado el 27 de septiembre de 1861 se ha convertido en uno de los puntos de desarrollo perif&eacute;ricos a la ciudad. Los fines de semana son los d&iacute;as con mayor afluencia de turistas, que se dirigen a Mosquera para disfrutar en familia la excelente oferta de restaurantes campestres, con gastronom&iacute;a y dulces mosquerunos.</p>', 'images', '1', '2', 'mosquera', 'admin', 'images/municipalities/Municipality_2018021616424225807906.jpg', NULL, 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 3, 'municipio,mosquera,provincia,sabana occidente,cundinamarca,colombia', 'inactivo', '2018-02-16 23:42:42', '2018-03-02 02:05:22'),
(31, 'Facatativá', '<p>Ubicado al final de sabana de Bogot&aacute;, su nombre significa precisamente, &ldquo;cercado al final de la llanura&rdquo; en chibcha. Es un lugar con profundas ra&iacute;ces hist&oacute;ricas, tanto en la historia moderna de la naci&oacute;n, como en la &eacute;poca precolombina.</p>\r\n\r\n<p>En Facatativ&aacute;, bordeada por los cerros Negro y Maju&iacute;, podr&aacute; disfrutar de planes ecotur&iacute;sticos y culturales, como recorrer los caminos reales, declarados patrimonio cultural, por ser las v&iacute;as que comunicaban distintas ciudades en la &eacute;poca de la Colonia. Visite el <strong>sendero ecotur&iacute;stico Pe&ntilde;as del Aserradero</strong>, en la zona rural<ins>,</ins> y el <strong>Parque Arqueol&oacute;gico</strong> <strong>Piedras del Tunjo</strong>, en el casco urbano del municipio.</p>', 'images', '4.809984', '-74.354009', 'facatativa', 'admin', 'images/municipalities/Municipality_20180228165211818591211.jpg', 'images/municipalities/Municipality_201802281652111089638825.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,facatativá,provincia,sabana occidente,cundinamarca,colombia', 'activo', '2018-02-16 23:46:55', '2018-02-28 23:53:09'),
(32, 'Soacha', '<p style="text-align:justify">Es el m&aacute;s poblado del departamento. Fundado el de agosto de 1600, como una peque&ntilde;a poblaci&oacute;n campestre, con los a&ntilde;os ha crecido hasta ser parte del per&iacute;metro metropolitano de Bogot&aacute;. Entre sus principales atractivos, se encuentra el ic&oacute;nico <strong>salto del Tequendama</strong>, en la v&iacute;a hacia el municipio de El Colegio, desde la cual puede tomar espectaculares fotos a la incre&iacute;ble ca&iacute;da de agua de 157 m, que tantas historias y leyendas ha despertado.</p>\r\n\r\n<p style="text-align:justify">Visite el <strong>parque la Poma</strong>, que en un recorrido de 18 km, le ofrece al visitante la oportunidad de conocer el valioso ecosistema de bosque andino, donde tambi&eacute;n podr&aacute; apreciar pinturas rupestres, con m&aacute;s de 11.000 a&ntilde;os.</p>', 'images', '4.582723', '-74.211747', 'soacha', 'admin', 'images/municipalities/Municipality_20180301194740895544073.jpg', 'images/municipalities/Municipality_201803011947401994550676.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,soacha,provincia,cundinamarca,colombia', 'activo', '2018-02-16 23:51:07', '2018-03-02 02:47:40'),
(33, 'Silvania', '<p style="text-align:justify">Fundado por Ismael Silva en febrero de 1935, es un importante corredor de desarrollo en la v&iacute;a hacia grandes ciudades como Cali, Ibagu&eacute;, Girardot y Melgar. Ofrece una conservada arquitectura de principios del siglo XX, museos y artesan&iacute;as. Estas constituyen una de las industrias de mayor importancia en el municipio y se elaboran con lana, cuero, fique y bamb&uacute;; se destacan los muebles y la cester&iacute;a en mimbre.</p>', 'images', '4.390780', '-74.396782', 'silvania', 'admin', 'images/municipalities/Municipality_201803012001231056768672.jpg', 'images/municipalities/Municipality_20180301200123548335093.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,silvania,provincia,sumapaz,cundinamarca,colombia', 'activo', '2018-02-16 23:59:22', '2018-03-02 03:01:23'),
(34, 'Fusagasugá', '<p style="text-align:justify">En los lindes de la sabana cundiboyacense, <strong>La Ciudad Jard&iacute;n de Colombia</strong>, como se le llama, recibe a quienes la visitan con espectaculares parajes naturales en lugares como el <strong>Ecoparque Chinauta</strong>, donde podr&aacute; tener contacto con animales de granja y disfrutar de planes familiares y de aventura.</p>\r\n\r\n<p style="text-align:justify"><strong>Fusagasug&aacute;</strong> es el segundo municipio m&aacute;s poblado del departamento y su temperatura promedio es de 20&deg;C en el casco urbano, donde se encuentran la Casa de la Cultura, la iglesia Nuestra Se&ntilde;ora de Bel&eacute;n y el Parque Natural San Rafael.</p>', 'images', '4.345152', '-74.361823', 'fusagasuga', 'admin', 'images/municipalities/Municipality_201803012047412023570827.jpg', 'images/municipalities/Municipality_201803012047411115707852.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,fusagasugá,provincia,sumapaz,cundinamarca,colombia', 'activo', '2018-02-17 00:01:16', '2018-03-02 03:47:41'),
(35, 'Anapoima', '<p style="text-align:justify">Hist&oacute;ricamente ha sido un refugio para las bajas temperaturas que imperan en la capital del pa&iacute;s. Por su cercan&iacute;a con Bogot&aacute;, <strong>Anapoima</strong> ha recibido desde la &eacute;poca colonial a los habitantes de esta urbe, que encuentran en este municipio un clima perfecto para el esparcimiento y los planes campestres.</p>\r\n\r\n<p style="text-align:justify">Aqu&iacute; podr&aacute; realizar pesca deportiva, <em>camping</em> y darse un ba&ntilde;o en las <strong>fuentes termales de Santa Luc&iacute;a y Santa Ana</strong>. Para conocer la tradici&oacute;n y la cultura de Anapoima visite la Casa de la Cultura. No deje de visitar el Ecoparque el Gaitero, uno de los principales atractivos ecotur&iacute;sticos del municipio.</p>', 'images', '4.548942', '-74.535235', 'anapoima', 'admin', 'images/municipalities/Municipality_201803012114311706478423.jpg', 'images/municipalities/Municipality_20180301211431735454318.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,anapoima,provincia,tequendama,cundinamarca,colombia', 'activo', '2018-02-17 00:03:50', '2018-03-02 04:14:31'),
(36, 'Apulo', '<p style="text-align:justify">El desarrollo de municipios aleda&ntilde;os, como Anapoima, Girardot y Melgar, ha hecho que Apulo tambi&eacute;n experimente un crecimiento econ&oacute;mico y social. Visite parajes naturales como la laguna de Salcedo o el cerro de Guacan&aacute;. La Casa de Gobierno y Patrimonio Cultural y Tur&iacute;stico de Apulo se encuentra en el que antiguamente fue el casino municipal; no lo deje de conocer.</p>', 'images', '7', '8', 'apulo', 'admin', 'images/municipalities/Municipality_20180216190754943432784.jpg', NULL, 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 3, 'municipio,apulo,provincia,tequendama,cundinamarca,colombia', 'inactivo', '2018-02-17 02:07:54', '2018-03-02 02:04:56'),
(37, 'Viotá', '<p style="text-align:justify">Fundado el 27 de marzo de 1767, Viot&aacute; ha experimentado muy de cerca los avances econ&oacute;micos y sociales del departamento. A 86 km al suroccidente de Bogot&aacute;, y sobre los 500 msnm, Viot&aacute; tiene unos 26 &deg;C de temperatura promedio, ideales para el cultivo de valiosos alimentos. De hecho es el municipio con la mayor producci&oacute;n de caf&eacute; en Cundinamarca; entre 1920 y 1940 lider&oacute; los &iacute;ndices de producci&oacute;n nacional. Si solo va de paso pare en alg&uacute;n restaurante y deguste la gastronom&iacute;a regional; por supuesto, con una taza de caf&eacute;.</p>', 'images', '5', '6789', 'viota', 'admin', 'images/municipalities/Municipality_20180216191209824355453.jpg', NULL, 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 3, 'municipio,viotá,provincia,tequendama,cundinamarca,colombia', 'inactivo', '2018-02-17 02:12:09', '2018-03-02 02:05:06'),
(38, 'La Mesa', '<p style="text-align:justify">A tan solo 30 minutos de Bogot&aacute;, este municipio de excelente clima ofrece a sus visitantes actividades ecotur&iacute;sticas en reservas forestales, bosques naturales y caminos reales, con incre&iacute;bles paisajes. El municipio ostenta una variada oferta de restaurantes y paraderos, ideales para quienes van de paso, donde se pueden degustar preparaciones t&iacute;picas cundiboyacenses como la picada, la fritanga o el piquete de gallina. Para practicar deportes extremos y actividades de aventura visite el <strong>Ecoparque Macadamia</strong>.&nbsp;</p>', 'images', '4.632147', '-74.463014', 'la-mesa', 'admin', 'images/municipalities/Municipality_201803012129311547347768.jpg', 'images/municipalities/Municipality_201803012129311638541110.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,la mesa,provincia,tequendama,cundinamarca,colombia', 'activo', '2018-02-17 02:14:00', '2018-03-02 04:29:31'),
(39, 'San Antonio del Tequendama', '<p style="text-align:justify">Con su cabecera municipal a 1540 msnm, los sanantoniunos gozan de unos 18&deg;C de temperatura promedio. Por sus fant&aacute;sticas caracter&iacute;sticas geogr&aacute;ficas y clim&aacute;ticas San Antonio de Tequendama se dedica al cultivo de orqu&iacute;deas y plantas arom&aacute;ticas,&nbsp; su principal actividad&nbsp; econ&oacute;mica. Visite el <strong>parque tem&aacute;tico Orqu&iacute;deas del Tequendama</strong>, donde podr&aacute; apreciar m&aacute;s de 6000 flores en 1300 m de recorrido y avistar aves. Si desea hacer turismo de aventura, visite el parque de habilidades <strong>Ecocenter</strong>, que ofrece actividades de deportes extremos y exigencia f&iacute;sica, as&iacute; como servicios de hospedaje.</p>', 'images', '5', '6', 'san-antonio-del-tequendama', 'admin', 'images/municipalities/Municipality_201802161919061903301534.jpg', NULL, 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 3, 'municipio,san antonio del tequendama,provincia,tequendama,cundinamarca,colombia', 'inactivo', '2018-02-17 02:19:06', '2018-03-02 02:05:56'),
(40, 'Ubaté', '<p style="text-align:justify">Fundado el 12 de abril de 1592, se le conoce como la Capital Lechera de Colombia pues produce diariamente 70.830 litros de&nbsp; leche, para fines comerciales y de consumo. Esta industria representa la principal actividad econ&oacute;mica del municipio y una de sus m&aacute;s importantes fuentes de empleo.</p>\r\n\r\n<p style="text-align:justify">En Ubat&eacute; encontrar&aacute; atractivos tur&iacute;sticos naturales y culturales. Visite los <strong>Chorros de Saog&aacute;</strong>, donde podr&aacute; tener una envidiable vista del paisaje ubatense, y recorrer senderos naturales; conozca el <strong>embalse El Hato</strong>, de 173 hect&aacute;reas y frecuentado para planes de <em>camping</em>, en medio de la fauna y la flora de la zona, as&iacute; como para practicar deportes n&aacute;uticos en la represa de 27 metros de profundidad. En esta reserva pase por la Casona, de arquitectura colonial, con alrededor de 400 a&ntilde;os de antig&uuml;edad.</p>\r\n\r\n<p style="text-align:justify">En el casco urbano, con sus habituales 13&deg;C de temperatura, visite la <strong>Bas&iacute;lica Menor Divino Salvador</strong>, ejemplo del g&oacute;tico franc&eacute;s, con suelos en m&aacute;rmol e ilustraciones religiosas en cuadros y vitrales.</p>', 'images', '5.312024', '-73.818222', 'ubate', 'admin', 'images/municipalities/Municipality_201803011901101355493496.jpg', 'images/municipalities/Municipality_20180301190110925889962.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,ubaté,provincia,cundinamarca,colombia', 'activo', '2018-02-17 02:21:16', '2018-03-02 02:01:10'),
(41, 'Cucunubá', '<p style="text-align:justify">La Ni&ntilde;a Bonita del Valle de Ubat&eacute;, como se conoce popularmente a este municipio, atesora una conservada arquitectura colonial en el parque principal y las construcciones que lo rodean. Aunque el territorio estuvo poblado por muiscas en la &eacute;poca precolombina, se tiene como fecha de fundaci&oacute;n el 2 de agosto de 1600. A m&aacute;s de 2500 msnm y 14 &deg;C de temperatura promedio, en su casco urbano encontrar&aacute; edificaciones de paredes blancas y techos de tejas. Visite la <strong>iglesia del Divino Salvador</strong>, que acoge a cientos de devotos y representa uno de los templos con m&aacute;s tradici&oacute;n en el municipio. En la vereda La Toma se encuentra el <strong>Parque Lineal y Ecol&oacute;gico Cerro de Lourdes</strong>, donde podr&aacute; tener una espectacular vista de la regi&oacute;n desde el mirador de Las Tres Lagunas, que permite avistar las lagunas de Cucunub&aacute;, F&uacute;quene y Suesca, as&iacute; como los municipios de Zipaquir&aacute; y Nemoc&oacute;n;&nbsp; en el parque se encuentra la capilla de Lourdes, importante sitio de peregrinaci&oacute;n para los cucunubenses y los fieles cat&oacute;licos de la zona.</p>', 'images', '6', '8', 'cucunuba', 'admin', 'images/municipalities/Municipality_201802161925161269837448.jpg', NULL, 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 3, 'municipio,cucunubá,provincia,ubaté,cundinamarca,colombia', 'inactivo', '2018-02-17 02:25:16', '2018-03-02 02:03:01'),
(42, 'Fúquene', '<p style="text-align:justify">Fundado el 6 de noviembre de 1638 y llamado como la laguna que lo representa e identifica, F&uacute;quene est&aacute; en el norte del departamento, a 116 km de Bogot&aacute;. Situado a 2700 msnm, en su casco urbano y periferia imperan unos habituales 13&deg;C, ideales para la principal fuente econ&oacute;mica del municipio: la ganader&iacute;a. Deguste exquisitas preparaciones de la gastronom&iacute;a regional. Tambi&eacute;n conocida como la Ciudad Pesebre de Colombia, es un lugar con atractivos parajes naturales, como la laguna de F&uacute;quene.</p>', 'images', '6', '7', 'fuquene', 'admin', 'images/municipalities/Municipality_20180216192709599153372.jpg', NULL, 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 3, 'municipio,fúquene,provincia,ubaté,cundinamarca,colombia', 'inactivo', '2018-02-17 02:27:09', '2018-03-02 02:03:05');
INSERT INTO `municipalities` (`id_municipality`, `municipality_name`, `description`, `multimedia_type`, `latitude`, `longitude`, `slug`, `type_last_user`, `link_image`, `downloadimage`, `iframe`, `fk_last_edition`, `keywords`, `state`, `created_at`, `updated_at`) VALUES
(43, 'Cabrera', '<p>Gracias a su variedad de pisos t&eacute;rmicos, Cabrera posee atractivos ecol&oacute;gicos como las <strong>reservas Natural del Sumapaz</strong> y <strong>Forestal de Palma de Cera</strong>, en las que habitan diversas especies de animales como &aacute;guilas, osos de anteojos, borugos, tigrillos y soches (ciervos que habitan bosques espesos).</p>\r\n\r\n<p>Otros sitios interesantes para visitar son las <strong>cascadas Santa Rita</strong> y <strong>del Bajo Ariari, </strong>conocida porque en sus aguas se le dio vida a la leyenda del Tunjo, que cuenta la historia de un ni&ntilde;o de ojos azules que invita a las ni&ntilde;as a ingresar a las aguas con &eacute;l. Si pasa por Cabrera conozca la <strong>laguna los morti&ntilde;os</strong>, un espl&eacute;ndido espejo natural de agua.</p>', 'images', '3.984790', '-74.484181', 'cabrera', 'admin', 'images/municipalities/Municipality_20180301221312312690752.jpg', 'images/municipalities/Municipality_20180301221312706342501.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'municipio,cabrera,provincia,sumapaz,cundinamarca,colombia', 'activo', '2018-02-18 04:32:04', '2018-03-02 05:13:12'),
(44, 'Guasca', '<p><strong>Guasca</strong> se caracteriza por sus escenarios ecol&oacute;gicos, bosques y senderos ideales para realizar caminatas.</p>\r\n\r\n<p>Visite la <strong>reserva Encenillo</strong>, un bello h&aacute;bitat de especies como el guache y el armadillo. Si busca descanso, Guasca le ofrece termales naturales o el plan de ir a respirar aire puro en la laguna de Siecha, en donde puede encontrar trees imponentes cuerpos de agua de origen glaciar que hacen parte del <strong>Parque Nacional Natural Chingaza</strong>.</p>', 'images', '4.867236', '-73.877402', 'guasca', 'admin', 'images/municipalities/Municipality_20180228160229195242339.jpg', 'images/municipalities/Municipality_201802271541201486439270.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'Ruta del Agua,Cundinamarca,PNN Chinagaza,Reserva Encenillo', 'activo', '2018-02-27 05:14:27', '2018-02-28 23:02:29'),
(45, 'Fómeque', '<p><strong>F&oacute;meque</strong> cuenta con abundante vegetaci&oacute;n y gran riqueza h&iacute;drica, adem&aacute;s de una de las joyas ecol&oacute;gicas m&aacute;s importantes del departamento de Cundinamarca: el <strong>Parque Nacional Natural Chingaza</strong>, situado en las estribaciones de la cordillera Oriental y declarado Parque Nacional Natural en 1977.</p>', 'images', '4.486748', '-73.895359', 'fomeque', 'admin', 'images/municipalities/Municipality_20180226222725340072378.jpg', 'images/municipalities/Municipality_201802262227252141563014.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'Ruta del Agua,Cundinamarca', 'activo', '2018-02-27 05:27:26', '2018-02-28 23:37:13'),
(46, 'Junín', '<p><strong>Jun&iacute;n</strong>, cuyo nombre ind&iacute;gena es Chipazaque, y cuyo nombre colonial es Pueblo de Nuestra Se&ntilde;ora de la Concepci&oacute;n de Chipazaque, es un municipio del departamento de Cundinamarca (Colombia), ubicado en la Provincia del Guavio, a a 103 km de Bogot&aacute;.</p>', 'images', '4.791028', '-73.663027', 'junin', 'admin', 'images/municipalities/Municipality_201802262236011924070957.jpg', 'images/municipalities/Municipality_2018022622360174734673.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'Ruta del Agua,Cundinamarca', 'activo', '2018-02-27 05:36:01', '2018-03-01 03:31:38'),
(47, 'Gama', '<p><strong>Gama</strong> es un municipio del departamento de Cundinamarca (Colombia), ubicado en la <strong>Provincia del Guavio</strong>, a 113 km de Bogot&aacute;.</p>\r\n\r\n<p>En la &eacute;poca precolombina, el territorio del actual municipio de Gama estuvo poblado por los Ch&iacute;os, de la <strong>Confederaci&oacute;n Muisca</strong>, cuyo caser&iacute;o estaba en el sitio de Pauso, hoy Pueblo Viejo.</p>', 'images', '4.762033', '-73.610321', 'gama', 'admin', 'images/municipalities/Municipality_20180228155139130921627.jpg', 'images/municipalities/Municipality_20180228155139612454996.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'Cundinamarca,Ruta del Agua', 'activo', '2018-02-28 22:51:39', '2018-02-28 23:32:52'),
(48, 'Ubalá', '<p>Perfecto para descansar, <strong>Ubal&aacute;</strong> es rico en recursos h&iacute;dricos y mineros. Puede realizar una caminata en ascenso hasta llegar a la <strong>Laguna Verde</strong>, que tiene un hermoso color esmeralda y es h&aacute;bitat de alguas especies nativas de la regi&oacute;n.</p>', 'images', '4.743647', '-73.534989', 'ubala', 'admin', 'images/municipalities/Municipality_201802281614571217797709.jpg', 'images/municipalities/Municipality_201802281614571726542408.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'Ruta del Agua,Cundinamarca', 'activo', '2018-02-28 23:14:57', '2018-02-28 23:40:19'),
(49, 'Funza', '<p><strong>Funza</strong>, fundado como un antiguo asentamiento de la comunidad ind&iacute;gena muisca y perteneciente al &aacute;rea metropolitana de Bogot&aacute;, es reconocido en la actualidad por su alto nivel de desarrollo urban&iacute;stico y comercial, que lo ha llevado a convertirse en referente para los habitantes de la provincia de la Sabana de Occidente. Las edificaciones modernas contrastan con los espacios rurales que contin&uacute;an representando un amplio margen de la vocaci&oacute;n agr&iacute;cola de este territorio, que fue destinado tradicionalmente a la pr&aacute;ctica de la agricultura y la ganader&iacute;a.</p>\r\n\r\n<p>En sus calles encontrar&aacute;s establecimientos comerciales que te permitir&aacute;n conocer la oferta gastron&oacute;mica local, adem&aacute;s del legado de construcciones que se remontan a la &eacute;poca colonial y que dan cuenta de la historia de este municipio y sus habitantes. <strong>Recorre el Centro Hist&oacute;rico, la Casa de la Cultura, el Museo Parroquial, la Capilla de San Mart&iacute;n</strong> y las haciendas circundantes ubicadas en la vereda La Florida.</p>', 'images', '4.717029', '-74.212250', 'funza', 'admin', 'images/municipalities/Municipality_201803021610381454140136.jpg', 'images/municipalities/Municipality_201803021610381130232128.jpg', 'http://www.airpano.ru/files/Goa-North-1/2-3-2', 2, 'Cundinamarca', 'activo', '2018-03-02 23:04:54', '2018-03-02 23:10:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipality_images`
--

CREATE TABLE IF NOT EXISTS `municipality_images` (
  `id_municipality_image` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_municipality` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_municipality_image`),
  KEY `municipality_images_fk_municipality_foreign` (`fk_municipality`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=85 ;

--
-- Volcado de datos para la tabla `municipality_images`
--

INSERT INTO `municipality_images` (`id_municipality_image`, `link_image`, `fk_municipality`, `created_at`, `updated_at`) VALUES
(1, 'images/municipalities/Municipality_20180226221931105286065.jpg', 44, '2018-02-27 05:19:31', '2018-02-27 05:19:31'),
(2, 'images/municipalities/Municipality_201802262228351734717895.jpg', 45, '2018-02-27 05:28:35', '2018-02-27 05:28:35'),
(3, 'images/municipalities/Municipality_20180226223637944099813.jpg', 46, '2018-02-27 05:36:37', '2018-02-27 05:36:37'),
(4, 'images/municipalities/Municipality_201802262251121726386024.jpg', 13, '2018-02-27 05:51:12', '2018-02-27 05:51:12'),
(5, 'images/municipalities/Municipality_20180226225203958533461.jpg', 15, '2018-02-27 05:52:03', '2018-02-27 05:52:03'),
(6, 'images/municipalities/Municipality_201802262252431459562580.jpg', 16, '2018-02-27 05:52:43', '2018-02-27 05:52:43'),
(7, 'images/municipalities/Municipality_201802281943391376397236.jpg', 13, '2018-03-01 02:43:39', '2018-03-01 02:43:39'),
(8, 'images/municipalities/Municipality_201802281953582080174771.jpg', 15, '2018-03-01 02:53:58', '2018-03-01 02:53:58'),
(9, 'images/municipalities/Municipality_20180228195942303176947.jpg', 16, '2018-03-01 02:59:42', '2018-03-01 02:59:42'),
(10, 'images/municipalities/Municipality_20180228200516662787860.jpg', 17, '2018-03-01 03:05:16', '2018-03-01 03:05:16'),
(11, 'images/municipalities/Municipality_201802282007541892615396.jpg', 17, '2018-03-01 03:07:54', '2018-03-01 03:07:54'),
(12, 'images/municipalities/Municipality_201802282021002126890010.jpg', 44, '2018-03-01 03:21:02', '2018-03-01 03:21:02'),
(13, 'images/municipalities/Municipality_20180228202426610427347.jpg', 45, '2018-03-01 03:24:26', '2018-03-01 03:24:26'),
(14, 'images/municipalities/Municipality_201802282034221467696514.jpg', 47, '2018-03-01 03:34:22', '2018-03-01 03:34:22'),
(15, 'images/municipalities/Municipality_20180228203931850219189.jpg', 47, '2018-03-01 03:39:31', '2018-03-01 03:39:31'),
(16, 'images/municipalities/Municipality_201802282043261905705180.jpg', 48, '2018-03-01 03:43:26', '2018-03-01 03:43:26'),
(17, 'images/municipalities/Municipality_20180228204557192567588.jpg', 48, '2018-03-01 03:45:57', '2018-03-01 03:45:57'),
(18, 'images/municipalities/Municipality_20180228205320498559406.jpg', 31, '2018-03-01 03:53:20', '2018-03-01 03:53:20'),
(19, 'images/municipalities/Municipality_20180228210029164696582.jpg', 5, '2018-03-01 04:00:29', '2018-03-01 04:00:29'),
(20, 'images/municipalities/Municipality_201802282102361834826232.jpg', 6, '2018-03-01 04:02:36', '2018-03-01 04:02:36'),
(21, 'images/municipalities/Municipality_201802282111491328137322.jpg', 7, '2018-03-01 04:11:49', '2018-03-01 04:11:49'),
(22, 'images/municipalities/Municipality_201802282112551191795486.jpg', 7, '2018-03-01 04:12:55', '2018-03-01 04:12:55'),
(23, 'images/municipalities/Municipality_201802282117231650248853.jpg', 10, '2018-03-01 04:17:23', '2018-03-01 04:17:23'),
(24, 'images/municipalities/Municipality_20180228212411432051763.jpg', 11, '2018-03-01 04:24:11', '2018-03-01 04:24:11'),
(25, 'images/municipalities/Municipality_20180228212411324602051.jpg', 11, '2018-03-01 04:24:11', '2018-03-01 04:24:11'),
(26, 'images/municipalities/Municipality_20180228212420268784897.jpg', 11, '2018-03-01 04:24:20', '2018-03-01 04:24:20'),
(27, 'images/municipalities/Municipality_201802282124201948626423.jpg', 11, '2018-03-01 04:24:20', '2018-03-01 04:24:20'),
(29, 'images/municipalities/Municipality_20180228212638693030074.jpg', 12, '2018-03-01 04:26:38', '2018-03-01 04:26:38'),
(30, 'images/municipalities/Municipality_20180228213204831283488.jpg', 23, '2018-03-01 04:32:04', '2018-03-01 04:32:04'),
(31, 'images/municipalities/Municipality_20180228213204215411675.jpg', 23, '2018-03-01 04:32:04', '2018-03-01 04:32:04'),
(32, 'images/municipalities/Municipality_201802282157471340393466.jpg', 1, '2018-03-01 04:57:47', '2018-03-01 04:57:47'),
(33, 'images/municipalities/Municipality_20180228220553304448309.jpg', 1, '2018-03-01 05:05:53', '2018-03-01 05:05:53'),
(34, 'images/municipalities/Municipality_20180228220553315299986.jpg', 1, '2018-03-01 05:05:53', '2018-03-01 05:05:53'),
(35, 'images/municipalities/Municipality_201802282205571210133450.jpg', 1, '2018-03-01 05:05:57', '2018-03-01 05:05:57'),
(37, 'images/municipalities/Municipality_201803011758431503881052.jpg', 14, '2018-03-02 00:58:43', '2018-03-02 00:58:43'),
(38, 'images/municipalities/Municipality_20180301175843655335665.jpg', 14, '2018-03-02 00:58:43', '2018-03-02 00:58:43'),
(39, 'images/municipalities/Municipality_201803011758512031458608.jpg', 14, '2018-03-02 00:58:51', '2018-03-02 00:58:51'),
(40, 'images/municipalities/Municipality_201803011808291532144671.jpg', 22, '2018-03-02 01:08:29', '2018-03-02 01:08:29'),
(41, 'images/municipalities/Municipality_201803011808291310577499.jpg', 22, '2018-03-02 01:08:29', '2018-03-02 01:08:29'),
(42, 'images/municipalities/Municipality_201803011808411373594843.jpg', 22, '2018-03-02 01:08:41', '2018-03-02 01:08:41'),
(43, 'images/municipalities/Municipality_201803011810491081708933.jpg', 26, '2018-03-02 01:10:49', '2018-03-02 01:10:49'),
(44, 'images/municipalities/Municipality_20180301182031334579969.jpg', 25, '2018-03-02 01:20:31', '2018-03-02 01:20:31'),
(45, 'images/municipalities/Municipality_201803011820311524402368.jpg', 25, '2018-03-02 01:20:31', '2018-03-02 01:20:31'),
(46, 'images/municipalities/Municipality_20180301182814564902501.jpg', 27, '2018-03-02 01:28:14', '2018-03-02 01:28:14'),
(47, 'images/municipalities/Municipality_201803011828141222674663.jpg', 27, '2018-03-02 01:28:14', '2018-03-02 01:28:14'),
(48, 'images/municipalities/Municipality_20180301182828660937419.jpg', 27, '2018-03-02 01:28:28', '2018-03-02 01:28:28'),
(49, 'images/municipalities/Municipality_201803011836031705410894.jpg', 28, '2018-03-02 01:36:03', '2018-03-02 01:36:03'),
(50, 'images/municipalities/Municipality_201803011836031313041634.jpg', 28, '2018-03-02 01:36:03', '2018-03-02 01:36:03'),
(51, 'images/municipalities/Municipality_2018030119021595968551.jpg', 40, '2018-03-02 02:02:15', '2018-03-02 02:02:15'),
(52, 'images/municipalities/Municipality_20180301190215698151821.jpg', 40, '2018-03-02 02:02:15', '2018-03-02 02:02:15'),
(53, 'images/municipalities/Municipality_20180301190220732501492.jpg', 40, '2018-03-02 02:02:20', '2018-03-02 02:02:20'),
(54, 'images/municipalities/Municipality_201803011916532132510000.jpg', 4, '2018-03-02 02:16:53', '2018-03-02 02:16:53'),
(55, 'images/municipalities/Municipality_20180301191653616231605.jpg', 4, '2018-03-02 02:16:53', '2018-03-02 02:16:53'),
(56, 'images/municipalities/Municipality_201803011917051978769881.jpg', 4, '2018-03-02 02:17:06', '2018-03-02 02:17:06'),
(57, 'images/municipalities/Municipality_201803011925121972384057.jpg', 2, '2018-03-02 02:25:12', '2018-03-02 02:25:12'),
(58, 'images/municipalities/Municipality_201803011925121809344606.jpg', 2, '2018-03-02 02:25:12', '2018-03-02 02:25:12'),
(59, 'images/municipalities/Municipality_20180301193318628575700.jpg', 3, '2018-03-02 02:33:18', '2018-03-02 02:33:18'),
(60, 'images/municipalities/Municipality_20180301193318322919695.jpg', 3, '2018-03-02 02:33:18', '2018-03-02 02:33:18'),
(61, 'images/municipalities/Municipality_20180301193932420478524.jpg', 29, '2018-03-02 02:39:32', '2018-03-02 02:39:32'),
(62, 'images/municipalities/Municipality_20180301194845502570510.jpg', 32, '2018-03-02 02:48:45', '2018-03-02 02:48:45'),
(63, 'images/municipalities/Municipality_201803012002361682625300.jpg', 33, '2018-03-02 03:02:36', '2018-03-02 03:02:36'),
(64, 'images/municipalities/Municipality_2018030120023649745340.jpg', 33, '2018-03-02 03:02:36', '2018-03-02 03:02:36'),
(65, 'images/municipalities/Municipality_20180301204823511646892.jpg', 34, '2018-03-02 03:48:23', '2018-03-02 03:48:23'),
(66, 'images/municipalities/Municipality_20180301212505321055443.jpg', 35, '2018-03-02 04:25:05', '2018-03-02 04:25:05'),
(67, 'images/municipalities/Municipality_20180301213050875696693.jpg', 38, '2018-03-02 04:30:50', '2018-03-02 04:30:50'),
(68, 'images/municipalities/Municipality_20180301220148598383977.jpg', 9, '2018-03-02 05:01:48', '2018-03-02 05:01:48'),
(69, 'images/municipalities/Municipality_201803012213431067021058.jpg', 43, '2018-03-02 05:13:44', '2018-03-02 05:13:44'),
(70, 'images/municipalities/Municipality_20180301221344363356437.jpg', 43, '2018-03-02 05:13:44', '2018-03-02 05:13:44'),
(71, 'images/municipalities/Municipality_20180301222928682078676.jpg', 8, '2018-03-02 05:29:28', '2018-03-02 05:29:28'),
(72, 'images/municipalities/Municipality_201803012236271370136239.jpg', 18, '2018-03-02 05:36:27', '2018-03-02 05:36:27'),
(73, 'images/municipalities/Municipality_20180301224222603820871.jpg', 19, '2018-03-02 05:42:22', '2018-03-02 05:42:22'),
(74, 'images/municipalities/Municipality_20180301224222639544436.jpg', 19, '2018-03-02 05:42:22', '2018-03-02 05:42:22'),
(75, 'images/municipalities/Municipality_201803021504091881857512.jpg', 20, '2018-03-02 22:04:09', '2018-03-02 22:04:09'),
(76, 'images/municipalities/Municipality_20180302151612994269504.jpg', 21, '2018-03-02 22:16:12', '2018-03-02 22:16:12'),
(77, 'images/municipalities/Municipality_201803021516121194092375.jpg', 21, '2018-03-02 22:16:12', '2018-03-02 22:16:12'),
(78, 'images/municipalities/Municipality_201803021516151090897314.jpg', 21, '2018-03-02 22:16:15', '2018-03-02 22:16:15'),
(79, 'images/municipalities/Municipality_2018030215293095012983.jpg', 24, '2018-03-02 22:29:30', '2018-03-02 22:29:30'),
(80, 'images/municipalities/Municipality_201803021529301689056154.jpg', 24, '2018-03-02 22:29:30', '2018-03-02 22:29:30'),
(81, 'images/municipalities/Municipality_201803021529351138794699.jpg', 24, '2018-03-02 22:29:35', '2018-03-02 22:29:35'),
(82, 'images/municipalities/Municipality_20180302161335903816806.jpg', 49, '2018-03-02 23:13:35', '2018-03-02 23:13:35'),
(83, 'images/municipalities/Municipality_201803021613351988127368.jpg', 49, '2018-03-02 23:13:35', '2018-03-02 23:13:35'),
(84, 'images/municipalities/Municipality_20180302161338314636872.jpg', 49, '2018-03-02 23:13:38', '2018-03-02 23:13:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notices`
--

CREATE TABLE IF NOT EXISTS `notices` (
  `id_notice` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `notice_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_fair` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_notice`),
  KEY `notices_fk_fair_foreign` (`fk_fair`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `notices`
--

INSERT INTO `notices` (`id_notice`, `notice_name`, `description`, `image`, `state`, `slug`, `fk_fair`, `created_at`, `updated_at`) VALUES
(4, 'Expocundinamarca 2018: El Dorado, la leyenda vive', '<p>Del 13 al 17&nbsp;de septiembre se llevar&aacute; a cabo en las afueras de Bogot&aacute;, en el Hip&oacute;dromo de los Andr&eacute;s&nbsp;de Ch&iacute;a, la primera feria tur&iacute;stica, empresarial, gastron&oacute;mica y cultural del departamento de Cundinamarca, Expocundinamarca 2017.</p>\r\n\r\n<p>La feria pretende mostrar la riqueza y diversidad de Cundinamarca, involucrando a los artesanos, cocineros y ciudadanos&nbsp;en 56.000 m2 de imponente sabana cundinamarquesa. Se extender&aacute; la muestra de la gastronom&iacute;a, la cultura, la oferta tur&iacute;stica y la actividad econ&oacute;mica del departamento.</p>\r\n\r\n<p>El escenario de Expocundinamarca 2017&nbsp;contar&aacute; con parqueadero vigilado, ba&ntilde;os, Internet Wifi, Vigilancia privada y Plazoleta de comidas. Para mayor informaci&oacute;n del evento haga clic&nbsp;<a href="http://www.expocundinamarca.com.co/" target="_blank">aqu&iacute;.</a></p>', 'images/notices/Notice_20180301215400250305701.jpg', 'activo', 'expocundinamarca-2018-el-dorado-la-leyenda-vive', 16, '2018-03-02 04:54:00', '2018-03-02 04:54:00'),
(5, 'Expo Cundinamarca, vive la leyenda', '<p>Unos 250 expositores provenientes de los 116 municipios que hay en Cundinamarca se han dado cita en el hip&oacute;dromo de los Andes, en el kil&oacute;metro 20 de la autopista Norte, para participar de las ruedas de negocios apoyadas por la C&aacute;mara de Comercio de Bogot&aacute; y la Gobernaci&oacute;n. Tambi&eacute;n se realiz&oacute; una feria de empleo organizada por el Sena, en donde se presentaron cerca de 1.500 vacantes.</p>\r\n\r\n<p>Esta microrrueda laboral se repetir&aacute; el 20 de septiembre en Soacha, en donde se espera que se ofrezcan m&aacute;s de 4.500 en los sectores de servicios, comercio y transporte.</p>\r\n\r\n<p>La feria es una oportunidad para que quienes han emprendido sus propios proyectos productivos, a trav&eacute;s del fondo Emprender, del Sena, encuentren mercados nuevos.</p>', 'images/notices/Notice_201803022335101399100787.jpg', 'activo', 'expo-cundinamarca-vive-la-leyenda', 16, '2018-03-03 06:35:10', '2018-03-03 06:35:10'),
(6, 'El escenario de Expocundinamarca', '<p>Expocundinamarca, la&nbsp;primera feria tur&iacute;stica, empresarial, gastron&oacute;mica y cultural de Cundinamarca se llevar&aacute; a cabo a las afueras de Bogot&aacute;, en el Hip&oacute;dromo de los Andes del municipio de Ch&iacute;a, del 13 al 19 de Septiembre&nbsp;de 2017.</p>\r\n\r\n<p>La autopista norte, con sus 3 carriles de ingreso a la capital, permiten un acceso vehicular r&aacute;pido y seguro al sitio.</p>\r\n\r\n<p><strong>&Aacute;rea</strong></p>\r\n\r\n<p>En 56.000 m2 de imponente sabana cundinamarquesa, se extender&aacute; la muestra de la gastronom&iacute;a, la cultura, la oferta tur&iacute;stica y la actividad econ&oacute;mica del&nbsp;departamento.</p>', 'images/notices/Notice_2018030223411894950217.jpg', 'activo', 'el-escenario-de-expocundinamarca', 16, '2018-03-03 06:41:18', '2018-03-03 06:56:47'),
(7, 'Chía: luna de templo', '<p>Ch&iacute;a tiene aproximadamente 114.881 habitantes y es&nbsp;considerado el reino Chibcha de la regi&oacute;n del Cundinamarca. Es el lugar ideal para disfrutar de la arquitectura colonial, las historias de los antepasados mientras se delita el paladar con su gastronom&iacute;a t&iacute;pica e internacional.</p>\r\n\r\n<p>Uno de sus principales atractivos tur&iacute;sticos es el Castillo de Marroqu&iacute;n, situado en los predios de la Hacienda &ldquo;El Castillo&rdquo;; este fue edificado sobre planos traidos de Europa entre 1899 y 1900, bajo la direcci&oacute;n el arquitecto franc&eacute;s Gast&oacute;n Lelarge.</p>\r\n\r\n<p>Aparate de la cultura y la gastronom&iacute;a, en Ch&iacute;a los turistas pueden amenizar su viaje con buenas historias y relatos que el municipio tiene para contarles.</p>', 'images/notices/Notice_20180302234313162008082.jpg', 'activo', 'chia-luna-de-templo', 16, '2018-03-03 06:43:13', '2018-03-03 06:43:13'),
(8, 'Agenda de Expo Cundinamarca', '<p>Acceda de primera mano a la programaci&oacute;n de&nbsp;<a href="https://www.facebook.com/hashtag/expocundinamarca?source=feed_text">#Expocundinamarca</a></p>', 'images/notices/Notice_201803022351251293060479.jpg', 'activo', 'agenda-de-expo-cundinamarca', 16, '2018-03-03 06:51:25', '2018-03-03 06:51:25'),
(9, 'Así se vive Expo Cundinamarca', '<p><iframe frameborder="0" height="308" scrolling="no" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FJorgeReyCundinamarca%2Fvideos%2F1941280179453738%2F&amp;show_text=0&amp;width=560" style="border:none;overflow:hidden" width="540"></iframe></p>\r\n\r\n<p>Con una apuesta de identidad y uni&oacute;n entre la ciudad capital y Cundinamarca se ha lanzado oficialmente la marca Cundinamarca y su estrategia de posicionamiento: #ExpoCundinamarca; evento sorprendi&oacute; a los asistentes con la iluminaci&oacute;n de la Torre Colpatria; la cual destac&oacute; los colores del departamento y su nueva marca al ritmo de la agrupaci&oacute;n musical Jamaruk con la canci&oacute;n &quot;Kuntur Marqa&quot;, tema alusivo a las bondades de Cundinamarca.</p>\r\n\r\n<p>Reiteramos nuestra invitaci&oacute;n para que capitalinos, cundinamarqueses, colombianos y extranjeros apoyen esta gran iniciativa que promueve la identidad y la competitividad de nuestro departamento, del 13 al 17 de septiembre en el hip&oacute;dromo Los Andes.</p>', 'images/notices/Notice_20180302235620478312887.jpg', 'activo', 'asi-se-vive-expo-cundinamarca', 16, '2018-03-03 06:56:20', '2018-03-03 06:58:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operators`
--

CREATE TABLE IF NOT EXISTS `operators` (
  `id_operator` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `national_register` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_personal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `service_category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `web` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `days_for_reservation` int(11) NOT NULL DEFAULT '2',
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'operator',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_operator`),
  UNIQUE KEY `operators_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operator_password_resets`
--

CREATE TABLE IF NOT EXISTS `operator_password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `operator_password_resets_email_index` (`email`),
  KEY `operator_password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `portable_documents`
--

CREATE TABLE IF NOT EXISTS `portable_documents` (
  `id_pdf` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `link_pdf` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `type_relation` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_relation` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_pdf`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `portable_documents`
--

INSERT INTO `portable_documents` (`id_pdf`, `link_pdf`, `type_relation`, `fk_relation`, `created_at`, `updated_at`) VALUES
(1, 'pdf/fairs/Fair_201803030008261877420582.pdf', 'fair', 6, '2018-03-03 07:08:26', '2018-03-03 07:08:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `relation_routes`
--

CREATE TABLE IF NOT EXISTS `relation_routes` (
  `id_relation_route` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_relation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_relation` int(11) NOT NULL,
  `fk_route` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_relation_route`),
  KEY `relation_routes_fk_route_foreign` (`fk_route`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=204 ;

--
-- Volcado de datos para la tabla `relation_routes`
--

INSERT INTO `relation_routes` (`id_relation_route`, `type_relation`, `fk_relation`, `fk_route`, `created_at`, `updated_at`) VALUES
(158, 'municipality', 15, 1, '2018-02-28 23:23:24', '2018-02-28 23:23:24'),
(159, 'municipality', 16, 1, '2018-02-28 23:23:24', '2018-02-28 23:23:24'),
(160, 'municipality', 17, 1, '2018-02-28 23:23:24', '2018-02-28 23:23:24'),
(161, 'municipality', 44, 1, '2018-02-28 23:23:24', '2018-02-28 23:23:24'),
(162, 'municipality', 45, 1, '2018-02-28 23:23:24', '2018-02-28 23:23:24'),
(163, 'municipality', 46, 1, '2018-02-28 23:23:24', '2018-02-28 23:23:24'),
(164, 'municipality', 47, 1, '2018-02-28 23:23:24', '2018-02-28 23:23:24'),
(165, 'municipality', 48, 1, '2018-02-28 23:23:24', '2018-02-28 23:23:24'),
(166, 'municipality', 1, 3, '2018-03-01 00:48:58', '2018-03-01 00:48:58'),
(167, 'municipality', 14, 3, '2018-03-01 00:48:58', '2018-03-01 00:48:58'),
(168, 'municipality', 22, 3, '2018-03-01 00:48:58', '2018-03-01 00:48:58'),
(169, 'municipality', 25, 3, '2018-03-01 00:48:58', '2018-03-01 00:48:58'),
(170, 'municipality', 26, 3, '2018-03-01 00:48:58', '2018-03-01 00:48:58'),
(171, 'municipality', 27, 3, '2018-03-01 00:48:58', '2018-03-01 00:48:58'),
(172, 'municipality', 28, 3, '2018-03-01 00:48:58', '2018-03-01 00:48:58'),
(173, 'municipality', 40, 3, '2018-03-01 00:48:58', '2018-03-01 00:48:58'),
(174, 'municipality', 41, 3, '2018-03-01 00:48:58', '2018-03-01 00:48:58'),
(175, 'municipality', 42, 3, '2018-03-01 00:48:58', '2018-03-01 00:48:58'),
(182, 'municipality', 5, 2, '2018-03-02 04:43:37', '2018-03-02 04:43:37'),
(183, 'municipality', 6, 2, '2018-03-02 04:43:37', '2018-03-02 04:43:37'),
(184, 'municipality', 7, 2, '2018-03-02 04:43:37', '2018-03-02 04:43:37'),
(185, 'municipality', 9, 2, '2018-03-02 04:43:37', '2018-03-02 04:43:37'),
(186, 'municipality', 10, 2, '2018-03-02 04:43:37', '2018-03-02 04:43:37'),
(187, 'municipality', 11, 2, '2018-03-02 04:43:37', '2018-03-02 04:43:37'),
(188, 'municipality', 12, 2, '2018-03-02 04:43:37', '2018-03-02 04:43:37'),
(189, 'municipality', 8, 5, '2018-03-02 04:46:11', '2018-03-02 04:46:11'),
(190, 'municipality', 18, 5, '2018-03-02 04:46:11', '2018-03-02 04:46:11'),
(191, 'municipality', 19, 5, '2018-03-02 04:46:11', '2018-03-02 04:46:11'),
(192, 'municipality', 20, 5, '2018-03-02 04:46:11', '2018-03-02 04:46:11'),
(193, 'municipality', 21, 5, '2018-03-02 04:46:11', '2018-03-02 04:46:11'),
(194, 'municipality', 24, 5, '2018-03-02 04:46:11', '2018-03-02 04:46:11'),
(195, 'municipality', 2, 4, '2018-03-02 23:12:43', '2018-03-02 23:12:43'),
(196, 'municipality', 3, 4, '2018-03-02 23:12:43', '2018-03-02 23:12:43'),
(197, 'municipality', 29, 4, '2018-03-02 23:12:43', '2018-03-02 23:12:43'),
(198, 'municipality', 32, 4, '2018-03-02 23:12:43', '2018-03-02 23:12:43'),
(199, 'municipality', 33, 4, '2018-03-02 23:12:43', '2018-03-02 23:12:43'),
(200, 'municipality', 34, 4, '2018-03-02 23:12:43', '2018-03-02 23:12:43'),
(201, 'municipality', 35, 4, '2018-03-02 23:12:43', '2018-03-02 23:12:43'),
(202, 'municipality', 38, 4, '2018-03-02 23:12:43', '2018-03-02 23:12:43'),
(203, 'municipality', 49, 4, '2018-03-02 23:12:43', '2018-03-02 23:12:43');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `routes`
--

CREATE TABLE IF NOT EXISTS `routes` (
  `id_route` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_relation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `link_map` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `fk_relation` int(11) NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_route`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `routes`
--

INSERT INTO `routes` (`id_route`, `name`, `type_relation`, `description`, `link_map`, `keywords`, `fk_relation`, `state`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Ruta del Agua', 'municipality', '<p>Al nororiente de <strong>Cundinamarca</strong> se encuentra el escenario ideal para realizar un recorrido ecotur&iacute;stico y cultural. Empieza en el municipio de <strong>La Calera</strong> para continuar por <strong>F&oacute;meque</strong>, <strong>Guasca</strong>, <strong>Jun&iacute;n</strong>, <strong>Gachet&aacute;</strong>, <strong>Gama</strong>, <strong>Gachal&aacute;</strong>, <strong>Ubal&aacute;</strong> y <strong>Medina</strong>. Este viaje, que se adentra en la cuna del pueblo muisca, simboliza el balance perfecto entre la naturaleza y la historia.</p>\r\n\r\n<p>Su nombre, <strong>Ruta del Agua</strong>, nace por ser este territorio la m&aacute;s importante fuente de recursos h&iacute;dricos del departamento, tanto que abastece a Bogot&aacute; y genera el 25% de la energ&iacute;a hidroel&eacute;ctrica del pa&iacute;s, situando el preciado l&iacute;quido como elemento integrador para el desarrollo de la actividad tur&iacute;stica de la regi&oacute;n.</p>', 'images/routes/route201802201809371817851463.png', 'Fómeque,Guasca,Junín,Gachetá,Gama,Gachalá,Ubalá,Medina,La Calera', 13, 'activo', 'ruta-del-agua', '2018-02-15 21:21:32', '2018-02-21 01:09:37'),
(2, 'Ruta Dulce y de Aventura', 'municipality', '<p>Deleitarse con los dulces derivados de la ca&ntilde;a de az&uacute;car, como la panela, las melcochas y otros deliciosos postres, cautivar&aacute; tu paladar, igual que los platos t&iacute;picos de la exquisita gastronom&iacute;a de esta&nbsp;regi&oacute;n como el chupao, el triveguno, el viudo de capaz, el brazo de reina, el cartucho de Guaduas y el rosc&oacute;n resobado, entre otras muchas recetas que vienen de anta&ntilde;o.</p>\r\n\r\n<p>Conformada por los municipios de <strong>Facatativ&aacute;, El Rosal, San Francisco, La Vega, Nocaima, Vergara, Nimaima, &Uacute;tica, Villeta, Guaduas</strong> y <strong>Puerto Salgar</strong>, esta ruta permite el disfrute de diversas actividades en espacios naturales y culturales. Se destaca la pr&aacute;ctica de deportes de aventura como rafting, parapente o torrentismo, as&iacute; como el recorrido por lugares de invaluable valor hist&oacute;rico, entre ellos Guaduas, cuna de la hero&iacute;na Policarpa Salavarrieta.</p>', 'images/routes/route201802201809541785004455.png', 'Facatativá,La Vega,El Rosal,Nocaima,San Francisco,Vergara,Nimaima,Útica,Villeta,Guaduas,Puerto Salgar,La Peña,Cundinamarca', 31, 'activo', 'ruta-dulce-y-de-aventura', '2018-02-17 04:48:25', '2018-02-21 01:09:54'),
(3, 'Ruta del Dorado', 'municipality', '<p>Desde &eacute;pocas inmemoriales los seres humanos hemos tenido las necesidades de comunicarnos y de encontrar nuevos horizontes. Esto nos ha llevado a establecer relaciones con otros grupos humanos y realizar un m&aacute;gico e invaluable intercambio cultural, social, econ&oacute;mico y natural, brind&aacute;ndonos la oportunidad de conocer distintas formas de vida, lugares, productos y entornos naturales.</p>\r\n\r\n<p>En esta ruta descubrir&aacute;s que muchos de los productos artesanales y gastron&oacute;micos que ofrecen los campesinos evocan la tradici&oacute;n ind&iacute;gena: <strong>objetos a base de carb&oacute;n, prendas hechas en lana y diversos alimentos derivados de la leche</strong>.</p>', 'images/routes/route20180220181011531857923.png', 'Cota,Tenjo,Tabio,Chía,Cajicá,Zipaquirá,Tocancipá,Gachancipá,Sesquilé,Nemocón,Cogua,Suesca,Guatavita,Tausa,Sutatausa,Cucunubá,Lenguazaque,Ubaté,Carmen de Caupa,Fúquena,Guachetá,Susa,Simijaca', 23, 'activo', 'ruta-del-dorado', '2018-02-17 05:13:42', '2018-03-01 00:48:58'),
(4, 'Ruta del Río y el Encanto Natural', 'municipality', '<p>Este es uno de los m&aacute;s antiguos recorridos tur&iacute;sticos del departamento, predilecto de propios y turistas durante la temporada de vacaciones.</p>\r\n\r\n<p>Integrada por los municipios de <strong>Soacha, Sibat&eacute;, granada, Silvania, Fusagasug&aacute;, Nilo, Ricaurte, Girardot, Tocaima, Agua de Dios, Viot&aacute;, Apulo, Anapoima, El Colegio, La Mesa, Tena, San Antonio del Tequendama, Mosquera, Funza</strong> y <strong>Madrid</strong>, la Ruta del R&iacute;o y el Encanto Natural invita a la contemplaci&oacute;n alrededor de los r&iacute;os Magdalena, Calandaima, apulo y Sumapaz, entre otros.</p>\r\n\r\n<p>La Ribera del r&iacute;o m&aacute;s grande del pa&iacute;s, insignia del comercio y la industria nacional, te espera para vivir las experiencias y actividades m&aacute;s encantadoras en torno del Reinado Nacional del Turismo. Sum&eacute;rgete en los ba&ntilde;os de lodo o en las m&aacute;s de mil piscinas de la zona y del&eacute;itate con el mejor caf&eacute; de la regi&oacute;n, mientras recorres los antiguos caminos reales del Tequendama, que fueron testigos de la Expedici&oacute;n Bot&aacute;nica.</p>', 'images/routes/route20180220181032128321161.png', 'Funza', 4, 'activo', 'ruta-del-rio-y-el-encanto-natural', '2018-02-17 05:31:53', '2018-02-27 04:58:19'),
(5, 'Ruta del del Vuelo del Cóndor', 'municipality', '<p>Recorrer&nbsp;Cundinamarca es adentrarse en el vuelo del c&oacute;ndor, ave que le da origen al nombre del departamento. Es viajar por valles y monta&ntilde;as, disfrutar de su magia y encanto, contemplar y deleitarse con la inmortal tierra de Don Antonio Nari&ntilde;o.</p>\r\n\r\n<p>Esta <strong>Ruta del C&oacute;ndor</strong> se convierte en el mejor escenario para descubrir los tesoros del ecosistema natural, que se entrelaza con las bondades de sus gentes, su hospitalidad, su amabilidad y su pujanza.</p>', 'images/routes/route20180301214012284545819.png', 'Cundinamarca,Sasaima,Paratebueno,Choachí,Pacho,Sopó,Une', 43, 'activo', 'ruta-del-del-vuelo-del-condor', '2018-03-02 04:40:12', '2018-03-02 04:46:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `schedules`
--

CREATE TABLE IF NOT EXISTS `schedules` (
  `id_schedule` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_service` int(10) unsigned NOT NULL,
  `day` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hora_inicio` time DEFAULT NULL,
  `hora_receso` time DEFAULT NULL,
  `tiempo_receso` int(11) DEFAULT NULL,
  `hora_final` time DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_schedule`),
  KEY `schedules_fk_service_foreign` (`fk_service`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `id_service` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_service` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fk_service_category` int(10) unsigned NOT NULL,
  `fk_ecosystem_category` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_service`),
  KEY `services_fk_service_category_foreign` (`fk_service_category`),
  KEY `services_fk_ecosystem_category_foreign` (`fk_ecosystem_category`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=32 ;

--
-- Volcado de datos para la tabla `services`
--

INSERT INTO `services` (`id_service`, `service`, `type_service`, `description`, `image`, `fk_service_category`, `fk_ecosystem_category`, `created_at`, `updated_at`) VALUES
(1, 'Torrentismo', 'Alto impacto', '', 'images/torentismo-1.jpg', 1, 1, '2017-10-24 01:55:27', '2017-10-24 01:55:27'),
(2, 'Rafting', 'Alto impacto', '', 'images/rafting.jpg', 1, 1, '2017-10-24 01:55:28', '2017-10-24 01:55:28'),
(4, 'Buceo ', 'Alto impacto', '', NULL, 1, 1, '2017-10-24 01:55:28', '2017-10-24 01:55:28'),
(5, 'Pesca Deportiva ', 'Alto impacto', '', NULL, 1, 1, '2017-10-24 01:55:28', '2017-10-24 01:55:28'),
(6, 'Canyoning ', 'Alto impacto', '', NULL, 1, 1, '2017-10-24 01:55:28', '2017-10-24 01:55:28'),
(7, 'Balsaje ', 'Alto impacto', '', NULL, 1, 1, '2017-10-24 01:55:29', '2017-10-24 01:55:29'),
(8, 'Bote ', 'Alto impacto', '', NULL, 1, 1, '2017-10-24 01:55:29', '2017-10-24 01:55:29'),
(9, 'Kayaking ', 'Alto impacto', '', NULL, 1, 1, '2017-10-24 01:55:29', '2017-10-24 01:55:29'),
(10, 'Jet Sky ', 'Alto impacto', '', NULL, 1, 1, '2017-10-24 01:55:29', '2017-10-24 01:55:29'),
(11, 'Surf ', 'Alto impacto', '', NULL, 1, 1, '2017-10-24 01:55:29', '2017-10-24 01:55:29'),
(12, 'Barranquismo ', 'Alto impacto', '', NULL, 1, 1, '2017-10-24 01:55:29', '2017-10-24 01:55:29'),
(13, 'Parapente ', 'Alto impacto', '', 'images/parapente.jpg', 1, 3, '2017-10-24 01:55:29', '2017-10-24 01:55:29'),
(14, 'Ala Delta ', 'Alto impacto', '', NULL, 1, 3, '2017-10-24 01:55:30', '2017-10-24 01:55:30'),
(15, 'Paracaidismo', 'Alto impacto', '', NULL, 1, 3, '2017-10-24 01:55:30', '2017-10-24 01:55:30'),
(16, 'Globo', 'Alto impacto', '', NULL, 1, 3, '2017-10-24 01:55:30', '2017-10-24 01:55:30'),
(17, 'Ultraliviano', 'Alto impacto', '', NULL, 1, 3, '2017-10-24 01:55:30', '2017-10-24 01:55:30'),
(18, 'Canopy', 'Alto impacto', '', NULL, 1, 3, '2017-10-24 01:55:30', '2017-10-24 01:55:30'),
(19, 'Tirolesa', 'Alto impacto', '', NULL, 1, 3, '2017-10-24 01:55:30', '2017-10-24 01:55:30'),
(20, 'Tirolina ', 'Alto impacto', '', NULL, 1, 3, '2017-10-24 01:55:30', '2017-10-24 01:55:30'),
(21, 'Tirolina ', 'Alto impacto', '', NULL, 1, 2, '2017-10-24 01:55:30', '2017-10-24 01:55:30'),
(22, 'Rappel ', 'Alto impacto', '', NULL, 1, 2, '2017-10-24 01:55:31', '2017-10-24 01:55:31'),
(23, 'Trekking ', 'Alto impacto', '', NULL, 1, 2, '2017-10-24 01:55:31', '2017-10-24 01:55:31'),
(24, 'Alta montaña ', 'Alto impacto', '', NULL, 1, 2, '2017-10-24 01:55:31', '2017-10-24 01:55:31'),
(25, 'Espeleología ', 'Alto impacto', '', NULL, 1, 2, '2017-10-24 01:55:32', '2017-10-24 01:55:32'),
(26, 'Bungee Jumping  ', 'Alto impacto', '', NULL, 1, 2, '2017-10-24 01:55:32', '2017-10-24 01:55:32'),
(27, 'Escalada  ', 'Alto impacto', '', NULL, 1, 2, '2017-10-24 01:55:32', '2017-10-24 01:55:32'),
(28, 'Espeleísmo', 'Alto impacto', '', NULL, 1, 2, '2017-10-24 01:55:33', '2017-10-24 01:55:33'),
(29, 'Excursiones 4x4', 'Alto impacto', '', NULL, 1, 2, '2017-10-24 01:55:33', '2017-10-24 01:55:33'),
(30, 'Cabalgata', 'Alto impacto', '', NULL, 1, 2, '2017-10-24 01:55:34', '2017-10-24 01:55:34'),
(31, 'Agencia de viajes', '', 'Una agencia de viajes es una empresa asociada al turismo, cuyo oficio es la intermediación, organización y realización de proyectos, planes e itinerarios, elaboración y venta de productos turísticos entre sus clientes y determinados proveedores de viajes', NULL, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_categories`
--

CREATE TABLE IF NOT EXISTS `service_categories` (
  `id_category` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service_category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_category`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `service_categories`
--

INSERT INTO `service_categories` (`id_category`, `service_category`, `created_at`, `updated_at`) VALUES
(1, 'Aventura', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_operators`
--

CREATE TABLE IF NOT EXISTS `service_operators` (
  `id_service_operator` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_service` int(10) unsigned NOT NULL,
  `fk_operator` int(10) unsigned NOT NULL,
  `cost` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `capacity` int(11) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `requisites` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Inhabilitado',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_service_operator`),
  KEY `service_operators_fk_service_foreign` (`fk_service`),
  KEY `service_operators_fk_operator_foreign` (`fk_operator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_pictures`
--

CREATE TABLE IF NOT EXISTS `service_pictures` (
  `id_picture` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `link_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_service` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_picture`),
  KEY `service_pictures_fk_service_foreign` (`fk_service`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user',
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_change` tinyint(1) NOT NULL DEFAULT '1',
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_password_resets`
--

CREATE TABLE IF NOT EXISTS `user_password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `user_password_resets_email_index` (`email`),
  KEY `user_password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

CREATE TABLE IF NOT EXISTS `videos` (
  `id_video` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `link_video` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `type_relation` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_relation` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_video`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=105 ;

--
-- Volcado de datos para la tabla `videos`
--

INSERT INTO `videos` (`id_video`, `link_video`, `type_relation`, `fk_relation`, `created_at`, `updated_at`) VALUES
(3, 'QKm-SOOMC4c', 'municipality', 13, '2018-03-01 02:47:38', '2018-03-01 02:47:38'),
(4, '8lsB-P8nGSM', 'municipality', 13, '2018-03-01 02:49:28', '2018-03-01 02:49:28'),
(5, 'MgJITGvVfR0', 'municipality', 15, '2018-03-01 02:54:46', '2018-03-01 02:54:46'),
(6, 'a1OoOdTNiUM', 'municipality', 15, '2018-03-01 02:56:31', '2018-03-01 02:56:31'),
(7, 'a1OoOdTNiUM', 'municipality', 16, '2018-03-01 02:57:38', '2018-03-01 02:57:38'),
(8, 'MgJITGvVfR0', 'municipality', 16, '2018-03-01 02:57:46', '2018-03-01 02:57:46'),
(9, 'MgJITGvVfR0', 'municipality', 17, '2018-03-01 03:03:59', '2018-03-01 03:03:59'),
(10, 'a1OoOdTNiUM', 'municipality', 17, '2018-03-01 03:04:08', '2018-03-01 03:04:08'),
(11, 'a1OoOdTNiUM', 'municipality', 44, '2018-03-01 03:20:09', '2018-03-01 03:20:09'),
(12, 'MgJITGvVfR0', 'municipality', 44, '2018-03-01 03:20:16', '2018-03-01 03:20:16'),
(13, 'a1OoOdTNiUM', 'municipality', 45, '2018-03-01 03:21:59', '2018-03-01 03:21:59'),
(14, 'MgJITGvVfR0', 'municipality', 45, '2018-03-01 03:22:06', '2018-03-01 03:22:06'),
(15, 'MgJITGvVfR0', 'municipality', 46, '2018-03-01 03:32:14', '2018-03-01 03:32:14'),
(17, 'a1OoOdTNiUM', 'municipality', 47, '2018-03-01 03:33:56', '2018-03-01 03:33:56'),
(18, 'MgJITGvVfR0', 'municipality', 47, '2018-03-01 03:34:02', '2018-03-01 03:34:02'),
(19, 'MgJITGvVfR0', 'municipality', 48, '2018-03-01 03:42:58', '2018-03-01 03:42:58'),
(20, 'a1OoOdTNiUM', 'municipality', 48, '2018-03-01 03:43:04', '2018-03-01 03:43:04'),
(21, 'a1OoOdTNiUM', 'municipality', 31, '2018-03-01 03:52:41', '2018-03-01 03:52:41'),
(22, 'MgJITGvVfR0', 'municipality', 31, '2018-03-01 03:52:48', '2018-03-01 03:52:48'),
(23, 'MgJITGvVfR0', 'municipality', 5, '2018-03-01 03:59:58', '2018-03-01 03:59:58'),
(24, 'a1OoOdTNiUM', 'municipality', 5, '2018-03-01 04:00:04', '2018-03-01 04:00:04'),
(25, '3xGJZoaTODQ', 'municipality', 6, '2018-03-01 04:02:08', '2018-03-01 04:02:08'),
(26, 'L_tqK4eqelA', 'municipality', 6, '2018-03-01 04:02:14', '2018-03-01 04:02:14'),
(27, 'L_tqK4eqelA', 'municipality', 7, '2018-03-01 04:03:08', '2018-03-01 04:03:08'),
(28, 'a1OoOdTNiUM', 'municipality', 7, '2018-03-01 04:03:14', '2018-03-01 04:03:14'),
(29, 'a1OoOdTNiUM', 'municipality', 10, '2018-03-01 04:16:44', '2018-03-01 04:16:44'),
(30, '3xGJZoaTODQ', 'municipality', 10, '2018-03-01 04:17:05', '2018-03-01 04:17:05'),
(31, 'L_tqK4eqelA', 'municipality', 11, '2018-03-01 04:18:14', '2018-03-01 04:18:14'),
(32, '3xGJZoaTODQ', 'municipality', 11, '2018-03-01 04:18:33', '2018-03-01 04:18:33'),
(33, '3xGJZoaTODQ', 'municipality', 12, '2018-03-01 04:25:42', '2018-03-01 04:25:42'),
(34, 'a1OoOdTNiUM', 'municipality', 12, '2018-03-01 04:25:51', '2018-03-01 04:25:51'),
(35, 'UfEiKK-iX70', 'municipality', 23, '2018-03-01 04:31:19', '2018-03-01 04:31:19'),
(36, 'L_tqK4eqelA', 'municipality', 23, '2018-03-01 04:31:25', '2018-03-01 04:31:25'),
(37, 'UfEiKK-iX70', 'municipality', 1, '2018-03-01 04:57:24', '2018-03-01 04:57:24'),
(38, '3xGJZoaTODQ', 'municipality', 1, '2018-03-01 04:57:32', '2018-03-01 04:57:32'),
(39, 'UfEiKK-iX70', 'municipality', 14, '2018-03-02 00:45:51', '2018-03-02 00:45:51'),
(40, '3xGJZoaTODQ', 'municipality', 14, '2018-03-02 00:45:57', '2018-03-02 00:45:57'),
(41, 'f-9ijiN31LI', 'municipality', 22, '2018-03-02 01:08:51', '2018-03-02 01:08:51'),
(42, 'a1OoOdTNiUM', 'municipality', 22, '2018-03-02 01:08:59', '2018-03-02 01:08:59'),
(43, 'UfEiKK-iX70', 'municipality', 26, '2018-03-02 01:10:16', '2018-03-02 01:10:16'),
(44, 'L_tqK4eqelA', 'municipality', 26, '2018-03-02 01:10:24', '2018-03-02 01:10:24'),
(45, 'f-9ijiN31LI', 'municipality', 25, '2018-03-02 01:19:52', '2018-03-02 01:19:52'),
(46, '3xGJZoaTODQ', 'municipality', 25, '2018-03-02 01:19:59', '2018-03-02 01:19:59'),
(47, 'f-9ijiN31LI', 'municipality', 27, '2018-03-02 01:28:34', '2018-03-02 01:28:34'),
(48, 'MgJITGvVfR0', 'municipality', 27, '2018-03-02 01:28:41', '2018-03-02 01:28:41'),
(49, 'a1OoOdTNiUM', 'municipality', 28, '2018-03-02 01:35:10', '2018-03-02 01:35:10'),
(50, 'UfEiKK-iX70', 'municipality', 28, '2018-03-02 01:35:17', '2018-03-02 01:35:17'),
(51, 'L_tqK4eqelA', 'municipality', 40, '2018-03-02 02:01:43', '2018-03-02 02:01:43'),
(52, '3xGJZoaTODQ', 'municipality', 40, '2018-03-02 02:01:50', '2018-03-02 02:01:50'),
(53, 'L_tqK4eqelA', 'municipality', 4, '2018-03-02 02:17:11', '2018-03-02 02:17:11'),
(54, 'UfEiKK-iX70', 'municipality', 4, '2018-03-02 02:17:19', '2018-03-02 02:17:19'),
(55, 'VsjA3jkYwws', 'municipality', 2, '2018-03-02 02:26:52', '2018-03-02 02:26:52'),
(56, '0FK-Hig3SGw', 'municipality', 2, '2018-03-02 02:26:58', '2018-03-02 02:26:58'),
(57, '0FK-Hig3SGw', 'municipality', 3, '2018-03-02 02:32:46', '2018-03-02 02:32:46'),
(58, 'VsjA3jkYwws', 'municipality', 3, '2018-03-02 02:32:54', '2018-03-02 02:32:54'),
(59, 'a1OoOdTNiUM', 'municipality', 29, '2018-03-02 02:38:58', '2018-03-02 02:38:58'),
(60, '0FK-Hig3SGw', 'municipality', 29, '2018-03-02 02:39:06', '2018-03-02 02:39:06'),
(61, 'VsjA3jkYwws', 'municipality', 32, '2018-03-02 02:48:50', '2018-03-02 02:48:50'),
(62, 'L_tqK4eqelA', 'municipality', 32, '2018-03-02 02:48:57', '2018-03-02 02:48:57'),
(63, 'UfEiKK-iX70', 'municipality', 34, '2018-03-02 03:47:57', '2018-03-02 03:47:57'),
(64, '0FK-Hig3SGw', 'municipality', 34, '2018-03-02 03:48:04', '2018-03-02 03:48:04'),
(65, 'f-9ijiN31LI', 'municipality', 35, '2018-03-02 04:25:09', '2018-03-02 04:25:09'),
(66, 'VsjA3jkYwws', 'municipality', 35, '2018-03-02 04:25:20', '2018-03-02 04:25:20'),
(67, '0FK-Hig3SGw', 'municipality', 38, '2018-03-02 04:30:28', '2018-03-02 04:30:28'),
(68, 'VsjA3jkYwws', 'municipality', 38, '2018-03-02 04:30:34', '2018-03-02 04:30:34'),
(69, 'UfEiKK-iX70', 'municipality', 9, '2018-03-02 05:01:21', '2018-03-02 05:01:21'),
(70, 'f-9ijiN31LI', 'municipality', 9, '2018-03-02 05:01:28', '2018-03-02 05:01:28'),
(71, 'VsjA3jkYwws', 'municipality', 43, '2018-03-02 05:13:52', '2018-03-02 05:13:52'),
(72, 'L_tqK4eqelA', 'municipality', 43, '2018-03-02 05:14:01', '2018-03-02 05:14:01'),
(73, '3xGJZoaTODQ', 'municipality', 8, '2018-03-02 05:29:56', '2018-03-02 05:29:56'),
(74, 'f-9ijiN31LI', 'municipality', 8, '2018-03-02 05:30:07', '2018-03-02 05:30:07'),
(75, 'UfEiKK-iX70', 'municipality', 18, '2018-03-02 05:34:04', '2018-03-02 05:34:04'),
(76, 'VsjA3jkYwws', 'municipality', 18, '2018-03-02 05:34:09', '2018-03-02 05:34:09'),
(77, 'L_tqK4eqelA', 'municipality', 19, '2018-03-02 05:41:54', '2018-03-02 05:41:54'),
(78, '0FK-Hig3SGw', 'municipality', 19, '2018-03-02 05:42:01', '2018-03-02 05:42:01'),
(79, 'UfEiKK-iX70', 'municipality', 20, '2018-03-02 22:03:08', '2018-03-02 22:03:08'),
(80, '0FK-Hig3SGw', 'municipality', 20, '2018-03-02 22:03:15', '2018-03-02 22:03:15'),
(81, 'a1OoOdTNiUM', 'municipality', 21, '2018-03-02 22:15:36', '2018-03-02 22:15:36'),
(82, 'L_tqK4eqelA', 'municipality', 21, '2018-03-02 22:15:44', '2018-03-02 22:15:44'),
(83, 'a1OoOdTNiUM', 'municipality', 24, '2018-03-02 22:29:42', '2018-03-02 22:29:42'),
(84, 'f-9ijiN31LI', 'municipality', 24, '2018-03-02 22:29:50', '2018-03-02 22:29:50'),
(85, 'MgJITGvVfR0', 'municipality', 49, '2018-03-02 23:14:11', '2018-03-02 23:14:11'),
(86, '0FK-Hig3SGw', 'municipality', 49, '2018-03-02 23:14:18', '2018-03-02 23:14:18'),
(87, 'o9oVbXeBHvM', 'site', 13, '2018-03-03 01:12:33', '2018-03-03 01:12:33'),
(88, 'o9oVbXeBHvM', 'site', 19, '2018-03-03 01:18:03', '2018-03-03 01:18:03'),
(89, 'o9oVbXeBHvM', 'site', 23, '2018-03-03 01:18:20', '2018-03-03 01:18:20'),
(90, 'o9oVbXeBHvM', 'site', 12, '2018-03-03 01:18:47', '2018-03-03 01:18:47'),
(91, 'o9oVbXeBHvM', 'site', 15, '2018-03-03 01:19:55', '2018-03-03 01:19:55'),
(92, 'o9oVbXeBHvM', 'site', 16, '2018-03-03 03:04:44', '2018-03-03 03:04:44'),
(93, 'o9oVbXeBHvM', 'site', 22, '2018-03-03 03:18:21', '2018-03-03 03:18:21'),
(94, 'o9oVbXeBHvM', 'site', 14, '2018-03-03 03:35:31', '2018-03-03 03:35:31'),
(95, 'o9oVbXeBHvM', 'site', 17, '2018-03-03 03:40:38', '2018-03-03 03:40:38'),
(96, 'o9oVbXeBHvM', 'site', 3, '2018-03-03 03:41:55', '2018-03-03 03:41:55'),
(97, 'o9oVbXeBHvM', 'site', 18, '2018-03-03 03:55:40', '2018-03-03 03:55:40'),
(98, 'o9oVbXeBHvM', 'site', 11, '2018-03-03 04:00:09', '2018-03-03 04:00:09'),
(99, 't99N223fqCo', 'fair', 7, '2018-03-03 11:18:36', '2018-03-03 11:18:36'),
(100, 't99N223fqCo', 'fair', 2, '2018-03-03 11:21:03', '2018-03-03 11:21:03'),
(101, 't99N223fqCo', 'fair', 6, '2018-03-03 11:23:59', '2018-03-03 11:23:59'),
(102, 't99N223fqCo', 'fair', 3, '2018-03-03 12:05:51', '2018-03-03 12:05:51'),
(103, 't99N223fqCo', 'fair', 4, '2018-03-03 12:09:06', '2018-03-03 12:09:06'),
(104, 't99N223fqCo', 'fair', 5, '2018-03-03 12:13:04', '2018-03-03 12:13:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wallpapers`
--

CREATE TABLE IF NOT EXISTS `wallpapers` (
  `id_wallpaper` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `link_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `primary_color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `secundary_color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_wallpaper`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `wallpapers`
--

INSERT INTO `wallpapers` (`id_wallpaper`, `link_image`, `primary_color`, `secundary_color`, `state`, `created_at`, `updated_at`) VALUES
(1, 'wallpapers/wall_20180301163458816419915.jpg', '#1e2338', '#de463c', 'activo', '2018-02-21 01:33:55', '2018-03-01 23:34:58'),
(2, 'wallpapers/wall_20180301163525449202876.jpg', '#1e2338', '#de463c', 'activo', '2018-02-21 01:34:40', '2018-03-01 23:35:25'),
(3, 'wallpapers/wall_20180301163557997122518.jpg', '#1e2338', '#de463c', 'activo', '2018-02-27 04:19:16', '2018-03-01 23:36:01'),
(4, 'wallpapers/wall_201802272145581035028375.jpg', '#1e2338', '#de463c', 'inactivo', '2018-02-28 03:33:37', '2018-02-28 04:45:58'),
(5, 'wallpapers/wall_20180227214638349016605.jpg', '#1e2338', '#de463c', 'inactivo', '2018-02-28 03:35:20', '2018-02-28 04:46:38'),
(6, 'wallpapers/wall_20180227214658142699138.jpg', '#1e2338', '#de463c', 'inactivo', '2018-02-28 03:36:37', '2018-02-28 04:46:58');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `fair_images`
--
ALTER TABLE `fair_images`
  ADD CONSTRAINT `fair_images_fk_fair_foreign` FOREIGN KEY (`fk_fair`) REFERENCES `fairs` (`id_fair`);

--
-- Filtros para la tabla `interest_sites`
--
ALTER TABLE `interest_sites`
  ADD CONSTRAINT `interest_sites_fk_category_foreign` FOREIGN KEY (`fk_category`) REFERENCES `interest_site_categories` (`id_category`),
  ADD CONSTRAINT `interest_sites_fk_municipality_foreign` FOREIGN KEY (`fk_municipality`) REFERENCES `municipalities` (`id_municipality`);

--
-- Filtros para la tabla `interest_site_images`
--
ALTER TABLE `interest_site_images`
  ADD CONSTRAINT `interest_site_images_fk_site_foreign` FOREIGN KEY (`fk_site`) REFERENCES `interest_sites` (`id_site`);

--
-- Filtros para la tabla `municipality_images`
--
ALTER TABLE `municipality_images`
  ADD CONSTRAINT `municipality_images_fk_municipality_foreign` FOREIGN KEY (`fk_municipality`) REFERENCES `municipalities` (`id_municipality`);

--
-- Filtros para la tabla `notices`
--
ALTER TABLE `notices`
  ADD CONSTRAINT `notices_fk_fair_foreign` FOREIGN KEY (`fk_fair`) REFERENCES `fairs` (`id_fair`);

--
-- Filtros para la tabla `relation_routes`
--
ALTER TABLE `relation_routes`
  ADD CONSTRAINT `relation_routes_fk_route_foreign` FOREIGN KEY (`fk_route`) REFERENCES `routes` (`id_route`);

--
-- Filtros para la tabla `schedules`
--
ALTER TABLE `schedules`
  ADD CONSTRAINT `schedules_fk_service_foreign` FOREIGN KEY (`fk_service`) REFERENCES `service_operators` (`id_service_operator`);

--
-- Filtros para la tabla `services`
--
ALTER TABLE `services`
  ADD CONSTRAINT `services_fk_ecosystem_category_foreign` FOREIGN KEY (`fk_ecosystem_category`) REFERENCES `ecosystem_categories` (`id_ecosystem_category`),
  ADD CONSTRAINT `services_fk_service_category_foreign` FOREIGN KEY (`fk_service_category`) REFERENCES `service_categories` (`id_category`);

--
-- Filtros para la tabla `service_operators`
--
ALTER TABLE `service_operators`
  ADD CONSTRAINT `service_operators_fk_operator_foreign` FOREIGN KEY (`fk_operator`) REFERENCES `operators` (`id_operator`),
  ADD CONSTRAINT `service_operators_fk_service_foreign` FOREIGN KEY (`fk_service`) REFERENCES `services` (`id_service`);

--
-- Filtros para la tabla `service_pictures`
--
ALTER TABLE `service_pictures`
  ADD CONSTRAINT `service_pictures_fk_service_foreign` FOREIGN KEY (`fk_service`) REFERENCES `service_operators` (`id_service_operator`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
