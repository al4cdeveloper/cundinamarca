## Sistema de gestión para Gobernación de Cundinamarca

Aplicación realizada para gobernación de cundinamarca.
Se soluciona la problemática de su falta de información acerca del departamento y cada uno de sus municipios.
El CSM permite la gestión de información y todo tipo de multimedia. 
- Municipios.
- Sitios relacionados.
- Operadores turisticos.
- Ferias y fiestas.


Además tiene un panel administrativo para la gestión de cada imagen de inicio, teniendo como parametros un texto principal, un texto secundario y un  link de redirección.



## Licencia

Licencia privada, todos los derechos reservados, propiedad intelectual Puntos Suspensivos editores.
