<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Socialite;
use Auth;
use App\User;
use App\Operator;

class SocialAuthController extends Controller
{
	// Metodo encargado de la redireccion a Facebook
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }
    
    public function handleProviderCallback($provider)
    {
        // Obtenemos los datos del usuario
        $social_user = Socialite::driver($provider)->user(); 
        // Comprobamos si el usuario ya existe
        if ($user = User::where('email', $social_user->email)->first()) 
        {
            return $this->authAndRedirect($user); // Login y redirección
        }
        elseif($operator = Operator::where('email',$social_user->email)->first())
        {
        	return $this->AuthOperator($operator);
        } 
        else {  
            // En caso de que no exista creamos un nuevo usuario con sus datos.
            $user = User::create([
                'first_name' => $social_user->name,
                'email' => $social_user->email,
                'role' => 'user',
                'avatar' => $social_user->avatar,
                'password_change'=>0,
            ]);

            return $this->authAndRedirect($user); // Login y redirección
        }
    }

    // Login y redirección
    public function authAndRedirect($user)
    {
        Auth::login($user);


        return redirect()->to('/home');
    }

    public function AuthOperator($operator)
    {
    	Auth::guard('operator')->login($operator);

    	return redirect()->to('operator/home');
    }
}
