@extends('admin.layout.auth')

@if(isset($municipality))
    @section('title', 'Actualizar municipio')
@else
    @section('title', 'Crear municipio')
@endif
@section('aditionalStyle')
    <link href="{{asset('plugins/kartik-v-bootstrap-fileinput/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <style>
.kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar {
    display: inline-block;
}
.kv-avatar .file-input {
    display: table-cell;
    width: 213px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@endsection
@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Municipio</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
            @if(isset($municipality))
			{!!Form::model($municipality,['url'=>['admin/municipalities/update',$municipality->id_municipality],'method'=>'POST', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@else
			{!!Form::open(['url'=>'admin/municipalities/store', 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@endif
            <div class="col-sm-12">
                <div class="col-sm-4 text-center col-sm-offset-4">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen principal del municipio</label>
                            <div class="file-loading">

                                {!!Form::file('link_image',['id'=>'avatar-1'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('link_image') }}</span>
                        </div>
                    </div>
                </div>
            </div>
			<div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Nombre</label>
                    {!!Form::text('municipality_name', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre del municipio', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('municipality_name') }}</span>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div class="col-sm-5 col-xs-12">
                    <div class="form-group">
                        <label for="costo">Latitud</label>
                        {!!Form::text('latitude', null, ['class'=>'form-control', 'placeholder' => 'Ingrese latitud', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('latitude') }}</span>
                    </div>
                </div>
                <div class="col-sm-5 col-md-offset-1 col-xs-12">
                    <div class="form-group">
                        <label for="costo">Longitud</label>
                        {!!Form::text('longitude', null, ['class'=>'form-control', 'placeholder' => 'Ingrese longitud', 'required'])!!}
    					<span class="label label-danger">{{$errors->first('longitude') }}</span>
                    </div>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div class="col-sm-6 col-md-offset-3">
                    <div class="form-group">
                        <label for="costo">Imagen para descargar (opcional)</label>
                        @if(isset($municipality))
                            @if($municipality->downloadimage)
                                <h5>Ya se ha cargado anteriormente la imagen, si se quiere actualizar por favor seleccionar acontinuacion</h5>
                            @endif
                        @endif
                        <div class="file-loading">
                            <input id="input-20" type="file" placeholder="Seleccione" name="downloadimage">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="costo">Iframe (Opcional)</label>
                    {!!Form::text('iframe', null, ['class'=>'form-control', 'placeholder' => 'Inserte url de iframe en caso de tenerlo', 'required'])!!}
                    <span class="label label-danger">{{$errors->first('iframe') }}</span>
                </div>
            </div>
            <div class="col-sm-12"> 
                <div class="form-group col-md-5 col-xs-12">
                        <div class="form-group">
                            <label for="costo">Clima</label>
                            {!!Form::select('weather', $weather, null, ['class'=>'select-chosen'])!!}
                        </div>
                </div>
                <div class="col-sm-5 col-md-offset-1 col-xs-12">
                    <div class="form-group">
                            <label for="costo">URL Oficial del municipio (Opcional)</label>
                            {!!Form::text('officialpage', null, ['class'=>'form-control', 'placeholder' => 'Inserte url de iframe en caso de tenerlo', 'required'])!!}
                            <span class="label label-danger">{{$errors->first('officialpage') }}</span>
                        </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                <label >Palabras clave <small>Separadas por comas (,)</small></label>
                    {!!Form::text('keywords',null,['class'=>'form-control input-tags', 'placeholder'=>'Palabras clave'])!!}
                    <span class="label label-danger">{{$errors->first('keywords') }}</span>
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <label for="duration">Descripción</label>
                <div class="form-group">
                    <textarea id="textarea-ckeditor" name="description" class="ckeditor">@if(isset($municipality)){!!$municipality->description!!}@endif</textarea>
					<span class="label label-danger">{{$errors->first('description') }}</span>
                </div>
	        </div>
            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                <a href="{{url('admin/municipalities')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection

@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>
        <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/plugins/sortable.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/fileinput.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/fr.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/es.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/explorer-fa/theme.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/fa/theme.js')}}" type="text/javascript"></script>

    <script>
                $(document).on('ready', function() {
            $("#input-20").fileinput({
                browseClass: "btn btn-primary btn-block",
                browseLabel: "Seleccionar imagen",
                showRemove: false,
                showUpload: false

            });
        });

    $("#avatar-1").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="fab fa-searchengin"></i>',
        removeIcon: '<i class="far fa-times-circle"></i>',
        viewIcon:'<i class="far fa-times-circle"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        @if(isset($municipality))
        defaultPreviewContent: '<img src="{{asset($municipality->link_image)}}" alt="Avatar por defecto" width="50%">',
        @else
        defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
        @endif
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });
</script>
@endsection