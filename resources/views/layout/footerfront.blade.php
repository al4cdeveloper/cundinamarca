    
    <footer class="revealed">
        <div class="container">
            <div class="row" id="top_links">
                <div class="col-md-12" align="center">
                    <img src="{{asset('img/header/idecut.png')}}" >
                    <img src="{{asset('img/header/gobcundinamarca.png')}}" >
                </div>
            </div>
            {{-- <div class="row">
                <div class="col-md-4 col-sm-3">
                    <h3>Need help?</h3>
                    <a href="tel://004542344599" id="phone">+45 423 445 99</a>
                    <a href="mailto:help@citytours.com" id="email_footer">help@citytours.com</a>
                </div>
                <div class="col-md-3 col-sm-3">
                    <h3>About</h3>
                    <ul>
                        <li><a href="#">About us</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Login</a></li>
                        <li><a href="#">Register</a></li>
                         <li><a href="#">Terms and condition</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-3">
                    <h3>Discover</h3>
                    <ul>
                        <li><a href="#">Community blog</a></li>
                        <li><a href="#">Tour guide</a></li>
                        <li><a href="#">Wishlist</a></li>
                         <li><a href="#">Gallery</a></li>
                    </ul>
                </div>
                <div class="col-md-2 col-sm-3">
                    <h3>Settings</h3>
                    <div class="styled-select">
                        <select class="form-control" name="lang" id="lang">
                            <option value="English" selected>English</option>
                            <option value="French">French</option>
                            <option value="Spanish">Spanish</option>
                            <option value="Russian">Russian</option>
                        </select>
                    </div>
                    <div class="styled-select">
                        <select class="form-control" name="currency" id="currency">
                            <option value="USD" selected>USD</option>
                            <option value="EUR">EUR</option>
                            <option value="GBP">GBP</option>
                            <option value="RUB">RUB</option>
                        </select>
                    </div>
                </div>
            </div><!-- End row --> --}}
            <div class="row">
                <div class="col-md-12">
                    <div id="social_footer">
                               <h3> <a href="{{url('contact')}}" ><i class="icon_set_1_icon-84"></i>  Contáctanos </a></h3>
                        <ul>
                            <li><a href="#"><i class="icon-facebook"></i></a></li>
                            <li><a href="#"><i class="icon-twitter"></i></a></li>
                            <li><a href="#"><i class="icon-google"></i></a></li>
                            <li><a href="#"><i class="icon-instagram"></i></a></li>
                            <li><a href="#"><i class="icon-pinterest"></i></a></li>
                            <li><a href="#"><i class="icon-vimeo"></i></a></li>
                            <li><a href="#"><i class="icon-youtube-play"></i></a></li>
                            <li><a href="#"><i class="icon-linkedin"></i></a></li>
                        </ul>
                        <p>© Puntos Suspensivos Editores 2018</p>
                    </div>
                </div>
            </div><!-- End row -->
        </div><!-- End container -->
    </footer><!-- End footer -->

    <div id="toTop"></div><!-- Back to top button -->
    
    <!-- Search Menu -->
    <div class="search-overlay-menu">
        <span class="search-overlay-close"><i class="icon_set_1_icon-77"></i></span>
        <form role="search" id="searchform" method="get">
            <input value=""  type="search" placeholder="Buscar..." id="searchInput" />
            <ul id="result">
                
            </ul>
            <button type="button"><i class="icon_set_1_icon-78"></i>
            </button>
        </form>
    </div><!-- End Search Menu -->

 <!-- Common scripts -->
<script src="{{asset('front/js/jquery-2.2.4.min.js')}}"></script>
<script src="{{asset('front/js/common_scripts_min.js')}}"></script>
<script src="{{asset('front/js/functions.js')}}"></script>

<script type="text/javascript">
    var controladorTiempo = "";


    $("#searchInput").on("keyup", function() {
        clearTimeout(controladorTiempo);
        controladorTiempo = setTimeout(search, 800);
    });
    function search()
    {
        busqueda = $("#searchInput").val();

        if(busqueda!="")
        {
            var token = "{{csrf_token()}}";
            var route = '/api/search'
            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'GET',
                dataType: 'json',
                data:{busqueda},
                complete: function(transport)
                {
                    data = transport.responseJSON;

                    // console.log(data);
                    $("#result").empty();
                    if(data.length>0)
                    {
                        for(x in data)
                        {
                            html = '<li class="selectSearch"><a href="{{url('')}}'+data[x].redirect+'">'+data[x].name+'</a></li>'
                            $("#result").append(html);
                        }
                    }
                    else
                    {
                        html = '<li class="selectSearch">No se encontraron resultados</li>'
                            $("#result").append(html);
                    }
                    //DEPARTAMENTOS
                    // departments = data['departments'];
                }
              });
        }
    }
</script>
