<?php

namespace App\Http\Controllers\Operator;

use Illuminate\Http\Request;
use App\Http\Requests\ImageRequest;
use App\Http\Controllers\Controller;
use App\Models\ServiceOperator;
use App\Models\ServiceItem;
use App\Models\Service;
use App\Models\Schedule;
use Auth;
use App\Models\Reservation;
use App\Models\ServicePicture;
use File;
use Session;
use \Cviebrock\EloquentSluggable\Services\SlugService;

class serviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $services = Auth::user()->services;

        $days = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sábado','Domingo'];
        foreach($services as $service)
        {
            $detailService = ServiceOperator::find($service->pivot->id_service_operator);
            $horario = Schedule::where('fk_service',$detailService->id_service_operator)->where('state','active')->get();

            if(count($horario)>0)
            {
                array_add($service,'days',$horario);
            }

        }
        // return ['services'=>$services,'days'=>$days];

        return view('operator.Service.service',compact('services','days'));
    }

    public function indexitem($id)
    {
        $items = ServiceItem::all()->where('fk_service',$id);
        $service = ServiceOperator::find($id);
        // dd($items);
        return view('operator.Service.listItem',compact('items','service'));
    }

    public function create()
    {   
        $days = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sábado','Domingo'];
        $services = Service::all();
        return view('operator/Service/new-editService',compact('services','days'));
    }

    public function createItem($id)
    {
        $service = ServiceOperator::find($id);
        return view('operator.Service.createEditItem',compact('service'));
    }

    public function edit($id)
    {
        $service = ServiceOperator::find($id);
        $days = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sábado','Domingo'];
        $services = Service::all();
        return view('operator/Service/new-editService',compact('services','days','service'));
    }

    public function editItem($id)
    {
        $item = ServiceItem::find($id);
        $service = ServiceOperator::where('id_service_operator',$item->fk_service)->first();
        return view('operator.Service.createEditItem',compact('item','service'));
    }

    public function shedule($id)
    {
        $service = ServiceOperator::find($id);
        $horario = Schedule::where('fk_service',$service->id_service_operator)->where('state','active')->get();

        return view('operator.Service.calendar',compact('service','horario'));
    }

    public function updateshedule(Request $request)
    {
        foreach ($request->service as $key => $value) 
        {
            $service = Schedule::find($key);

            $service->hora_inicio = $value['hora_inicio'];
            $service->hora_receso = $value['hora_receso'];
            $service->tiempo_receso = $value['tiempo_receso'];
            $service->hora_final = $value['hora_final'];
            $service->save();
        }

        $principalService = ServiceOperator::find($service->fk_service);
        $principalService->state = "Activo";
        $principalService->save();

        return redirect('operator/services');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'service'     => 'required|max:150',
            'cost' => 'required',
            'address'    => 'required|max:150',
            'days'    => 'required',
            'capacity'    => 'required',
            'duration'    => 'required',
            'location'    => 'required',
            'description'    => 'required',
            'requisites'    => 'required'
        ]);


        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $service = new ServiceOperator;
            $service->fk_service = $request->service;
            $service->fk_operator  =Auth::user()->id_operator;
            $service->cost = $request->cost;
            $service->address = $request->address;
            $service->capacity = $request->capacity;
            $service->duration = $request->duration;
            $service->location = $request->location;
            $service->requisites = $request->requisites;
            $service->description = $request->description;
            $service->save();

            foreach($request->days as $day)
            {
                $schedule = new Schedule;
                $schedule->fk_service = $service->id_service_operator;
                $schedule->day = $day;
                $schedule->state = 'active';
                $schedule->save();
            }
        }

        \Session::flash('message', 'Servicio registrado correctamente');

        return redirect('operator/services');
    }

    public function storeItem(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'item_name'     => 'required|max:150',
            'cost' => 'required',
            'description'    => 'required',
        ]);


        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else 
        {
            $item = new ServiceItem;
            $item->item_name = $request->item_name;
            $item->cost = $request->cost;
            $item->description = $request->description;
            $item->fk_service = $request->fk_service;
            $item->save();

            Session::flash('message','Se ha realizado el registro correctamente');
            return redirect('operator/items/'.$request->fk_service);
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'service'     => 'required|max:150',
            'cost' => 'required',
            'address'    => 'required|max:150',
            'days'    => 'required',
            'capacity'    => 'required',
            'duration'    => 'required',
            'location'    => 'required',
            'description'    => 'required',
            'requisites'    => 'required'
        ]);


        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else
        {

            $service = ServiceOperator::find($id);
            $service->cost = $request->cost;
            $service->address = $request->address;
            $service->capacity = $request->capacity;
            $service->duration = $request->duration;
            $service->location = $request->location;
            $service->requisites = $request->requisites;
            $service->description = $request->description;
            $service->save();

            foreach($request->days as $day)
            {
                $newDay = Schedule::where('fk_service',$service->id_service_operator)->where('day',$day)->first();
                if($newDay)
                {
                    $newDay->state = 'active';
                    $newDay->save();
                }
                else
                {
                    $schedule = new Schedule;
                    $schedule->fk_service = $service->id_service_operator;
                    $schedule->day = $day;
                    $schedule->state = 'active';
                    $schedule->save();
                }

            }

            // Inhabilitación de días que no se trabaja
            $days = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sábado','Domingo'];

            foreach($request->days as $day)
            {
                if(in_array($day,$days))
                {
                    $pos = array_search($day,$days);
                    unset($days[$pos]);
                }
            }
            foreach ($days as $dayoff) 
            {
                $newDay = Schedule::where('fk_service',$service->id_service_operator)->where('day',$dayoff)->first();
                if($newDay)
                {

                    $newDay->state = 'inactive';
                    $newDay->save();
                }
            }
            // ServiceOperator::find($id)->update($request->all());
            // if($service->state =="Inhabilitado")
            // {
            //     $service->state = 'Activo';
            //     $service->save();
            // }
            \Session::flash('message', 'Se ha realizado la actualización correctamente');

            return redirect('operator/services');
        }
        
    }

    public function updateItem($id,Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'item_name'     => 'required|max:150',
            'cost' => 'required',
            'description'    => 'required',
        ]);


        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else 
        {
            $item = ServiceItem::find($id);
            $item->item_name = $request->item_name;
            $item->cost = $request->cost;
            $item->description = $request->description;
            $item->fk_service = $request->fk_service;
            $item->save();

            Session::flash('message','Se ha actualizado la infomación correctamente');
            return redirect('operator/items/'.$request->fk_service);
        }
    }

    public function desactivateService($id)
    {
        $service = ServiceOperator::find($id);
        if($service)
        {
            $service->state = "inactivo";
            $service->save();
            Session::flash('message','Se ha desactivado el servicio correctamente');
            return redirect('operator/services/');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el servicio.");
            return redirect('operator');
        }
    }

    public function activateService($id)
    {
        $service = ServiceOperator::find($id);
        if($service)
        {
            $service->state = "Activo";
            $service->save();
            Session::flash('message','Se ha activado el servicio correctamente');
            return redirect('operator/services/');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el servicio.");
            return redirect('operator');
        }
    }

    public function desactivate($id)
    {
        $item = ServiceItem::find($id);
        if($item)
        {
            $item->state = "inactivo";
            $item->save();
            Session::flash('message','Se ha desactivado el servicio adicional correctamente');
            return redirect('operator/items/'.$item->fk_service);
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el servicio adicional.");
            return redirect('operator/services');
        }
    }

    public function activate($id)
    {
        $item = ServiceItem::find($id);
        if($item)
        {
            $item->state = "activo";
            $item->save();
            Session::flash('message','Se ha activado el servicio adicional correctamente');
            return redirect('operator/items/'.$item->fk_service);
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el servicio adicional.");
            return redirect('operator/services');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = ServiceOperator::find($id);
        $service->state = 'Desactivado';
        $service->save();
    }

    public function listService()
    {
        $services = Auth::user()->services;
        foreach ($services as $service) 
        {
            $reservations[] = Reservation::where('fk_service',$service->pivot->id_service_operator)->get();
        }
        return view('operator.Service.listReservation',compact('reservations'));
    }

    public function calendarReservation()
    {
        $services = Auth::user()->services;
        foreach ($services as $service) 
        {
            $reservations[] = Reservation::where('fk_service',$service->pivot->id_service_operator)->get();
        }
        return view('operator.Service.calendarReservation',compact('reservations'));
    }

    public function images($id)
    {
        $service = ServiceOperator::find($id);
        $images = ServicePicture::where('fk_service',$service->id_service_operator)->get();

        return view('operator.Service.imageService', compact('images','service'));
    }

    public function upload_images($id, ImageRequest $request)
    {
        $files = $request->file('file');

        $new_service = [];

        foreach ($files as $file) {
            $imageName = $this->cargar_imagen($file);

            if ($imageName)  
            {
                $new_image = ['link_image' => 'images/services/'.$imageName, 'fk_service' => $id];
                $service = ServicePicture::create($new_image);
                $new_service[] = $service->toArray();
                unset($new_image);
            }
        }

        if (count($new_service) > 0) {
            return \Response::json(['response' => 'Subida de imágenes completa'], 200);
        } else {
            return \Response::json(['response' => 'Falla en la subida de imágenes2'], 404);
        }
    }

    public function delete_image($id)
    {
        $image = ServicePicture::find($id);

        $exists = File::exists(public_path("images/services/".$image->link_imagen));
        if ($exists) {
            File::delete(public_path("images/services/".$image->link_imagen));
        }

        if ($image->delete()) 
        {
            \Session::flash('message', 'Imagen Borrada Correctamente');
        } 
        else 
        {
            \Session::flash('message-error', 'La Imagen no puede ser borrada');
        }

        return redirect()->back();
    }

    public function serviceSuccess($id)
    {
        $reservation = Reservation::find($id);
        $reservation->state = "Realizada";
        $reservation->save();

        return redirect('operator/reservas'); 
    }

    private function cargar_imagen($file, $imageName = false)
    {
        if ($imageName) 
        {
            $exists = File::exists(public_path("images/services/".$imageName));
            if ($exists) 
            {
                File::delete(public_path("images/services/".$imageName));
            }

            $image = explode('.', $imageName);
            $imageName = $image[0].'.'.$file->getClientOriginalExtension();
        } 
        else 
        {
            $imageName = 'Service_'.date('YmdHis', time()).rand().'.'.$file->getClientOriginalExtension();
        }

        $file->move(public_path('images/services'), $imageName);

        $exists = File::exists(public_path("images/services/".$imageName));

        if ($exists) 
        {
            return $imageName;
        } 
        else 
        {
            return false;
        }
    }

}
