<div id="sidebar-scroll" style="margin-top: 15%">
    <!-- Sidebar Content -->
    <div class="sidebar-content sidebar-light">
        <!-- Sidebar Navigation -->
        <ul class="sidebar-nav">
            <li @if(Request::path()=="admin/home") class="active" @endif>
                <a href="{{url('admin/home')}}"><i class="gi gi-compass sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard</span></a>
            </li>
            {{-- <li class="sidebar-separator">
                <i class="fa fa-ellipsis-h"></i>
            </li> --}}
            <li @if(substr(Request::path(), 0,20)=="admin/municipalities") class="active" @endif>
                <a href="#" class="sidebar-nav-menu">
                    {{-- Flecha que indica select --}}
                    <i class="fa fa-chevron-left sidebar-nav-indicator sidebar-nav-mini-hide icon-mt"></i> 
                    {{-- Icono de la izquierda --}}
                    <i class="fab fa-cloudsmith"></i>
                    <span class="sidebar-nav-mini-hide">Municipios</span></a>
                <ul>
                    <li @if(Request::path()=="admin/municipalities") class="active" @endif>
                        <a href="{{url('admin/municipalities/')}}">Lista de municipios</a>
                    </li>
                    <li @if(Request::path()=="admin/municipalities/create") class="active" @endif>
                        <a href="{{url('admin/municipalities/create')}}">Crear municipio</a>
                    </li>
                </ul>
            </li>
            
            <li @if(substr(Request::path(), 0,19)=="admin/interestsites") class="active" @endif>
                <a href="#" class="sidebar-nav-menu">
                    {{-- Flecha que indica select --}}
                    <i class="fa fa-chevron-left sidebar-nav-indicator sidebar-nav-mini-hide icon-mt"></i> 
                    {{-- Icono de la izquierda --}}
                    <i class="fab fa-connectdevelop"></i>
                    <span class="sidebar-nav-mini-hide">Sitios de interés</span></a>
                <ul>
                    <li @if(Request::path()=="admin/interestsites/category") class="active" @endif>
                        <a href="{{url('admin/interestsites/category')}}">Categorias</a>
                    </li>
                    <li @if(Request::path()=="admin/interestsites") class="active" @endif>
                        <a href="{{url('admin/interestsites/')}}">Sitios de interes</a>
                    </li>
                </ul>
            </li>
            <li @if(substr(Request::path(), 0,12)=="admin/routes") class="active" @endif>
                <a href="{{url('admin/routes')}}"><i class="fa fa-road sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Rutas</span></a>
            </li>
            <li @if(substr(Request::path(), 0,11)=="admin/fairs") class="active" @endif>
                <a href="{{url('admin/fairs')}}"><i class="fab fa-first-order sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Ferias y fiestas</span></a>
            </li>
            <li @if(substr(Request::path(), 0,19)=="admin/notices" || substr(Request::path(), 0,34)=="admin/fairs/edit/expo-cundinamarca") class="active" @endif>
                <a href="#" class="sidebar-nav-menu">
                    {{-- Flecha que indica select --}}
                    <i class="fa fa-chevron-left sidebar-nav-indicator sidebar-nav-mini-hide icon-mt"></i> 
                    {{-- Icono de la izquierda --}}
                    <i class="fas fa-star"></i>
                    <span class="sidebar-nav-mini-hide">Expocundinamarca</span></a>
                <ul>
                    <li @if(substr(Request::path(), 0,13)=="admin/notices") class="active" @endif>
                        <a href="{{url('admin/notices')}}">Noticias</a>
                    </li>
                    <li @if(substr(Request::path(), 0,34)=="admin/fairs/edit/expo-cundinamarca") class="active" @endif>
                        <a href="{{url('admin/fairs/edit/expo-cundinamarca')}}">Editar</a>
                    </li>
                </ul>
            </li>
            <li class="sidebar-separator">
                <i class="fas fa-ellipsis-h"></i>
            </li> 
            <li @if(substr(Request::path(), 0,16)=="admin/wallpapers") class="active" @endif>
                <a href="{{url('admin/wallpapers')}}"><i class="fab fa-mix sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Wallpapers inicio</span></a>
            </li>
            <li class="sidebar-separator">
                <i class="fas fa-ellipsis-h"></i>
            </li> 
            <li @if(substr(Request::path(), 0,15)=="admin/operators") class="active" @endif>
                <a href="{{url('admin/operators')}}"><i class="fas fa-id-card sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Operadores</span></a>
            </li>
            <li @if(substr(Request::path(), 0,11)=="admin/users") class="active" @endif>
                <a href="{{url('admin/users')}}"><i class="fas fa-users sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Usuarios</span></a>
            </li>

            
{{--             <li class="sidebar-separator">
                <i class="fas fa-ellipsis-h"></i>
            </li>
            <li @if(substr(Request::path(), 0,13)=="admin/contact") class="active" @endif>
                <a href="{{url('admin/contact')}}"><i class="fas fa-comment-alt sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Contacto <STRONG>FALTAAA!!!</STRONG></span></a>
            </li>
 --}}
            
            {{-- <li @if(substr(Request::path(), 0,14)=="admin/circuits") class="active" @endif>
                <a href="{{url('admin/services')}}"><i class="fa fa-subway sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Servicios</span></a>
            </li> --}}
        </ul>
        <!-- END Sidebar Navigation -->
    </div>
    <!-- END Sidebar Content -->
</div>
