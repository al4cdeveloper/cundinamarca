@extends('admin.layout.auth')

@section('title', 'Listado de imágenes')


@section('content')
    <div id="page-content">
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Imágenes</h1>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="block full"> --}}
            {{-- <form action="{{url('admin/wallpapers/store')}}" enctype="multipart/form-data" method="post">
                {{csrf_field()}}
                <div class="row">   
                    <div class="col-sm-4">
                        <input type="file" name="image" required>
                    </div>
                    <div class="col-md-5">
                        {!!Form::text('url', null, ['class'=>'form-control', 'placeholder' => 'Ingrese URL de redirección de imagen', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('url') }}</span>
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-primary" type="submit" >Registrar</button>
                    </div>
                </div>
            </form> --}}

        {{-- </div> --}}

            <div class="block full">
                <div class="block-title">
                    <h2>Imágenes</h2>
                    <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/wallpapers/create')}}" class="btn btn-primary btn-sm"><i class="far fa-plus-square fa-lg"></i></a></div>
                </div>
                <div class="table-responsive">
                    <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                        <thead>
                            <tr>
                                <th >Imagen</th>
                                <th class="text-center" style="width: 90px;">URL de redirección</th>
                                {{-- <th class="text-center" style="width: 90px;">Color secundario</th> --}}
                                <th >Estado</th>
                                <th class="text-center" style="width: 90px;"><i class="fas fa-sync-alt"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($wallpapers as $wall)
                            <tr>
                                
                                <td><img src="{{asset($wall->link_image)}}" height="100px" width="auto"></td>
                                <td>{{$wall->url}}</td>
                                {{-- <td><div class="input-group input-colorpicker"><input type="text" id="example-colorpicker2" name="secundary_color" class="form-control" value="{{$wall->secundary_color}}" required><span class="input-group-addon"><i></i></span></div>
                                </td> --}}
                                <td>@if($wall->state=="activo")
                                    <a class="label label-info">{{$wall->state}}</a>
                                    @else
                                    <span class="label label-danger">{{$wall->state}}</span>
                                    @endif
                                </td>
                                <td class="text-center">

                                    <a href="{{url('admin/wallpapers/edit/'.$wall->id_wallpaper)}}" data-toggle="tooltip" title="Editar wallpaper" class="btn btn-effect-ripple btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                    @if($wall->state=="activo")
                                    <a href="{{url('admin/wall/desactivate/'.$wall->id_wallpaper)}}" data-toggle="tooltip" title="Desactivar wallpaper" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                    @else
                                    <a href="{{url('admin/wall/activate/'.$wall->id_wallpaper)}}" data-toggle="tooltip" title="Activar wallpaper" class="btn btn-effect-ripple btn-xs btn-success"><i class="fas fa-check"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
@endsection

@section('aditionalScript')
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
@endsection