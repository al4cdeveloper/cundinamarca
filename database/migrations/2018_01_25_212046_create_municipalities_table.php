<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMunicipalitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('municipalities', function (Blueprint $table) {
            $table->increments('id_municipality');
            $table->string('municipality_name');
            $table->text('description');
            $table->string('multimedia_type');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('slug');
            $table->string('type_last_user');
            $table->string('downloadimage')->nullable();
            $table->string('iframe')->nullable();
            $table->string('officialpage')->nullable();
            $table->string('weather');
            //Existirán 2 tipos de edición, en caso de ser realizada por administrador quedará directamente guardado el id. En caso de que sea por un reporter quedará con la referencia del historial
            $table->string('link_image');
            $table->integer('fk_last_edition');
            $table->text('keywords');
            $table->string('state')->default('activo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('municipalities');
    }
}
