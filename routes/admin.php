<?php

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('admin')->user();

    //dd($users);
    return view('admin.home');
})->name('home');

//ROUTE REGIONS
Route::resource('regions','Admin\RegionController');
Route::post('regions/store','Admin\RegionController@store');
Route::get('regions/edit/{slug}','Admin\RegionController@edit');
Route::post('regions/update/{id}','Admin\RegionController@update');
Route::get('regions/desactivate/{id}','Admin\RegionController@desactivate');
Route::get('regions/activate/{id}','Admin\RegionController@activate');
Route::get('regions/images/{slug}','Admin\RegionController@images');
Route::put('regions/upload_images/{id}','Admin\RegionController@upload_images');
Route::delete('regions/delete_image/{id}','Admin\RegionController@delete_image');

//ROUTE DEPARTMENT
Route::resource('departments','Admin\DepartmentController');
Route::post('departments/store','Admin\DepartmentController@store');
Route::get('departments/edit/{slug}','Admin\DepartmentController@edit');
Route::post('departments/update/{id}','Admin\DepartmentController@update');
Route::get('departments/desactivate/{id}','Admin\DepartmentController@desactivate');
Route::get('departments/activate/{id}','Admin\DepartmentController@activate');
Route::get('departments/images/{slug}','Admin\DepartmentController@images');
Route::put('departments/upload_images/{id}','Admin\DepartmentController@upload_images');
Route::delete('departments/delete_image/{id}','Admin\DepartmentController@delete_image');

//ROUTE MUNICIPALY
route::resource('municipalities','Admin\MunicipalityController');
Route::post('municipalities/store','Admin\MunicipalityController@store');
Route::get('municipalities/edit/{slug}','Admin\MunicipalityController@edit');
Route::post('municipalities/update/{id}','Admin\MunicipalityController@update');
Route::get('municipalities/desactivate/{id}','Admin\MunicipalityController@desactivate');
Route::get('municipalities/activate/{id}','Admin\MunicipalityController@activate');
Route::get('municipalities/images/{slug}','Admin\MunicipalityController@images');
Route::put('municipalities/upload_images/{id}','Admin\MunicipalityController@upload_images');
Route::delete('municipalities/delete_image/{id}','Admin\MunicipalityController@delete_image');
Route::post('municipalities/upload_video/{id}','Admin\MunicipalityController@upload_video');
Route::delete('municipalities/delete_video/{id}','Admin\MunicipalityController@delete_video');

//INTEREST SITES
	//CATEGORY
	Route::get('interestsites/category','Admin\InterestSiteController@indexCategory');
	Route::get('interestsites/category/create','Admin\InterestSiteController@createCategory');
	Route::post('interestsites/category/store','Admin\InterestSiteController@storeCategory');
	Route::get('interestsites/category/edit/{slug}','Admin\InterestSiteController@editCategory');
	Route::post('interestsites/category/update/{id}','Admin\InterestSiteController@updateCategory');
	Route::get('interestsites/category/desactivate/{id}','Admin\InterestSiteController@desactivateCategory');
	Route::get('interestsites/category/activate/{id}','Admin\InterestSiteController@activateCategory');

Route::get('interestsites/getoperators','Admin\InterestSiteController@getOperators');
Route::resource('interestsites','Admin\InterestSiteController');
Route::post('interestsites/store','Admin\InterestSiteController@store');
Route::get('interestsites/edit/{slug}','Admin\InterestSiteController@edit');
Route::post('interestsites/update/{id}','Admin\InterestSiteController@update');
Route::get('interestsites/desactivate/{id}','Admin\InterestSiteController@desactivate');
Route::get('interestsites/activate/{id}','Admin\InterestSiteController@activate');
Route::get('interestsites/images/{slug}','Admin\InterestSiteController@images');
Route::put('interestsites/upload_images/{id}','Admin\InterestSiteController@upload_images');
Route::delete('interestsites/delete_image/{id}','Admin\InterestSiteController@delete_image');
Route::post('interestsites/upload_video/{id}','Admin\InterestSiteController@upload_video');
Route::delete('interestsites/delete_video/{id}','Admin\InterestSiteController@delete_video');

Route::post('circuits/getmunicipality','Admin\CircuitController@getMunicipality');
Route::post('circuits/getreladepartment','Admin\CircuitController@getRelationalDepartment');
Route::post('circuits/getdepartment','Admin\CircuitController@getDepartment');
Route::get('circuits/edit/{slug}','Admin\CircuitController@edit');
Route::post('circuits/update/{id}','Admin\CircuitController@update');
Route::get('circuits/desactivate/{id}','Admin\CircuitController@desactivate');
Route::get('circuits/activate/{id}','Admin\CircuitController@activate');
Route::resource('circuits','Admin\CircuitController');
Route::post('circuits/store','Admin\CircuitController@store');

Route::get('routes/edit/{slug}','Admin\RouteController@edit');
Route::post('routes/update/{id}','Admin\RouteController@update');
Route::get('routes/desactivate/{id}','Admin\RouteController@desactivate');
Route::get('routes/activate/{id}','Admin\RouteController@activate');
Route::post('routes/store','Admin\RouteController@store');
Route::resource('routes','Admin\RouteController');

Route::post('fairs/upload_video/{id}','Admin\FairController@upload_video');
Route::delete('fairs/delete_video/{id}','Admin\FairController@delete_video');
Route::delete('fairs/delete_pdf/{id}','Admin\FairController@delete_pdf');
Route::put('fairs/upload_images/{id}','Admin\FairController@upload_images');
Route::put('fairs/upload_pdf/{id}','Admin\FairController@upload_pdf');
Route::resource('fairs','Admin\FairController');
Route::post('fairs/store','Admin\FairController@store');
Route::get('fairs/edit/{slug}','Admin\FairController@edit');
Route::post('fairs/update/{id}','Admin\FairController@update');
Route::get('fairs/images/{slug}','Admin\FairController@images');
Route::delete('fairs/delete_image/{id}','Admin\FairController@delete_image');
Route::get('fairs/desactivate/{id}','Admin\FairController@desactivate');
Route::get('fairs/activate/{id}','Admin\FairController@activate');

Route::post('wallpapers/store','Admin\WallpaperController@store');
Route::get('wallpapers/edit/{id}','Admin\WallpaperController@edit');
Route::put('wallpapers/update/{id}','Admin\WallpaperController@update');
Route::resource('wallpapers','Admin\WallpaperController');
Route::get('wall/desactivate/{id}','Admin\WallpaperController@desactivate');
Route::get('wall/activate/{id}','Admin\WallpaperController@activate');
// Route::get('operators','Admin\AdminController@operators');
// Route::get('operators/services/{id}','Admin\AdminController@operatorServices');
// Route::get('operators/services/edit/{id}','Admin\AdminController@editServiceOperator');
// Route::put('operators/services/update/{id}','Admin\AdminController@updateServiceOperator');
// Route::delete('operators/service/delete_image/{id}','Admin\AdminController@delete_image');
// Route::get('operators/serviceimages/{id}','Admin\AdminController@imagesServiceOperator');
// Route::put('operators/service/upload_images/{id}','Admin\AdminController@upload_images');
// Route::get('operators/service/desactivate/{id}','Admin\AdminController@desactivate');
// Route::get('operators/service/activate/{id}','Admin\AdminController@activate');
// Route::get('operators/items/{id}','Admin\AdminController@itemsOperator');
// Route::get('operators/items/edit/{id}','Admin\AdminController@editItemOperator');
// Route::put('operators/items/update/{id}','Admin\AdminController@updateItemOperator');
// Route::get('operators/items/desactivate/{id}','Admin\AdminController@desactivateItem');
// Route::get('operators/items/activate/{id}','Admin\AdminController@activateItem');


Route::get('operators','Admin\AdminController@lyingOperators');
Route::get('operators/create','Admin\AdminController@createLyingOperators');
Route::post('operators/store','Admin\AdminController@storeLyingOperators');
Route::get('operators/edit/{slug}','Admin\AdminController@editLyingOperators');
Route::put('operators/update/{id}','Admin\AdminController@updateLyingOperators');
Route::get('operators/images/{slug}','Admin\AdminController@imagesLyingOperators');
Route::put('operators/upload_images/{id}','Admin\AdminController@upload_imagesLyingOperators');
Route::delete('operators/delete_image/{id}','Admin\AdminController@delete_imageLyingOperators');
Route::get('operators/desactivate/{id}','Admin\AdminController@desactivateLyingOperators');
Route::get('operators/activate/{id}','Admin\AdminController@activateLyingOperators');

Route::get('users','Admin\AdminController@users');

Route::get('contact','Admin\AdminController@contact');

Route::resource('notices','Admin\NoticeController');
Route::post('notice/store','Admin\NoticeController@store');
Route::get('notice/edit/{slug}','Admin\NoticeController@edit');
Route::put('notice/update/{id}','Admin\NoticeController@update');