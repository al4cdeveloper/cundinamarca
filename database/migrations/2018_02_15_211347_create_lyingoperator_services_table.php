<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLyingoperatorServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lyingoperator_services', function (Blueprint $table) {
            $table->increments('id_lyingoperator_service');
            $table->integer('fk_service')->unsigned();
            $table->foreign('fk_service')->references('id_service')->on('services'); 
            $table->integer('fk_operator')->unsigned();
            $table->foreign('fk_operator')->references('id_site')->on('interest_sites');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lyingoperator_services');
    }
}
