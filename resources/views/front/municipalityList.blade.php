@extends('layout.header')

    @section('title','Lista de municipios')
    @section('keywords','Cundinamarca, municipios, listado')
    @section('additionalStyle')
	<!-- Radio and check inputs -->
	<link href="{{asset('front/css/skins/square/grey.css')}}" rel="stylesheet">

	<!-- Range slider -->
	<link href="{{asset('front/css/ion.rangeSlider.css')}}" rel="stylesheet">
	<link href="{{asset('front/css/ion.rangeSlider.skinFlat.css')}}" rel="stylesheet">

	<!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    @endsection

    @section('additionalPreMain')
	<section >
		<div id="map" class="map"></div>
	</section>
    
    @endsection

    
    @section('main')
        @if(isset($src))
    		<div id="position">
    			<div class="container">
    				<ul>
    					<li><a href="#">Home</a>
    					</li>
    					<li><a href="#">Category</a>
    					</li>
    					<li>Page active</li>
    				</ul>
    			</div>
    		</div>
        @else
            <div id="position">
                <div class="container">
                    <ul>
                        <li><a href="{{url('/')}}">Home</a>
                        </li>
                        <li>Municipios</li>
                    </ul>
                </div>
            </div>
        @endif
		<!-- Position -->

		<div class="collapse" id="collapseMap">
			
		</div>
		<!-- End Map -->

		<div class="container margin_60">
			<div class="row">
				<!--End aside -->

				<div class="col-lg-12 col-md-12">
                    @if(Session::has('message-error'))
                            <div class="alert alert-danger">
                              <strong>Error!</strong>  {{ Session::get('message-error') }}
                            </div>
                    @endif
					<div id="tools">
                        <div class="row">
                            <form action="{{url('municipalities')}}" method="get">
                                <div class="col-md-3 col-sm-3 col-md-offset-3">
                                    <div class="styled-select-filters">
                                        <select name="filter" id="sort_price" required>
                                            <option value="" selected="">Filtrar por clima</option>
                                            @foreach($weather as $weath)
                                                <option value="{{$weath}}">{{$weath}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                {{-- <div class="col-md-3 col-sm-3 col-xs-6">
                                    <div class="styled-select-filters">
                                        <select name="sort_rating" id="sort_rating">
                                            <option value="" selected="">Sort by ranking</option>
                                            <option value="lower">Lowest ranking</option>
                                            <option value="higher">Highest ranking</option>
                                        </select>
                                    </div>
                                </div> --}}

                                <div class="col-md-3 col-sm-3 col-xs-6 hidden-xs text-right">
                                    <button class="btn" type="submit">Buscar</button>
                                </div>
                            </form>
                        </div>
                    </div>

					<div class="row">
                            @foreach($municipalities as $municipality)
                                <div class="col-md-4 col-sm-6 wow zoomIn targetList" data-wow-delay="0.1s">
                                    <div class="tour_container">
                                        <div class="img_container">
                                            <a href="{{url('municipality/'.$municipality->slug)}}">
                                                <img src="{{asset($municipality->link_image)}}" class="img-responsive" alt="image">
                                                <div class="short_info">
                                                    <h2 style="color:white!important">{{$municipality->municipality_name}}</h2>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="tour_title" style="padding-top: 0px!important">
                                                <p>{!!substr($municipality->description,0,120) !!}... <a href="{{url('municipality/'.$municipality->slug)}}">Ver más</a> {!!'</p>'!!}</p>
                                        </div>
                                    </div>
                                    <!-- End box tour -->
                                </div >
                                <!-- End col-md-4 -->
                            @endforeach
						<!-- End col-md-6 -->
					</div>
					<!-- End row -->
				</div>
				<!-- End col lg 9 -->
			</div>
			<!-- End row -->
		</div>
		<!-- End container -->
    @endsection

	@section('additionalScript')
	<script src="j{{asset('front/js/icheck.js')}}"></script>
	<script>
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-grey',
			radioClass: 'iradio_square-grey'
		});
	</script>
	<!-- Map -->

    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBXtArR0GzW4Y_EezVpjGdvjrlwsQBcayw"></script>

    <script type="text/javascript">
        $(document).ready(function(e){
    (function(A) {

    if (!Array.prototype.forEach)
        A.forEach = A.forEach || function(action, that) {
            for (var i = 0, l = this.length; i < l; i++)
                if (i in this)
                    action.call(that, this[i], i, this);
            };

        })(Array.prototype);

        var
        mapObject,
        markers = [],
        markersData = {
            'Hotels': [
            @foreach($municipalities as $municipality)
            {
                name: '{{$municipality->municipality_name}}',
                location_latitude: {{$municipality->latitude}}, 
                location_longitude: {{$municipality->longitude}},
                map_image_url: '{{asset($municipality->link_image)}}',
                name_point: '{{$municipality->municipality_name}}',
                description_point: '{!!substr($municipality->description,0,120) !!}... <a href="{{url('municipality/'.$municipality->slug)}}">Ver más</a> {!!'</p>'!!}',
                get_directions_start_address: '',
                // phone: '{{$municipality->phone}}',
                // url_point: '{{$municipality->municipality_name}}'
            },
            @endforeach
            ]
        };

            var mapOptions = {
                center: new google.maps.LatLng(4.7925319, -74.3999466),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                zoom: 5,

                mapTypeControl: false,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                panControl: false,
                panControlOptions: {
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.TOP_LEFT
                },
                scrollwheel: false,
                scaleControl: false,
                scaleControlOptions: {
                    position: google.maps.ControlPosition.TOP_LEFT
                },
                streetViewControl: true,
                streetViewControlOptions: {
                    position: google.maps.ControlPosition.LEFT_TOP
                },
                styles: [
                                             {
                    "featureType": "landscape",
                    "stylers": [
                        {
                            "hue": "#FFBF00"
                        },
                        {
                            "saturation": 43.400000000000006
                        },
                        {
                            "lightness": 37.599999999999994
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "stylers": [
                        {
                            "hue": "#B18904"
                        },
                        {
                            "saturation": -61.8
                        },
                        {
                            "lightness": 45.599999999999994
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "stylers": [
                        {
                            "hue": "#DBA901"
                        },
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 51.19999999999999
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "stylers": [
                        {
                            "hue": "#FFBF00"
                        },
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 52
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "stylers": [
                        {
                            "hue": "#0078FF"
                        },
                        {
                            "saturation": -13.200000000000003
                        },
                        {
                            "lightness": 2.4000000000000057
                        },
                        {
                            "gamma": 1
                        }
                    ]
                },
                ]
            };
            var
            marker;
            mapObject = new google.maps.Map(document.getElementById('map'), mapOptions);

    var layercp = new google.maps.FusionTablesLayer({
                  query: {
                  select: 'geometry',
                  /* from: '1hJEPXEvBtRbAbrMcvbHhxcQik6s2S_4SeFc9Gzw3'  este es sin codigo de color*/
                  from: '1wGK1Hy5OLhR-OYFuzCZiG90oVCOHdJO0oiqMamr5'
                  },
                 map: null ,
                 zIndex: 0,
                 styles: [{
                       polygonOptions: {
                           fillOpacity: 0.01 ,
                           strokeColor: '#0000ff',
                           strokeWeight: 1}
                       } ]
              });

            var cpLayer = new google.maps.KmlLayer({
            url: 'http://www.rutascolombia.com/Cundinamarca_dorado.kml',
            map: mapObject});
            var image = {
                url: 'img/pins/Walking.png',
                // This marker is 20 pixels wide by 32 pixels high.
                size: new google.maps.Size(20, 20),
                // The origin for this image is (0, 0).
                origin: new google.maps.Point(0, 0),
                // The anchor for this image is the base of the flagpole at (0, 32).
                anchor: new google.maps.Point(0, 20)
              };
            for (var key in markersData)
                markersData[key].forEach(function (item) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(item.location_latitude, item.location_longitude),
                        map: mapObject,
                        icon: image,
                    });

                    if ('undefined' === typeof markers[key])
                        markers[key] = [];
                    markers[key].push(marker);
                    google.maps.event.addListener(marker, 'click', (function () {
                      closeInfoBox();
                      getInfoBox(item).open(mapObject, this);
                     }));

                    
                });



     //        function initMap(cp) {
     //     var map = new google.maps.Map(document.getElementById('map'),{
     //         center: {lat: 40.48, lng: -4.46},
     //         zoom: 6
     //       });
         
     //      var layercp = new google.maps.FusionTablesLayer({
     //              query: {
     //              select: 'geometry',
     //              /* from: '1hJEPXEvBtRbAbrMcvbHhxcQik6s2S_4SeFc9Gzw3'  este es sin codigo de color*/
     //              from: '1wGK1Hy5OLhR-OYFuzCZiG90oVCOHdJO0oiqMamr5'
     //              },
     //             map: null ,
     //             zIndex: 0,
     //             styles: [{
     //                   polygonOptions: {
     //                       fillOpacity: 0.01 ,
     //                       strokeColor: '#0000ff',
     //                       strokeWeight: 1}
     //                   } ]
     //          });
     
     // map.addListener('zoom_changed', function() {
     //      var minZoom = 11;
     //      var zoomLevel = map.getZoom();
     //      if ( zoomLevel >= minZoom) { layercp.setMap(map);}
     //                      else { layercp.setMap(null);} 
     //                }); 
     //        var cpLayer = new google.maps.KmlLayer({
     //        url: 'https://www.codigospostales.com/kml/28/28020.kml',
     //        map: map});
     //        }



        function hideAllMarkers () {
            for (var key in markers)
                markers[key].forEach(function (marker) {
                    marker.setMap(null);
                });
        };

        function closeInfoBox() {
            $('div.infoBox').remove();
        };

        function getInfoBox(item) {
            return new InfoBox({
                content:
                '<div class="marker_info" id="marker_info">' +
                '<img src="' + item.map_image_url + '" alt="Image" width="280" height="auto"/>' +
                '<h3>'+ item.name_point +'</h3>' +
                '<span>'+ item.description_point +'</span>' +
                '<div class="marker_tools">' +
                '<form action="http://maps.google.com/maps" method="get" target="_blank" style="display:inline-block""><input name="saddr" value="'+ item.get_directions_start_address +'" type="hidden"><input type="hidden" name="daddr" value="'+ item.location_latitude +',' +item.location_longitude +'"><button type="submit" value="Get directions" class="btn_infobox_get_directions">¿Como llegar?</button></form>' +
                    // '<a href="tel://'+ item.phone +'" class="btn_infobox_phone">'+ item.phone +'</a>' +
                    '</div>' +
                    // '<a href="'+ item.url_point + '" class="btn_infobox">Details</a>' +
                '</div>',
                disableAutoPan: false,
                maxWidth: 0,
                pixelOffset: new google.maps.Size(10, 125),
                closeBoxMargin: '5px -20px 2px 2px',
                closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                isHidden: false,
                alignBottom: true,
                pane: 'floatPane',
                enableEventPropagation: true
            });


        };

    });

     
    
    </script>
	<script src="{{asset('front/js/infobox.js')}}"></script>
    @endsection