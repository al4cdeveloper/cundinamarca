<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Notice;
use App\Models\Fair;
use File;
use Session;
class NoticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notices = Notice::all();
        return view('admin.Notice.noticeList',compact('notices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.Notice.createEditNotice');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
             'notice_name' => 'required|max:50',
             'description'  => 'required',
             'image'  => 'required',
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $expo = Fair::where('slug','expo-cundinamarca')->first();

            $files = $request->file('image');
            $link_image = $this->cargar_imagen($files);

            $notice = new Notice;
            $notice->notice_name = $request->notice_name;
            $notice->description = $request->description;
            $notice->image = "images/notices/".$link_image;
            $notice->fk_fair = $expo->id_fair;
            $notice->save();

            Session::flash('message', 'Se ha realizado correctamente el registro de información.');
            return redirect('admin/notices');
        }
    }

    private function cargar_imagen($file, $imageName = false)
    {
        if ($imageName) 
        {
            $exists = File::exists(public_path("images/notices/".$imageName));
            if ($exists) 
            {
                File::delete(public_path("images/notices/".$imageName));
            }

            $image = explode('.', $imageName);
            $imageName = $image[0].'.'.$file->getClientOriginalExtension();
        } 
        else 
        {
            $imageName = 'Notice_'.date('YmdHis', time()).rand().'.'.$file->getClientOriginalExtension();
        }

        $file->move(public_path('images/notices'), $imageName);

        $exists = File::exists(public_path("images/notices/".$imageName));

        if ($exists) 
        {
            return $imageName;
        } 
        else 
        {
            return false;
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $notice = Notice::where('slug',$slug)->first();
        if($notice)
        {
            return view('admin.Notice.createEditNotice',compact('notice'));
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado la noticia.");
            return redirect('admin/notices');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
             'notice_name' => 'required|max:50',
             'description'  => 'required',
            ]); 
        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {

            $notice = Notice::find($id);
            $notice->notice_name = $request->notice_name;
            $notice->description = $request->description;
            if($request->image)
            {
                $exists = File::exists(public_path($municipality->image));
                if ($exists) 
                {
                    File::delete(public_path($municipality->image));
                }
                $files = $request->file('image');

                $link_image = $this->cargar_imagen($files);

                $notice->link_image = "images/notices/".$link_image;
            }

            $notice->save();

            Session::flash('message', 'Se ha realizado correctamente la actualización de información.');

            return redirect('admin/notices');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
