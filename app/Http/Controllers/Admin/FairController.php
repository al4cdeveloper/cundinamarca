<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ImageRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Fair;
use App\Models\FairImage;
use App\Models\Department;
use App\Models\Municipality;
use App\Models\Video;
use App\Models\PortableDocument as PDF;
use Session;
use File;
class FairController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fairs = Fair::all();
        return view('admin.Fair.listFair',compact('fairs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $municipalities = Municipality::where('state','activo')->get();

        return view('admin.Fair.newEditFair',compact('municipalities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
             'principal'=>'required',
             'fair_name'=>'required',
             'date' => 'required|max:50',
             'enddate' => 'required|max:50|after_or_equal:date',
             'keywords'  => 'required',
             'description'  => 'required'
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $fair = new Fair;
            $fair->fair_name = $request->fair_name;
            $fair->date = $request->date;
            $fair->enddate = $request->enddate;
            $fair->type_relation = $request->typePrincipal;
            $fair->fk_relation = $request->principal;
            $fair->multimedia_type = "images";
            $fair->keywords = $request->keywords;
            $fair->description = $request->description;
            $fair->save();


            Session::flash('message', 'Se ha realizado correctamente el registro de información.');
            return redirect('admin/fairs');
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $fair = Fair::where('slug',$slug)->first();
        if($fair)
        {
            $municipalities = Municipality::where('state','activo')->get();
            return view('admin.Fair.newEditFair',compact('fair','municipalities'));
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el evento.");
            return redirect('admin/fairs');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
             'principal'=>'required',
             'fair_name'=>'required',
             'date' => 'required|max:50',
             'enddate' => 'required|max:50|after_or_equal:date',
             'keywords'  => 'required',
             'description'  => 'required'
            ]); 

        if ($validator->fails()) 
        {
            foreach ($validator->errors()->all() as $error)
            {
                Session::flash('message-error', $error);
            }

            return redirect()->back()->withErrors($validator)->withInput();
        } 
        else 
        {
            $fair = Fair::find($id);
            $fair->fair_name = $request->fair_name;
            $fair->date = $request->date;
            $fair->enddate = $request->enddate;
            $fair->type_relation = $request->typePrincipal;
            $fair->fk_relation = $request->principal;
            $fair->keywords = $request->keywords;
            $fair->description = $request->description;
            $fair->save();


            Session::flash('message', 'Se ha realizado correctamente la actualización de información.');
            return redirect('admin/fairs');
        }
    }

    public function desactivate($id)
    {
        $fair = Fair::find($id);
        if($fair)
        {
            $fair->state = "inactivo";
            $fair->save();
            Session::flash('message','Se ha desactivado el evento correctamente');
            return redirect('admin/fairs');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el evento.");
            return redirect('admin/fairs');
        }
    }

    public function activate($id)
    {
        $fair = Fair::find($id);
        if($fair)
        {
            $fair->state = "activo";
            $fair->save();
            Session::flash('message','Se ha activado el evento correctamente');
            return redirect('admin/fairs');
        }
        else
        {
            Session::flash('message-error', "No se ha encontrado el evento.");
            return redirect('admin/fairs');
        }
    }

    public function images($slug)
    {
        
        $fair = Fair::where('slug',$slug)->first();

        return view('admin.Fair.imagesFair', compact('fair'));
    }

    public function upload_images($id, ImageRequest $request)
    {
        $files = $request->file('file');

        $new_service = [];

        foreach ($files as $file) {
            $imageName = $this->cargar_imagen($file);

            if ($imageName)  
            {
                $new_image = ['link_image' => 'images/fairs/'.$imageName, 'fk_fair' => $id];
                $service = FairImage::create($new_image);
                $new_service[] = $service->toArray();
                unset($new_image);
            }
        }

        if (count($new_service) > 0) {
            return \Response::json(['response' => 'Subida de imágenes completa'], 200);
        } else {
            return \Response::json(['response' => 'Falla en la subida de imágenes2'], 404);
        }
    }

    public function upload_pdf($id, ImageRequest $request)
    {
        $files = $request->file('file');

        $new_service = [];

        foreach ($files as $file) {
            $imageName = $this->cargar_pdf($file);

            if ($imageName)  
            {
                $new_image = ['link_pdf' => 'pdf/fairs/'.$imageName, 'type_relation'=>'fair','fk_relation' => $id];
                $service = PDF::create($new_image);
                $new_service[] = $service->toArray();
                unset($new_image);
            }
        }

        if (count($new_service) > 0) {
            return \Response::json(['response' => 'Subida de imágenes completa'], 200);
        } else {
            return \Response::json(['response' => 'Falla en la subida de imágenes2'], 404);
        }
    }

    public function upload_video(Request $request,$id)
    {
        // dd(youtube_match('https://www.youtube.com/watch?v=pXRviuL6vMY'));
        // https://www.youtube.com/embed/pXRviuL6vMY
        // <a href="#{{slugify_text($additional->title)}}" aria-controls="videos" role="tab" data-toggle="tab">{{$additional->title}}</a>
        
        $url = youtube_match($request->link_url);
        
        $newvideo = ['link_video' => $url, 'type_relation'=>'fair','fk_relation' => $id];
        $create = Video::create($newvideo);

        Session::flash('message', 'Vídeo cargado correctamente');

        return redirect()->back();        
    }

    public function delete_video($id)
    {
        Video::destroy($id);
        Session::flash('message', 'El vídeo se ha borrado');
        return redirect()->back();
    }
    public function delete_image($id)
    {
        $image = FairImage::find($id);

        $exists = File::exists(public_path("images/fairs/".$image->link_imagen));
        if ($exists) {
            File::delete(public_path("images/fairs/".$image->link_imagen));
        }

        if ($image->delete()) 
        {
            \Session::flash('message', 'Imagen Borrada Correctamente');
        } 
        else 
        {
            \Session::flash('message-error', 'La Imagen no puede ser borrada');
        }

        return redirect()->back();
    }

    public function delete_pdf($id)
    {
        $image = PDF::find($id);

        $exists = File::exists(public_path("pdf/fairs/".$image->link_imagen));
        if ($exists) {
            File::delete(public_path("pdf/fairs/".$image->link_imagen));
        }

        if ($image->delete()) 
        {
            \Session::flash('message', 'Documento Borrado Correctamente');
        } 
        else 
        {
            \Session::flash('message-error', 'El documento no puede ser borrado');
        }

        return redirect()->back();
    }

    private function cargar_imagen($file, $imageName = false)
    {
        if ($imageName) 
        {
            $exists = File::exists(public_path("images/fairs/".$imageName));
            if ($exists) 
            {
                File::delete(public_path("images/fairs/".$imageName));
            }

            $image = explode('.', $imageName);
            $imageName = $image[0].'.'.$file->getClientOriginalExtension();
        } 
        else 
        {
            $imageName = 'Fair_'.date('YmdHis', time()).rand().'.'.$file->getClientOriginalExtension();
        }

        $file->move(public_path('images/fairs'), $imageName);

        $exists = File::exists(public_path("images/fairs/".$imageName));

        if ($exists) 
        {
            return $imageName;
        } 
        else 
        {
            return false;
        }
    }

    private function cargar_pdf($file, $imageName = false)
    {
        if ($imageName) 
        {
            $exists = File::exists(public_path("pdf/fairs/".$imageName));
            if ($exists) 
            {
                File::delete(public_path("pdf/fairs/".$imageName));
            }

            $image = explode('.', $imageName);
            $imageName = $image[0].'.'.$file->getClientOriginalExtension();
        } 
        else 
        {
            $imageName = 'Fair_'.date('YmdHis', time()).rand().'.'.$file->getClientOriginalExtension();
        }

        $file->move(public_path('pdf/fairs'), $imageName);

        $exists = File::exists(public_path("pdf/fairs/".$imageName));

        if ($exists) 
        {
            return $imageName;
        } 
        else 
        {
            return false;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
