@extends('admin.layout.auth')

@section('title', 'Listado operadores')


@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Operadores</h1>
                    </div>
                </div>
                {{-- <div class="col-sm-6 hidden-xs">
                    <div class="header-section">
                        <ul class="breadcrumb breadcrumb-top">
                            <li>Category</li>
                            <li><a href="">Page</a></li>
                        </ul>
                    </div>
                </div> --}}
            </div>
        </div>
        <!-- END Page Header -->

        {{-- <div class="block"> --}}
            {{-- <div class="row"> --}}
                <div class="block full">
                    <div class="block-title">
                        <h2>Operadores</h2>
                    </div>
                    <div class="table-responsive">
                        <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 50px;">RNT</th>
                                    <th>User</th>
                                    <th>Email</th>
                                    <th>Telefono</th>
                                    <th style="width: 120px;">Estado</th>
                                    {{-- <th>R. realizadas</th> --}}
                                    {{-- <th>R. completadas</th> --}}
                                    <th class="text-center" style="width: 75px;"><i class="fa fa-flash"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($operators as $operator)
                                <tr>
                                    <td class="text-center">{{$operator->national_register}}</td>
                                    <td><strong>{{$operator->name}}</strong></td>
                                    <td>{{$operator->email}}</td>
                                    <td>{{$operator->contact_personal}}</td>
                                    <td>@if($operator->status=="active")
                                        <a class="label label-info">{{$operator->status}}</a>
                                        @else
                                        <span class="label label-danger">{{$operator->status}}</span>
                                        @endif
                                    </td>
                                    {{-- <td>{{$operator->totalReservations}}</td> --}}
                                    {{-- <td>{{$operator->totalServices}}</td> --}}
                                    <td class="text-center">
                                        <button class="btn btn-effect-ripple btn-xs btn-success" data-toggle="modal" data-target="#detailOperator{{$operator->id_operator}}"><i class="fa fa-eye"></i></button>
                                        <a href="javascript:void(0)" data-toggle="tooltip" title="Desactivar operador" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @foreach($operators as $operator)
                    <div class="modal fade" id="detailOperator{{$operator->id_operator}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Detalle operador</h4>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="widget">
                                        <div class="widget-image widget-image-sm">
                                            <img src="{{asset('/auth-panel/img/placeholders/photo1@2x.jpg')}}" alt="image">
                                            <div class="widget-image-content text-center">
                                                <img src="@if($operator->avatar) {{asset($operator->avatar)}} @else {{asset('img/no-profile-image.png')}} @endif" alt="avatar" class="img-circle img-thumbnail img-thumbnail-transparent img-thumbnail-avatar-2x push">
                                                <h2 class="widget-heading text-light"><strong>{{$operator->name}}</strong></h2>
                                                <h4 class="widget-heading text-light-op"><em>Operador</em></h4>
                                            </div>
                                        </div>
                                        <div class="widget-content widget-content-full border-bottom">
                                            <div class="row text-center">
                                                <div class="col-xs-6 push-inner-top-bottom border-right">
                                                    <h3 class="widget-heading"><i class="fa fa-ravelry text-danger push"></i> <br><small><strong>{{count($operator->services)}}</strong> Servicios ofrecidos</small></h3>
                                                </div>
                                                <div class="col-xs-6 push-inner-top-bottom">
                                                    <h3 class="widget-heading"><i class="fa fa-check themed-color-social push"></i> <br>
                                                        @if($operator->totalServices>0)
                                                        <a href="{{url('admin/complreservation/'.$operator->national_register)}}"><small><strong>{{$operator->totalServices}}</strong> Reservaciones realizadas</small></a>
                                                        @else
                                                        <small><strong>{{$operator->totalServices}}</strong> Reservaciones realizadas</small>
                                                        @endif
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget-content border-bottom">
                                            <h4>Servicios</h4>
                                            @if(count($operator->directService)>0)

                                                <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center" style="width: 50px;">Servicio</th>
                                                            <th>Ubicacion</th>
                                                            <th>Dirección</th>
                                                            <th style="width: 80px;">Estado</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($operator->directService as $service)
                                                        <tr>
                                                            <td class="text-center">{{$service->PrincipalService->service}}</td>
                                                            <td><strong>{{$service->location}}</strong></td>
                                                            <td>{{$service->address}}</td>
                                                            <td>@if($service->state=="Activo")
                                                                <a class="label label-info">{{$service->state}}</a>
                                                                @else
                                                                <span class="label label-danger">{{$service->state}}</span>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            @else
                                                <p>No se encuentran servicios registrados</p>
                                            @endif
                                            <a href="{{url('admin/operators/services/'.$operator->id_operator)}}">Ver todos los servicios</a>
                                        </div>
                                        {{-- <div class="widget-content">
                                            <h4>Social</h4>
                                            <div class="btn-group">
                                                <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="Facebook"><i class="fa fa-facebook fa-fw"></i></a>
                                                <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="Twitter"><i class="fa fa-twitter fa-fw"></i></a>
                                                <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="Google Plus"><i class="fa fa-google-plus fa-fw"></i></a>
                                                <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="Pinterest"><i class="fa fa-pinterest fa-fw"></i></a>
                                                <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="Dribbble"><i class="fa fa-dribbble fa-fw"></i></a>
                                            </div>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                          </div>
                        </div>
                      </div>
                    </div>

                @endforeach
                <!-- END Datatables Block -->
            {{-- </div> --}}
        {{-- </div> --}}
    </div>
@endsection

@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
@endsection