<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $primaryKey="id_order";
	protected $fillable = ['fk_user','estado','tipo_pago'];
}
