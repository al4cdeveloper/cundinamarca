<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Municipality extends Model
{
    use Sluggable;


	protected $primaryKey = 'id_municipality';

	protected $fillable = ['id_municipality','municipality_name','description','multimedia_type','latitude','longitude','slug','link_image','type_last_user','fk_last_edition','officialpage','weather','fk_department','keywords','state'];

	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'municipality_name'
            ]
        ];
    }	

    public function Department()
    {
    	return $this->hasOne('App\Models\Department','id_department','fk_department');
    }

    public function images()
    {
        return $this->hasMany('App\Models\MunicipalityImage', 'fk_municipality');
    }

    public function InterestSite()
    {
        return $this->hasMany('App\Models\InterestSite','fk_municipality','id_municipality');
    }

    public function InterestSiteW()
    {
        return $this->hasMany('App\Models\InterestSite','fk_municipality','id_municipality')->where('fk_category','!=',2)->where('fk_category','!=',7);
    }

    public function Activities()
    {
        return $this->hasMany('App\Models\InterestSite','fk_municipality','id_municipality')->where('fk_category',7);
    }

    public function LyingOperators()
    {
        return $this->hasMany('App\Models\InterestSite','fk_municipality','id_municipality')->where('fk_category',2);
    }

    public function Video()
    {
        return $this->hasMany('App\Models\Video','fk_relation','id_municipality')->where('type_relation','municipality');
    }
}
