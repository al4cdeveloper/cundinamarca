<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceCategory extends Model
{
	protected $primaryKey="id_category";
	protected $fillable = ['id_category','service_category'];
}
