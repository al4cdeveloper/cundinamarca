<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Department extends Model
{
	use Sluggable;
	
	protected $primaryKey = "id_department";
	protected $fillable = ['department_name','description','multimedia_type','latitude','longitude','slug','type_last_user','link_image','fk_region','fk_last_edition','keywords','state'];

	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'department_name'
            ]
        ];
    }

    public function Region()
    {
        return $this->hasOne('App\Models\Region','id_region','fk_region');
    }

    public function images()
    {
        return $this->hasMany('App\Models\DepartmentImage', 'fk_department');
    }

    public function Municipalities()
    {
        return $this->hasMany('App\Models\Municipality','fk_department','id_department')->where('state','activo');
    }
}
