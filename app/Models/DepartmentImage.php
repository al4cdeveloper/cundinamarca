<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DepartmentImage extends Model
{
	
	protected $primaryKey = 'id_department_image';

	protected $fillable = ['link_image','fk_department'];
}
