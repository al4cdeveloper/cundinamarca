<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServicePicture extends Model
{
	protected $primaryKey = 'id_picture';

	protected $fillable = ['link_image','fk_service'];
}
