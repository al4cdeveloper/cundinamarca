@extends('layout.header')

    @section('additionalStyle')
	@section('title','Lista de operadores')
	<!-- Radio and check inputs -->
	<link href="{{asset('front/css/skins/square/grey.css')}}" rel="stylesheet">

	<!-- Range slider -->
	<link href="{{asset('front/css/ion.rangeSlider.css')}}" rel="stylesheet">
	<link href="{{asset('front/css/ion.rangeSlider.skinFlat.css')}}" rel="stylesheet">

    @endsection
    @section('additionalPreMain')

	<section class="parallax-window" data-parallax="scroll" data-image-src="{{asset('img/operators.jpg')}}" data-natural-width="1400" data-natural-height="470">
		<div class="parallax-content-1">
			<div class="animated fadeInDown">
				<h1>Operadores turisticos</h1>
			</div>
		</div>
	</section>
	<!-- End section -->
    @endsection


	@section('main')
		<div id="position">
			<div class="container">
				<ul>
					<li><a href="{{url('/')}}">Home</a>
					<li>Operadores</li>
				</ul>
			</div>
		</div>
		<!-- Position -->

{{-- 		<div class="collapse" id="collapseMap">
			<div id="map" class="map"></div>
		</div>
		<!-- End Map --> --}}

		<div class="container margin_60">
			<div class="row">
				<aside class="col-lg-3 col-md-3">
					{{-- <p>
						<a class="btn_map" data-toggle="collapse" href="#collapseMap" aria-expanded="false" aria-controls="collapseMap" data-text-swap="Hide map" data-text-original="View on map">View on map</a>
					</p> --}}

					<div class="box_style_cat">
						<ul id="cat_nav">
							<li><a href="{{url('operators')}}" @if(!isset($filter)) id="active" @endif>
								<i class="icon_set_1_icon-37"></i>Todos los operadores <span>{{$numOperators}}</span></a>
							</li>
							@foreach($services as $service)
							<li><a href="{{url('operators?filter='.$service)}}" @if(isset($filter) && $filter==$service) id="active" @endif><i class="icon_set_1_icon-37"></i>{{$service}}</a>
							</li>
							@endforeach
						</ul>
					</div>
				</aside>
				<!--End aside -->

				<div class="col-lg-9 col-md-8">

					<div class="row">

						@foreach($operators as $operator)
						<div class="col-md-6 col-sm-6 col-lg-6 wow zoomIn targetListOperatorLg" data-wow-delay="0.1s">
							<div class="tour_container">
								<div class="ribbon_3 popular"><span>{{$operator->Municipality->municipality_name}}</span>
								</div>
								<div class="img_container">
									<a href="{{url('operator/'.$operator->slug)}}">
										<img src="@if($operator->link_icon){{asset($operator->link_icon)}}@else{{asset('img/defaultimg.jpg')}}@endif" width="800" height="533" class="img-responsive" alt="Image">
									</a>
								</div>
								<div class="tour_title">
									<h3><strong> {{$operator->site_name}}</strong></h3>
									<div class="rating">
										@foreach($operator->Services as $service)
											- {{$service->service}}
										@endforeach
									</div>
								</div>
							</div>
							<!-- End box tour -->
						</div>
						@endforeach
						<!-- End col-md-6 -->

					</div>
					<!-- End row -->

				</div>
				<!-- End col lg 9 -->
			</div>
			<!-- End row -->
		</div>
		<!-- End container -->
    @endsection
	<!-- End main -->


    @section('additionalScript')
	<!-- Specific scripts -->
	<!-- Cat nav mobile -->
	<script src="{{asset('front/js/cat_nav_mobile.js')}}"></script>
	<script>
		$('#cat_nav').mobileMenu();
	</script>
	<!-- Check and radio inputs -->
	<script src="{{asset('front/js/icheck.js')}}"></script>
	<script>
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-grey',
			radioClass: 'iradio_square-grey'
		});
	</script>
{{-- 	<!-- Map -->
	<script src="http://maps.googleapis.com/maps/api/js"></script>
	<script src="js/map_restaurants.js"></script>
	<script src="js/infobox.js"></script>
 --}}
    @endsection

