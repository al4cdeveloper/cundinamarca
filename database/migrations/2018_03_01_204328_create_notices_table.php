<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notices', function (Blueprint $table) {
            $table->increments('id_notice');
            $table->string('notice_name');
            $table->text('description');
            $table->string('image')->nullable();
            $table->string('state')->default('activo');
            $table->string('slug');
            $table->integer('fk_fair')->unsigned();
            $table->foreign('fk_fair')->references('id_fair')->on('fairs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notices');
    }
}
