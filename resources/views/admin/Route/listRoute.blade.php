@extends('admin.layout.auth')

@section('title', 'Listado de Rutas')


@section('content')
    <div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Rutas</h1>
                    </div>
                </div>
            </div>
        </div>
            <div class="block full">
                <div class="block-title">
                    <h2>Rutas</h2>
                    <div class="pull-right" style="padding: 5px;"><a href="{{url('admin/routes/create')}}" class="btn btn-primary btn-sm"><i class="far fa-plus-square fa-lg"></i></a></div>
                </div>
                <div class="table-responsive">
                    <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 50px;">Tipo de relacion</th>
                                <th>Lugar principal</th>
                                <th>Nombre</th>
                                {{-- <th>Palabras clave</th> --}}
                                <th style="width: 120px;">Estado</th>
                                <th class="text-center" style="width: 75px;"><i class="fas fa-sync-alt"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($routes as $route)
                            <tr>
                                <td>@if($route->type_relation =="department") Departamento @else Municipio @endif</td>
                                <td>@if($route->type_relation =="department"){{$route->PrincipalDepartment->department_name}} @else {{$route->PrincipalMunicipality->municipality_name}} @endif</td>
                                <td>{{$route->name}}</td>
                                {{-- <td>{{$route->keywords}}</td> --}}
                                <td>@if($route->state=="activo")
                                    <a class="label label-info">{{$route->state}}</a>
                                    @else
                                    <span class="label label-danger">{{$route->state}}</span>
                                    @endif
                                </td>
                                <td class="text-center">

                                    <a href="{{url('admin/routes/edit/'.$route->slug)}}" data-toggle="tooltip" title="Editar ruta" class="btn btn-effect-ripple btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                    @if($route->state=="activo")
                                    <a href="{{url('admin/routes/desactivate/'.$route->id_route)}}" data-toggle="tooltip" title="Desactivar ruta" class="btn btn-effect-ripple btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                    @else
                                    <a href="{{url('admin/routes/activate/'.$route->id_route)}}" data-toggle="tooltip" title="Activar ruta" class="btn btn-effect-ripple btn-xs btn-success"><i class="fas fa-check"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
@endsection

@section('aditionalScript')
<!-- Load and execute javascript code used only in this page -->
        <script src="{{asset('auth-panel/js/pages/uiTables.js')}}"></script>
        <script>$(function(){ UiTables.init(); });</script>
@endsection