@extends('layout.header')

    @section('title',$route->name)
    @section('keywords',$route->keywords)
    

    @section('additionalPreMain')
    <section id="hero" style="background: #4d536d url({{asset($route->link_map)}}) no-repeat center center; width: 100%;">
        <div class="intro_title">
            @if($route->id_route!=6)<h3 class="animated fadeInDown">{{$route->name}}</h3>@endif
        </div>
    </section>
    @endsection


    @section('main')
            
        <div class="white_bg">
            <div class="container margin_60">
                <div class="main_title">
                    <p>{!!$route->description!!}</p>
                </div>
                <!-- End row -->
            </div>
            <!-- End container -->
        </div>
        <!-- End white_bg -->
        
        <div class="container margin_60">


    			<div class="main_title">
                    <h2><span>Municipios</span> relacionados</h2>
    			</div>

    			<div class="row" >
                    <div class="col-md-4 col-sm-6 wow zoomIn targetList" data-wow-delay="0.1s">
                        <div class="tour_container">
                            <div class="img_container">
                                <a href="{{url('municipality/'.$route->PrincipalMunicipality->slug)}}">
                                    <img src="{{asset($route->PrincipalMunicipality->link_image)}}" class="img-responsive" alt="image">
                                    <div class="short_info">
                                        <h2 style="color:white!important">{{$route->PrincipalMunicipality->municipality_name}}</h2>
                                    </div>
                                </a>
                            </div>
                            <div class="tour_title" style="padding-top: 0px!important">
                                    <p>{!!substr($route->PrincipalMunicipality->description,0,120) !!}... <a href="{{url('municipality/'.$route->PrincipalMunicipality->slug)}}">Ver más</a> {!!'</p>'!!}</p>
                            </div>
                        </div>
                        <!-- End box tour -->
                    </div >
                    @foreach($route->MunicipalitiesA as $municipality)
    				<div class="col-md-4 col-sm-6 wow zoomIn targetList" data-wow-delay="0.1s" >
    					<div class="tour_container">
    						<div class="img_container">
    							<a href="{{url('municipality/'.$municipality->slug)}}">
    								<img src="{{asset($municipality->link_image)}}" class="img-responsive" alt="image">
    								<div class="short_info">
    									<h2 style="color:white!important">{{$municipality->municipality_name}}</h2>
    								</div>
    							</a>
    						</div>
    						<div class="tour_title" style="padding-top: 0px!important">
    								<p>{!!substr($municipality->description,0,120) !!}... <a href="{{url('municipality/'.$municipality->slug)}}">Ver más</a> {!!'</p>'!!}</p>
    						</div>
    					</div>
    					<!-- End box tour -->
    				</div >
                    <!-- End col-md-4 -->
                    @endforeach 
                    
                </div>

                <div class="col-xs-12">
                     <hr>
                </div>

        </div>

        @if(count($route->SitesF)>0)
        <div class="container ">

			<div class="main_title ">
				<h2>Sitios de <span>Interés</span> relacionados</h2>
			</div>

			<div class="row">

				@foreach($route->SitesF as $site)
                    <div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.1s">
                        <div class="tour_container">
                            <div class="img_container">
                                <a href="{{url('site/'.$site->slug)}}">
                                    <img src="@if($site->link_image){{asset($site->link_image)}} @else {{asset($site->Category->default_image)}} @endif" class="img-responsive" alt="image">
                                    <div class="short_info">
                                        <h2 style="color:white!important">{{$site->site_name}}</h2>
                                    </div>
                                </a>
                            </div>
                            <div class="tour_title" style="padding-top: 0px!important">
                                    <p>{!!substr($site->description,0,120) !!} <a href="{{url('site/'.$site->slug)}}">Ver más</a> {!!'</p>'!!}</p>
                            </div>
                        </div>
                        <!-- End box tour -->
                    </div>
                    <!-- End col-md-4 -->
                @endforeach
 
          
			</div>
        </div>
        @endif
		<!-- End container -->

		
		<section class="promo_full" style="background: url({{asset('img/videoRutas.jpg')}}) no-repeat center center;">
			<div class="promo_full_wp magnific">
				<div>
					<h3>VIVE NUESTRA EXPERIENCIA EN 360º</h3>

					<a href="https://www.youtube.com/watch?v=-xNN-bJQ4vI" class="video"><i class="icon-play-circled2-1"></i></a>
				</div>
			</div>
		</section>
		<!-- End section -->

    @endsection
	<!-- End main -->