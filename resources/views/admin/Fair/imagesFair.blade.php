@extends('admin.layout.auth')

@section('title', 'Imágenes de Evento')

@section('content')
    <div id="page-content" class="inner-sidebar-left">
        <!-- Inner Sidebar -->
        <div id="page-content-sidebar">
            <!-- Collapsible Options -->
            
            <div id="media-options" class="collapse navbar-collapse remove-padding">
                <!-- Filter -->
                <div class="block-section">
                    <h4 class="inner-sidebar-header">
                        Filtro
                    </h4>
                    <!-- Filter by Type links -->
                    <!-- Custom files functionality is initialized in js/pages/appMedia.js -->
                    <!-- Add the category value you want each link in .media-filter to filter out in its data-category attribute. Add the value 'all' to show all items -->
                    <ul class="nav nav-pills nav-stacked nav-icons media-filter">
                        <li class="active">
                            <a href="javascript:void(0)" data-category="image" onclick="verdrop()">
                                <i class="fa fa-fw fa-folder text-success icon-push"></i> <strong>Imagenes</strong>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" data-category="video" onclick="verdrop()">
                                <i class="fa fa-fw fa-folder text-danger icon-push"></i> <strong>Videos</strong>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" data-category="pdf" onclick="verdrop()">
                                <i class="fa fa-fw fa-folder text-warning icon-push"></i> <strong>Pdf</strong>
                            </a>
                        </li>
                    </ul>
                    <!-- END Filter by Type links -->
                </div>
                <!-- END Filter -->
            </div>
            <!-- END Collapsible Options -->
        </div>
        <!-- END Inner Sidebar -->

        <!-- Media Box Content -->
        <!-- Add the category value for each item in its data-category attribute (for the filter functionality to work) -->
        <div class="row media-filter-items">
            <div class="col-sm-12" id="dropzone">
                <div class="media-items animation-fadeInQuick2" data-category="image" >
                    {!!Form::open(['url'=>['admin/fairs/upload_images',$fair->id_fair],'method'=>'PUT', 'class'=> 'form-horizontal dropzone', 'enctype' => 'multipart/form-data', 'id' => 'my-dropzone'])!!}
                        <div class="panel-body">
                            <div class="dropzone-previews"></div>
                            <div class="dz-message" style="height:30px;">
                                Sube las imágenes aquí
                            </div>
                        </div>
                        {!! Form::close() !!}
                </div>
            </div>
            <div class="col-sm-12" id="dropzonePDF" style="display: none">
                <div class="media-items animation-fadeInQuick2" data-category="pdf" >
                    {!!Form::open(['url'=>['admin/fairs/upload_pdf',$fair->id_fair],'method'=>'PUT', 'class'=> 'form-horizontal dropzone', 'enctype' => 'multipart/form-data', 'id' => 'my-dropzone2'])!!}
                        <div class="panel-body">
                            <div class="dropzone-previews"></div>
                            <div class="dz-message" style="height:30px;">
                                Sube los PDF aquí
                            </div>
                        </div>
                        {!! Form::close() !!}
                </div>
            </div>
            <div class="col-sm-12" id="input" style="display: none">
                <div class="media-items animation-fadeInQuick2" data-category="video" >
                    {!!Form::open(['url'=>['admin/fairs/upload_video',$fair->id_fair],'method'=>'POST','class'=> 'form-horizontal'])!!}
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="costo">URL</label>
                                {!!Form::text('link_url', null, ['class'=>'form-control', 'placeholder' => 'Inserte url de youtube', 'required'])!!}
                                <span class="label label-danger">{{$errors->first('link_url') }}</span>
                            </div>
                        </div>
                        <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                <a href="{{url('admin/fairs')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
                        {!! Form::close() !!}
                </div>
            </div>
            @foreach($fair->images as $image)
                <div class="col-sm-4 col-lg-3">
                    <div class="media-items animation-fadeInQuick2" data-category="image">
                        <div class="media-items-options text-right">
                            <a href="{{asset($image->link_image)}}" class="btn btn-xs btn-info" data-toggle="lightbox-image">Ver</a>
                            {!!Form::open(['url'=> ['admin/fairs/delete_image', $image->id_fair_image] , 'method'=>'DELETE', 'class'=> 'form-horizontal'])!!}
                            <button type="sumbit" class="btn btn-xs btn-danger" onclick='return confirm("¿Desea eliminar éste registro?");'><i class="fa fa-times"></i></button>
                            {!!Form::close()!!}
                            
                            
                        </div>
                        <div class="media-items-content">
                            <img src="{{asset($image->link_image)}}" class="img-responsive">
                        </div>
                    </div>
                </div>
            @endforeach

            @foreach($fair->PDF as $pdf)
                <div class="col-sm-4 col-lg-3">
                    <div class="media-items animation-fadeInQuick2" data-category="pdf">
                        <div class="media-items-options text-right">
                            <a href="{{asset($pdf->link_pdf)}}" target="_blank" class="btn btn-xs btn-info" data-toggle="lightbox-pdf">Ver</a>
                            {!!Form::open(['url'=> ['admin/fairs/delete_pdf', $pdf->id_pdf] , 'method'=>'DELETE', 'class'=> 'form-horizontal'])!!}
                            <button type="sumbit" class="btn btn-xs btn-danger" onclick='return confirm("¿Desea eliminar éste registro?");'><i class="fa fa-times"></i></button>
                            {!!Form::close()!!}
                            
                            
                        </div>
                        <div class="media-items-content">
                            <i class="far fa-file-pdf fa-5x"></i>
                        </div>
                    </div>
                </div>
            @endforeach

            @foreach($fair->Video as $video)
                <div class="col-sm-4 col-lg-3">
                    <div class="media-items animation-fadeInQuick2" data-category="video">
                        <div class="media-items-options text-right">
                            <a href="https://www.youtube.com/embed/{{$video->link_video}}" target="_blank" class="btn btn-xs btn-info" data-toggle="lightbox-video">Ver</a>
                            {!!Form::open(['url'=> ['admin/fairs/delete_video', $video->id_video] , 'method'=>'DELETE', 'class'=> 'form-horizontal'])!!}
                            <button type="sumbit" class="btn btn-xs btn-danger" onclick='return confirm("¿Desea eliminar éste registro?");'><i class="fa fa-times"></i></button>
                            {!!Form::close()!!}
                            
                            
                        </div>
                        <div class="media-items-content">
                            <iframe width="190" height="130" src="https://www.youtube.com/embed/{{$video->link_video}}"  allowfullscreen></iframe>

                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <!-- END Media Box Content -->
    </div>
    <!-- END Page Content -->
@endsection
@section('aditionalScript')
<script src="{{asset('plugins/dropzone/dist/min/dropzone.min.js')}}"></script>

        <!-- jQuery, Bootstrap, jQuery plugins and Custom JS code -->
        <script src="{{asset('auth-panel/js/vendor/jquery-2.2.4.min.js')}}"></script>


 <!-- Load and execute javascript code used only in this page -->
    <script src="{{asset('auth-panel/js/pages/appMedia.js')}}"></script>
    <script>$(function(){ AppMedia.init(); });</script>
<script type="text/javascript">
    function verdrop()
    {
        $("#dropzone").css('display','inline');
        $("#dropzonePDF").css('display','inline');
        $("#input").css('display','inline');
    }
    var myDropzone = $("#my-dropzone");
    Dropzone.options.myDropzone = {
        autoProcessQueue: true,
        uploadMultiple: true,
        maxFiles: 4,
        maxFilesize: 2, // MB
        acceptedFiles: '.jpg, .jpeg, .png ',

        init: function() {
            var submitBtn = document.querySelector("#submit");
            myDropzone = this;

            this.on("complete", function(file) {
                myDropzone.removeFile(file);
                if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
                    location.reload();
                }
            });

            this.on("success", function() {
                myDropzone.processQueue.bind(myDropzone);
            });
        }
    };
    var myDropzone2 = $("#my-dropzone2");
    Dropzone.options.myDropzone2 = {
        autoProcessQueue: true,
        uploadMultiple: true,
        maxFiles: 4,
        maxFilesize: 2, // MB
        acceptedFiles: ' .pdf',

        init: function() {
            var submitBtn = document.querySelector("#submit");
            myDropzone2 = this;

            this.on("complete", function(file) {
                myDropzone2.removeFile(file);
                if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
                    location.reload();
                }
            });

            this.on("success", function() {
                myDropzone2.processQueue.bind(myDropzone2);
            });
        }
    };




</script>