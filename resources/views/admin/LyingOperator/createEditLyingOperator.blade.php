@extends('admin.layout.auth')

@if(isset($operator))
    @section('title', 'Actualizar operador')
@else
    @section('title', 'Crear operador')
@endif

@section('aditionalStyle')
    <link href="{{asset('plugins/kartik-v-bootstrap-fileinput/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <style>
.kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar {
    display: inline-block;
}
.kv-avatar .file-input {
    display: table-cell;
    width: 213px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@endsection

@section('content')
<div id="page-content">
        <!-- Page Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>Operador</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <!-- Labels on top Form Title -->
            <div class="block-title">
                <h2>Ingrese la información solicitada</h2>
            </div>
            @if(isset($operator))
			{!!Form::model($operator,['url'=>['admin/operators/update',$operator->id_site],'method'=>'PUT', 'class'=> 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@else
			{!!Form::open(['url'=>'admin/operators/store', 'method'=>'POST', 'class'=> 'form-horizontal nuevo', 'enctype' => 'multipart/form-data', 'id' => 'userForm', 'novalidate'])!!}
			@endif
            <div class="form-group col-sm-12">
                    <div class="form-group">
                        <label for="costo">Municipio al que pertenece</label>
                        {!!Form::select('fk_municipality', $municipalities, null, ['class'=>'select-chosen'])!!}
                    </div>
            </div>
            <div class="form-group col-sm-12">
                    <div class="form-group">
                        <label for="costo">Servicios que ofrece</label>
                         <select name="services[]" id="services" class="select-chosen" multiple="">
                        @if(isset($operator))
                            @foreach($services as $servicesO)
                                <option value="{{$servicesO->id_service}}" 
                                @if(count($operator->Services)>0) 
                                    @foreach($operator->Services as $relationService)
                                     @if($relationService->id_service == $servicesO->id_service) selected @endif
                                    @endforeach
                                @endif>{{$servicesO->service}}</option>
                            @endforeach
                        @else
                            @foreach($services as $servicesO)
                                <option value="{{$servicesO->id_service}}" >{{$servicesO->service}}</option>
                            @endforeach
                        @endif
                    </select>
                    </div>
            </div>
            <div class="form-group col-sm-12">
    			<div class="col-sm-5 col-xs-12">
                    <div class="form-group">
                        <label for="costo">Nombre</label>
                        {!!Form::text('site_name', null, ['class'=>'form-control', 'placeholder' => 'Inserte nombre del operador', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('site_name') }}</span>
                    </div>
                </div>
                <div class="col-sm-5 col-sm-offset-1  col-xs-12">
                    <div class="form-group">
                        <label for="costo">Dirección</label>
                        {!!Form::text('address', null, ['class'=>'form-control', 'placeholder' => 'Inserte dirección', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('address') }}</span>
                    </div>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div class="col-sm-5  col-xs-12">
                    <div class="form-group">
                        <label for="costo">Teléfono</label>
                        {!!Form::text('phone', null, ['class'=>'form-control', 'placeholder' => 'Inserte telefono', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('phone') }}</span>
                    </div>
                </div>
                <div class="col-sm-5 col-sm-offset-1  col-xs-12">
                    <div class="form-group">
                        <label for="costo">Web</label>
                        {!!Form::text('web', null, ['class'=>'form-control', 'placeholder' => 'Inserte sitio web (en caso de tenerlo)', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('web') }}</span>
                    </div>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div class="col-sm-5  col-xs-12">
                    <div class="form-group">
                        <label for="costo">Facebook</label>
                        {!!Form::text('facebook', null, ['class'=>'form-control', 'placeholder' => 'Inserte link de facebook (en caso de tenerlo)', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('facebook') }}</span>
                    </div>
                </div>
                <div class="col-sm-5 col-sm-offset-1  col-xs-12">
                    <div class="form-group">
                        <label for="costo">Twitter</label>
                        {!!Form::text('twitter', null, ['class'=>'form-control', 'placeholder' => 'Inserte link de twitter (en caso de tenerlo)', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('twitter') }}</span>
                    </div>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div class="col-sm-5  col-xs-12">
                    <div class="form-group">
                        <label for="costo">Instagram</label>
                        {!!Form::text('instagram', null, ['class'=>'form-control', 'placeholder' => 'Inserte link de instagram (en caso de tenerlo)', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('instagram') }}</span>
                    </div>
                </div>
                <div class="col-sm-5 col-sm-offset-1  col-xs-12">
                    <div class="form-group">
                        <label for="costo">Canal de youtube</label>
                        {!!Form::text('youtube', null, ['class'=>'form-control', 'placeholder' => 'Inserte link de youtube (en caso de tenerlo)', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('youtube') }}</span>
                    </div>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div class="col-sm-5 col-xs-12">
                    <div class="form-group">
                        <label for="costo">Latitud</label>
                        {!!Form::text('latitude', null, ['class'=>'form-control', 'placeholder' => 'Ingrese latitud', 'required'])!!}
                        <span class="label label-danger">{{$errors->first('latitude') }}</span>
                    </div>
                </div>
                <div class="col-sm-5 col-sm-offset-1 col-xs-12">
                    <div class="form-group">
                        <label for="costo">Longitud</label>
                        {!!Form::text('longitude', null, ['class'=>'form-control', 'placeholder' => 'Ingrese longitud', 'required'])!!}
    					<span class="label label-danger">{{$errors->first('longitude') }}</span>
                    </div>
                </div>
            </div>
            @if(isset($operator)&& $operator->description!=null)
            <div class="col-sm-12 form-group" id="description">
                <label for="duration">Descripción</label>
                <div class="form-group">
                    <textarea id="textarea-ckeditor" name="description" class="ckeditor">{!!$operator->description!!}</textarea>
					<span class="label label-danger">{{$errors->first('description') }}</span>
                </div>
	        </div>
            @else
            <div class="col-sm-12 form-group" id="description">
                <label for="duration">Descripción</label>
                <div class="form-group">
                    <textarea id="textarea-ckeditor" name="description" class="ckeditor"></textarea>
                    <span class="label label-danger">{{$errors->first('description') }}</span>
                </div>
            </div>
            @endif
            <div class="col-sm-12">

                 <div class="col-sm-4 text-center col-sm-offset-1">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Imagen</label>
                            <div class="file-loading">

                                {!!Form::file('link_image',['id'=>'avatar-1'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('link_image') }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 text-center col-sm-offset-1">
                    <div class="kv-avatar">
                        <div class="form-group">
                        <label for="costo">Icono</label>
                            <div class="file-loading">

                                {!!Form::file('link_icon',['id'=>'avatar-2'])!!}
                            </div>
                            <span class="label label-danger">{{$errors->first('link_icon')}}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-actions" align="center">
                <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                <a href="{{url('admin/operators')}}" class="btn btn-effect-ripple btn-danger">Cancelar</a>
            </div>
            {!!Form::close()!!}
        </div>
</div>
@endsection

@section('aditionalScript')

    <script src="{{asset('auth-panel/js/plugins/ckeditor/ckeditor.js')}}"></script>

    <script src="{{asset('auth-panel/js/pages/formsComponents.js')}}"></script>
    <script>$(function(){ FormsComponents.init(); });</script>

    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/plugins/sortable.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/fileinput.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/fr.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/js/locales/es.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/explorer-fa/theme.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/kartik-v-bootstrap-fileinput/themes/fa/theme.js')}}" type="text/javascript"></script>

    <script>


    $("#avatar-1").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="fab fa-searchengin"></i>',
        removeIcon: '<i class="far fa-times-circle"></i>',
        viewIcon:'<i class="far fa-times-circle"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        @if(isset($operator) && $operator->link_image!=null)
        defaultPreviewContent: '<img src="{{asset($operator->link_image)}}" alt="Avatar por defecto" width="50%">',
        @else
        defaultPreviewContent: '<img src="{{asset('img/default-avatar.png')}}" alt="Avatar por defecto" width="50%">',
        @endif
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif"]
    });
    $("#avatar-2").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="fab fa-searchengin"></i>',
        removeIcon: '<i class="far fa-times-circle"></i>',
        viewIcon:'<i class="far fa-times-circle"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        @if(isset($operator)&& $operator->link_icon!=null)
        defaultPreviewContent: '<img src="{{asset($operator->link_icon)}}" alt="Icono por defecto" width="50%">',
        @else
        defaultPreviewContent: '<img src="{{asset('img/default-icon.png')}}" alt="Icono por defecto" width="50%">',
        @endif
        layoutTemplates: {main2: '{preview} ' + ' {remove} {browse}'},
        allowedFileExtensions: ["ico", "png",]
    });
</script>
@endsection