<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RelationRoute extends Model
{
	protected $primaryKey = 'id_relation_route';

	public function Municipality()
	{
		return $this->hasOne('App\Models\Municipality','id_municipality','fk_relation');
	}

	public function Site()
	{
		return $this->hasOne('App\Models\InterestSite','id_site','fk_relation');
	}
}
